<sld_project_info>
  <sld_infos>
    <sld_info hpath="feam:feam_qsys" name="feam_qsys">
      <assignment_values>
        <assignment_value text="QSYS_NAME feam HAS_SOPCINFO 1 GENERATION_ID 1679959176"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sata_link_rx:sata_link_qsys_rx" name="sata_link_qsys_rx">
      <assignment_values>
        <assignment_value text="QSYS_NAME sata_link_rx HAS_SOPCINFO 1 GENERATION_ID 1574374058"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sata_link_tx:sata_link_qsys_tx" name="sata_link_qsys_tx">
      <assignment_values>
        <assignment_value text="QSYS_NAME sata_link_tx HAS_SOPCINFO 1 GENERATION_ID 1574373983"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sld_hub:auto_hub|alt_sld_fab:\instrumentation_fabric_with_node_gen:instrumentation_fabric" library="alt_sld_fab" name="instrumentation_fabric">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
