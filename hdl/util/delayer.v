// Goes 'active' after incoming signal is 'active' for delay count time. 
// Goes 'inactive' immediately on low, and holds the counter in reset until incoming signal goes high again
module delayer (
	clk,
	rst,
	d,
	q
);

// Setting DELAY_CNT to 0 or 1 is the same, there will be a one-cycle delay
parameter DELAY_CNT	= 3;

localparam SZ_DELAY 	= $clog2(DELAY_CNT+1);

input wire clk;
input wire rst;
input wire d;
output reg q;

reg [SZ_DELAY-1:0] delay_cnt 	= {{(SZ_DELAY-1){1'b0}}, 1'b1 };

// 'gate' reset input, designed for altera pll locked signal 
always@(posedge rst, posedge clk) begin 
	if(rst) begin 		
		delay_cnt 	<= {{(SZ_DELAY-1){1'b0}}, 1'b1 };
		q <= 1'b0;
	end else begin 
		if(d) begin 			
			if(delay_cnt < DELAY_CNT[SZ_DELAY-1:0]) begin
				delay_cnt <= delay_cnt + 1'b1;
				q <= 1'b0;
			end else begin 
				delay_cnt <= delay_cnt;
				q <= 1'b1;
			end 						
		end else begin 
			delay_cnt <= {{(SZ_DELAY-1){1'b0}}, 1'b1 };
			q <= 1'b0;
		end 
	end 
end 

endmodule
