/*
 Generates an efficient wide OR tree
*/
module wide_or (
	d,
	q // high if any of the inputs are high
);

parameter SZ_WIDTH = 2;

localparam LEVELS = $clog2(SZ_WIDTH);

input wire [SZ_WIDTH-1:0] d;
output wire q;

wire [LEVELS:0][SZ_WIDTH-1:0] comb;

genvar n;
genvar j;
generate 

// Make sure we have enough channels to do work
if(SZ_WIDTH > 1) begin 
	
	// First level is just the input from in
	for(n=0; n<SZ_WIDTH; n+=1) begin: gen_ppc_start
		assign comb[0][n] = d[n];
	end
	
	// perform Parallel Partial Computation OR
	for(j=1; j<=LEVELS; j+=1) begin: gen_lvl
		for(n=0; n<SZ_WIDTH; n+=1) begin: gen_ppc		  
			if(n > (((n - (n % (2**j)) - 1))+((2**j)/2))) begin
				// Verilog doesn't let us do this step-wise, so we have to find the value 
				assign comb[j][n] = comb[j-1][n] | comb[j-1][n - (n % (2**j)) + (((2**j)/2)-1)];
			end else begin
				// if no work is to be done, just connect to the last wire without
				assign comb[j][n] = comb[j-1][n];
			end		
		end
	end
	
    // The ORs all come down to this:
	assign q = comb[LEVELS][SZ_WIDTH-1];
	
end else begin
	// The algorithm breaks if SZ_WIDTH == 1, so skip it and put a wire in if so	
	assign q = d[0];
end

endgenerate

endmodule
