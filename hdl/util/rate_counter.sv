// Returns rate of change per second
module rate_counter (
    clk,
    rst,
    d,
    q
);

parameter SZ_DATA = 8;
parameter CLOCK_PERIOD = 1;

localparam SZ_PERIOD = $clog2(CLOCK_PERIOD+1);

input wire clk;
input wire rst;
input wire [SZ_DATA-1:0] d;
output reg [SZ_DATA-1:0] q;

reg [SZ_PERIOD-1:0] count;
reg [SZ_DATA-1:0] d_prev;

always@(posedge rst, posedge clk) begin
    if(rst) begin
        count   <= {SZ_PERIOD{1'b0}};
        q       <= {SZ_DATA{1'b0}};
        d_prev  <= {SZ_DATA{1'b0}};
    end else begin
        if(count < CLOCK_PERIOD) begin
            count   <= count + 1'b1;
            q       <= q;
            d_prev  <= d_prev;
        end else begin
            count   <= {SZ_PERIOD{1'b0}};
            d_prev  <= d;
            q       <= d - d_prev;
        end        
    end
end

endmodule
