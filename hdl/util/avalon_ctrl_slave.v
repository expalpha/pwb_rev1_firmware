// Assumes 1-wait state read from status 
// Assumes 0-wait state write to control
// Reset should DEASSERT synchronously
module avalon_ctrl_slave (
	clk,
	rst,
	address,
	write,
	writedata,
	byteenable,
	read,
	readdata,
	readdatavalid,
	ctrl
);

parameter NUM_CTRL_REGS = 0;
parameter SZ_WIDTH = 32;

localparam SZ_ADDR = $clog2(NUM_CTRL_REGS);

input wire clk;
input wire rst;
input wire [SZ_ADDR-1:0] address;
input wire write;
input wire [SZ_WIDTH-1:0] writedata;
input wire read;
input wire [3:0] byteenable;
output reg readdatavalid;
output reg [SZ_WIDTH-1:0] readdata;

output reg [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] ctrl;

wire [31:0] bytemask = { {8{byteenable[3]}}, {8{byteenable[2]}}, {8{byteenable[1]}}, {8{byteenable[0]}} };


reg write_pipe;
reg [SZ_ADDR-1:0] addr_pipe;
reg [SZ_WIDTH-1:0] writedata_pipe0;
reg [SZ_WIDTH-1:0] writedata_pipe1;

always@(posedge clk) begin 

	write_pipe <= write;
	addr_pipe <= address;

	// half the write masking and spread it over two cycles to reduce the combinatorial path and timing
	// mask the control register bytes
	writedata_pipe0 <= (ctrl[address] & ~bytemask);
	// mask the writedata bytes
	writedata_pipe1 <= (writedata & bytemask);
	
	if(write_pipe) 
		ctrl[addr_pipe] <= writedata_pipe0 | writedata_pipe1;
end

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		readdatavalid <= 1'b0;
		readdata <= {SZ_WIDTH{1'b0}};
	end else begin 
		// Just always update the control register with the current address 
		// The avalon decoding logic higher up handles this properly for us
		readdata <= ctrl[address];

		// On a read request assert readdatavalid, so pipelining works correctly!
		readdatavalid <= read;				
	end 			
end 

endmodule

