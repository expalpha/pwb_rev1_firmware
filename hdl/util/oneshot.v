module oneshot ( 
	clk,
	rst,
	d,
	q
);

input wire clk;
input wire rst; 
input wire d;
output reg q;

reg d_prev;
wire edge_occurred = ({d_prev,d} == 2'b01);

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q 	<= 1'b0;
		d_prev <= 1'b0;
	end else begin 
		d_prev <= d;
		q 	<= edge_occurred ? 1'b1 : 1'b0;
	end 
end

endmodule

module oneshot_extend ( 
	clk,
	rst,
	d,
	extend,
	q
);

parameter MAX_EXTEND = 4;

localparam SZ_EXTEND = $clog2(MAX_EXTEND+1);

input wire clk;
input wire rst; 
input wire d;
input wire [SZ_EXTEND-1:0] extend;
output reg q;

reg d_prev;
reg [SZ_EXTEND-1:0] cnt;

wire edge_occurred = ({d_prev,d} == 2'b01);

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		q 	<= 1'b0;
		d_prev <= 1'b0;
		cnt  <= 0;
	end else begin 
		d_prev <= d;
	   // if the input signal is active and the output is low, or if we are still counting, keep the output high
		q 	<= ( (edge_occurred && (q==1'b0)) || ((cnt > {SZ_EXTEND{1'b0}}) && (cnt < extend)) )? 1'b1 : 1'b0;

		cnt <= (edge_occurred && (cnt < extend)) ? // should we start counting
			cnt + 1'b1 : 
					((cnt > {SZ_EXTEND{1'b0}}) && (cnt < extend)) ? // should we continue counting
						cnt + 1'b1 : 
							{SZ_EXTEND{1'b0}};
	end 
end

endmodule
