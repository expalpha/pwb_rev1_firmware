// Goes into reset immediately upon receiving a low, comes out when all of the sync chain is high
module reset_n_synchronizer ( 
	clk,
	d,
	q
);

input wire clk;
input wire d;
output reg q;

reg [2:0] r_sync;

always@(posedge clk) begin
	r_sync <= { r_sync[1], r_sync[0], d};
	q <= r_sync[2] & r_sync[1] & r_sync[0];
end


endmodule

// Goes into reset immediately upon receiving a high, comes out when all of the sync chain is low
module reset_synchronizer ( 
	clk,
	d,
	q
);

input wire clk;
input wire d;
output reg q;

reg [2:0] r_sync;

always@(posedge clk) begin
	r_sync <= { r_sync[1], r_sync[0], d};
	q 		<= {r_sync[2] | r_sync[1] | r_sync[0]};
end


endmodule
