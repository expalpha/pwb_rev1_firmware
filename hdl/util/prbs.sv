module PRBS7 (
	clk,
	seed,
	seed_load,
	q
);

input wire clk;
input wire [6:0] seed;
input wire seed_load;
output reg [6:0] q;


always@(posedge clk) begin
	if(seed_load) begin
		q <= seed;
	end else begin
		q <= { q[5:0], q[6] ^ q[5] };
	end
end

endmodule


module PRBS23 (
	clk,
	seed,
	seed_load,
	q
);

input wire clk;
input wire [22:0] seed;
input wire seed_load;
output reg [22:0] q;

always@(posedge clk) begin
	if(seed_load) begin
		q <= seed;
	end else begin
		q <= {  q[21:0], q[22] ^ q[17] };
	end
end

endmodule


module PRBS31 (
	clk,
	seed,
	seed_load,
	q
);

input wire clk;
input wire [30:0] seed;
input wire seed_load;
output reg [30:0] q;


always@(posedge clk) begin
	if(seed_load) begin
		q <= seed;
	end else begin
		q <= { q[29:0], q[30] ^ q[27] };
	end
end

endmodule
