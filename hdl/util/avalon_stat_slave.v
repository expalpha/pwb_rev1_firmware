// Assumes 1-wait state read from status 
// Reset should DEASSERT synchronously
module avalon_stat_slave (
	clk,
	rst,
	address,
	read,
	readdata,
	readdatavalid,
	status
);

parameter NUM_STAT_REGS = 0;
parameter SZ_WIDTH = 32;

localparam SZ_ADDR = $clog2(NUM_STAT_REGS);

input wire clk;
input wire rst;

input wire [SZ_ADDR-1:0] address;
input wire read;
input wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] status;

output reg readdatavalid;
output reg [SZ_WIDTH-1:0] readdata;

reg [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] r_status;

// Give up a cycle of latency and registers for easier timing
genvar n;
generate
	for(n=0; n<NUM_STAT_REGS; n+=1) begin: gen_cache
		always@(posedge clk) begin
			r_status[n] <= status[n];
		end
	end
endgenerate

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		readdata <= {SZ_WIDTH{1'b0}};
		readdatavalid <= 1'b0;
	end else begin 
		// Just always update the status register with the current address 
		// The avalon decoding logic higher up handles this properly for us
		readdata <= r_status[address];

		// On a read request assert readdatavalid, so pipelining works correctly!
		readdatavalid <= read; 
	end
end 

endmodule
