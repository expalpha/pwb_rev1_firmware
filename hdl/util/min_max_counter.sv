/*
    Basic module to store min/max value of an input, typically for diagnostic purposes

    On reset, the min defaults to fullscale and max defaults to zero. This will be overwritten once first read is done
*/
module min_max_val ( 
    clk,
    rst,
    d,
    min,
    max
);

parameter SZ_VALUE = 16;

input wire clk;
input wire rst;
input wire [SZ_VALUE-1:0] d;
output reg [SZ_VALUE-1:0] min;
output reg [SZ_VALUE-1:0] max;

always@(posedge rst, posedge clk) begin
	// Set min to maximum value and max to minimum value on reset
	// This will immediately set both to 'd' on first read after reset
	if(rst) begin
		min <= {SZ_VALUE{1'b1}};
		max <= {SZ_VALUE{1'b0}};    
	end else begin
		min <= (d < min) ? d : min;
		max <= (d > max) ? d : max;
	end
end

endmodule
