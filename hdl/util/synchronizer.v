/*** Examples of Synchronizer usage

// Positive Reset Sync
synchronizer #(
	.RST_STATE(1)		// When reset occurs go immediately into reset state
) ref_rst_sync (
	.clk	(clk),
	.rst	(rst),
	.d		(rst),
	.q		(registered_reset)
);

// Negative Reset Sync
synchronizer #(
	.RST_STATE(0)		// When reset occurs go immediately into reset_n state
) ref_rst_sync (
	.clk	(clk),
	.rst	(!reset_n), 	
	.d		(reset_n),
	.q		(registered_reset_n)
);

// Signal Sync - Warning: Extra steps need to be taken to ensure data bus integrity is kept across clock domains!
synchronizer #(
	.RST_STATE({SZ_SYNC{1'b0}}),	// When reset occurs, the entire sync bus goes to zero
	.SZ_DATA(SZ_SYNC),				// Width of signal bus
	.NUM_SYNC(2)						// Number of synchronizing registers
) ref_sig_sync (
	.clk	(clk),
	.rst	(rst),
	.d		(sig_to_clk),
	.q		(sig_from_clk)
);

*/
module synchronizer ( 
	clk,
	rst,		
	d,
	q
);

parameter NUM_SYNC = 3;
parameter SZ_DATA = 1;

input wire rst;
input wire clk;
input wire [SZ_DATA-1:0] d;
output wire [SZ_DATA-1:0] q;

reg [NUM_SYNC-1:0][SZ_DATA-1:0] r_sync;

assign q = r_sync[NUM_SYNC-1];

genvar n;
generate 

always@(posedge rst, posedge clk) begin
	if(rst) begin
		r_sync[0] <= {SZ_DATA{1'b0}};
	end else begin
		r_sync[0] <= d;
	end
end

for(n=1; n<NUM_SYNC; n+=1) begin: sync_stage

always@(posedge rst, posedge clk) begin
	if(rst) begin
		r_sync[n] <= {SZ_DATA{1'b0}};
	end else begin
		r_sync[n] <= r_sync[n-1];
	end
end

end // for 
endgenerate

endmodule
