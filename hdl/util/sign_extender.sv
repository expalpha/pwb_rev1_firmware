module sign_extender (
	in,
	out
);

parameter SZ_IN = 12;
parameter SZ_OUT = 16;

// Obviously SZ_OUT must be at least SZ_IN, but there is no reason to sign extend if this is not true
localparam SZ_EXTEND = SZ_OUT - SZ_IN;

input wire [SZ_IN-1:0] in;
output wire [SZ_OUT-1:0] out;

generate

if(SZ_EXTEND == 0) begin
	// In = Out in size, no extension required
	assign out = in;
end else if (SZ_IN == 1) begin
	// Can't really sign extend one bit, but we might as well compile
	assign out = {SZ_OUT{in[SZ_IN-1]}};
end else begin
	// Normal sign extend
	assign out = {{SZ_EXTEND{in[SZ_IN-1]}}, in[SZ_IN-1:0]};
end

endgenerate

endmodule
