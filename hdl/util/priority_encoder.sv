module priority_enc (
	in,
	out
);

parameter NUM_INPUTS = 2;

localparam SZ_OUT = $clog2(NUM_INPUTS);

input wire [NUM_INPUTS-1:0] in;
output reg [SZ_OUT-1:0] out;

integer i;
always @* begin
	out = 0; // default value if 'in' is all 0's
	for(i=0; i<NUM_INPUTS; i+=1)
		if(in[i]) out = i;
end

endmodule
