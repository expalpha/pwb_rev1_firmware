// Module to take in  trigger requests and fires a trigger if the request is accepted
// Performs the 'accept' test *after* a user-configured delay period
// By performing the test after the variable delay, we allow trigger requests to occur during a 'busy' period,
// and be accepted if their delay finishes just as the 'busy' period ends. This reduces our dead-time. 
// Very handy when used for SCA triggering, as we have a forced 'write' deadtime, but a valid trigger may occur while 
// filling up the SCA buffer
//
// There is a one clock cycle output delay with zero delay set, due to registered output
// Continuous triggering is not allowed, as we have to block new triggers from being accepted (not requested) when we output the trigger
// This allows the triggered system time to flag 'busy' once it has received a trigger 
//
// It is recommended to place a one-shot in front of the trigger requests, to avoid continuously attempting to trigger 
module trigger_block (
	clk,					// Clock
	rst,					// Reset
	delay,				// Clock cycles to delay trigger request, zero is allowed
	request,				// Trigger requests
	busy,					// System to trigger is busy (cause drop)
	requested_count,		// Request counter per 
	dropped_count,		// Dropped counter per
	accepted_count,	// Accepted counter per
	total_requested,	// Total Requested
	total_accepted,	// Total Accepted 
	total_dropped,		// Total Dropped
  	trigger_delayed,	// Clock cycles that have passed since trigger request fired
	trigger_source,	// Which trigger source was fired
	trigger				// Trigger System  
);
  
parameter NUM_TRIGGER_SOURCES = 2;
parameter SZ_COUNTER = 32;
parameter SZ_DELAY = 16;

localparam SZ_DELAYED = SZ_DELAY+1;
localparam SZ_TRIGGER = (NUM_TRIGGER_SOURCES > 1) ? $clog2(NUM_TRIGGER_SOURCES) : 1;
localparam SZ_TRIGGER_COUNT = ($clog2(NUM_TRIGGER_SOURCES+1) < 1) ? 1 : $clog2(NUM_TRIGGER_SOURCES+1);

input  wire clk;  
input  wire rst;
input  wire busy;
input  wire [NUM_TRIGGER_SOURCES-1:0][SZ_DELAY-1:0] delay;
input  wire [NUM_TRIGGER_SOURCES-1:0] request;
output reg [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] requested_count;
output reg [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] dropped_count;
output reg [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] accepted_count;

output reg [SZ_COUNTER-1:0] total_requested;
output reg [SZ_COUNTER-1:0] total_accepted;
output reg [SZ_COUNTER-1:0] total_dropped;
output reg [SZ_DELAYED-1:0] trigger_delayed;
output reg [SZ_TRIGGER-1:0] trigger_source;
output reg trigger;

wire trig_active;
wire triggered_or_busy;
wire [NUM_TRIGGER_SOURCES-1:0] trig_thermo;
wire [NUM_TRIGGER_SOURCES-1:0] trig_onehot;
wire [SZ_TRIGGER-1:0] trig_binary;

wire [NUM_TRIGGER_SOURCES-1:0] trigger_sources_dropping;

reg [NUM_TRIGGER_SOURCES-1:0] internal_trigger;
reg [NUM_TRIGGER_SOURCES-1:0][SZ_DELAYED-1:0] internal_delayed;

assign triggered_or_busy = busy | trigger; // don't allow a trigger if we just set the trigger high, guarantees closest triggers are separated by a one clock cycle spacer at minimum.

// Priority encodes all trigger channels (0=highest priority)
// and returns a thermometer code and onehot of chosen trigger
ppc_or #(
	.NUM_CH ( NUM_TRIGGER_SOURCES )
) trigger_or (
	.d				( internal_trigger ),
	.q_thermo	( trig_thermo ),
	.q_onehot 	( trig_onehot )
);

// Thermometer to binary, used to grab the trigger delay used and determine if the ppc has found a trigger (trig_active)
thermo2binary #(
	.NUM_CH( NUM_TRIGGER_SOURCES )
) t2b_trigger (
	.d		( ~trig_thermo ),
	.q		( trig_binary ),
	.val	( trig_active )
);


wire [SZ_TRIGGER_COUNT-1:0] num_triggers_requesting;
ones_counter #(
	.SZ_WIDTH( NUM_TRIGGER_SOURCES )
) requesting_trigger_count (
	.d ( request ),
	.q ( num_triggers_requesting )
);


wire [SZ_TRIGGER_COUNT-1:0] num_triggers_dropping;
ones_counter #(
	.SZ_WIDTH( NUM_TRIGGER_SOURCES )
) dropping_triggers_dropped (
	.d ( trigger_sources_dropping ),
	.q ( num_triggers_dropping )
);

// Total counters
always@(posedge rst, posedge clk) begin
	if(rst) begin
		total_requested 	<= {SZ_COUNTER{1'b0}};
		total_accepted 	<= {SZ_COUNTER{1'b0}};
		total_dropped 		<= {SZ_COUNTER{1'b0}};
	end else begin
		total_accepted 	<= (trig_active && (!triggered_or_busy)) ? total_accepted + 1'b1 : total_accepted;
		total_requested 	<= total_requested + num_triggers_requesting;
		total_dropped 		<= total_dropped + num_triggers_dropping;
	end
end


// Output trigger decision, count accepted trigger total,
// Subtract trigger_delayed from the current timestamp to get the time when the request occurred
always@(posedge rst, posedge clk) begin
	if(rst) begin
		trigger <= 1'b0;
		trigger_delayed <= {SZ_DELAYED{1'b0}};
		trigger_source <= {SZ_TRIGGER{1'b0}};
	end else begin
		// If any trigger goes high, and we're not busy (either from trigger or busy signal) then output
		if(trig_active) begin
			if(!triggered_or_busy) begin
				trigger <= 1'b1;
				trigger_source <= trig_binary;
				trigger_delayed <= internal_delayed[trig_binary];
			end else begin
				trigger <= 1'b0;
				trigger_delayed <= {SZ_DELAYED{1'b0}};
				trigger_source <= {SZ_TRIGGER{1'b0}};
			end
		end else begin
			trigger <= 1'b0;
			trigger_delayed <= {SZ_DELAYED{1'b0}};
			trigger_source <= {SZ_TRIGGER{1'b0}};
		end
	end
end

// Generate trigger source logic
genvar n;
generate

integer i; // used to index dropped_reasons

for(n=0; n<NUM_TRIGGER_SOURCES; n+=1) begin: gen_triggers

	enum int unsigned { ST_WAIT = 0, ST_DELAY = 2, ST_TRIGGER = 4} trig_state;
	
	reg [SZ_DELAY-1:0] internal_delay;
	
	assign trigger_sources_dropping[n] = ((trig_state == ST_DELAY) && (request[n])) | // can't take another request while delayed
												(internal_trigger[n] & (!trig_onehot[n]))  | // triggered but lost out to higher priority trigger
												(trig_onehot[n] & triggered_or_busy); 	  // selected as trigger, but were currently triggering or busy
	
	// Counters
	always@(posedge rst, posedge clk) begin
		if(rst) begin
			requested_count[n] 	<= {SZ_COUNTER{1'b0}};
			dropped_count[n] 	<= {SZ_COUNTER{1'b0}};          
			accepted_count[n] <= {SZ_COUNTER{1'b0}};		
		end else begin
			requested_count[n] 	<= (request[n]) ? requested_count[n] + 1'b1 : requested_count[n];			
			accepted_count[n] <= ((trig_onehot[n] == 1'b1) && (!triggered_or_busy)) ? accepted_count[n] + 1'b1 : accepted_count[n];
			dropped_count[n] 	<= (trigger_sources_dropping[n]) ? dropped_count[n] + 1'b1 : dropped_count[n];			
		end
	end
	
	// Internal trigger logic for source
	always@(posedge rst, posedge clk) begin      
	  if(rst) begin
			trig_state 				<= ST_WAIT;
			internal_delay 		<= {SZ_DELAY{1'b0}};
			internal_trigger[n] 	<= 1'b0;
        	internal_delayed[n]	<= {SZ_DELAYED{1'b0}};
		end else begin
			case(trig_state)
				ST_WAIT: begin
					internal_delay 	<= delay[n] - 1'b1; // when requests are made, there is a special case for delay[n] == 0
					internal_delayed[n] <= delay[n] + 1'b1;
					
					if(request[n]) begin                
						if(delay[n] == {SZ_DELAY{1'b0}}) begin
							// skip delay state if there is no delay set
							trig_state <= ST_TRIGGER;
							internal_trigger[n] <= 1'b1;                          
						end else begin
							// Go to delay
							trig_state <= ST_DELAY;
							internal_trigger[n] <= 1'b0;
                  end
					end else begin
						// no trigger request, continue to wait
						trig_state <= ST_WAIT;
						internal_trigger[n] <= 1'b0;
					end
				end
				ST_DELAY: begin
					internal_delay 	<= internal_delay - 1'b1;
					internal_delayed[n] <= internal_delayed[n];
					
					// Stay in ST_DELAY until we've counted to zero
					if(internal_delay > {SZ_DELAY{1'b0}}) begin
						trig_state 				<= ST_DELAY;
						internal_trigger[n] 	<= 1'b0;
					end else begin
						trig_state 				<= ST_TRIGGER;
						internal_trigger[n] 	<= 1'b1;
					end
				end
				ST_TRIGGER: begin
					internal_delay 		<= delay[n] - 1'b1;              
					internal_delayed[n]	<= delay[n] + 1'b1;    
					
					// If a request comes in as we internally allow the previous request, handle it as normal      
					if(request[n]) begin                
						if(delay[n] == {SZ_DELAY{1'b0}}) begin
							// skip delay state if there is no delay set
							trig_state <= ST_TRIGGER;
							internal_trigger[n] <= 1'b1;                          
						end else begin
							// Go to delay
							trig_state <= ST_DELAY;
							internal_trigger[n] <= 1'b0;
                 	 end
					end else begin
						// no trigger request, continue to wait
						trig_state <= ST_WAIT;
						internal_trigger[n] <= 1'b0;
					end
                  
				end
			endcase
		end 
	end // end always
end // end for
endgenerate

endmodule
