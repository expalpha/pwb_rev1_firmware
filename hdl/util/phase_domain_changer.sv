// Assuming both clocks are IDENTICAL in frequency
// This code will push data from one clock domain to the other
// With a ~(DEPTH/2) cycle clock delay
module phase_domain_changer (  
  wr_clk,
  rd_clk,
  wr_rst,
  rd_rst,
  d,
  q,
  val
);
  
  parameter SZ_DATA = 8;
  parameter DEPTH = 4;
  
  localparam INT_DEPTH = (DEPTH < 2) ? 4 : DEPTH;
  localparam SZ_DEPTH = $clog2(DEPTH);
  localparam RD_START = DEPTH / 2;
    
  input wire wr_clk;
  input wire rd_clk;
  input wire wr_rst;
  input wire rd_rst;
  input wire [SZ_DATA-1:0] d;
  output reg [SZ_DATA-1:0] q;
  output reg val;
  
  reg [SZ_DATA-1:0] ram[INT_DEPTH-1:0];
  reg [SZ_DEPTH-1:0] wr_addr;
  reg [SZ_DEPTH-1:0] rd_addr;
  
  always@(posedge wr_rst, posedge wr_clk) begin
    if(wr_rst) begin
      wr_addr <= {SZ_DEPTH{1'b0}};
    end else begin
      wr_addr <= wr_addr + 1'b1;
    end
  end
  
  always@(posedge rd_rst, posedge rd_clk) begin
    if(rd_rst) begin
      rd_addr <= RD_START[SZ_DEPTH-1:0];
      val <= 1'b0;
    end else begin
      rd_addr <= rd_addr + 1'b1;
      val <= (rd_addr == {SZ_DEPTH{1'b0}}) ? 1'b1 : val;
    end
  end
    	
  always@(posedge wr_clk) begin
		ram[wr_addr] <= d;
  end
	
  always@(posedge rd_clk) begin
		q <= ram[rd_addr];
  end
  
endmodule
