`default_nettype none
module gxb_module (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_readdata,
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_byteenable,
	s1_readdatavalid, 
	s1_read,
	
	// Sync clocks
	rx_core_clk,
	tx_core_clk,

	// Status
	pll_locked,
	is_locked_to_ref,
	is_locked_to_data,
	signal_detect,
	rx_pattern_det,
	rx_err_detect,
	rx_disp_err,
	rx_sync_status,
	link_status,

	stat_trig_tx,
	stat_stop_tx,
	stat_pkts_tx,
	stat_octets_tx,
	stat_flow_tx,

        stat_badk_rx,
        stat_trig_rx,
        stat_stop_rx,
        stat_ovfl_rx,
        stat_octets_rx,
        stat_pkts_rx,

	// Ctrl
	serial_lpbk_en,
	rx_invert,
	link_ctrl
);

localparam SZ_WIDTH 			= 32; // always NIOS 32bit bus size

// Control Registers
localparam RESET_COUNTERS		= 0;
localparam SERIAL_LPBK_EN		= 1;
localparam RX_INVERT			= 2;
localparam LINK_CTRL			= 3;

localparam NUM_CTRL_BASE_REGS 	= 4;
localparam NUM_CTRL_REGS		= NUM_CTRL_BASE_REGS;

// Status Registers		
localparam PLL_LOCKED		 	= 0;
localparam IS_RX_LOCKED_TO_DATA         = 1;
localparam IS_RX_LOCKED_TO_REF          = 2;
localparam SIGNAL_DETECT		= 3;
localparam RX_ERR_DETECT		= 4;
localparam RX_ERR_DISPARITY		= 5;
localparam RX_PATTERN_DETECT 	        = 6;
localparam RX_SYNC_STATUS		= 7;
localparam LINK_STATUS			= 8;
localparam CNT_PLL_LOCKED		= 9;
localparam CNT_IS_LOCKED_TO_DATA        = 10; 
localparam CNT_IS_LOCKED_TO_REF         = 11;
localparam CNT_SIGNAL_DETECT	        = 12;
localparam CNT_RX_ERR_DETECT	        = 13;
localparam CNT_RX_ERR_DISPARITY	        = 14;
localparam CNT_RX_SYNC_STATUS	        = 15;
localparam CNT_LINK_STATUS		= 16;
localparam CNT_RX_PATTERN_DETECT        = 17; 
localparam CNT_BADK_RX			= 18;
localparam CNT_STOP_RX			= 19;
localparam CNT_PKTS_TX			= 20;
localparam CNT_PKTS_RX			= 21;
localparam CNT_OCTETS_TX		= 22;
localparam CNT_OCTETS_RX		= 23;
localparam CNT_TRIG_TX			= 24;
localparam CNT_TRIG_RX			= 25;
localparam CNT_OVFL_RX			= 26;
localparam CNT_STOP_TX			= 27;
localparam CNT_FLOW_TX			= 28;

localparam NUM_STAT_BASE_REGS	= 29;
localparam NUM_STAT_REGS 	= NUM_STAT_BASE_REGS;

localparam SZ_STAT		= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL		= $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire clk;
input wire rst;

// Sync clocks
input wire rx_core_clk;
input wire tx_core_clk;

// Status
input wire pll_locked;
input wire is_locked_to_ref;
input wire is_locked_to_data;
input wire signal_detect;
input wire rx_err_detect;
input wire rx_disp_err;
input wire rx_sync_status;
input wire link_status;
input wire rx_pattern_det;

   input wire [31:0] stat_trig_tx;
   input wire [31:0] stat_stop_tx;
   input wire [31:0] stat_pkts_tx;
   input wire [31:0] stat_octets_tx;
   input wire [31:0] stat_flow_tx;
   
   input wire [31:0] stat_badk_rx;
   input wire [31:0] stat_trig_rx;
   input wire [31:0] stat_stop_rx;
   input wire [31:0] stat_ovfl_rx;
   input wire [31:0] stat_octets_rx;
   input wire [31:0] stat_pkts_rx;

// Ctrl
output wire serial_lpbk_en;
output wire rx_invert;
output wire [31:0] link_ctrl;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
output [SZ_WIDTH-1:0] 		s0_readdata;	// data bus input
input        				s0_read;	// valid bus cycle input
output      				s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input        				s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 		s1_writedata;	// data bus output
input        				s1_read;	// valid bus cycle input
input  [3:0] 				s1_byteenable;
output [SZ_WIDTH-1:0] 		s1_readdata;	// data bus input
output      				s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] status;

wire reset_counters;

assign reset_counters			= ctrl[RESET_COUNTERS][0];

synchronizer #( .SZ_DATA( 1 ) ) sync_serial_lpbk_en ( .clk( tx_core_clk ), .rst( 1'b0 ), .d( ctrl[SERIAL_LPBK_EN][0] ),	.q ( serial_lpbk_en ));
synchronizer #( .SZ_DATA( 1 ) ) sync_rx_invert 		( .clk( rx_core_clk ), .rst( 1'b0 ), .d( ctrl[RX_INVERT][0] ),		.q ( rx_invert ));
assign link_ctrl = ctrl[LINK_CTRL];

assign status[PLL_LOCKED][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_pll_locked( .clk( clk ), .rst( rst ), .d( pll_locked ),	.q ( status[PLL_LOCKED][0] ));

assign status[IS_RX_LOCKED_TO_DATA][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_is_locked_to_data( .clk( clk ), .rst( rst ), .d( is_locked_to_data ),	.q ( status[IS_RX_LOCKED_TO_DATA][0] ));

assign status[IS_RX_LOCKED_TO_REF][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_is_locked_to_ref( .clk( clk ), .rst( rst ), .d( is_locked_to_ref ),	.q ( status[IS_RX_LOCKED_TO_REF][0] ));

assign status[SIGNAL_DETECT][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_signal_detect( .clk( clk ), .rst( rst ), .d( signal_detect ),	.q ( status[SIGNAL_DETECT][0] ));

assign status[RX_ERR_DETECT][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_rx_err_detect( .clk( clk ), .rst( rst ), .d( rx_err_detect ),	.q ( status[RX_ERR_DETECT][0] ));

assign status[RX_ERR_DISPARITY][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_rx_disp_err( .clk( clk ), .rst( rst ), .d( rx_disp_err ),	.q ( status[RX_ERR_DISPARITY][0] ));

assign status[RX_SYNC_STATUS][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_rx_sync_status( .clk( clk ), .rst( rst ), .d( rx_sync_status ),	.q ( status[RX_SYNC_STATUS][0] ));

assign status[LINK_STATUS][31:0]	= {31'h0, link_status};

assign status[RX_PATTERN_DETECT][31:1]	= 31'h0;
synchronizer #( .SZ_DATA( 1 ) ) sync_rx_pattern_detect ( .clk( clk ), .rst( rst ), .d( rx_pattern_det ),	.q ( status[RX_PATTERN_DETECT][0] ));

pattern_counter #(.SZ_PATTERN(2)) pc_pll_locked		( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( pll_locked ), .q( status[CNT_PLL_LOCKED] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_is_locked_to_data  ( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( is_locked_to_data ),.q( status[CNT_IS_LOCKED_TO_DATA] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_is_locked_to_ref	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( is_locked_to_ref ), .q( status[CNT_IS_LOCKED_TO_REF] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_signal_detect  	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( signal_detect ), 	.q( status[CNT_SIGNAL_DETECT] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_rx_err_detect  	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( rx_err_detect ), 	.q( status[CNT_RX_ERR_DETECT] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_rx_disp_err 	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( rx_disp_err ), 		.q( status[CNT_RX_ERR_DISPARITY] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_rx_sync_status	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( rx_sync_status ), 	.q( status[CNT_RX_SYNC_STATUS] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_link_status	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ),	.d( link_status ),		.q( status[CNT_LINK_STATUS] )); 
pattern_counter #(.SZ_PATTERN(2)) pc_rx_pattern_det	( .clk( clk ), .rst( rst | reset_counters), .pattern( 2'b01 ), .clk_cap( rx_core_clk ), .d( rx_pattern_det ),	.q( status[CNT_RX_PATTERN_DETECT] )); 

   assign status[CNT_TRIG_TX] = stat_trig_tx;
   assign status[CNT_STOP_TX] = stat_stop_tx;
   assign status[CNT_PKTS_TX] = stat_pkts_tx;
   assign status[CNT_OCTETS_TX] = stat_octets_tx;
   assign status[CNT_FLOW_TX] = stat_flow_tx;

   assign status[CNT_BADK_RX] = stat_badk_rx;
   assign status[CNT_TRIG_RX] = stat_trig_rx;
   assign status[CNT_STOP_RX] = stat_stop_rx;
   assign status[CNT_OVFL_RX] = stat_ovfl_rx;
   assign status[CNT_OCTETS_RX] = stat_octets_rx;
   assign status[CNT_PKTS_RX] = stat_pkts_rx;

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_stat (	
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s0_address ),
	.readdata		( s0_readdata ), 
	.read			( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 		( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_ctrl (
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s1_address ),
	.write			( s1_write ), 
	.writedata		( s1_writedata ),
	.byteenable     ( s1_byteenable ),
	.read			( s1_read  ), 
	.readdata		( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 			( ctrl )
);

endmodule
