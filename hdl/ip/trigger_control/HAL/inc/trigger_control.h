#ifndef __TRIGGER_CONTROL_H__
#define __TRIGGER_CONTROL_H__

#include <alt_types.h>
#include <io.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


#define GET_TRIGCTRL_REG_CTRL_OFFSET(base, reg) (((uint8_t*)(base) + ((reg)*4)))
#define GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(base, src, reg) ((uint8_t*)(base) + ((TRIG_CTRL_NUM_BASE_CTRL_REGS)*4) + ((TRIG_CTRL_NUM_SRC_CTRL_REGS*4)*(src)) + ((reg)*4))

#define GET_TRIGCTRL_REG_STAT_OFFSET(base, reg) (((uint8_t*)(base) + ((reg)*4)))
#define GET_TRIGCTRL_REG_STAT_SRC_OFFSET(base, num_busy, src, reg) ((uint8_t*)(base) + (num_busy*4) + ((TRIG_CTRL_NUM_BASE_STAT_REGS)*4) + ((TRIG_CTRL_NUM_SRC_STAT_REGS*4)*(src)) + ((reg)*4))

#define TRIGCTRL_WRITE_REG_CTRL(base, reg, val) IOWR(base, reg, val)
#define TRIGCTRL_WRITE_REG_CTRL_SRC(base, ch, reg, val) IOWR(base, TRIG_CTRL_NUM_BASE_CTRL_REGS+(ch*TRIG_CTRL_NUM_SRC_CTRL_REGS)+reg, val)

#define TRIGCTRL_READ_REG_CTRL(base, reg) IORD(base, reg)
#define TRIGCTRL_READ_REG_CTRL_SRC(base, ch, reg) IORD(base, TRIG_CTRL_NUM_BASE_CTRL_REGS+ (ch*TRIG_CTRL_NUM_SRC_CTRL_REGS)+reg)

#define TRIGCTRL_READ_REG_STAT(base, reg) IORD(base, reg)
#define TRIGCTRL_READ_REG_STAT_SRC(base, num_busy, ch, reg) IORD(base, TRIG_CTRL_NUM_BASE_STAT_REGS+num_busy+(ch*TRIG_CTRL_NUM_SRC_STAT_REGS)+reg)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __TRIGGER_CONTROL_H__ */
