/*
 * Copyright (c) 2016, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: June 29, 2018
 */

#include <unistd.h>
#include <drivers/inc/mv2_magnetometer.h>

void MV2_ReadSettings(mv2_spi_read rd, mv2_settings* settings) {
	uint8_t reg;

	// Bail if settings is null for some reason
	if(!settings) {
		return;
	}

	// Read all three registers
	reg = rd(0x00);

	settings->ma = (reg >> 6) & 0x3;
	settings->re = (reg >> 4) & 0x3;
	settings->ra = (reg >> 2) & 0x3;
	settings->os = (reg >> 0) & 0x3;

	reg = rd(0x01);
	settings->lmr = (reg >> 7) & 0x01;
	settings->emr = (reg >> 5) & 0x01;
	settings->hc  = (reg >> 4) & 0x01;
	settings->inv = (reg >> 3) & 0x01;
	settings->lp  = (reg >> 2) & 0x01;
	settings->po  = (reg >> 1) & 0x01;
	settings->sp  = (reg >> 0) & 0x01;

	reg = rd(0x02);
	settings->tc = (reg >> 2) & 0x0F;
}

void MV2_WriteSettings(mv2_spi_write wr, mv2_settings* settings) {
	uint8_t reg;

	// Bail if settings is null for some reason
	if(!settings) {
		return;
	}

	// Write all three registers
	reg = 0;
	reg |= (settings->ma & 0x3) << 6;
	reg |= (settings->re & 0x3) << 4;
	reg |= (settings->ra & 0x3) << 2;
	reg |= (settings->os & 0x3);
	wr(0x00, reg);

	reg = 0;
	reg |= (settings->lmr & 0x01) << 7;
	reg |= (settings->emr & 0x01) << 5;
	reg |= (settings->hc  & 0x01) << 4;
	reg |= (settings->inv & 0x01) << 3;
	reg |= (settings->lp  & 0x01) << 2;
	reg |= (settings->po  & 0x01) << 1;
	reg |= (settings->sp  & 0x01);
	wr(0x01, reg);

	reg = 0;
	reg |= (settings->tc & 0x0F) << 2;
	wr(0x02, reg);
}

uint16_t MV2_ReadAxis(mv2_spi_write wr, uint8_t next_axis_to_read, mv2_settings* settings) {
	uint8_t reg;

	if(!settings) {
		return 0;
	}

	// Update settings for next axis to read
	settings->os = next_axis_to_read & 0x3;

	reg = 0;
	reg |= (settings->ma & 0x3) << 6;
	reg |= (settings->re & 0x3) << 4;
	reg |= (settings->ra & 0x3) << 2;
	reg |= (settings->os & 0x3);

	return wr(0x00, reg);
}
