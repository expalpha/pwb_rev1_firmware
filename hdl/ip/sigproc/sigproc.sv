module sigproc (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_readdata,
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_byteenable,
	s1_readdatavalid, 
	s1_read,
	
	// Conduits	
	clk_sp,
	rst_sp,

	run_status,
	force_run,
    force_rst,
    trigger_out,
	ts_run,

	sca_enable,
	sca_samples_to_read,
	//sca_ch_threshold,
	sca_ch_ctrl,
    
    mem_ready,
    mem_slot,

	event_fifo_busy,
	event_fifo_full,
	event_fifo_err,
    
	test_pulse_interval_ena,
	test_pulse_interval,
    test_pulse_ena,
	test_pulse_width,
   
    sca_ch_enable,
	sca_ch_force,
    start_delay,
	test_mode,
	auto_mode
);

parameter SZ_TIME 	= 48;
parameter NUM_MEM_SLOTS = 16;

localparam SZ_DELAY = 32;
localparam NUM_SCA_DATA_CH = 79;
localparam SZ_WIDTH 			= 32; // always NIOS 32bit bus size

// Control Registers
localparam CTRL_FORCE_RUN		= 0;
localparam CTRL_FORCE_RESET     = 1;
localparam CTRL_TRIG_OUT      	= 2;
localparam CTRL_START_DELAY     = 3;
localparam CTRL_TEST_PULSE_ENA  = 4;

localparam CTRL_TEST_PULSE_ENA_INTERVAL = 5;
localparam CTRL_TEST_PULSE_INTERVAL = 6;
localparam CTRL_TEST_PULSE_WIDTH0_1 = 7;
localparam CTRL_TEST_PULSE_WIDTH2_3 = 8;
localparam CTRL_TEST_MODE = 9;
localparam CTRL_AUTO_MODE = 10;
localparam CTRL_MEM_READY = 11;
localparam CTRL_SCA_ENA = 12;
localparam CTRL_SCA_SAMPLES = 13;

localparam CTRL_SCA_A_CH_ENA_M0 = 14;
localparam CTRL_SCA_A_CH_ENA_M1 = 15;
localparam CTRL_SCA_A_CH_ENA_M2 = 16;

localparam CTRL_SCA_B_CH_ENA_M0 = 17;
localparam CTRL_SCA_B_CH_ENA_M1 = 18;
localparam CTRL_SCA_B_CH_ENA_M2 = 19;

localparam CTRL_SCA_C_CH_ENA_M0 = 20;
localparam CTRL_SCA_C_CH_ENA_M1 = 21;
localparam CTRL_SCA_C_CH_ENA_M2 = 22;

localparam CTRL_SCA_D_CH_ENA_M0 = 23;
localparam CTRL_SCA_D_CH_ENA_M1 = 23;
localparam CTRL_SCA_D_CH_ENA_M2 = 25;

localparam CTRL_SCA_A_CH_FORCE_M0 = 26;
localparam CTRL_SCA_A_CH_FORCE_M1 = 27;
localparam CTRL_SCA_A_CH_FORCE_M2 = 28;

localparam CTRL_SCA_B_CH_FORCE_M0 = 29;
localparam CTRL_SCA_B_CH_FORCE_M1 = 30;
localparam CTRL_SCA_B_CH_FORCE_M2 = 31;

localparam CTRL_SCA_C_CH_FORCE_M0 = 32;
localparam CTRL_SCA_C_CH_FORCE_M1 = 33;
localparam CTRL_SCA_C_CH_FORCE_M2 = 34;

localparam CTRL_SCA_D_CH_FORCE_M0 = 35;
localparam CTRL_SCA_D_CH_FORCE_M1 = 36;
localparam CTRL_SCA_D_CH_FORCE_M2 = 37;

localparam CTRL_SCA_A_CH_CTRL = 38;
localparam CTRL_SCA_B_CH_CTRL = 39;
localparam CTRL_SCA_C_CH_CTRL = 40;
localparam CTRL_SCA_D_CH_CTRL = 41;

//localparam CTRL_SCA_A_CH_ENA0 = 14;
//localparam CTRL_SCA_B_CH_ENA0 = CTRL_SCA_A_CH_ENA0+20;
//localparam CTRL_SCA_C_CH_ENA0 = CTRL_SCA_B_CH_ENA0+20;
//localparam CTRL_SCA_D_CH_ENA0 = CTRL_SCA_C_CH_ENA0+20;

//localparam CTRL_SCA_A_CH_FORCE0 = CTRL_SCA_D_CH_ENA0+20;
//localparam CTRL_SCA_B_CH_FORCE0 = CTRL_SCA_A_CH_FORCE0+20;
//localparam CTRL_SCA_C_CH_FORCE0 = CTRL_SCA_B_CH_FORCE0+20;
//localparam CTRL_SCA_D_CH_FORCE0 = CTRL_SCA_C_CH_FORCE0+20;

//localparam CTRL_SCA_A_CH_THRES0 = CTRL_SCA_D_CH_FORCE0+20;
//localparam CTRL_SCA_B_CH_THRES0 = CTRL_SCA_A_CH_THRES0+40;
//localparam CTRL_SCA_C_CH_THRES0 = CTRL_SCA_B_CH_THRES0+40;
//localparam CTRL_SCA_D_CH_THRES0 = CTRL_SCA_C_CH_THRES0+40;

//localparam CTRL_MEM_SLOT0 = CTRL_SCA_D_CH_THRES0+40;
//localparam NUM_CTRL_BASE_REGS 	= 14 + (20*8) + (40*4) + NUM_MEM_SLOTS;

localparam CTRL_MEM_SLOT0 = 42;

localparam NUM_CTRL_BASE_REGS 	= 42 + NUM_MEM_SLOTS;

localparam NUM_CTRL_REGS		= NUM_CTRL_BASE_REGS;

// Status Registers		
localparam STAT_RUN							=	0;
localparam STAT_TS_RUN_LO		         = 1;
localparam STAT_TS_RUN_HI		         = 2;

localparam STAT_EVENT_FIFO_BUSY = 3;
localparam STAT_EVENT_FIFO_FULL = 4;
localparam STAT_EVENT_FIFO_ERR	= 5;

localparam NUM_STAT_BASE_REGS	 = 6;

localparam NUM_STAT_REGS = NUM_STAT_BASE_REGS;

localparam SZ_STAT				= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL				= $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire	clk;
input wire	rst;

input wire	clk_sp;
input wire	rst_sp;

// Conduits
output wire force_run;
output wire force_rst;
output wire trigger_out;
output wire [3:0] sca_enable;
output wire [8:0] sca_samples_to_read;
output wire [3:0][7:0] start_delay;
output wire test_pulse_interval_ena;
output wire [31:0] test_pulse_interval;
output wire [3:0] test_pulse_ena;
output wire [3:0][15:0] test_pulse_width;

// DDR memory location conduits
output wire mem_ready; // set high when the memory slots are allocated
output wire [NUM_MEM_SLOTS-1:0][28:0] mem_slot; // locations of allocated memory regions
// TODO: Add memory slot size? Current method is UNSAFE, but as long as the fabric and the nios are in agreement on the size of the slots, this will be OK
output wire test_mode;
output wire auto_mode;

output wire [3:0][NUM_SCA_DATA_CH-1:0] sca_ch_enable;
output wire [3:0][NUM_SCA_DATA_CH-1:0] sca_ch_force;
//output wire [3:0][NUM_SCA_DATA_CH-1:0][11:0] sca_ch_threshold;
output wire [3:0][31:0] sca_ch_ctrl;

input wire run_status;
input wire [SZ_TIME-1:0] ts_run;
input wire [31:0] event_fifo_busy;
input wire [31:0] event_fifo_full;
input wire [31:0] event_fifo_err;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
output [SZ_WIDTH-1:0] 		s0_readdata;	// data bus input
input  	s0_read;	                        // valid bus cycle input
output	s0_readdatavalid;                       // bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input  				s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 		s1_writedata;	// data bus output
input        			s1_read;	// valid bus cycle input
input  [3:0]                    s1_byteenable;
output [SZ_WIDTH-1:0] 		s1_readdata;	// data bus input
output      			s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0]	ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0]	status;

reg [1:0] trig_ed;
   
   //[NUM_SCA_DATA_CH-1:0]
   assign sca_ch_enable[0] = { ctrl[CTRL_SCA_A_CH_ENA_M2], ctrl[CTRL_SCA_A_CH_ENA_M1], ctrl[CTRL_SCA_A_CH_ENA_M0] };
   assign sca_ch_enable[1] = { ctrl[CTRL_SCA_B_CH_ENA_M2], ctrl[CTRL_SCA_B_CH_ENA_M1], ctrl[CTRL_SCA_B_CH_ENA_M0] };
   assign sca_ch_enable[2] = { ctrl[CTRL_SCA_C_CH_ENA_M2], ctrl[CTRL_SCA_C_CH_ENA_M1], ctrl[CTRL_SCA_C_CH_ENA_M0] };
   assign sca_ch_enable[3] = { ctrl[CTRL_SCA_D_CH_ENA_M2], ctrl[CTRL_SCA_D_CH_ENA_M1], ctrl[CTRL_SCA_D_CH_ENA_M0] };

   //[NUM_SCA_DATA_CH-1:0]
   assign sca_ch_force[0] = { ctrl[CTRL_SCA_A_CH_FORCE_M2], ctrl[CTRL_SCA_A_CH_FORCE_M1], ctrl[CTRL_SCA_A_CH_FORCE_M0] };
   assign sca_ch_force[1] = { ctrl[CTRL_SCA_B_CH_FORCE_M2], ctrl[CTRL_SCA_B_CH_FORCE_M1], ctrl[CTRL_SCA_B_CH_FORCE_M0] };
   assign sca_ch_force[2] = { ctrl[CTRL_SCA_C_CH_FORCE_M2], ctrl[CTRL_SCA_C_CH_FORCE_M1], ctrl[CTRL_SCA_C_CH_FORCE_M0] };
   assign sca_ch_force[3] = { ctrl[CTRL_SCA_D_CH_FORCE_M2], ctrl[CTRL_SCA_D_CH_FORCE_M1], ctrl[CTRL_SCA_D_CH_FORCE_M0] };

   // [31:0]
   assign sca_ch_ctrl[0] = ctrl[CTRL_SCA_A_CH_CTRL];
   assign sca_ch_ctrl[1] = ctrl[CTRL_SCA_B_CH_CTRL];
   assign sca_ch_ctrl[2] = ctrl[CTRL_SCA_C_CH_CTRL];
   assign sca_ch_ctrl[3] = ctrl[CTRL_SCA_D_CH_CTRL];

genvar n;

synchronizer 		 	#( .SZ_DATA( 32  ) ) sync_run_stat 		( .clk( clk ), .rst( rst ), 				  .d( {31'h0, run_status} ), 		.q ( status[STAT_RUN] ));
synchronizer_counter #( .SZ_WIDTH( 32 ) ) sync_run_lo 		( .clk( clk ), .rst( rst ), .d_clk( clk_sp ), .d( ts_run[31:0] ), 				.q ( status[STAT_TS_RUN_LO] ));
synchronizer_counter #( .SZ_WIDTH( 32 ) ) sync_run_hi 		( .clk( clk ), .rst( rst ), .d_clk( clk_sp ), .d( {16'h0, ts_run[47:32]} ), 	.q ( status[STAT_TS_RUN_HI] ));
synchronizer_counter #( .SZ_WIDTH( 32 ) ) sync_evt_fifo_busy ( .clk( clk ), .rst( rst ), .d_clk( clk_sp ), .d( event_fifo_busy ), 			.q ( status[STAT_EVENT_FIFO_BUSY] ));
synchronizer_counter #( .SZ_WIDTH( 32 ) ) sync_evt_fifo_full ( .clk( clk ), .rst( rst ), .d_clk( clk_sp ), .d( event_fifo_full ), 			.q ( status[STAT_EVENT_FIFO_FULL] ));
synchronizer_counter #( .SZ_WIDTH( 32 ) ) sync_evt_fifo_err 	( .clk( clk ), .rst( rst ), .d_clk( clk_sp ), .d( event_fifo_err ), 			.q ( status[STAT_EVENT_FIFO_ERR] ));

synchronizer 		 #( .SZ_DATA( 1  ) ) sync_force_run 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_FORCE_RUN][0] ), 	.q ( force_run ));
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_force_rst 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_FORCE_RESET][0] ), 	.q ( force_rst ));
synchronizer 		 #( .SZ_DATA( 32  ) ) sync_start_dly 	( .clk( clk_sp ), .rst( rst_sp ), .d( {ctrl[CTRL_START_DELAY][31:24], ctrl[CTRL_START_DELAY][23:16], ctrl[CTRL_START_DELAY][15:8], ctrl[CTRL_START_DELAY][7:0]} ), 	.q ( start_delay ));
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_test_pulse_interval_ena 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_TEST_PULSE_ENA_INTERVAL][0] ), 	.q ( test_pulse_interval_ena ));
synchronizer 		 #( .SZ_DATA( 32  ) ) sync_test_pulse_interval 		( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_TEST_PULSE_INTERVAL][31:0] ), 	.q ( test_pulse_interval ));
synchronizer 		 #( .SZ_DATA( 4  ) ) sync_test_pulse_ena 	( .clk( clk_sp ), .rst( rst_sp ), .d( { ctrl[CTRL_TEST_PULSE_ENA][24], ctrl[CTRL_TEST_PULSE_ENA][16], ctrl[CTRL_TEST_PULSE_ENA][8], ctrl[CTRL_TEST_PULSE_ENA][0] } ), 	.q ( test_pulse_ena ));
synchronizer 		 #( .SZ_DATA( 64  ) ) sync_test_pulse_width 	( .clk( clk_sp ), .rst( rst_sp ), .d( {ctrl[CTRL_TEST_PULSE_WIDTH2_3][31:16], ctrl[CTRL_TEST_PULSE_WIDTH2_3][15:0], ctrl[CTRL_TEST_PULSE_WIDTH0_1][31:16], ctrl[CTRL_TEST_PULSE_WIDTH0_1][15:0]} ), 	.q ( test_pulse_width ));
synchronizer 		 #( .SZ_DATA( 4  ) ) sync_sca_enable 	( .clk( clk_sp ), .rst( rst_sp ), .d( { ctrl[CTRL_SCA_ENA][24], ctrl[CTRL_SCA_ENA][16], ctrl[CTRL_SCA_ENA][8], ctrl[CTRL_SCA_ENA][0] } ), 	.q ( sca_enable ));
synchronizer 		 #( .SZ_DATA( 9  ) ) sync_sca_samples_to_read 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_SAMPLES][8:0] ), 	.q ( sca_samples_to_read ));
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_mem_ready 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_MEM_READY][0] ), 	.q ( mem_ready ));
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_test_mode 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_TEST_MODE][0] ), 	.q ( test_mode ));
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_auto_mode 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_AUTO_MODE][0] ), 	.q ( auto_mode ));

generate
for(n=0; n< NUM_MEM_SLOTS; n+=1) begin: gen_mem_slots
	synchronizer 		 #( .SZ_DATA( 29  ) ) sync_mem_slot 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_MEM_SLOT0+n][28:0]  ), 	.q ( mem_slot[n] ));
end
endgenerate 

wire trig_out_synced;
synchronizer 		 #( .SZ_DATA( 1  ) ) sync_trig_out 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_TRIG_OUT][0] ), 	.q ( trig_out_synced ));

always@(posedge rst_sp, posedge clk_sp) begin 
   if(rst_sp) begin 
         trig_ed <= 2'b00;
     end else begin 
         trig_ed <= { trig_ed[0] , trig_out_synced };
     end
end 
assign trigger_out = (trig_ed == 2'b01) ? 1'b1 : 1'b0; 


//generate
//   
//for(n=0; n<78; n+=2) begin: gen_ch_thres
//	// SCA A
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_a0	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_A_CH_THRES0+(n/2)][11:0] ), 	.q ( sca_ch_threshold[0][n+0][11:0] ));
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_a1 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_A_CH_THRES0+(n/2)][27:16] ), 	.q ( sca_ch_threshold[0][n+1][11:0] ));

//	// SCA B
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_b0	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_B_CH_THRES0+(n/2)][11:0] ), 	.q ( sca_ch_threshold[1][n+0][11:0] ));
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_b1 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_B_CH_THRES0+(n/2)][27:16] ), 	.q ( sca_ch_threshold[1][n+1][11:0] ));

//	// SCA C
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_c0	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_C_CH_THRES0+(n/2)][11:0] ), 	.q ( sca_ch_threshold[2][n+0][11:0] ));
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_c1 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_C_CH_THRES0+(n/2)][27:16] ), 	.q ( sca_ch_threshold[2][n+1][11:0] ));

//	// SCA D
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_d0	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_D_CH_THRES0+(n/2)][11:0] ), 	.q ( sca_ch_threshold[3][n+0][11:0] ));
//	synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_d1 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_D_CH_THRES0+(n/2)][27:16] ), 	.q ( sca_ch_threshold[3][n+1][11:0] ));
//end

//// SCA A
//synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_a_last 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_A_CH_THRES0+(78/2)][11:0] ), 	.q ( sca_ch_threshold[0][78][11:0] ));

//// SCA B
//synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_b_last 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_B_CH_THRES0+(78/2)][11:0] ), 	.q ( sca_ch_threshold[1][78][11:0] ));

//// SCA C
//synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_c_last 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_C_CH_THRES0+(78/2)][11:0] ), 	.q ( sca_ch_threshold[2][78][11:0] ));

//// SCA D
//synchronizer 		 #( .SZ_DATA( 12  ) ) sync_ch_thres_d_last 	( .clk( clk_sp ), .rst( rst_sp ), .d( ctrl[CTRL_SCA_D_CH_THRES0+(78/2)][11:0] ), 	.q ( sca_ch_threshold[3][78][11:0] ));


//for(n=0; n<76; n+=4) begin: gen_ch_ctrls
//	// Channel Enable
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_enable_a 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[0][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_A_CH_ENA0+(n/4)][24], ctrl[CTRL_SCA_A_CH_ENA0+(n/4)][16], ctrl[CTRL_SCA_A_CH_ENA0+(n/4)][8], ctrl[CTRL_SCA_A_CH_ENA0+(n/4)][0] } ));
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_enable_b 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[1][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_B_CH_ENA0+(n/4)][24], ctrl[CTRL_SCA_B_CH_ENA0+(n/4)][16], ctrl[CTRL_SCA_B_CH_ENA0+(n/4)][8], ctrl[CTRL_SCA_B_CH_ENA0+(n/4)][0] } ));	
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_enable_c 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[2][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_C_CH_ENA0+(n/4)][24], ctrl[CTRL_SCA_C_CH_ENA0+(n/4)][16], ctrl[CTRL_SCA_C_CH_ENA0+(n/4)][8], ctrl[CTRL_SCA_C_CH_ENA0+(n/4)][0] } ));
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_enable_d 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[3][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_D_CH_ENA0+(n/4)][24], ctrl[CTRL_SCA_D_CH_ENA0+(n/4)][16], ctrl[CTRL_SCA_D_CH_ENA0+(n/4)][8], ctrl[CTRL_SCA_D_CH_ENA0+(n/4)][0] } ));
//
//	// Channel Force
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_force_a 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[0][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_A_CH_FORCE0+(n/4)][24], ctrl[CTRL_SCA_A_CH_FORCE0+(n/4)][16], ctrl[CTRL_SCA_A_CH_FORCE0+(n/4)][8], ctrl[CTRL_SCA_A_CH_FORCE0+(n/4)][0] } ));
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_force_b 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[1][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_B_CH_FORCE0+(n/4)][24], ctrl[CTRL_SCA_B_CH_FORCE0+(n/4)][16], ctrl[CTRL_SCA_B_CH_FORCE0+(n/4)][8], ctrl[CTRL_SCA_B_CH_FORCE0+(n/4)][0] } ));	
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_force_c 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[2][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_C_CH_FORCE0+(n/4)][24], ctrl[CTRL_SCA_C_CH_FORCE0+(n/4)][16], ctrl[CTRL_SCA_C_CH_FORCE0+(n/4)][8], ctrl[CTRL_SCA_C_CH_FORCE0+(n/4)][0] } ));
//	synchronizer 		 #( .SZ_DATA( 4  ) ) sync_ch_force_d 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[3][(n+3):n] ), 	.d ( { ctrl[CTRL_SCA_D_CH_FORCE0+(n/4)][24], ctrl[CTRL_SCA_D_CH_FORCE0+(n/4)][16], ctrl[CTRL_SCA_D_CH_FORCE0+(n/4)][8], ctrl[CTRL_SCA_D_CH_FORCE0+(n/4)][0] } ));
//end

// Channel Enable
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_enable_a 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[0][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_A_CH_ENA0+(76/4)][16], ctrl[CTRL_SCA_A_CH_ENA0+(76/4)][8], ctrl[CTRL_SCA_A_CH_ENA0+(76/4)][0] } ));
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_enable_b 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[1][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_B_CH_ENA0+(76/4)][16], ctrl[CTRL_SCA_B_CH_ENA0+(76/4)][8], ctrl[CTRL_SCA_B_CH_ENA0+(76/4)][0] } ));	
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_enable_c 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[2][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_C_CH_ENA0+(76/4)][16], ctrl[CTRL_SCA_C_CH_ENA0+(76/4)][8], ctrl[CTRL_SCA_C_CH_ENA0+(76/4)][0] } ));
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_enable_d 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_enable[3][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_D_CH_ENA0+(76/4)][16], ctrl[CTRL_SCA_D_CH_ENA0+(76/4)][8], ctrl[CTRL_SCA_D_CH_ENA0+(76/4)][0] } ));

// Channel Force
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_force_a 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[0][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_A_CH_FORCE0+(76/4)][16], ctrl[CTRL_SCA_A_CH_FORCE0+(76/4)][8], ctrl[CTRL_SCA_A_CH_FORCE0+(76/4)][0] } ));
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_force_b 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[1][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_B_CH_FORCE0+(76/4)][16], ctrl[CTRL_SCA_B_CH_FORCE0+(76/4)][8], ctrl[CTRL_SCA_B_CH_FORCE0+(76/4)][0] } ));	
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_force_c 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[2][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_C_CH_FORCE0+(76/4)][16], ctrl[CTRL_SCA_C_CH_FORCE0+(76/4)][8], ctrl[CTRL_SCA_C_CH_FORCE0+(76/4)][0] } ));
//synchronizer 		 #( .SZ_DATA( 3  ) ) sync_ch_force_d 	( .clk( clk_sp ), .rst( rst_sp ), .q( sca_ch_force[3][(76+2):76] ), 	.d ( { ctrl[CTRL_SCA_D_CH_FORCE0+(76/4)][16], ctrl[CTRL_SCA_D_CH_FORCE0+(76/4)][8], ctrl[CTRL_SCA_D_CH_FORCE0+(76/4)][0] } ));

//endgenerate

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_stat (	
	.clk						( clk ), 
	.rst						( rst ), 
	.address				( s0_address ),
	.readdata			( s0_readdata ), 
	.read					( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 				( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_ctrl (
	.clk						( clk ), 
	.rst						( rst ), 
	.address				( s1_address ),
	.write					( s1_write ), 
	.writedata			( s1_writedata ),
	.byteenable         (s1_byteenable),
	.read					( s1_read  ), 
	.readdata			( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 					( ctrl )
);

endmodule
