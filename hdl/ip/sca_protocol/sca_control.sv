 
module sca_control (
    clk,
    rst,
    wr_n,
    addr,
    request,
    active,
    len,
    d,
    q,
    
    sc_ck,
    sc_en,
    sc_din,
    sc_dout
);

parameter SZ_ADDR = 7;
parameter SZ_SHIFT = 38;
parameter CLOCK_PERIOD = 10;

localparam SZ_OF_ADDR = $clog2(SZ_ADDR+1);
localparam SZ_LEN = $clog2(SZ_SHIFT+1);
localparam SZ_SHIFTOUT = SZ_SHIFT+SZ_ADDR+1; 
localparam SZ_CLOCK = $clog2(CLOCK_PERIOD+1);
localparam SZ_COUNT = $clog2(SZ_SHIFT + SZ_ADDR + 5); // extra 5 is for final 3 clock cycles + 1 for Y bit on read + 1 to ensure count # can be stored

input wire clk;
input wire rst;
input wire wr_n;
input wire [SZ_ADDR-1:0] addr;
input wire request;
input wire [SZ_LEN-1:0] len;
input wire [SZ_SHIFT-1:0] d;
output reg [SZ_SHIFT-1:0] q;
output reg active;

output reg sc_ck;
output reg sc_en;
output wire sc_din;
input  wire sc_dout;

wire clock_active;

wire transmit_active;
reg [SZ_COUNT-1:0] r_count;
reg [SZ_COUNT-1:0] r_endcount;
reg [SZ_CLOCK-1:0] r_clock;
reg [SZ_SHIFTOUT-1:0] r_shiftout;

assign sc_din = r_shiftout[SZ_SHIFTOUT-1];
assign transmit_active = (r_count < r_endcount) ? 1'b1 : 1'b0;
assign clock_active = (transmit_active || (r_clock != 0)) ? 1'b1 : 1'b0;

always@(posedge rst, posedge clk) begin 
    if(rst) begin 
        sc_ck   <= 1'b0;
        sc_en   <= 1'b0;                
        active  <= 1'b0;
        r_clock <= {SZ_CLOCK{1'b0}};
        q       <= {SZ_SHIFT{1'b0}};                
        r_count <= {SZ_COUNT{1'b0}};
        r_endcount <= {SZ_COUNT{1'b0}};
    end else begin         
        // transmission request? 
        if(request && !active) begin 
            r_clock <= CLOCK_PERIOD[SZ_CLOCK-1:0] -1'b1;
            r_count <= {SZ_COUNT{1'b0}};
            r_endcount <= (wr_n) ? 1'b1 + SZ_ADDR[SZ_OF_ADDR-1:0] + len : 2'h3 + SZ_ADDR[SZ_OF_ADDR-1:0] + len; // read length is extended by one due to 'Y' bit
            r_shiftout <= { wr_n, addr, d }; // Gather up requested bits into shift register             
            q <= {SZ_SHIFT{1'b0}};
            sc_en <= 1'b1;
            sc_ck <= 1'b0;
            active  <= 1'b1;
        // currently transmitting?
        end else begin 
            active  <= clock_active;
            r_endcount <= r_endcount;
            r_clock <= (r_clock > 0) ? (r_clock - 1'b1) : (transmit_active) ? (CLOCK_PERIOD[SZ_CLOCK-1:0] - 1'b1) : {SZ_CLOCK{1'b0}}; // just keep clocking 
            
            if(clock_active) begin 
                sc_ck <= (clock_active) ? (r_clock >= (CLOCK_PERIOD[SZ_CLOCK-1:0] >> 1)) ? 1'b1 : 1'b0 : 1'b0;
                q   	 <= ((r_clock == (CLOCK_PERIOD[SZ_CLOCK-1:0] - 1'b1) ) && (r_count > (SZ_ADDR+1))) ? { q[SZ_SHIFT-2:0], sc_dout } : q;
            end else begin 
                sc_ck <= 1'b0;
                q <= q;
            end 
            
            if(transmit_active) begin 
                r_count 	<= (r_clock == 0) ? r_count + 1'b1 : r_count;
                sc_en 		<= (r_clock == 0) ? ((r_endcount - r_count) > 3) ? 1'b1 : 1'b0 : sc_en;
                r_shiftout <= (r_clock == 0) ? { r_shiftout[SZ_SHIFTOUT-2:0], 1'b0 } : r_shiftout;                                
            end else begin 
                r_count <= r_count;            
                sc_en <= 1'b0;                
                r_shiftout <= r_shiftout;               
            end
            
        end 
    end
end 

endmodule 
