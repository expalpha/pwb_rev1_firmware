`default_nettype none
module sca_read_control
  (
   input wire clk,
   input wire rst,
   input wire start,
   input wire [SZ_START_DLY-1:0] start_delay,
   input wire auto_mode,
   input wire [SZ_SAMPLE_CNT-1:0] num_samples_to_read,
   input wire test_mode,
   input wire [31:0] ch_ctrl,

   input wire [SZ_DATA-1:0] sca_dat,

   output reg [SZ_DATA-1:0] out_dat,
   output reg out_sop,
   output reg out_eop,
   output reg out_val,
   output reg [ROW_LEN-1:0] ch_crossed_out,

   output reg sca_read_ena,
   output reg sca_read_clk,
   output reg done
   );

parameter SZ_DATA = 12; // this has to be at least 9 bits, or else sending out the cell address at the end will fail...
parameter MAX_SAMPLES = 511; // the physical max in the SCA
parameter SZ_START_DLY = 8;
  
localparam SZ_SAMPLE_CNT = $clog2(MAX_SAMPLES+1);
localparam MAX_CH_VALUE = {1'b0, {(SZ_DATA-1){1'b1}}};
  
localparam RST_LEN = 3;
localparam DAT_LEN = 76;
localparam ROW_LEN = RST_LEN + DAT_LEN; // 72 signal channels, 4 FPN channels, 3 'reset' clocks at the start of every row

localparam ZERO_LEN = 1; // documentation states 8, but this appears to be incorrect!
localparam ADDR_LEN = 9;
localparam LAST_LEN = ZERO_LEN + ADDR_LEN;  
    
localparam SZ_ROW = $clog2(ROW_LEN+1);
  

   wire [11:0]       threshold = ch_ctrl[11:0];
   wire [2:0]        ctrl_test_mode = ch_ctrl[14:12];
   wire              ctrl_supp_mode = ch_ctrl[15];
  
reg read_ena;  
reg read_clk;

reg [SZ_DATA-1:0] r_sca_dat0;
reg [SZ_DATA-1:0] r_sca_dat1;
  
reg [SZ_ROW-1:0] row_cnt;
reg [SZ_SAMPLE_CNT-1:0] samples_to_read;
reg [SZ_SAMPLE_CNT-1:0] samples_read;
reg [SZ_START_DLY-1:0] delay;
reg [LAST_LEN-1:0] addr;
  
wire msb_set;
wire row_done = (row_cnt == (ROW_LEN-1'b1));
wire read_done = ((samples_read == (samples_to_read-1'b1)) && (row_done)) ? 1'b1 : 1'b0;  

// this assumes sca_dat is SIGNED data, it's possible to configure the ADC in binary offset instead... but don't do that without changing this!
// msb_set is used to get the ADDRESS bits at the end of the READ, which is useful for alignment checks as well
assign msb_set = (r_sca_dat1[SZ_DATA-1 : SZ_DATA-2] == 2'b01) ? 1'b1 : 1'b0;

enum int unsigned { ST_IDLE = 0, ST_START = 2, ST_ROW = 4, ST_ADDR = 8, ST_DONE = 16 } sca_state, ena_state, clk_state;
  
// transition the actual pins on the negative edge, as per datasheet recommendation
always@(negedge clk) begin
  sca_read_ena <= read_ena;
  sca_read_clk <= read_clk;
end

   reg [ROW_LEN-1:0] ch_crossed_min;

// Register sca_dat to be one clock cycle 'behind' so we can effectively 'look-ahead' for low signal for start detection
always@(posedge clk) begin
	r_sca_dat0 <= sca_dat;
	r_sca_dat1 <= r_sca_dat0;
end
 
// Start Detection with delay backup
wire start_detected = (sca_state == ST_START) && ($signed(sca_dat) > $signed(12'h000)) && ($signed(r_sca_dat0) < $signed(12'h000)) ? 1'b1 : (delay > 0) ? 1'b0 : 1'b1;

wire start_signal = (auto_mode) ? start_detected : (delay > 0) ? 1'b0 : 1'b1;

reg [31:0] test_counter;

reg trig_reset;
reg [ROW_LEN-1:0] trig_sel;
reg [11:0]        trig_adc;

wire [ROW_LEN-1:0] trig_pos;
wire [ROW_LEN-1:0] trig_neg;
wire [ROW_LEN-1:0] trig;
 
always@(posedge rst, posedge clk) begin
  if(rst) begin
    samples_read 	<= {SZ_SAMPLE_CNT{1'b0}};
    samples_to_read <= {SZ_SAMPLE_CNT{1'b0}};
    delay			<= {SZ_START_DLY{1'b0}};
    done				<= 1'b0;
    out_dat 		<= {SZ_DATA{1'b0}};
    out_sop 		<= 1'b0;
    out_eop 		<= 1'b0;
    out_val 		<= 1'b0;      
    sca_state		<= ST_IDLE;
    addr 			<= {ADDR_LEN{1'b0}};
    row_cnt 		<= {SZ_ROW{1'b0}};	 
    test_counter <= 0;
	 read_ena		<= 1'b0;
	 read_clk		<= 1'b0;
     for(int i=0; i<ROW_LEN; i+=1) begin
        ch_crossed_out[i] <= 0;
        ch_crossed_min[i] <= 0;
     end
     trig_reset <= 1;
  end else begin
    case(sca_state) 
      ST_IDLE: begin		
        sca_state 		<= (start) ? ST_START : ST_IDLE;
        samples_to_read <= num_samples_to_read;
        samples_read	<= {SZ_SAMPLE_CNT{1'b0}};
		  // if not in auto mode, use the start_delay
		 if(auto_mode == 1'b0) begin
			delay 			<= (start_delay > 0) ? start_delay - 1'b1 : {SZ_START_DLY{1'b0}};
		end else begin
		// we want to force the delay if start_detect is in auto mode to prevent a hang if the signal is not found
			delay				<= 8'd31;
		end
        done			<= 1'b0;
        out_dat 		<= {SZ_DATA{1'b0}};
        out_sop 		<= 1'b0;
        out_eop 		<= 1'b0;
        out_val 		<= 1'b0;               
        row_cnt 		<= {SZ_ROW{1'b0}};
        test_counter <= 0;
        addr 			<= {ADDR_LEN{1'b0}};
	 read_ena		<= 1'b0;
	 read_clk		<= 1'b0;
         for(int i=0; i<ROW_LEN; i+=1) begin
            trig_sel[i] <= 0;
            ch_crossed_min[i] <= 0;
         end
         trig_reset <= 1;
      end
      // wait until ADC SERDES delay is done, then start reading!
      // just before reading starts, output the number of samples that will be read
      ST_START: begin
        sca_state 		<= (start_signal) ? ST_ROW : ST_START;
        samples_to_read <= samples_to_read;
        samples_read	<= {SZ_SAMPLE_CNT{1'b0}};
        delay 			<= (delay > 0) ? (read_clk == 1'b1) ? delay - 1'b1 : delay : {SZ_START_DLY{1'b0}};          	          	
        done			<= 1'b0;
        out_dat 		<= (start_signal) ? samples_to_read : {SZ_DATA{1'b0}};
        out_sop 		<= (start_signal) ? 1'b1 : 1'b0;
        out_eop 		<= 1'b0;
        out_val 		<= (start_signal) ? 1'b1 : 1'b0; 
        row_cnt 		<= {SZ_ROW{1'b0}};
        test_counter <= 0;
        addr 			<= {ADDR_LEN{1'b0}};
	 read_ena		<= 1'b1;
	 read_clk		<= 1'b1;
         for(int i=0; i<ROW_LEN; i+=1) begin
            trig_sel[i] <= 0;
            ch_crossed_out[i] <= 0;
            ch_crossed_min[i] <= 0;
         end
         trig_reset <= 1;
      end
      ST_ROW: begin
        sca_state 		<= (read_done) ? ST_ADDR : ST_ROW;
        samples_to_read <= samples_to_read;
        samples_read	<= (row_done) ? samples_read + 1'b1 : samples_read;
        delay 			<= {SZ_START_DLY{1'b0}}; 
        done			<= 1'b0;
        if (test_mode) begin
           case(ctrl_test_mode) 
             0: begin		
                out_dat	<= 12'ha5a; // fixed test pattern
             end
             1: begin		
                out_dat	<= samples_read; // time bin counter
             end
             2: begin		
                out_dat	<= {row_cnt[2:0], samples_read[8:0]}; // channel number and time bin counter
             end
             3: begin		
                out_dat	<= test_counter[SZ_DATA-1:0]; // sequential adc sample counter
             end
             4: begin		
                out_dat <= {ch_crossed_out[row_cnt], trig_pos[row_cnt], trig_neg[row_cnt], r_sca_dat1[8:0]};
             end
             5: begin		
                out_dat <= {trig[row_cnt], r_sca_dat1[10:0]};
             end
             6: begin		
                out_dat <= {ch_crossed_min[row_cnt], r_sca_dat1[10:0]};
             end
             default: begin
                out_dat <= 0;
             end
           endcase
        end else begin
           out_dat 		<= r_sca_dat1;
        end
        test_counter <= test_counter + 1;
        out_sop 		<= 1'b0;
        out_eop 		<= 1'b0;
        out_val 		<= 1'b1;    
        row_cnt		<= (row_done) ? {SZ_ROW{1'b0}} : row_cnt + 1'b1;
        addr 			<= {ADDR_LEN{1'b0}};
	 read_ena		<= (read_done) ? 1'b0 : 1'b1;
	 read_clk		<= 1'b1;		  		 
         
         //trig_sel <= {ROW_LEN{1'b0}};
         for(int i=0; i<ROW_LEN; i+=1) begin
            if (row_cnt == i) begin
               trig_sel[i] <= 1;
            end else begin
               trig_sel[i] <= 0;
            end
            if (samples_read > 32) begin
               ch_crossed_min[i] <= ch_crossed_min[i] | (trig_sel[i] & ($signed(trig_adc) <= $signed(threshold)));
               if (ctrl_supp_mode) begin
                  ch_crossed_out[i] <= ch_crossed_out[i] | ch_crossed_min[i];
               end else begin
                  ch_crossed_out[i] <= ch_crossed_out[i] | trig[i];
               end
            end
         end
         trig_adc <= r_sca_dat1;
         if (samples_read < 4) begin
            trig_reset <= 1;
         end else begin
            trig_reset <= 0;
         end
      end 
      ST_ADDR: begin
        sca_state 		<= (row_cnt == (LAST_LEN)) ? ST_DONE : ST_ADDR;
        samples_to_read <= samples_to_read;
        samples_read	<= {SZ_SAMPLE_CNT{1'b0}};
        delay 			<= {SZ_START_DLY{1'b0}}; 
        done			<= (row_cnt == (LAST_LEN)) ? 1'b1 : 1'b0;
        out_dat 		<= { {(SZ_DATA-ADDR_LEN){1'b0}}, addr[ADDR_LEN-1:0] };
        out_sop 		<= 1'b0;
        out_eop 		<= (row_cnt == (LAST_LEN)) ? 1'b1 : 1'b0; 
        out_val 		<= (row_cnt == (LAST_LEN)) ? 1'b1 : 1'b0;    
        row_cnt		<= (row_cnt == (LAST_LEN)) ? {SZ_ROW{1'b0}} : row_cnt + 1'b1;          
        addr 			<= {addr[ADDR_LEN-2:0], msb_set};  
	 read_ena		<= 1'b0;
	 read_clk		<= 1'b1;
         for(int i=0; i<ROW_LEN; i+=1) begin
            trig_sel[i] <= 0;
         end
         trig_reset <= 0;
      end 

      ST_DONE: begin
        sca_state 		<= ST_IDLE;
        samples_to_read <= num_samples_to_read;
        samples_read 	<= {SZ_SAMPLE_CNT{1'b0}};
        done			<= 1'b1;
        delay 			<= start_delay;
        out_dat 		<= {SZ_DATA{1'b0}};
        out_sop 		<= 1'b0;
        out_eop 		<= 1'b0;
        out_val 		<= 1'b0;               
        row_cnt 		<= {SZ_ROW{1'b0}};
        test_counter <= 0;
        addr 			<= {ADDR_LEN{1'b0}};          
	 read_ena		<= 1'b0;
	 read_clk		<= 1'b0;
         for(int i=0; i<ROW_LEN; i+=1) begin
            trig_sel[i] <= 0;
         end
         trig_reset <= 1;
      end

    endcase
  end
end

genvar n;
generate 
for(n=0; n<ROW_LEN; n++) begin : gen_sca_trig
   sca_trig_one sca_trig_one
     (
      .reset   ( trig_reset ),
      .clk     ( clk ),
      .adc_stream ( trig_adc ),
      .adc_strobe ( trig_sel[n]),
      .threshold ( threshold ),
      //output reg [11:0] baseline_out,
      //output reg [11:0] adc_out,
      .trig_pos ( trig_pos[n] ),
      .trig_neg ( trig_neg[n] ),
      .trig_out ( trig[n] )
      );
end
endgenerate

endmodule
