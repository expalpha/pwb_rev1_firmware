//
// channel suppression for PWB data
//
`default_nettype none
module sca_trig_one
  (
   input wire 	     reset,
   input wire 	     clk,
   input wire [11:0] threshold,
   input wire [11:0] adc_stream,
   input wire        adc_strobe,
   output reg [11:0] baseline_out,
   output reg [11:0] adc_out,
   output reg 	     trig_out,
   output wire 	     trig_pos,
   output wire 	     trig_neg
   );

   // width of ADC signal

   //parameter ADC_NBITS = 12;
   //parameter ADC_NBITS1 = ADC_NBITS-1;

   // compute the baseline

   localparam NSAMPLES = 64; // baseline is sum of 64 samples
   localparam NEXT = $clog2(NSAMPLES); // 6 bits of overflow
   localparam NBITS = 12 + NEXT; // 12+6 = 18 bits of adc sum

   localparam NEXT1 = NEXT-1;
   localparam NBITS1 = NBITS-1;

   reg signed [NBITS1:0] baseline_sum;
   reg [7:0]         baseline_count;
   reg               baseline_ready;

   // construct sign-extended adc value
   wire signed [NBITS1:0] adc_sext;
   assign adc_sext[11:0] = adc_stream[11:0];
   assign adc_sext[NBITS1:12] = (adc_stream[11]?{NEXT{1'b1}}:{NEXT{1'b0}});

   wire signed [11:0] adc_value = adc_stream - baseline_out; // signed!

   assign trig_neg = baseline_ready & (adc_value <= $signed(-threshold)); // signed!
   assign trig_pos = baseline_ready & (adc_value >= $signed(threshold)); // signed!

   always_ff@ (posedge clk) begin
      if (reset) begin
         baseline_sum <= 0;
         baseline_count <= 0;
         baseline_ready <= 0;
         adc_out <= 0;
         baseline_out <= 0;
         trig_out <= 0;
      end else if (adc_strobe) begin
         if (baseline_ready) begin
            adc_out <= adc_value;
            trig_out <= trig_pos | trig_neg;
         end else begin
            baseline_sum <= baseline_sum + adc_sext; // signed add
            baseline_count <= baseline_count + 8'h01;
            adc_out <= 0;
            trig_out <= 0;
         end
      end else if (!baseline_ready) begin
         if (baseline_count == NSAMPLES) begin
            baseline_out[11:0] <= baseline_sum[NBITS1:NEXT]; // div by 64
            baseline_ready <= 1;
            adc_out <= 0;
            trig_out <= 0;
         end
      end
   end // always_ff@ (posedge clk)
   
endmodule
