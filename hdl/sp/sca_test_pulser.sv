module sca_test_pulser (
	clk,
	rst,
	
	trigger_in,
	trigger_out,
	
	ena_interval,
	interval,
	
	ena_sca,
	pulse_width,
	
	q
);

parameter NUM_SCA = 4;

input wire clk;
input wire rst;

input wire trigger_in;	// Accept a trigger in to immediatly fire a pulse (ignored if currently firing a pulse)
output reg trigger_out;	// When interval is reached, fire a trigger

input wire ena_interval;	// Enable trigger on interval set by interval
input wire [31:0] interval;

input wire [NUM_SCA-1:0] ena_sca;
input wire [NUM_SCA-1:0][15:0] pulse_width;

output reg [NUM_SCA-1:0] q;

wire int_trigger;
reg [31:0] interval_timer;

assign int_trigger = trigger_out | trigger_in;

always@(posedge rst, posedge clk) begin
	if(rst) begin
		interval_timer <= 32'h0;
		trigger_out 	<= 1'b0;
	end else begin
		if(ena_interval) begin
			interval_timer <= (interval_timer < interval) ? interval_timer + 1'b1 : 32'h0;
			trigger_out		<= (interval_timer < interval) ? 1'b0 : 1'b1;
		end else begin
			interval_timer	<= 32'h0;
			trigger_out		<= 1'b0;
		end		
	end
end


genvar n;
generate
	for(n=0; n<NUM_SCA; n+=1) begin: gen_sca_test
	
		reg [15:0] pulser_timer;
	
		// count until width exceeded, then wait for trigger to restart	
		// if enable is false, just wait with pulse_timer high
		always@(posedge rst, posedge clk) begin
			if(rst) begin
				q[n] <= 1'b0;
				pulser_timer <= 16'hffff;
			end else begin
				q[n] <= (pulser_timer < pulse_width[n]) ? 1'b1 : 1'b0;				
				if(ena_sca[n]) begin					
					pulser_timer <= (pulser_timer < pulse_width[n]) ? pulser_timer + 1'b1 : (int_trigger) ? 16'h0 : 16'hffff;	
				end else begin					
					pulser_timer <= 16'hffff;
				end				
			end
		end
	end
endgenerate


endmodule
