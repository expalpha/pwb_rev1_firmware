//
// detector for ADC pulses
//
`default_nettype none
module sca_trig_one
  (
   input wire 	     reset,
   input wire 	     clk,
   input wire [11:0] threshold,
   input wire [11:0] adc_stream,
   input wire        adc_strobe,
   output reg [11:0] baseline_out,
   output reg [11:0] adc_out,
   output reg 	     trig_out,
   output wire 	     trig_pos,
   output wire 	     trig_neg
   );

   // compute the baseline

   reg [11:0] 			adc_in;
   //reg [11:0] 			adc0a;
   //reg [11:0] 			adc0b;
   //reg [11:0] 			adc0c;
   reg [11:0] 			adc0d;
   reg [11:0] 			adc0;
   reg [11:0] 			adc1;
   reg [11:0] 			adc2;
   reg [11:0] 			adc3;
   //reg [11:0]                   adc4;
   //reg [11:0] 			adc5;
   //reg [11:0] 			adc6;
   //reg [11:0] 			adc7;
   reg [15:0] 			adc_sum;

   //wire [11:0] adc_baseline = adc_sum[15:3]; // divide by 8
   wire [11:0] adc_baseline = adc_sum[13:2]; // divide by 4
   wire [11:0] adc_value = adc_in - adc_baseline + 12'h800;

   assign trig_neg = (adc_value <= (12'h800 - threshold));
   assign trig_pos = (adc_value >= (12'h800 + threshold));

   always_ff@ (posedge clk) begin
      if (adc_strobe) begin
         adc_in <= adc_stream + 12'h800; // convert from signed to unsigned centered at 0x800
         
         if (reset) begin
            //adc0a <= adc_in;
            //adc0b <= adc_in;
            //adc0c <= adc_in;
            //adc0d <= adc_in;
            adc0d <= adc_in;
            
            adc0 <= adc_in;
            adc1 <= adc_in;
            adc2 <= adc_in;
            adc3 <= adc_in;
            //adc4 <= adc_in;
            //adc5 <= adc_in;
            //adc6 <= adc_in;
            //adc7 <= adc_in;
            
            adc_out <= adc_in;
            baseline_out <= adc_in;
            trig_out <= 0;
            
         end else begin

            //adc0a <= adc_in;
            //adc0b <= adc0a;
            //adc0c <= adc0b;
            //adc0d <= adc0c;
            adc0d <= adc_in;
            
            adc0 <= adc0d;
            adc1 <= adc0;
            adc2 <= adc1;
            adc3 <= adc2;
            //adc4 <= adc3;
            //adc5 <= adc4;
            //adc6 <= adc5;
            //adc7 <= adc6;
            
            //adc_sum <= {4'b0,adc0} + {4'b0,adc1} + {4'b0,adc2} + {4'b0,adc3} + {4'b0,adc4} + {4'b0,adc5} + {4'b0,adc6} + {4'b0,adc7};
            adc_sum <= {4'b0,adc0} + {4'b0,adc1} + {4'b0,adc2} + {4'b0,adc3};
            
            adc_out <= adc_value;
            baseline_out <= adc_baseline;
            trig_out <= trig_pos | trig_neg;
            
         end
      end
   end // always_ff@ (posedge clk)
   
endmodule // alphag
