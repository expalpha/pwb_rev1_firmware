module sca_to_ddr ( 
	clk,
	rst,
	
	// FIFO Signals
	fifo_ack,
	fifo_val,
	fifo_sop,
	fifo_eop,
	fifo_data,
	
	// DDR Signals
	avl_waitrequest_n,
	avl_address,
	avl_readdatavalid,
	avl_readdata,
	avl_writedata,
	avl_byteenable,
	avl_read,
	avl_write,
	avl_burstcount,

	transfer_cnt,
	ena,
	hold,
	busy,
	done,
	ddr_base_addr
);

localparam SZ_CNT = 32;

input wire clk;
input wire rst;
	
	// FIFO Signals
output reg 				fifo_ack;
input  wire 			fifo_val;
input  wire 			fifo_sop;
input  wire  			fifo_eop;
input  wire [3:0][13:0]	fifo_data;
	
	// DDR Signals
input  wire 			avl_waitrequest_n;
output reg  [29:0] 	avl_address; 
input  wire 			avl_readdatavalid;
input  wire [63:0] 	avl_readdata;
output reg  [63:0] 	avl_writedata;
output reg 	[7:0]		avl_byteenable;
output reg 				avl_read;
output reg 				avl_write;
output reg  [2:0]		avl_burstcount;
		
input  wire 			ena;
input  wire				hold;
output reg				busy;
output reg				done;
input  wire [29:0] 	ddr_base_addr;

output reg [31:0] transfer_cnt;

wire bus_ack = avl_waitrequest_n;
wire sync_trigger_accepted;

reg transfer_sca_to_ddr;
reg r_fifo_eop;

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		busy 				<= 1'b0;
		done 				<= 1'b0;
		fifo_ack			<= 1'b0;
		avl_address 	<= 29'h0;
		avl_writedata	<= 64'h0;
		avl_write		<= 1'b0;
		avl_read			<= 1'b0;
		avl_burstcount <= 3'h0; // don't use bursting for now			
		transfer_cnt	<= {SZ_CNT{1'b0}};
		transfer_sca_to_ddr	<= 1'b0;
		avl_byteenable <= 8'hFF;
		r_fifo_eop		<= 1'b0;
	end else begin 
		avl_byteenable <= 8'hFF;
		avl_burstcount <= 3'h0; // don't use bursting for now		
		if(transfer_sca_to_ddr) begin 	
			busy <= 1'b1;
			done <= 1'b0;
			if(avl_write) begin 
				transfer_sca_to_ddr <= (bus_ack && r_fifo_eop) ? 1'b0 : 1'b1;
				done 				<= (bus_ack && r_fifo_eop) ? 1'b1 : 1'b0;
				fifo_ack			<= 1'b0;								
				avl_address 	<= (bus_ack) ? avl_address + 4'h8 : avl_address;
				avl_write		<= (bus_ack) ? 1'b0: avl_write;
				avl_writedata 	<= avl_writedata;
				avl_read			<= 1'b0;			
				transfer_cnt	<= (bus_ack) ? transfer_cnt + 1'b1 : transfer_cnt;
				r_fifo_eop 		<= (bus_ack) ? 1'b0 : r_fifo_eop;
			end else begin 
				transfer_sca_to_ddr <= 1'b1;
				done 				<= 1'b0;
				avl_address 	<= avl_address;
				r_fifo_eop		<= (fifo_val) ? fifo_eop : r_fifo_eop;
				fifo_ack			<= (fifo_val) ? 1'b1 : 1'b0;
				avl_write		<= (fifo_val) ? 1'b1 : 1'b0;
				avl_writedata	<= (fifo_val) ? {{fifo_data[3][13:0], 2'h0},{fifo_data[2][13:0], 2'h0},{fifo_data[1][13:0], 2'h0},{fifo_data[0][13:0], 2'h0}} : avl_writedata;
				avl_read			<= 1'b0;
				transfer_cnt	<= transfer_cnt;
			end				
		end else if(ena && fifo_sop && fifo_val && !busy) begin 		
			r_fifo_eop 		<= 1'b0;
			fifo_ack			<= 1'b0;
			avl_address 	<= ddr_base_addr;
			avl_writedata	<= 64'h0;
			avl_write		<= 1'b0;
			avl_read			<= 1'b0;
			transfer_cnt	<= {SZ_CNT{1'b0}};				
			transfer_sca_to_ddr 	<= 1'b1;
			done <= 1'b0;
			busy <= 1'b1;
		end else begin 
			done				<= (hold) ? done : 1'b0;
			busy				<= (hold) ? done : 1'b0;
			r_fifo_eop		<= 1'b0;
			fifo_ack 		<= (fifo_val) ? 1'b1 : 1'b0; // flow through data since we aren't operational yet
			avl_address 	<= 29'h0;
			avl_writedata	<= 64'h0;
			avl_write		<= 1'b0;
			avl_read			<= 1'b0;	
			transfer_cnt	<= {SZ_CNT{1'b0}};
			transfer_sca_to_ddr 	<= 1'b0;	
		end
	end
end



endmodule
