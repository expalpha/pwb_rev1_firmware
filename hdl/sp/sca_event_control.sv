// Module takes in accepted triggers and manages creation of SCA event packets
// In the future, this module will interface to the DDR in order to perform a circular linked-list capture
// and store of the SCA data 
// manages the pipe between transmitting the data to storage and reading it back out
`default_nettype none
module sca_event_control
  (
   input wire clk,		// Clock from SCA Write
   input wire rst,		// Reset

   input wire event_clk, 	// DDR memory and UDP read clock (125MHz)
   input wire event_rst, 
	
   input wire [47:0] hw_id, 	// 48-bit MAC address for device

   input wire trig_in,		//	Trigger start
   input wire [SZ_TIMESTAMP-1:0] trig_ts, // Trigger timestamp
   input wire [SZ_DELAY-1:0] trig_delay,  // Delay associated with trigger/timestamp
   input wire [SZ_TRIG_SRC-1:0] trig_src, // Trigger Source

   input wire sca_ready,
   input wire [NUM_SCA-1:0] sca_ena,		// SCA Enable bits
   output wire [NUM_SCA-1:0] sca_used,  	// SCA Used bits, set on trigger accept
   input wire [SZ_DESC_COMPRESSION-1:0] sca_compression,
	
   input wire mem_ready, 	// Memory is ready for read/write 
   input wire [NUM_MEM_SLOTS-1:0][SZ_DESC_ADDR-1:0] mem_slot, // Memory addresses to read/write from
	
   // DDR Avalon bridge, used to transfer SCA data INTO DDR
   output reg   [7:0] sca_avl_burstcount,
   input  wire        sca_avl_waitrequest,
   output reg         sca_avl_beginbursttransfer,
   output reg [SZ_DESC_ADDR-1:0] sca_avl_address,
   output reg  [63:0] sca_avl_writedata,
   output reg   [7:0] sca_avl_byteenable,
   output reg         sca_avl_write,

   // DDR Avalon bridge, used to transfer SCA data OUT of DDR and INTO ethernet/SATA links
   output reg [SZ_DESC_ADDR-1:0] eth_avl_address,
   input  wire        eth_avl_waitrequest,
   output reg         eth_avl_beginbursttransfer,
   output reg   [7:0] eth_avl_burstcount,
   output reg  [15:0] eth_avl_byteenable,
   output reg         eth_avl_read,
   input wire         eth_avl_readdatavalid,
   input wire [127:0] eth_avl_readdata,
	
   output reg busy_out,	// Indicate we are currently processing an SCA trigger event/waiting for an available memory slot/other
   output reg event_desc_fifo_full,

   input wire [NUM_SCA-1:0] [NUM_SCA_DATA_CH-1:0] ch_enabled_const,
   input wire [NUM_SCA-1:0] [NUM_SCA_DATA_CH-1:0] ch_forced_const,
   input wire [NUM_SCA-1:0] [NUM_SCA_DATA_CH-1:0] ch_crossed,

   // UDP stream source, expects to go to something that can break up the packet
   output reg [NUM_SCA-1:0] event_val,
   input wire [NUM_SCA-1:0] event_rdy,
   output reg [NUM_SCA-1:0][31:0] event_dat,
   output reg [NUM_SCA-1:0][1:0]  event_epy,
   output reg [NUM_SCA-1:0] event_sop,
   output reg [NUM_SCA-1:0] event_eop,

   // SCA data stream sink, from ADC
   input wire [15:0] sca_fifo_max,
   input wire sca_err,		// Error reading SCA data
   input wire [NUM_SCA-1:0][SZ_SCA_DATA-1:0] sca_dat, // SCA Data
   output reg sca_ack,		// SCA Data ACK
   input wire sca_val,		// SCA Data Valid
   input wire sca_sop,		// SCA Start-Of-Packet
   input wire sca_eop,		// SCA End-Of-Packet  
   
   output reg sca_fifo_rst,

   output reg event_not_ready
);
  
parameter NUM_SCA = 4;
parameter NUM_SCA_DATA_CH = 79;
parameter SZ_SCA_DATA = 12;
parameter NUM_MEM_SLOTS = 4;
parameter SZ_MEM_ADDR = 29;

parameter SZ_TIMESTAMP = 48;
parameter NUM_TRIGGER_SOURCES = 4;
parameter SZ_DELAY = 16;


localparam EVT_PKT_REV = 8'h2;
localparam SZ_TRIG_SRC = (NUM_TRIGGER_SOURCES > 1) ? $clog2(NUM_TRIGGER_SOURCES) : 1;
localparam SZ_MEM_SLOTS = $clog2(NUM_MEM_SLOTS);
localparam SZ_DESC_ADDR = SZ_MEM_ADDR;
localparam SZ_DESC_SCA_ENA 	= 4;
localparam SZ_DESC_COMPRESSION = 8;
localparam SZ_DESC_NUM_SAMPLE = 9;
localparam SZ_DESC_TIME = 48;
localparam SZ_DESC_TRIG_DELAY = 16;
localparam SZ_DESC_TRIG_SRC = 8;
localparam SZ_DESC_EVENT_CNT = 32;
localparam SZ_DESC_FIFO_CNT = 24;
localparam SZ_DESC_LAST_CELL = 9;
localparam SZ_DESC_CHAN_SENT	= NUM_SCA_DATA_CH; // a channel will be sent if it's ENABLED + (FORCED or THRESHOLD_CROSSED)
localparam SZ_DESC_CHAN_CROSS	= NUM_SCA_DATA_CH;



localparam SZ_DESC = SZ_DESC_ADDR + 							// 32
							SZ_DESC_SCA_ENA + 						// 4
							SZ_DESC_COMPRESSION + 					// 8
							SZ_DESC_NUM_SAMPLE + 					// 9
							SZ_DESC_TIME + 							// 48
							SZ_DESC_TRIG_DELAY +						// 16
							SZ_DESC_TRIG_SRC +						// 8
							SZ_DESC_EVENT_CNT + 						// 32
							SZ_DESC_FIFO_CNT	+ 						// 24
							(SZ_DESC_LAST_CELL 	* NUM_SCA) + 	// 9 * 4
							(SZ_DESC_CHAN_SENT	* NUM_SCA) + 	// 79 * 4
							(SZ_DESC_CHAN_CROSS	* NUM_SCA);		// 79 * 4

wire rd_empty;
wire [SZ_DESC-1:0] wr_descriptor;
wire [SZ_DESC-1:0] rd_descriptor;

wire [SZ_DESC_ADDR-1:0]							rd_desc_mem_addr;
wire [SZ_DESC_SCA_ENA-1:0] 					rd_desc_sca_ena;
wire [SZ_DESC_COMPRESSION-1:0]				rd_desc_compression;
wire [SZ_DESC_NUM_SAMPLE-1:0] 				rd_desc_num_sample;
wire [SZ_DESC_TIME-1:0] 						rd_desc_timestamp;
wire [SZ_DESC_TRIG_DELAY-1:0] 				rd_desc_trig_delay;
wire [SZ_DESC_TRIG_SRC-1:0]					rd_desc_trig_src;
wire [SZ_DESC_EVENT_CNT-1:0]					rd_desc_event_counter;
wire [SZ_DESC_FIFO_CNT-1:0]					rd_desc_fifo_counters;
wire [NUM_SCA-1:0][SZ_DESC_LAST_CELL-1:0] rd_desc_last_cell;
wire [NUM_SCA-1:0][SZ_DESC_CHAN_SENT-1:0] rd_desc_ch_sent;
wire [NUM_SCA-1:0][SZ_DESC_CHAN_CROSS-1:0]rd_desc_ch_crossed;

wire [NUM_SCA-1:0][78:0] rd_channels_enabled;
wire [NUM_SCA-1:0][15:0] sca_dat_extended;
wire [SZ_DESC_NUM_SAMPLE+2:0] wr_desc_num_sample_offset = { wr_desc_num_sample, {3{1'b0}} };

wire event_all_rdy = &event_rdy;
wire [NUM_SCA-1:0][7:0] SCA_ID = { 8'h44, 8'h43, 8'h42, 8'h41 };
wire [5:0] event_desc_fifo_wrusedw;
wire [5:0] event_desc_fifo_rdusedw;

reg [6:0] rd_mem_curr_ch;
reg [6:0] rd_mem_next_ch;
reg [SZ_DESC_NUM_SAMPLE-1:0] rd_mem_curr_sample;
reg [6:0] wr_row_count;
reg [SZ_DESC_ADDR-1:0] offset_avl_address;	
reg [SZ_MEM_SLOTS-1:0] current_mem_slot;
reg wr_req;
reg rd_ack;

// lets breakout the descriptor sections for ease of use
reg [SZ_DESC_ADDR-1:0]							wr_desc_mem_addr;
reg [SZ_DESC_SCA_ENA-1:0] 						wr_desc_sca_ena;
reg [SZ_DESC_COMPRESSION-1:0]					wr_desc_compression;
reg [SZ_DESC_NUM_SAMPLE-1:0] 					wr_desc_num_sample;
reg [SZ_DESC_TIME-1:0] 							wr_desc_timestamp;
reg [SZ_DESC_TRIG_DELAY-1:0] 					wr_desc_trig_delay;
reg [SZ_DESC_TRIG_SRC-1:0]						wr_desc_trig_src;
reg [SZ_DESC_EVENT_CNT-1:0]					wr_desc_event_counter;
reg [SZ_DESC_FIFO_CNT-1:0]						wr_desc_fifo_counters;
reg [NUM_SCA-1:0][SZ_DESC_LAST_CELL-1:0] 	wr_desc_last_cell;
reg [NUM_SCA-1:0][SZ_DESC_CHAN_SENT-1:0] 	wr_desc_ch_sent;
reg [NUM_SCA-1:0][SZ_DESC_CHAN_CROSS-1:0] wr_desc_ch_crossed;

// Register the output of the FIFO to for timing improvement
reg [SZ_DESC-1:0] r_rd_descriptor;


// Assign sca descriptors to regs/wires as appropriate
assign wr_descriptor = {
	wr_desc_mem_addr,
	wr_desc_sca_ena,
	wr_desc_compression,
	wr_desc_num_sample,
	wr_desc_timestamp,
	wr_desc_trig_delay,
	wr_desc_trig_src,
	wr_desc_event_counter,
	wr_desc_fifo_counters,
	wr_desc_last_cell,
	wr_desc_ch_sent,
	wr_desc_ch_crossed
};

assign {
	rd_desc_mem_addr,
	rd_desc_sca_ena,
	rd_desc_compression,
	rd_desc_num_sample,
	rd_desc_timestamp,
	rd_desc_trig_delay,
	rd_desc_trig_src,
	rd_desc_event_counter,
	rd_desc_fifo_counters,
	rd_desc_last_cell,
	rd_desc_ch_sent,
	rd_desc_ch_crossed
} = r_rd_descriptor;

assign sca_used = wr_desc_sca_ena;

// Event descriptor FIFO, we build a descriptor, then write it in once the SCA data is successfully pushed to memory
// Once a descriptor is placed in the FIFO, we read it out and build packets around it to send out
// A "read-ahead" fifo required, so we can inspect descriptors before ACKing them
event_descriptor_fifo #(
	.WIDTH( SZ_DESC ),
	.DEPTH( NUM_MEM_SLOTS )
) event_fifo (
	.aclr		( event_rst ),
	.wrclk	( clk ),
	.wrreq	( wr_req ),
	.wrusedw	( event_desc_fifo_wrusedw ),
	.wrfull	( event_desc_fifo_full ),
	.data		( wr_descriptor ),
	.rdclk	( event_clk ),
	.rdreq	( rd_ack ),			// The megafunction name is 'rdreq' but it's acting like an ACK
	.rdusedw	( event_desc_fifo_rdusedw ),
	.rdempty	( rd_empty ),
	.q			( rd_descriptor )
);


enum int unsigned { 
	ST_WRITE_NOT_READY 			= 0, 
	ST_WRITE_READY_FOR_TRIGGER	= 2, 
	ST_WRITE_TO_MEM_START 		= 4, 
	ST_WRITE_TO_MEM_START_DELAY= 8,
	ST_WRITE_TO_MEM_DATA_FETCH	= 16,
	ST_WRITE_TO_MEM_DATA_STORE	= 32,
	ST_WRITE_DONE					= 64,
	ST_WRITE_END					= 128
} sca_wr_state;

integer i;

genvar n;
generate
for(n=0; n<NUM_SCA; n+=1) begin: gen_sign_ext
	sign_extender #(
		.SZ_IN( SZ_SCA_DATA ),
		.SZ_OUT( 16 )
	) sca_dat_sign_extend (
		.in	( sca_dat[n] ),
		.out	( sca_dat_extended[n] )
	);
	
end  
endgenerate

   reg [NUM_SCA-1:0] [NUM_SCA_DATA_CH-1:0] ch_enabled;
   reg [NUM_SCA-1:0] [NUM_SCA_DATA_CH-1:0] ch_forced;
                     
   always @(posedge clk) begin
      ch_enabled <= ch_enabled_const;
      ch_forced  <= ch_forced_const;
   end

// Main Write State Machine
// Defers to sca->memory state machine once we are in ST_WRITE_TO_MEM state
always@(posedge rst, posedge clk) begin
	if(rst) begin
		sca_ack					<= 1'b0;
		sca_fifo_rst 			<= 1'b1;
		sca_wr_state 			<= ST_WRITE_NOT_READY;    
		wr_req 					<= 1'b0;
		busy_out 				<= 1'b1;
		current_mem_slot 		<= {SZ_MEM_SLOTS{1'b0}};
		
		sca_avl_byteenable 	<= 8'hff;
		sca_avl_burstcount 	<= 8'h1;
		sca_avl_beginbursttransfer <= 1'b0;
		sca_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
		sca_avl_writedata 	<= {64{1'b0}};
		sca_avl_byteenable 	<= 8'hff;
		sca_avl_write 			<= 1'b0;

		wr_desc_timestamp 	<= {SZ_DESC_TIME{1'b0}};
		wr_desc_sca_ena		<= {SZ_DESC_SCA_ENA{1'b0}};
		wr_desc_trig_delay	<= {SZ_DESC_TRIG_DELAY{1'b0}};
		wr_desc_compression	<= {SZ_DESC_COMPRESSION{1'b0}};
		wr_desc_trig_src		<= {SZ_DESC_TRIG_SRC{1'b0}};
		wr_desc_mem_addr		<= {SZ_DESC_ADDR{1'b0}};
		wr_desc_num_sample 	<= {SZ_DESC_NUM_SAMPLE{1'b0}};
		wr_desc_event_counter<= {SZ_DESC_EVENT_CNT{1'b0}};
		wr_desc_fifo_counters<= {SZ_DESC_FIFO_CNT{1'b0}};

	
		for(int i=0; i<NUM_SCA; i+=1) begin
			wr_desc_last_cell[i]		<= 9'h0;
			wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
			wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}};
		end
		
		wr_row_count 			<= 7'h0;
		offset_avl_address 	<= {SZ_DESC_ADDR{1'b0}};
	end else begin
		
		sca_avl_burstcount <= 8'h1;
		sca_avl_beginbursttransfer <= 1'b0;
		sca_avl_byteenable 	<= 8'hff;
		
		
		case(sca_wr_state)
			// Initial state, waiting for SCA to fill
			ST_WRITE_NOT_READY: begin
				// Is the SCA done filling and do we have a slot available to write the SCA data to?
				if(sca_ready && !event_desc_fifo_full && mem_ready) begin
					sca_wr_state <= ST_WRITE_READY_FOR_TRIGGER;         
					busy_out 	 <= 1'b0;
				end else begin
					sca_wr_state <= ST_WRITE_NOT_READY;
					busy_out 	 <= 1'b1;
				end

				sca_ack					<= 1'b0;
				sca_fifo_rst 			<= 1'b1;
				wr_req 					<= 1'b0;
				current_mem_slot 		<= current_mem_slot;
				
				sca_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
				sca_avl_writedata 	<= {64{1'b0}};				
				sca_avl_write 			<= 1'b0;
				
				wr_desc_timestamp 	<= {SZ_DESC_TIME{1'b0}};
				wr_desc_sca_ena		<= {SZ_DESC_SCA_ENA{1'b0}};
				wr_desc_trig_delay	<= {SZ_DESC_TRIG_DELAY{1'b0}};
				wr_desc_compression	<= {SZ_DESC_COMPRESSION{1'b0}};
				wr_desc_trig_src		<= {SZ_DESC_TRIG_SRC{1'b0}};
				wr_desc_mem_addr		<= {SZ_DESC_ADDR{1'b0}};
				wr_desc_num_sample 	<= {SZ_DESC_NUM_SAMPLE{1'b0}};
				wr_desc_fifo_counters<= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;
				

				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
					wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}};
				end	
				
				wr_row_count 		<= 7'h0;
				offset_avl_address <= sca_avl_address + 7'h8;				
			end          
			
			// waiting for a trigger
			ST_WRITE_READY_FOR_TRIGGER: begin          
				if(trig_in) begin
					sca_wr_state 	<= ST_WRITE_TO_MEM_START;
					busy_out 		<= 1'b1;
				end else begin
					sca_wr_state 	<= ST_WRITE_READY_FOR_TRIGGER;
					busy_out 		<= 1'b0;
				end

				sca_ack				<= 1'b0;
				sca_fifo_rst 		<= 1'b0;	
				wr_req 				<= 1'b0;			
				current_mem_slot 	<= current_mem_slot;
				
				sca_avl_address 		<= mem_slot[current_mem_slot];
				sca_avl_writedata 	<= {64{1'b0}};
				sca_avl_write 			<= 1'b0;				
				
				// continually capture descriptor info while we wait for a trigger
				wr_desc_timestamp 	<= trig_ts;
				wr_desc_sca_ena		<= sca_ena;
				wr_desc_trig_delay	<= trig_delay;
				wr_desc_trig_src		<= trig_src;
				wr_desc_compression	<= sca_compression;
				wr_desc_mem_addr		<= mem_slot[current_mem_slot];
				wr_desc_num_sample 	<= {SZ_DESC_NUM_SAMPLE{1'b0}};
				wr_desc_fifo_counters<= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;				
				
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
					wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}}; // clear this here, and then set bits as thresholds are crossed during ST_WRITE_TO_MEM				
				end

				wr_row_count 		<= 7'h0;
				offset_avl_address <= sca_avl_address + 7'h8;	
			end

			// Stream SCA data to memory
			// thresholding must occur here
			// once we're done, we will create a descriptor and put it in the FIFO
			ST_WRITE_TO_MEM_START: begin         
				sca_wr_state 		<= (sca_err) ? ST_WRITE_END : (sca_sop && sca_val) ? ST_WRITE_TO_MEM_START_DELAY : ST_WRITE_TO_MEM_START;
				busy_out 			<= 1'b1;		
				sca_ack				<= (sca_sop && sca_val) ? 1'b1 : 1'b0;
				wr_desc_event_counter <= (sca_sop && sca_val) ? wr_desc_event_counter + 1'b1 : wr_desc_event_counter;
				sca_fifo_rst 		<= 1'b0;
				wr_req 				<= 1'b0;
				current_mem_slot 	<= current_mem_slot;
									
				sca_avl_address 		<= sca_avl_address;
				sca_avl_writedata 	<= {64{1'b0}};
				sca_avl_write 			<= 1'b0;	
				
				// These parts of the descriptor don't change from the start
				wr_desc_timestamp 	<= wr_desc_timestamp;
				wr_desc_sca_ena		<= wr_desc_sca_ena;				
				wr_desc_trig_src		<= wr_desc_trig_src;
				wr_desc_trig_delay	<= wr_desc_trig_delay;
				wr_desc_compression	<= wr_desc_compression;
				wr_desc_mem_addr		<= wr_desc_mem_addr; 
				wr_desc_num_sample 	<= sca_dat[0][8:0]; 	// grab num samples from first word of packet
				wr_desc_fifo_counters<= wr_desc_fifo_counters;

				// Updated at least once during the write...
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
					wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}};
				end
				
				wr_row_count 		<= 7'h0;
				offset_avl_address <= sca_avl_address + 7'h8;			
			end
				
			// let the SOP data clear...
			ST_WRITE_TO_MEM_START_DELAY: begin        
				sca_wr_state 		<= (sca_err) ? ST_WRITE_END : ST_WRITE_TO_MEM_DATA_FETCH;
				busy_out 			<= 1'b1;		
				sca_ack				<= 1'b0;
				sca_fifo_rst 		<= 1'b0;
				wr_req 				<= 1'b0;
				current_mem_slot 	<= current_mem_slot;
									
				sca_avl_address 		<= sca_avl_address;
				sca_avl_writedata 	<= {64{1'b0}};
				sca_avl_write 			<= 1'b0;	
				
				// These parts of the descriptor don't change from the start
				wr_desc_timestamp 	<= wr_desc_timestamp;
				wr_desc_sca_ena		<= wr_desc_sca_ena;				
				wr_desc_trig_src		<= wr_desc_trig_src;
				wr_desc_trig_delay	<= wr_desc_trig_delay;
				wr_desc_compression	<= wr_desc_compression;
				wr_desc_mem_addr		<= wr_desc_mem_addr; 
				wr_desc_num_sample 	<= wr_desc_num_sample;
				wr_desc_fifo_counters<= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;
				
				// Updated at least once during the write...
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
					wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}};
				end
							
				wr_row_count 		<= 7'h0;
				offset_avl_address <= sca_avl_address + 7'h8;		
			end
			
			ST_WRITE_TO_MEM_DATA_FETCH: begin
				
				sca_wr_state 		<= (sca_err) ? ST_WRITE_END : (sca_val) ? (sca_eop) ? ST_WRITE_DONE : ST_WRITE_TO_MEM_DATA_STORE : ST_WRITE_TO_MEM_DATA_FETCH;
				busy_out 			<= 1'b1;
				sca_ack				<= (sca_val) ? 1'b1 : 1'b0;					
				sca_fifo_rst 		<= 1'b0;	
				wr_req 				<= 1'b0;
				current_mem_slot 	<= current_mem_slot;
				
				sca_avl_address 		<= sca_avl_address;
				sca_avl_writedata 	<= { sca_dat_extended[3], sca_dat_extended[2], sca_dat_extended[1], sca_dat_extended[0] };
				sca_avl_write 			<= (sca_val) ? (sca_eop) ? 1'b0 : 1'b1 : 1'b0;
				
				// This will get left alone during the write					
				wr_desc_timestamp 	<= wr_desc_timestamp;
				wr_desc_sca_ena		<= wr_desc_sca_ena;
				wr_desc_trig_delay	<= wr_desc_trig_delay;
				wr_desc_trig_src		<= wr_desc_trig_src;
				wr_desc_compression	<= wr_desc_compression;
				wr_desc_mem_addr		<= wr_desc_mem_addr;
				wr_desc_num_sample 	<= wr_desc_num_sample;
				wr_desc_fifo_counters<= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;

				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= (sca_eop) ? sca_dat[i][8:0] : 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{wr_desc_sca_ena[i]}} & ch_enabled[i] & (ch_crossed[i] | ch_forced[i]);
					wr_desc_ch_crossed[i]	<= ch_crossed[i];
				end
				wr_row_count 			<= wr_row_count;
				offset_avl_address 	<= offset_avl_address;				
			end			

			ST_WRITE_TO_MEM_DATA_STORE: begin
				
				busy_out 			<= 1'b1;
				sca_ack				<= 1'b0;					
				sca_fifo_rst 		<= 1'b0;	
				wr_req 				<= 1'b0;			
				current_mem_slot 	<= current_mem_slot;
				
				if(!sca_avl_waitrequest) begin
					if(wr_row_count < 7'd78) begin
						// still in row, just keep incrementing
						sca_wr_state 		<= (sca_err) ? ST_WRITE_END : ST_WRITE_TO_MEM_DATA_FETCH;
						wr_row_count 		<= wr_row_count + 1'b1;
						sca_avl_address 	<= sca_avl_address + wr_desc_num_sample_offset;
						offset_avl_address <= offset_avl_address;
					end else begin
						// reset to start next row, + row offset
						sca_wr_state 		<= (sca_err) ? ST_WRITE_END : ST_WRITE_TO_MEM_DATA_FETCH;
						wr_row_count 		<= 7'h0;
						sca_avl_address 	<= offset_avl_address;
						offset_avl_address <= offset_avl_address + 7'h8;
					end
				end else begin
					sca_wr_state 		<= (sca_err) ? ST_WRITE_END : ST_WRITE_TO_MEM_DATA_STORE;
					wr_row_count		<= wr_row_count;
					sca_avl_address	<= sca_avl_address;
					offset_avl_address <= offset_avl_address;
				end
				
				sca_avl_writedata 	<= sca_avl_writedata;
				sca_avl_byteenable 	<= 8'hff;
				sca_avl_write 			<= (!sca_avl_waitrequest) ? 1'b0 : 1'b1;			
				
				// This will get left alone during the write
				
				wr_desc_timestamp 	<= wr_desc_timestamp;
				wr_desc_sca_ena		<= wr_desc_sca_ena;
				wr_desc_trig_delay	<= wr_desc_trig_delay;
				wr_desc_trig_src		<= wr_desc_trig_src;
				wr_desc_compression	<= wr_desc_compression;
				wr_desc_mem_addr		<= wr_desc_mem_addr;
				wr_desc_num_sample 	<= wr_desc_num_sample;
				wr_desc_fifo_counters<= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;
				
				// clear this here, and then set bits as thresholds are crossed during ST_WRITE_TO_MEM				
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= wr_desc_last_cell[i];
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{wr_desc_sca_ena[i]}} & ch_enabled[i] & (ch_crossed[i] | ch_forced[i]);
					wr_desc_ch_crossed[i]	<= ch_crossed[i];
				end
			end			
			
			ST_WRITE_DONE: begin
				
				sca_wr_state 		<= ST_WRITE_END;
				busy_out 			<= 1'b1;
				sca_ack				<= 1'b0;
				sca_fifo_rst 		<= 1'b0;	
				wr_req 				<= 1'b1;
				current_mem_slot 	<= current_mem_slot + 1'b1;
				
				sca_avl_address 		<= sca_avl_address;
				sca_avl_writedata 	<= 64'h0;
				sca_avl_byteenable 	<= 8'hff;
				sca_avl_write 			<= 1'b0;
				
				// This will get left alone during the write					
				wr_desc_timestamp 	<= wr_desc_timestamp;
				wr_desc_sca_ena		<= wr_desc_sca_ena;
				wr_desc_trig_delay	<= wr_desc_trig_delay;
				wr_desc_trig_src		<= wr_desc_trig_src;
				wr_desc_compression	<= wr_desc_compression;
				wr_desc_mem_addr		<= wr_desc_mem_addr;
				wr_desc_num_sample 	<= wr_desc_num_sample;
				wr_desc_event_counter <= wr_desc_event_counter;
				wr_desc_fifo_counters <= { sca_fifo_max, {2'b00, event_desc_fifo_wrusedw } };
				
				// clear this here, and then set bits as thresholds are crossed during ST_WRITE_TO_MEM				
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= wr_desc_last_cell[i];
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{wr_desc_sca_ena[i]}} & ch_enabled[i] & (ch_crossed[i] | ch_forced[i]);
					wr_desc_ch_crossed[i]	<= ch_crossed[i];
				end
				
				wr_row_count 		<= 7'h0;
				offset_avl_address <= {SZ_DESC_ADDR{1'b0}};
			end			

			
			// Give the FIFO a chance to update the wrfull status after the wr_req
			ST_WRITE_END: begin
				sca_ack	<= 1'b0;				
				current_mem_slot <= current_mem_slot;
				busy_out 		<= 1'b1;
				sca_wr_state 	<= ST_WRITE_NOT_READY;
				wr_req 			<= 1'b0;
				sca_fifo_rst 	<= 1'b1;
				
				sca_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
				sca_avl_writedata 	<= 64'h0;
				sca_avl_byteenable 	<= 8'hff;
				sca_avl_write 			<= 1'b0;
				
				wr_desc_timestamp 	<= {SZ_DESC_TIME{1'b0}};
				wr_desc_sca_ena		<= 4'h0;
				wr_desc_trig_delay	<= {SZ_DESC_TRIG_DELAY{1'b0}};
				wr_desc_compression	<= 4'h0;
				wr_desc_trig_src		<= {SZ_DESC_TRIG_SRC{1'b0}};
				wr_desc_mem_addr		<= {SZ_DESC_ADDR{1'b0}};
				wr_desc_num_sample 	<= 9'h0;
				wr_desc_fifo_counters <= wr_desc_fifo_counters;
				wr_desc_event_counter <= wr_desc_event_counter;
				
					
				for(int i=0; i<NUM_SCA; i+=1) begin
					wr_desc_last_cell[i]		<= 9'h0;
					wr_desc_ch_sent[i]		<= {NUM_SCA_DATA_CH{1'b0}};
					wr_desc_ch_crossed[i]	<= {NUM_SCA_DATA_CH{1'b0}};
				end				
			end

		endcase
	end
end


// Read memory back based on descriptor
// SCAs that haven't been enabled should be skipped
// SCA channels that don't have crossings or are disabled should be skipped
// 

// For each SCA, run through the descriptor and start creating the SCA data packet. The packet is automatically broken up
// outside of this function, when it is sent to the message chunker (which then passes the chunks to the UDP offloader)
// because it is broken up elsewhere, the read descriptor write starts the packet, but the read memory finishes it.

enum int unsigned {
	ST_RD_DESC_IDLE 			= 0, 
	ST_RD_DESC_WAIT_FOR_FIFO = 1, 
	ST_RD_DESC_FIFO_CAPTURE = 2,
	ST_RD_DESC_H0 				= 4, 
	ST_RD_DESC_H1 				= 8, 
	ST_RD_DESC_H2 				= 16,
	ST_RD_DESC_H3 				= 32,
	ST_RD_DESC_H4 				= 64,
	ST_RD_DESC_H5 				= 128,
	ST_RD_DESC_H6 				= 256,
	ST_RD_DESC_H7 				= 512,
	ST_RD_DESC_H8 				= 1024,
	ST_RD_DESC_H9 				= 2048,
	ST_RD_DESC_H10 			= 4096,
	ST_RD_DESC_H11 			= 8192,
	ST_RD_DESC_H12 			= 16384,
	ST_RD_MEM_CHANNEL_CHECK = 32768,
	ST_RD_MEM_DATA_START_HI = 65536,
	ST_RD_MEM_DATA_FETCH_HI = 131072,
	ST_RD_MEM_DATA_FETCH_LO = 262144,
	ST_RD_MEM_DATA_LO			= 524288,
	ST_RD_MEM_DATA_HI			= 1048576,
	ST_RD_MEM_FOOTER			= 2097152,
	ST_RD_MEM_ACK				= 4194304,
	ST_RD_MEM_DONE 			= 8388608
} rd_desc_state;

// If none of the SCAs sent a given channel, it can be skipped
reg [NUM_SCA_DATA_CH-1:0] channel_can_be_skipped;
generate
for(n=0; n<NUM_SCA_DATA_CH; n+=1) begin : gen_ch_skip_test
	always@(posedge event_rst, posedge event_clk) begin
		if(event_rst) begin
			channel_can_be_skipped[n] <= 1'b0;
		end else begin
			channel_can_be_skipped[n] <= ~(rd_desc_ch_sent[0][n] | rd_desc_ch_sent[1][n] | rd_desc_ch_sent[2][n] | rd_desc_ch_sent[3][n]);
		end
	end
end
endgenerate
   
   always@(posedge event_rst, posedge event_clk) begin
      if(event_rst) begin
         rd_desc_state <= ST_RD_DESC_IDLE;
         
         rd_ack 					<= 1'b0;
         rd_mem_curr_ch 		<= 7'h0;
         rd_mem_next_ch			<= 7'h1;
         rd_mem_curr_sample	<= 9'h1;
         
	 eth_avl_burstcount	<= 8'h1;
	 eth_avl_beginbursttransfer <= 1'b0;
	 
	 eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	 eth_avl_byteenable 	<= 16'hffff;
	 eth_avl_read  			<= 1'b0;			
	 
	 for(i=0; i<NUM_SCA; i+=1) begin
	    event_val[i] <= 1'b0;
	    event_dat[i] <= 32'h0;
	    event_epy[i] <= 2'h0;
	    event_sop[i] <= 1'b0;
	    event_eop[i] <= 1'b0;
	 end

         event_not_ready <= 0;
	 
      end else begin		
	 eth_avl_byteenable 	<= 16'hffff;
	 eth_avl_burstcount	<= 8'h1;
	 eth_avl_beginbursttransfer <= 1'b0;		
	 case(rd_desc_state)
	   ST_RD_DESC_IDLE: begin
	      // Wait until sca_rd_state is set to read out the descriptor and event consumers (packet chunkers) are ready
	      rd_desc_state <= (!rd_empty & event_all_rdy) ? ST_RD_DESC_WAIT_FOR_FIFO : ST_RD_DESC_IDLE;
	      
	      rd_ack 					<= (!rd_empty & event_all_rdy) ? 1'b1 : 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= 1'b0;
		 event_dat[i] <= 32'h0;
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;
	      end

              event_not_ready <= 0;
	      
	      r_rd_descriptor <= r_rd_descriptor;
	   end
           
	   ST_RD_DESC_WAIT_FOR_FIFO: begin
	      // Wait until sca_rd_state is set to read out the descriptor and event consumers (packet chunkers) are ready
	      rd_desc_state <= ST_RD_DESC_FIFO_CAPTURE;
	      
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= 1'b0;
		 event_dat[i] <= 32'h0;
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;
	      end			
	      
              event_not_ready <= 0;

	      r_rd_descriptor <= r_rd_descriptor;
	   end
	   
	   ST_RD_DESC_FIFO_CAPTURE: begin
	      // Wait until sca_rd_state is set to read out the descriptor and event consumers (packet chunkers) are ready
	      rd_desc_state <= ST_RD_DESC_H0;
				
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= 1'b0;
		 event_dat[i] <= 32'h0;
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;
	      end				
	      
              event_not_ready <= 0;

	      r_rd_descriptor <= rd_descriptor;
	   end
	   
	   ST_RD_DESC_H0: begin
	      // 8 Packet Version
	      // 8 SCA_ID 
	      // 8 compression_type
	      // 8 trigger_source
	      rd_desc_state <= ST_RD_DESC_H1;
              
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
              
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= rd_desc_sca_ena[i];
		 event_dat[i] <= { EVT_PKT_REV, SCA_ID[i], rd_desc_compression, rd_desc_trig_src };
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b1;
		 event_eop[i] <= 1'b0;					
	      end

              event_not_ready <= !event_all_rdy;
	      
	      r_rd_descriptor <= r_rd_descriptor;
	   end
	   
	   ST_RD_DESC_H1: begin				
	      // 32 HW_ID (0,1,2,3)
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H2 : ST_RD_DESC_H1;
	      
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      if(event_all_rdy) begin									
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];
		    event_dat[i] <= { hw_id[7:0], hw_id[15:8], hw_id[23:16], hw_id[31:24] };
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;
		    event_epy[i] <= 2'h0;
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_sop[i] <= 1'b1;
		    event_eop[i] <= 1'b0;
		    event_epy[i] <= 2'h0;
		 end
	      end	
	      
              event_not_ready <= !event_all_rdy;

	      r_rd_descriptor <= r_rd_descriptor;	
	   end
	   
	   ST_RD_DESC_H2: begin
	      // 16 HW_ID (4,5)
	      // 16 trigger delay
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H3 : ST_RD_DESC_H2;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      if(event_all_rdy) begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { hw_id[39:32], hw_id[47:40], rd_desc_trig_delay[7:0],rd_desc_trig_delay[15:8] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end	

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H3: begin
	      // 32 // timestamp 0,1,2,3 
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H4 : ST_RD_DESC_H3;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;			
	      
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_timestamp[7:0], rd_desc_timestamp[15:8], rd_desc_timestamp[23:16], rd_desc_timestamp[31:24] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end				

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H4: begin
	      // 32 // timestamp 4,5, reserved, reserved
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H5 : ST_RD_DESC_H4;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;	
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
	      
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_timestamp[39:32], rd_desc_timestamp[47:40], 16'h0 };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H5: begin
	      // 16 last_cell
	      // 16 num sample
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H6 : ST_RD_DESC_H5;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_last_cell[i][7:0],  7'h0, rd_desc_last_cell[i][8], rd_desc_num_sample[7:0], 7'h0, rd_desc_num_sample[8] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H6: begin
	      // 32 ch_sent
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H7 : ST_RD_DESC_H6;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_ch_sent[i][7:0], rd_desc_ch_sent[i][15:8], rd_desc_ch_sent[i][23:16], rd_desc_ch_sent[i][31:24]  };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end		

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H7: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H8 : ST_RD_DESC_H7;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_ch_sent[i][39:32], rd_desc_ch_sent[i][47:40], rd_desc_ch_sent[i][55:48], rd_desc_ch_sent[i][63:56] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end				

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_DESC_H8: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H9 : ST_RD_DESC_H8;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_ch_sent[i][71:64], 1'b0, rd_desc_ch_sent[i][78:72], rd_desc_ch_crossed[i][7:0], rd_desc_ch_crossed[i][15:8] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end									

              event_not_ready <= !event_all_rdy;
	   end		
	   
	   ST_RD_DESC_H9: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H10 : ST_RD_DESC_H9;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= {SZ_DESC_ADDR{1'b0}};
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_ch_crossed[i][23:16], rd_desc_ch_crossed[i][31:24] , rd_desc_ch_crossed[i][39:32], rd_desc_ch_crossed[i][47:40] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end	

              event_not_ready <= !event_all_rdy;
	   end	
	   
	   ST_RD_DESC_H10: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H11 : ST_RD_DESC_H10;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= rd_desc_mem_addr;
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_ch_crossed[i][55:48], rd_desc_ch_crossed[i][63:56], rd_desc_ch_crossed[i][71:64], 1'b0, rd_desc_ch_crossed[i][78:72] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end

              event_not_ready <= !event_all_rdy;
	   end			
	   
	   ST_RD_DESC_H11: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_DESC_H12 : ST_RD_DESC_H11;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= rd_desc_mem_addr;
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_event_counter[7:0], rd_desc_event_counter[15:8], rd_desc_event_counter[23:16], rd_desc_event_counter[31:24] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end

              event_not_ready <= !event_all_rdy;
	   end		
	   
	   ST_RD_DESC_H12: begin
	      rd_desc_state <= (event_all_rdy) ? ST_RD_MEM_CHANNEL_CHECK : ST_RD_DESC_H12;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= 7'h0;
	      rd_mem_next_ch			<= 7'h1;
	      rd_mem_curr_sample	<= 9'h1;
	      
	      eth_avl_address 		<= rd_desc_mem_addr;
	      eth_avl_read  			<= 1'b0;		
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];	
		    event_dat[i] <= { rd_desc_fifo_counters[15:8], rd_desc_fifo_counters[23:16], rd_desc_fifo_counters[7:0], {2'b00, event_desc_fifo_rdusedw} };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end else begin					
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0: event_val[i];
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end
	      end

              event_not_ready <= !event_all_rdy;
	   end					
	   
	   ST_RD_MEM_CHANNEL_CHECK: begin				
	      rd_desc_state 			<= (event_all_rdy) ? channel_can_be_skipped[rd_mem_curr_ch] ? (rd_mem_next_ch < NUM_SCA_DATA_CH) ?  ST_RD_MEM_CHANNEL_CHECK : ST_RD_MEM_FOOTER : ST_RD_MEM_DATA_START_HI : ST_RD_MEM_CHANNEL_CHECK;
	      r_rd_descriptor 		<= r_rd_descriptor;
	      rd_ack 					<= 1'b0;
	      rd_mem_curr_ch 		<= (event_all_rdy & channel_can_be_skipped[rd_mem_curr_ch]) ? rd_mem_curr_ch + 1'b1 : rd_mem_curr_ch;
	      rd_mem_next_ch			<= (event_all_rdy & channel_can_be_skipped[rd_mem_curr_ch]) ? rd_mem_next_ch + 1'b1 : rd_mem_next_ch;
	      rd_mem_curr_sample 	<= 9'h1;
	      
	      eth_avl_address 		<= (event_all_rdy & channel_can_be_skipped[rd_mem_curr_ch]) ? eth_avl_address + {rd_desc_num_sample, 3'h0}: eth_avl_address;
	      eth_avl_read  			<= 1'b0;
	      
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= 1'b0;
		    event_dat[i] <= 32'h0;
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end				
	      end else begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end				
	      end			

              event_not_ready <= !event_all_rdy;
	   end
	   
	   // each channel starts off by noting the channel # and number of samples transferred for this channel
	   // we will repeat this start 79x per transmission
	   
	   // @TODO: If none of the SCA's crossed threshold for this channel, skip it entirely
	   // @TODO: Improve timing and handle 'end of channels' case
	   ST_RD_MEM_DATA_START_HI: begin
	      rd_desc_state      <= (event_all_rdy) ? ST_RD_MEM_DATA_FETCH_HI : ST_RD_MEM_DATA_START_HI;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= 9'h1;
	      
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= 1'b0;
	      
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_ch_sent[i][rd_mem_curr_ch];
		    // next_ch is placed in the event_dat because a request was made to order the channels 1-79 instead of 0-78
		    event_dat[i] <= { 1'b0, rd_mem_next_ch[6:0], 8'h0, rd_desc_num_sample[7:0], 7'h0, rd_desc_num_sample[8] };
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end				
	      end else begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end				
	      end			

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_MEM_DATA_FETCH_HI: begin				
	      // start fetch once we've written the initial channel #
	      rd_desc_state      <= (event_all_rdy) ? ST_RD_MEM_DATA_HI : ST_RD_MEM_DATA_FETCH_HI;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;				
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= rd_mem_curr_sample;
              
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= (event_all_rdy) ? 1'b1 : 1'b0;
              
	      // write previously written event in (if it wasn't ready y
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		 event_dat[i] <= event_dat[i];
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;					
	      end				

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_MEM_DATA_HI: begin
	      // Wait for DDR ack, then store data in event_dat register and go to retrieve high word (if necessary)
	      // if channel is done, go to next channel, or if none left, go to DONE
	      r_rd_descriptor <= r_rd_descriptor;
	      
	      if(eth_avl_readdatavalid) begin
		 rd_desc_state      <= (rd_mem_curr_sample < rd_desc_num_sample) ? ST_RD_MEM_DATA_FETCH_LO : (rd_mem_next_ch < NUM_SCA_DATA_CH) ? ST_RD_MEM_CHANNEL_CHECK : ST_RD_MEM_FOOTER;	
		 rd_mem_curr_ch     <= (rd_mem_curr_sample < rd_desc_num_sample) ? rd_mem_curr_ch : rd_mem_curr_ch + 1'b1;							
		 rd_mem_next_ch     <= (rd_mem_curr_sample < rd_desc_num_sample) ? rd_mem_next_ch : rd_mem_next_ch + 1'b1;
		 rd_mem_curr_sample <= rd_mem_curr_sample + 1'b1;											
		 eth_avl_address    <= eth_avl_address + 29'h8;					
	      end else begin
		 rd_desc_state      <= ST_RD_MEM_DATA_HI;
		 rd_mem_curr_ch     <= rd_mem_curr_ch;
		 rd_mem_next_ch     <= rd_mem_next_ch;
		 rd_mem_curr_sample <= rd_mem_curr_sample;
		 eth_avl_address    <= eth_avl_address;
	      end
	      
	      rd_ack         <= 1'b0;				
	      eth_avl_read   <= (!eth_avl_waitrequest) ? 1'b0 : 1'b1; // hold read until ACK
	      
	      for(i=0; i<NUM_SCA; i+=1) begin
		 // Only write the 'hi' sample if we have no more samples left (and this channel is set to be written)
		 event_val[i] <= (eth_avl_readdatavalid) ? (rd_mem_curr_sample < rd_desc_num_sample) ? 1'b0 : rd_desc_ch_sent[i][rd_mem_curr_ch] : 1'b0;
		 event_dat[i] <= { eth_avl_readdata[(i*16) +: 8],
                                   eth_avl_readdata[(i*16)+8 +: 8],
                                   eth_avl_readdata[64+(i*16) +: 8],
                                   eth_avl_readdata[64+(i*16)+8 +: 8],
                                   };
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;					
	      end				

              //event_not_ready <= !event_all_rdy;
              event_not_ready <= 0;
	   end			
	   
	   ST_RD_MEM_DATA_FETCH_LO: begin
              // Runt state left over after switching from 64-bit to 128-bit DDR read bus.
              //
	      // start fetch once we've written the initial channel #
              
	      rd_desc_state      <= (event_all_rdy) ? ST_RD_MEM_DATA_LO : ST_RD_MEM_DATA_FETCH_LO;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;	
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= rd_mem_curr_sample;
              
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= 1'b0;
	      
	      // Stop writing into the event buffer for a channel once the channel event has gone ready
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		 event_dat[i] <= event_dat[i];
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;					
	      end

              //event_not_ready <= !event_all_rdy;
              event_not_ready <= 0;
	   end
		  
	   ST_RD_MEM_DATA_LO: begin
              // Runt state left over after switching from 64-bit to 128-bit DDR read bus.
              //
	      // Wait for DDR ack, then store data in event_dat register and go to retrieve
              // high word (if necessary)
	      // if channel is done, go to next channel, or if none left, go to DONE
              //
              
	      r_rd_descriptor <= r_rd_descriptor;
	      
	      rd_desc_state   <= (rd_mem_curr_sample < rd_desc_num_sample) ? ST_RD_MEM_DATA_FETCH_HI : (rd_mem_next_ch < NUM_SCA_DATA_CH) ? ST_RD_MEM_CHANNEL_CHECK : ST_RD_MEM_FOOTER;						
	      rd_mem_curr_sample <= rd_mem_curr_sample + 1'b1;
	      rd_mem_curr_ch <= (rd_mem_curr_sample < rd_desc_num_sample) ? rd_mem_curr_ch : rd_mem_curr_ch + 1'b1;
	      rd_mem_next_ch <= (rd_mem_curr_sample < rd_desc_num_sample) ? rd_mem_next_ch : rd_mem_next_ch + 1'b1;					
	      eth_avl_address <= eth_avl_address + 29'h8;					
	      
	      rd_ack          <= 1'b0;
	      eth_avl_read    <= 1'b0;
	      
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= rd_desc_ch_sent[i][rd_mem_curr_ch];
		 event_dat[i] <= event_dat[i];
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;					
	      end				

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_MEM_FOOTER: begin
	      rd_desc_state      <= (event_all_rdy) ? ST_RD_MEM_ACK : ST_RD_MEM_FOOTER;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= rd_mem_curr_sample;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
              
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= 1'b0;
              
	      if(event_all_rdy) begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= rd_desc_sca_ena[i];
		    event_dat[i] <= 32'hCCCCCCCC;
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b1;
		 end				
	      end else begin
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;					
		 end				
	      end						

              event_not_ready <= !event_all_rdy;
	   end
	   
	   ST_RD_MEM_ACK: begin					
	      rd_desc_state      <= (event_all_rdy) ? ST_RD_MEM_DONE : ST_RD_MEM_ACK;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= rd_mem_curr_sample;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
	      
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= 1'b0;
              
	      if(event_all_rdy) begin				
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= 1'b0;
		    event_dat[i] <= 32'h0;
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b0;
		 end				
	      end else begin				
		 for(i=0; i<NUM_SCA; i+=1) begin
		    event_val[i] <= (event_rdy[i]) ? 1'b0 : event_val[i]; // if event was accepted, don't keep sending it!
		    event_dat[i] <= event_dat[i];
		    event_epy[i] <= 2'h0;
		    event_sop[i] <= 1'b0;
		    event_eop[i] <= 1'b1;					
		 end				
	      end	

              event_not_ready <= 0;
	   end
	   
	   ST_RD_MEM_DONE: begin
	      rd_desc_state      <= ST_RD_DESC_IDLE;
	      rd_mem_curr_ch     <= rd_mem_curr_ch;
	      rd_mem_next_ch     <= rd_mem_next_ch;
	      rd_mem_curr_sample <= rd_mem_curr_sample;
	      r_rd_descriptor    <= r_rd_descriptor;
	      rd_ack             <= 1'b0;
              
	      eth_avl_address    <= eth_avl_address;
	      eth_avl_read       <= 1'b0;
              
	      for(i=0; i<NUM_SCA; i+=1) begin
		 event_val[i] <= 1'b0;
		 event_dat[i] <= 32'h0;
		 event_epy[i] <= 2'h0;
		 event_sop[i] <= 1'b0;
		 event_eop[i] <= 1'b0;
	      end				

              event_not_ready <= 0;
	   end
	 endcase
      end
   end
  
endmodule
