module sca_write_control (
	clk,
  	rst,
  	start, // start up again
  	stop,  // stop until next start 
  	ready,  // goes high when num_samples_to_write has been reached. Will continue writing until stop 

  	num_samples_to_write,
  
	sca_write_ena,
  	sca_write_clk
);
  
parameter MAX_SAMPLES = 511;

localparam SZ_SAMPLE_CNT = $clog2(MAX_SAMPLES+1);
  
input wire clk;
input wire rst;
input wire start;
input wire stop;
input wire [SZ_SAMPLE_CNT-1:0] num_samples_to_write;

output reg ready;
output reg sca_write_ena;
output reg sca_write_clk;

reg write_ena;
reg write_clk;
reg [SZ_SAMPLE_CNT-1:0] sample_cnt;
reg [SZ_SAMPLE_CNT-1:0] samples_to_write;

enum int unsigned { ST_IDLE = 0, ST_WRITE = 2, ST_READY = 4} sca_state;
  
always@(negedge clk) begin
  sca_write_ena <= write_ena;
  sca_write_clk <= write_clk;
end

always@(posedge rst, posedge clk) begin
  if(rst) begin
    sca_state				<= ST_IDLE;    
    sample_cnt 			<= {SZ_SAMPLE_CNT{1'b0}};
    samples_to_write 	<= {SZ_SAMPLE_CNT{1'b0}};
    ready					<= 1'b0;
    write_ena				<= 1'b0;
	 write_clk				<= 1'b0;
  end else begin
    case(sca_state) 
		// Waiting to start writing to SCA
      ST_IDLE: begin		
        sca_state 			<= (start) ? ST_WRITE : ST_IDLE;
        samples_to_write 	<= num_samples_to_write;
        sample_cnt			<= {SZ_SAMPLE_CNT{1'b0}};
        ready					<= 1'b0;
        write_ena				<= 1'b0;
		  write_clk				<= (start) ? 1'b1 : 1'b0;
      end
		// Filling SCA
      ST_WRITE: begin
        sca_state 			<= (sample_cnt < samples_to_write) ? ST_WRITE : ST_READY;
        samples_to_write 	<= samples_to_write;
        sample_cnt			<= (sample_cnt < samples_to_write) ? sample_cnt + 1'b1 : sample_cnt;
        ready					<= (sample_cnt < samples_to_write) ? 1'b0 : 1'b1;
        write_ena				<= 1'b1;
		  write_clk				<= 1'b1;
      end 
		// SCA is filled to at least samples_to_write, can be stopped at any time
      ST_READY: begin
        sca_state 		<= (stop) ? ST_IDLE : ST_READY;
        samples_to_write <= samples_to_write;
        sample_cnt 		<= sample_cnt;
        ready				<= (stop) ? 1'b0 : 1'b1;
        write_ena			<= (stop) ? 1'b0 : 1'b1;
		  write_clk 		<= 1'b1;
      end
    endcase
  end
end  
  
endmodule