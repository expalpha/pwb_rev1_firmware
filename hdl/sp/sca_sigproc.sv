`default_nettype none
module sca_sigproc
  (
	clk_sca_wr,			// SCA Write Clock
	clk_sca_rd,			// SCA Read Clock
	clk_event,				// Event Data path Clock
	rst,					// Global Reset	
	rst_event,				// Event Data path Reset
	rst_sca_rd,			// SCA Channel Reset

	// Inputs
	hw_id,				// 48-bit MAC address for device		
	run,					// Run bit, run is active when set high
	trigger,				// Incoming trigger from trigger decision logic
	trigger_delayed,	// Amount trigger was delayed from initial request
	trigger_source,	// Trigger Request source	
	mem_ready,			// Incoming signal from NIOS letting sigproc know memory slots have been allocated
	mem_slots,			// Allocated memory slots for DDR transfers
	adc_data,			// Incoming 12-bit signed data from ADC SERDES, contains all four channels of data, one channel from each SCA	
	sca_enable,			// SCA Enable for transmission
	sca_compression,	// SCA Compression Type to use on channel data
	
	// Outputs
	run_timestamp,		// 48-bit run timestamp, given in 125MHz clock ticks, but run off the 62.5MHz SCA write clock (so every clock tick moves the counter by 2)	
	busy,					// Busy signal out from sigproc, lets trigger logic know when to drop triggers
	test_mode,			// Enable test mode, injects counter data instead of SCA data into DDR stream
	auto_mode,
	busy_sources,
	
	// Channel Controls
	ch_ena,				// 4x79-bit mask of which SCA channels are enabled (allowed to transmit)
	ch_forced,			// 4x79-bit mask of which SCA channels are forced on (transmit even if threshold is not crossed). Does not override enable!
   //ch_threshold,
	ch_ctrl,
						
	// DDR Avalon bridge, used to transfer SCA data INTO DDR
	sca_avl_beginbursttransfer,			
	sca_avl_waitrequest,
	sca_avl_burstcount,		
	sca_avl_address,
	sca_avl_writedata,
	sca_avl_byteenable,
	sca_avl_write,

   // DDR Avalon bridge, used to transfer SCA data OUT of DDR and INTO ethernet/SATA links
   eth_avl_waitrequest,
   eth_avl_beginbursttransfer,
   eth_avl_burstcount,
   eth_avl_address,
   eth_avl_byteenable,
   eth_avl_read,
   eth_avl_readdatavalid,
   eth_avl_readdata,
			
	// Avalon-ST stream of event data
	event_val,			 
	event_rdy,
	event_dat,
	event_epy,
	event_sop,
	event_eop,

        event_not_ready,
	
	// SCA Controls
	sca_samples_to_read,	// Number of samples to read out of SCA (note: SCA is always filled to capacity at this time, but the read can be less than the fill!)
	sca_start_delay,		// Number of clock cycles to delay before SCA read-out, this is used to correct for the delay between sca_read_ena going high and the SERDES+phase fifo
	sca_write_ena,			// Controls SCA Write Enable Pin on each SCA
	sca_write_clk,			// Controls Enable/Disable of the SCA Write Clock to the SCAs
	sca_read_ena,			// Controls SCA Read Enable Pin on each SCA
	sca_read_clk			// Controls Enable/Disable of the SCA Read Clock to the SCAs
);

parameter SZ_TRIG_DLY 			= 16;	// Trigger delay, will most likely stay at 16 bits
parameter SZ_SCA_DLY	 			= 8;	// SCA delay, will most likely stay at 8 bits
parameter NUM_TRIGGER_SOURCES = 1;	// Number of trigger sources, will be adjusted over time, fine to be changed externally
parameter NUM_MEM_SLOTS  		= 1;	// Number of memory slots available
parameter SZ_MEM_ADDR			= 29;

parameter SZ_TIME 				= 48; 	// Should never be changed from 48 bits
parameter MAX_BINS 				= 511;	// Should never be changed from 511
parameter SZ_ADC_DATA 			= 12;		// Set to the size of the ADC used, has been changed from 14 to 12 bits on Rev1

localparam SZ_TRIG_SRC = (NUM_TRIGGER_SOURCES > 1) ? $clog2(NUM_TRIGGER_SOURCES) : 1;

localparam NUM_SCA = 4;
localparam SZ_BIN_COUNT = $clog2(MAX_BINS+1);
localparam NUM_SCA_DATA_CH = 79;

input  wire clk_sca_wr;
input  wire [NUM_SCA-1:0] clk_sca_rd;
input  wire clk_event;

input  wire [47:0] hw_id;

input  wire rst;
input  wire rst_event;
input  wire [NUM_SCA-1:0] rst_sca_rd;
input  wire run;

input  wire trigger;
input  wire [SZ_TRIG_DLY-1:0] trigger_delayed;
input  wire [SZ_TRIG_SRC-1:0] trigger_source;

input  wire [NUM_SCA-1:0][SZ_SCA_DLY-1:0] sca_start_delay;
input  wire [SZ_BIN_COUNT-1:0] sca_samples_to_read;

input  wire [NUM_SCA-1:0][SZ_ADC_DATA-1:0] adc_data;

input  wire [NUM_SCA-1:0][NUM_SCA_DATA_CH-1:0] ch_ena;
input  wire [NUM_SCA-1:0][NUM_SCA_DATA_CH-1:0] ch_forced;
//input	 wire [NUM_SCA-1:0][NUM_SCA_DATA_CH-1:0][SZ_ADC_DATA-1:0] ch_threshold;
input	 wire [NUM_SCA-1:0][31:0] ch_ctrl;

input  wire mem_ready;			// Incoming signal from NIOS letting sigproc know memory slots have been allocated
input  wire [NUM_MEM_SLOTS-1:0][SZ_MEM_ADDR-1:0] mem_slots;

input wire [NUM_SCA-1:0] sca_enable;
input wire [3:0] sca_compression;
input wire  test_mode;
input wire  auto_mode;

output wire [NUM_SCA-1:0] sca_write_ena;
output wire [NUM_SCA-1:0] sca_read_ena;
output wire sca_write_clk;
output wire sca_read_clk;

output wire busy;

output reg [SZ_TIME-1:0] run_timestamp;

output wire [7:0]		sca_avl_burstcount;
input  wire 			sca_avl_waitrequest;
output wire				sca_avl_beginbursttransfer;
output wire [SZ_MEM_ADDR-1:0] 	sca_avl_address;
output wire [63:0] 	sca_avl_writedata;
output wire [7:0] 	sca_avl_byteenable;
output wire 			sca_avl_write;
   
   // DDR Avalon bridge, used to transfer SCA data OUT of DDR and INTO ethernet/SATA links
   input  wire                  eth_avl_waitrequest;
   output wire                  eth_avl_beginbursttransfer;
   output wire [7:0]            eth_avl_burstcount;
   output wire [SZ_MEM_ADDR-1:0] eth_avl_address;
   output wire [15:0]            eth_avl_byteenable;
   output wire                   eth_avl_read;
   input  wire                   eth_avl_readdatavalid;
   input  wire [127:0]           eth_avl_readdata;

output wire [NUM_SCA-1:0] 			event_val;
input  wire [NUM_SCA-1:0] 			event_rdy;
output wire [NUM_SCA-1:0][31:0]  event_dat;
output wire [NUM_SCA-1:0][1:0] 	event_epy;
output wire [NUM_SCA-1:0] 			event_sop;
output wire [NUM_SCA-1:0] 			event_eop;

output wire event_not_ready;

output wire [3:0] busy_sources;

assign busy_sources = { busy, event_desc_fifo_full, ~sca_write_filled, ~mem_ready };

wire event_desc_fifo_full;
wire [NUM_SCA-1:0][SZ_ADC_DATA-1:0] fifo_data;
wire fifo_ack;
wire fifo_val;
wire fifo_sop;
wire fifo_eop;

wire [NUM_SCA-1:0] ch_trigger_rdy;
wire [NUM_SCA-1:0] ch_fifo_empty;
wire int_rst;
wire run_os;
wire [NUM_SCA-1:0] ch_sop;
wire [NUM_SCA-1:0] ch_eop;
wire ch_fifo_error;
wire fifo_error;
wire [NUM_SCA-1:0] ch_wrfull;
wire sca_write_filled;
wire [47:0] hw_id_sync;
wire [NUM_SCA-1:0] sca_used;

wire [NUM_TRIGGER_SOURCES-1:0] trigger_request_synced;
wire fifo_reset;
wire [NUM_SCA-1:0] sca_write_clk_ch;
wire [NUM_SCA-1:0] sca_read_clk_ch;

wire [NUM_SCA-1:0][NUM_SCA_DATA_CH-1:0] ch_crossed;

assign sca_read_clk = |sca_read_clk_ch;
assign sca_write_clk = |sca_write_clk_ch;
assign sca_write_filled = &ch_trigger_rdy;
assign int_rst = rst | run_os;
assign fifo_val =  &(~(ch_fifo_empty & sca_used)); // start once all enabled channels have data
assign fifo_sop =  &ch_sop;
assign fifo_eop =  &ch_eop;
assign ch_fifo_error = (|(ch_wrfull & sca_used)) & fifo_val; // error can only occur if the fifo is not empty and full at the same time (occurs during fifo reset!)

// Timestamp which counts two per clock cycle
// ALPHAg timestamps given for 125MHz to allow direct comparison of all recorded timestamps between devices
ts_625_to_125 #(
	.SZ_TS ( SZ_TIME )
) timestamper ( 
	.clk	( clk_sca_wr ),
	.rst	( int_rst ),
	.q		( run_timestamp )
);

// Run Start Detect
// Used to force an internal reset of counters etc on start of run
oneshot os_run (
	.clk ( clk_sca_wr ),
	.rst ( rst ),
	.d   ( run ),
	.q	  ( run_os )
);

// Sync hardware ID to 'event' clock to avoid timing warnings
synchronizer #(
  .SZ_DATA( 48 )
) hw_id_syncer (
	.clk	( clk_event ),
	.rst	( rst ),
	.d		( hw_id ),
	.q		( hw_id_sync )
);


sca_event_control #(
	.NUM_SCA 				( NUM_SCA ),
	.NUM_SCA_DATA_CH		( NUM_SCA_DATA_CH ),
	.NUM_TRIGGER_SOURCES	( NUM_TRIGGER_SOURCES ),
	.NUM_MEM_SLOTS			( NUM_MEM_SLOTS ),
	.SZ_DELAY 				( SZ_TRIG_DLY ),
	.SZ_TIMESTAMP			( SZ_TIME ),
	.SZ_MEM_ADDR			( SZ_MEM_ADDR )
) flow_manager (
	.clk 					( clk_sca_wr ),			// Clock
	.event_clk			( clk_event ),
	.rst					( rst ),						// Reset
	.event_rst			( rst_event ),
	.trig_in 			( trigger ),   			// Trigger that was fired, in normal usage this will be delayed from the incoming trigger, to allow the SCA to fill
	.trig_delay			( trigger_delayed ),		// Time the trigger was delayed for, user-configurable. Used to allow the SCA time to fill after trigger, so we can see the waveform post trigger
	.trig_ts				( run_timestamp ),		// Capture timestamp on trigger input and use trigger_delayed to get the 'real' timestamp
	.trig_src			( trigger_source ),
	
	.hw_id				( hw_id_sync ),
	
	.mem_ready			( mem_ready ),					// Are the memory slots allocated (done by NIOS)
	.mem_slot			( mem_slots ),					// Array of Memory slots to use for DMA transfer
	
	// DDR Avalon bridge, used to transfer SCA data INTO DDR
	.sca_avl_burstcount			( sca_avl_burstcount ),
	.sca_avl_waitrequest			( sca_avl_waitrequest ),
	.sca_avl_beginbursttransfer( sca_avl_beginbursttransfer ),
	.sca_avl_address				( sca_avl_address ),
	.sca_avl_writedata			( sca_avl_writedata ),
	.sca_avl_byteenable			( sca_avl_byteenable ),
	.sca_avl_write					( sca_avl_write ),

	// DDR Avalon bridge, used to transfer SCA data OUT of DDR and INTO ethernet/SATA links
	.eth_avl_readdatavalid		( eth_avl_readdatavalid ),
	.eth_avl_burstcount			( eth_avl_burstcount ),
	.eth_avl_waitrequest			( eth_avl_waitrequest ),
	.eth_avl_beginbursttransfer( eth_avl_beginbursttransfer ),		
	.eth_avl_address				( eth_avl_address ),
	.eth_avl_readdata				( eth_avl_readdata ),
	.eth_avl_byteenable			( eth_avl_byteenable ),
	.eth_avl_read					( eth_avl_read ),
				
	
	.busy_out			( busy ),					// Used to control when triggers are allowed to be fired, criteria include: filled SCA, available memory space, previous trigger data has been written to memory
	.event_desc_fifo_full	( event_desc_fifo_full ),
	
	.sca_ready			( sca_write_filled ),
	.sca_ena				( sca_enable ),
	.sca_used			( sca_used ),
	.sca_compression	( sca_compression ),
	
	.ch_enabled_const 	( ch_ena ),		// Channel is active, can be sent, either FORCED or THRESHOLD must be high, otherwise not sent
	.ch_forced_const	( ch_forced ),		// Always send channel, even if threshold is not crossed
	.ch_crossed			( ch_crossed ),

	// Stream in from SCAs
	.sca_fifo_max		( sca_fifo_max ),
	.sca_fifo_rst		( fifo_reset ),
	.sca_err				( fifo_error ),			// If any of the FIFOs go full it's likely all four SCA datastreams are now corrupt, and we will bail out of the sca->ddr transfer
	.sca_dat				( fifo_data ),				// SCA Data
	.sca_val				( fifo_val ),				// SCA Data Valid
	.sca_sop				( fifo_sop ),				// SCA Start-Of-Packet
	.sca_eop				( fifo_eop ),				// SCA End-Of-Packet  
	.sca_ack				( fifo_ack ),
	
	// Streams out to UDP Offloader
	.event_val			( event_val ),
	.event_rdy			( event_rdy ),
	.event_dat			( event_dat ),
	.event_epy			( event_epy ),
	.event_sop			( event_sop ),
	.event_eop			( event_eop ),

        .event_not_ready ( event_not_ready )
	
	// Counters
	// Descriptors dropped due to fifo error
	// # of times we ran out of descriptors (finished creating descriptor, and held NOT_READY due to no more slots available)
	// # of times 
	
);
// If the FIFO ever becomes full, we need to cancel the SCA packet being sent to the DDR streamer
// since the streamer goes through a FIFO as well, this error flag should always arrive in time
// it also allows us to bail out late, if by the worst luck the very final EOP word got dropped due to wrfull
synchronizer #(
  .SZ_DATA( 1 )
) fifo_error_sync (
	.clk	( clk_sca_wr ),
	.rst	( rst ),
	.d		( ch_fifo_error ),
	.q		( fifo_error )
);


wire [NUM_SCA-1:0][9:0] rdusedw;
wire [15:0] sca_fifo_max;

assign sca_fifo_max[15:10] = 6'h00;

min_max_val #(
	.SZ_VALUE( 10 )
) fifo_min_max ( 
    .clk	( clk_sca_wr ),
    .rst	( rst /*fifo_reset*/ ),
    .d	( rdusedw ),
    .min	( ),
    .max	( sca_fifo_max[9:0] )
);

genvar n;
generate 
for(n=0; n<NUM_SCA; n++) begin : gen_sca_controls

//	wire ch_fifo_reset;
	
	// SCA controls
	// SCA Read/Write control
	// ADC data containing SCA waveforms comes in here
	// and is sent out in Avalon-ST format 
	// the first packet (containing the SOP) also contains the 
	// number of samples read per channel (1-511)
	// the last packet contains the 
	// SCA cell the readback ended on
	// this means for a given sample length, the total packet size is
	// 2 + (76*num_samples), which makes the maximum packet 38838 words (12bit words)
	sca_control #(
		.MAX_SAMPLES ( MAX_BINS ),
		.SZ_DATA( SZ_ADC_DATA )
	) sca_channel (
		.clk_wr			( clk_sca_wr ),
		.rst_wr			( rst ),
		.clk_rd			( clk_sca_rd[n] ),
		.rst_rd			( rst_sca_rd[n] ),    
		.trigger			( trigger ),	
		.test_mode		( test_mode ),
		.auto_mode		( auto_mode ),
		//.ch_threshold	( ch_threshold[n] ),
		.ch_ctrl	( ch_ctrl[n] ),
		.ch_crossed_out	( ch_crossed[n] ),
		.start_delay	( sca_start_delay[n] ),
		.num_samples 	( sca_samples_to_read ),
		.ready			( ch_trigger_rdy[n] ),    
		.sca_dat			( adc_data[n] ),
		.out_sop			( sop ),
		.out_eop			( eop ),
		.out_val			( val ),
		.out_dat			( dat ),
		.sca_write_ena	( sca_write_ena[n] ),
		.sca_write_clk	( sca_write_clk_ch[n] ),
		.sca_read_ena	( sca_read_ena[n] ),
		.sca_read_clk	( sca_read_clk_ch[n] )
	);

	wire val;
	wire sop;
	wire eop;
	wire [SZ_ADC_DATA-1:0] dat;

	// FIFO to buffer SCA data while it's being streamed into the DDR
	// the data into the DDR is sent as one 64-bit word
	// which means the 12-bit SCA data is sign-extended as it's being written into the DDR
	// Format: [SCA0][SCA1][SCA2][SCA3]
	// in order to preserve the SCA alignment, we must only write into the DDR when all four FIFOs have data
	// regardless if all four SCAs are to be considered 'active'

	// WARNING: The write full (wrfull) is monitored, so we can know if the buffer fills and the write is no longe valid
	// if this happens, the packet *must* be discarded as data has been lost, and a counter will be incremented
	// If this counter ever increments, we must increase the size of this FIFO to compensate (recompile required)

	//wire [13:0] wrusedw /* synthesis keep */;

        // NB: channel_fifo size is 1024 words usedw is 10 bits, "show ahead" mode
	
	channel_fifo ch_fifo ( 
		.aclr		( fifo_reset ),
		.wrclk	( clk_sca_rd[n] ),
		.wrreq	( val  ), // only write when there is data in ALL the channel fifos!
		.wrfull	( ch_wrfull[n] ),
		.data		( {sop, eop, dat} ),
		.rdclk	( clk_sca_wr ),
		.rdreq	( fifo_ack ),
		.rdempty	( ch_fifo_empty[n] ),
		.rdusedw ( rdusedw[n] ),
		//.wrusedw ( wrusedw ),
		.q			( {ch_sop[n], ch_eop[n], fifo_data[n]} )	
	);
		
end
endgenerate

endmodule
