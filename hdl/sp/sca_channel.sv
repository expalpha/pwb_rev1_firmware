`default_nettype none
module sca_control (
	clk_wr,
  	rst_wr,
  
  	clk_rd,
  	rst_rd,
  
  	trigger,
  	num_samples,
  	start_delay,
  	ready,
	test_mode,
	auto_mode,
	//ch_threshold,
	ch_ctrl,
	ch_crossed_out,
  	  	
  	sca_dat,
  
  	// Captured Output from SCA
  	out_sop,
  	out_eop,
   out_val,
  	out_dat,

  	// SCA Write Pins
	sca_write_ena,
  	sca_write_clk,
  
  	// SCA Read Pins
	sca_read_ena,
  	sca_read_clk  
);
  
parameter MAX_SAMPLES = 511;
parameter SZ_DATA = 12;  
parameter SZ_START_DLY = 8;

localparam NUM_SCA_CH = 79;  
localparam SZ_SAMPLE_CNT = $clog2(MAX_SAMPLES+1);
  
input wire clk_wr;
input wire clk_rd;
input wire rst_wr;
input wire rst_rd;
input wire trigger;
input wire [SZ_SAMPLE_CNT-1:0] num_samples;
input wire [SZ_START_DLY-1:0] start_delay;
input wire [SZ_DATA-1:0] sca_dat;
input wire test_mode;
input wire auto_mode;
//input wire [NUM_SCA_CH-1:0][SZ_DATA-1:0] ch_threshold;
input wire [31:0] ch_ctrl;
  
output wire [SZ_DATA-1:0] out_dat;
output wire [NUM_SCA_CH-1:0] ch_crossed_out;
output wire out_sop;
output wire out_eop;
output wire out_val;
  
output wire ready;
  
output wire sca_write_ena;
output wire sca_write_clk;
  
output wire sca_read_ena;
output wire sca_read_clk;

enum int unsigned { ST_START_WRITE = 0, ST_WRITE = 2, ST_START_READ = 4, ST_READ = 8} sca_state;
    
  
reg start_wr;
reg stop_wr;
reg start_rd;
wire ready_wr;
wire done;  
wire done_sync;  
  
wire start_rd_sync;
wire start_rd_resync;
wire test_mode_sync;
wire auto_mode_sync;
 
wire [SZ_SAMPLE_CNT-1:0] num_samples_sync;
wire [SZ_START_DLY-1:0] start_delay_sync;

wire [NUM_SCA_CH-1:0] ch_crossed;
//wire [NUM_SCA_CH-1:0][SZ_DATA-1:0] ch_threshold_sync; 
wire [31:0] ch_ctrl_sync; 

synchronizer #(
  .SZ_DATA( SZ_SAMPLE_CNT )
) num_samples_syncer (
  .clk	( clk_rd ),
  .rst	( rst_rd ),
  .d		( num_samples ),
  .q		( num_samples_sync )
);
  
synchronizer #(
  .SZ_DATA( SZ_START_DLY )
) start_delay_syncer (
  .clk	( clk_rd ),
  .rst	( rst_rd ),
  .d		( start_delay ),
  .q		( start_delay_sync )
);
  
// synchronize done bit back from read clock to write
// since the read clock is slower we don't have to extend this  
synchronizer #(
	.SZ_DATA ( 1 )
) done_syncer (
	.clk	( clk_wr ),
	.rst	( rst_wr ),
	.d		( done ),
	.q		( done_sync )
);

synchronizer #(
	.SZ_DATA ( 1 )
) start_rd_syncer (
	.clk	( clk_rd ),
	.rst	( rst_rd ),
	.d		( start_rd ),
	.q		( start_rd_sync )
);

synchronizer #(
	.SZ_DATA ( 1 )
) test_mode_syncer (
	.clk	( clk_rd ),
	.rst	( rst_rd ),
	.d		( test_mode ),
	.q		( test_mode_sync )
);

synchronizer #(
	.SZ_DATA ( 1 )
) auto_mode_syncer (
	.clk	( clk_rd ),
	.rst	( rst_rd ),
	.d		( auto_mode ),
	.q		( auto_mode_sync )
);
  
synchronizer #(
	.SZ_DATA ( 1 )
) start_rd_resyncer (
	.clk	( clk_wr ),
	.rst	( rst_wr ),
	.d		( start_rd_sync ),
	.q		( start_rd_resync )
);

//synchronizer #(
//	.SZ_DATA ( NUM_SCA_CH*SZ_DATA ) 
//) threshold_resyncer (
//	.clk	( clk_rd ),
//	.rst	( rst_rd ),
//	.d		( ch_threshold ),
//	.q		( ch_threshold_sync )
//);

synchronizer #(
	.SZ_DATA ( 32 ) 
) ctrl_resyncer (
	.clk	( clk_rd ),
	.rst	( rst_rd ),
	.d	( ch_ctrl ),
	.q	( ch_ctrl_sync )
);

synchronizer #(
	.SZ_DATA ( NUM_SCA_CH ) 
) crossed_resyncer (
	.clk	( clk_wr ),
	.rst	( rst_wr ),
	.d		( ch_crossed ),
	.q		( ch_crossed_out )
);
    
    
always@(posedge rst_wr, posedge clk_wr) begin
	if(rst_wr) begin
		sca_state 	<= ST_START_WRITE;
		start_wr		<= 1'b0;
		stop_wr		<= 1'b0;
		start_rd		<= 1'b0;
	end else begin
		case(sca_state) 
			ST_START_WRITE: begin		
				sca_state 	<= ST_WRITE;
				start_wr		<= 1'b1;
				stop_wr		<= 1'b0;
				start_rd		<= 1'b0;
			end
			ST_WRITE: begin
				sca_state 	<= (trigger && ready) ? ST_START_READ : ST_WRITE;
				start_wr		<= 1'b0;
				stop_wr		<= (trigger && ready) ? 1'b1 : 1'b0;        
				start_rd		<= 1'b0;
			end 
			ST_START_READ: begin
				sca_state		<= ( start_rd_resync ) ? ST_READ : ST_START_READ; // delay until we know the READ has started
				start_wr		<= 1'b0;
				stop_wr		<= 1'b0;
				start_rd		<= 1'b1;        
			end
			ST_READ: begin
				sca_state		<= (done_sync) ? ST_START_WRITE : ST_READ;
				start_wr		<= 1'b0;
				stop_wr		<= 1'b0;
				start_rd		<= 1'b0;
			end      
		endcase
	end
end  
  
sca_write_control #(
  .MAX_SAMPLES( MAX_SAMPLES )
) writer (
  .clk				( clk_wr ),
  .rst				( rst_wr ),
  .start				( start_wr ),  // start up again
  .stop				( stop_wr ),  // stop until next start 
  .ready				( ready ),  // goes high when num_samples_to_write has been reached. Will continue writing until stop 
  .num_samples_to_write ( num_samples ), 
  .sca_write_ena	( sca_write_ena ),
  .sca_write_clk	( sca_write_clk )
);
    
sca_read_control #(
  .MAX_SAMPLES 	( MAX_SAMPLES ),
  .SZ_DATA 			( SZ_DATA ),
  .SZ_START_DLY	( SZ_START_DLY )
) reader (    
  .clk				( clk_rd ),
  .rst				( rst_rd ),	
  .start				( start_rd_sync ),	
  .start_delay		( start_delay_sync ),
  .auto_mode		( auto_mode_sync ),
  .test_mode		( test_mode_sync ),
  //.ch_threshold	( ch_threshold_sync ),
  .ch_ctrl              ( ch_ctrl_sync ),
  .ch_crossed_out		( ch_crossed ),
  .num_samples_to_read ( num_samples_sync ),
  .done				( done ),	
  .sca_dat			( sca_dat ),
  .out_dat			( out_dat ),
  .out_sop			( out_sop ),
  .out_eop			( out_eop ),
  .out_val			( out_val ),	
  .sca_read_ena	( sca_read_ena),
  .sca_read_clk	( sca_read_clk )
);
  
endmodule
