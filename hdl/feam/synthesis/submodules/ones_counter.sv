/*
 Counts the number of ones in the input
 Not the most efficient, but good enough for most use cases.
*/
module ones_counter (
    d,
    q
);

parameter SZ_WIDTH = 2;

localparam SZ_OUTPUT = ($clog2(SZ_WIDTH+1) < 1) ? 1 : $clog2(SZ_WIDTH+1);

input wire [SZ_WIDTH-1:0] d;
output reg [SZ_OUTPUT-1:0] q;

integer n;

always @* begin
	q = {SZ_OUTPUT{1'b0}};  
	for( n = 0; n < SZ_WIDTH; n = n + 1) begin
		q = q + d[n];
	end
end

endmodule
