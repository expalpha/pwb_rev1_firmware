module board (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_readdata,
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_byteenable,
	s1_readdatavalid, 
	s1_read,
	
	// Conduits
	clk_serdes,
	clk_io,
	clk_sca_wr,
	clk_sca_rd,
	clk_rcvd_eth,
	clk_rcvd_xcvr,
	adc_locked,
	adc_aligned,
	lmk_sync_clkin2,
	lmk_stat_clkin1,
	lmk_stat_hold,
	lmk_stat_ld,
	
	sfp_moddet,
	sfp_los,
	sfp_txfault,
	trigger,
	
	adc_pwr_en,
	temp_rstn,
	serdes_rst,
	adc_randomizer_en,
	ext_test_pulse,
	ext_gate,
	use_sata_gxb,
	mac_addr,
        fpga_timestamp,
	invert_ext_trig
);

parameter CLOCK_FREQ = 100000000;

localparam SZ_WIDTH 			= 32; // always NIOS 32bit bus size

// Control Registers
localparam ADC_PWR_EN			= 0;
localparam SERDES_RST			= 1;
localparam TEMP_RSTN			= 2;
localparam ADC_RAND_ENA			= 3;
localparam EXT_TEST_PULSE		= 4;
localparam RESET_COUNTERS		= 5;
localparam USE_SATA_GXB			= 6;
localparam EXT_TEST_PULSE_WIDTH = 7;
localparam EXT_TEST_PULSE_ON_TRIGGER = 8;
localparam EXT_GATE_WIDTH		= 9;
localparam EXT_GATE_ENA			= 10;
localparam MAC_ADDR_LO			= 11;
localparam MAC_ADDR_HI			= 12;
localparam INVERT_EXT_TRIG		= 13;
localparam NUM_CTRL_BASE_REGS 	= 14;
localparam NUM_CTRL_REGS		= NUM_CTRL_BASE_REGS;

// Status Registers		
localparam FPGA_TIMESTAMP		 = 0;
localparam CHIPID_LO		     = 1;
localparam CHIPID_HI		     = 2;
localparam CHIPID_VAL		     = 3;
localparam CLOCK_SERDES0		 = 4;
localparam CLOCK_SERDES1		 = 5;
localparam CLOCK_IO				 = 6;
localparam CLOCK_SCA_WR			 = 7;
localparam CLOCK_SCA_RD		 	 = 8;
localparam CLOCK_RCVD_ETH		 = 9;
localparam CLOCK_RCVD_XCVR		 = 10;

localparam ADC_LOCKED		 	= 11;
localparam ADC_LOCKED_CNT0	 	= 12;
localparam ADC_LOCKED_CNT1	 	= 13;

localparam ADC_ALIGNED		 	= 14;
localparam ADC_ALIGNED_CNT0	 	= 15;
localparam ADC_ALIGNED_CNT1	 	= 16;

localparam LMK_SYNC_CLKIN2		= 17;
localparam LMK_STAT_CLKIN1		= 18;
localparam LMK_STAT_HOLD		= 19;
localparam LMK_STAT_LD			= 20;
localparam LMK_LOCK_CNT			= 21;

localparam SFP_MODDET			= 22;
localparam SFP_LOS				= 23;
localparam SFP_TXFAULT			= 24;

localparam NUM_STAT_BASE_REGS	= 25;
localparam NUM_STAT_REGS 		= NUM_STAT_BASE_REGS;

localparam SZ_STAT		= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL		= $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire clk;
input wire rst;

// Conduits
input wire [1:0] clk_serdes;
input wire clk_io;
input wire clk_sca_wr;
input wire clk_sca_rd;
input wire clk_rcvd_eth;
input wire clk_rcvd_xcvr;

input wire [1:0] adc_locked;
input wire [1:0] adc_aligned;
input wire lmk_sync_clkin2;
input wire lmk_stat_clkin1;
input wire lmk_stat_hold;
input wire lmk_stat_ld;
	
input wire sfp_moddet;
input wire sfp_los;
input wire sfp_txfault;

input wire trigger;
	
output wire [1:0] adc_pwr_en;
output wire ext_test_pulse;
output wire temp_rstn;
output wire serdes_rst;
output wire [1:0] adc_randomizer_en;
output wire use_sata_gxb;
output wire ext_gate;
output wire [47:0] mac_addr;
output wire invert_ext_trig;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
output [SZ_WIDTH-1:0] 		s0_readdata;	// data bus input
input        				s0_read;	// valid bus cycle input
output      				s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input        				s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 		s1_writedata;	// data bus output
input        				s1_read;	// valid bus cycle input
input  [3:0] 				s1_byteenable;
output [SZ_WIDTH-1:0] 		s1_readdata;	// data bus input
output      				s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] status;

input wire [31:0] fpga_timestamp;
//wire [31:0] fpga_timestamp;
wire [63:0] chipid_data;
wire chipid_valid;
wire [1:0][31:0] freq_serdes;
wire [31:0] freq_io;
wire [31:0] freq_sca_wr;
wire [31:0] freq_sca_rd;
wire [31:0] freq_rcvd_eth;
wire [31:0] freq_rcvd_xcvr;

wire [1:0][31:0] pc_align_cnt;
wire [1:0][31:0] pc_locked_cnt;
wire [31:0] lmk_locked_cnt;
wire [15:0] ext_test_pulse_width;
wire [15:0] ext_gate_width;
wire reset_counters;
wire test_pulse;
wire ext_test_pulse_on_trigger;
wire ext_gate_ena;

wire comb_test_pulse;
wire comb_test_gate;

synchronizer #( .SZ_DATA( 1 ) ) sync_ext_test_pulse ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[EXT_TEST_PULSE][0] ), 	.q ( test_pulse ));
synchronizer #( .SZ_DATA( 1 ) ) sync_ext_test_pulse_on_trigger ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[EXT_TEST_PULSE_ON_TRIGGER][0] ), 	.q ( ext_test_pulse_on_trigger ));
synchronizer #( .SZ_DATA( 16 ) ) sync_ext_test_pulse_width ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[EXT_TEST_PULSE_WIDTH][15:0] ), 	.q ( ext_test_pulse_width ));
synchronizer #( .SZ_DATA( 16 ) ) sync_ext_gate_width ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[EXT_GATE_WIDTH][15:0] ), 	.q ( ext_gate_width ));
synchronizer #( .SZ_DATA( 1 ) ) sync_ext_gate_ena ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[EXT_GATE_ENA][0] ), 	.q ( ext_gate_ena ));
synchronizer #( .SZ_DATA( 1 ) ) sync_invert_ext_trig ( .clk( clk_sca_wr ), .rst( rst ), .d( ctrl[INVERT_EXT_TRIG][0] ), 	.q ( invert_ext_trig ));


synchronizer #( .SZ_DATA( 1 ) ) sync_serdes_rst 	( .clk( clk_sca_rd ), .rst( rst ), .d( ctrl[SERDES_RST][0] ), 		.q ( serdes_rst ));
synchronizer #( .SZ_DATA( 2 ) ) sync_adc_rand_ena 	( .clk( clk_sca_rd ), .rst( rst ), .d( { ctrl[ADC_RAND_ENA][8], ctrl[ADC_RAND_ENA][0] } ), 	.q ( adc_randomizer_en ));

assign comb_test_pulse = test_pulse | ( trigger && ext_test_pulse_on_trigger);
assign comb_test_gate = comb_test_pulse & ext_gate_ena;

oneshot_extend #(
	.MAX_EXTEND( 2**16 )
) os_ext_test_pulse (
	.clk		( clk_sca_wr ),
	.rst		( 1'b0 ),
	.d			( comb_test_pulse ) ,
	.extend 	( ext_test_pulse_width ),
	.q			( ext_test_pulse )
);

oneshot_extend #(
	.MAX_EXTEND( 2**16 )
) os_ext_gate (
	.clk		( clk_sca_wr ),
	.rst		( 1'b0 ),
	.d			( comb_test_gate ) ,
	.extend 	( ext_gate_width ),
	.q			( ext_gate )
);

assign adc_pwr_en[1:0] 			= {ctrl[ADC_PWR_EN][8], ctrl[ADC_PWR_EN][0]};
assign temp_rstn				= ctrl[TEMP_RSTN][0];
assign reset_counters			= ctrl[RESET_COUNTERS][0];
assign use_sata_gxb				= ctrl[USE_SATA_GXB][0];
assign mac_addr					= { ctrl[MAC_ADDR_HI][15:0], ctrl[MAC_ADDR_LO][31:0] };

assign status[FPGA_TIMESTAMP]	= fpga_timestamp;

assign status[CLOCK_SERDES0]	= freq_serdes[0];
assign status[CLOCK_SERDES1]	= freq_serdes[1];
assign status[CLOCK_IO]			= freq_io;
assign status[CLOCK_SCA_WR]		= freq_sca_wr;
assign status[CLOCK_SCA_RD]		= freq_sca_rd;
assign status[CLOCK_RCVD_ETH]	= freq_rcvd_eth;
assign status[CLOCK_RCVD_XCVR]	= freq_rcvd_xcvr;
assign status[ADC_LOCKED_CNT0]	= pc_locked_cnt[0];
assign status[ADC_LOCKED_CNT1] 	= pc_locked_cnt[1];
assign status[ADC_ALIGNED_CNT0]	 = pc_align_cnt[0];
assign status[ADC_ALIGNED_CNT1]	 = pc_align_cnt[1];
assign status[LMK_LOCK_CNT]		= lmk_locked_cnt;


synchronizer #( .SZ_DATA( 32 ) ) sync_chip_lo 	( .clk( clk ), .rst( rst ), .d( (chipid_valid) ? chipid_data[31:0] : 32'h0 ), 	.q ( status[CHIPID_LO] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_chip_hi 	( .clk( clk ), .rst( rst ), .d( (chipid_valid) ? chipid_data[63:32] : 32'h0 ), 	.q ( status[CHIPID_HI] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_chip_val 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, chipid_valid} ), 					.q ( status[CHIPID_VAL] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_adc_lock 	( .clk( clk ), .rst( rst ), .d( {23'h0, adc_locked[1], 7'h0, adc_locked[0]} ), 	.q ( status[ADC_LOCKED] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_adc_align	( .clk( clk ), .rst( rst ), .d( {23'h0, adc_aligned[1], 7'h0, adc_aligned[0]} ),.q ( status[ADC_ALIGNED] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_lmk_clk2 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, lmk_sync_clkin2} ),				.q ( status[LMK_SYNC_CLKIN2] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_lmk_clk1 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, lmk_stat_clkin1} ),				.q ( status[LMK_STAT_CLKIN1] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_lmk_hold 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, lmk_stat_hold} ),					.q ( status[LMK_STAT_HOLD] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_lmk_ld 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, lmk_stat_ld} ),					.q ( status[LMK_STAT_LD] ));

synchronizer #( .SZ_DATA( 32 ) ) sync_sfp_mod 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, sfp_moddet} ),						.q ( status[SFP_MODDET] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_sfp_los 	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, sfp_los} ),						.q ( status[SFP_LOS] ));
synchronizer #( .SZ_DATA( 32 ) ) sync_sfp_fault	( .clk( clk ), .rst( rst ), .d( {{31{1'b0}}, sfp_txfault} ),					.q ( status[SFP_TXFAULT] ));

//build_timestamp build_ts ( .data_out( fpga_timestamp ) );

chipid chip_id ( .clkin( clk ), .reset( rst ), .data_valid( chipid_valid ),	.chip_id( chipid_data ) );

freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_serdes0 	( .clk( clk ), .rst( rst ), .clk_freq( clk_serdes[0] ), .q( freq_serdes[0] ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_serdes1 	( .clk( clk ), .rst( rst ), .clk_freq( clk_serdes[1] ), .q( freq_serdes[1] ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_io 		( .clk( clk ), .rst( rst ), .clk_freq( clk_io ), 		.q( freq_io ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_sca_wr 	( .clk( clk ), .rst( rst ), .clk_freq( clk_sca_wr ), 	.q( freq_sca_wr ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_sca_rd 	( .clk( clk ), .rst( rst ), .clk_freq( clk_sca_rd ), 	.q( freq_sca_rd ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_rcvd_eth ( .clk( clk ), .rst( rst ), .clk_freq( clk_rcvd_eth ), 	.q( freq_rcvd_eth ) );
freq_counter #(.REF_FREQ(CLOCK_FREQ)) freq_counter_rcvd_xvcr( .clk( clk ), .rst( rst ), .clk_freq( clk_rcvd_xcvr ), .q( freq_rcvd_xcvr ) );

pattern_counter #(.SZ_PATTERN(2)) pc_align0  ( .clk( clk ), .rst( rst | reset_counters ), .pattern( 2'b10 ), .clk_cap(clk_sca_rd), .d( adc_aligned[0] ), .q( pc_align_cnt[0] ) ); 
pattern_counter #(.SZ_PATTERN(2)) pc_align1  ( .clk( clk ), .rst( rst | reset_counters ), .pattern( 2'b10 ), .clk_cap(clk_sca_rd), .d( adc_aligned[1] ), .q( pc_align_cnt[1] ) ); 
pattern_counter #(.SZ_PATTERN(2)) pc_locked0 ( .clk( clk ), .rst( rst | reset_counters ), .pattern( 2'b10 ), .clk_cap(clk_sca_rd), .d( adc_locked[0] ),  .q( pc_locked_cnt[0] ) ); 
pattern_counter #(.SZ_PATTERN(2)) pc_locked1 ( .clk( clk ), .rst( rst | reset_counters ), .pattern( 2'b10 ), .clk_cap(clk_sca_rd), .d( adc_locked[1] ),  .q( pc_locked_cnt[1] ) ); 
pattern_counter #(.SZ_PATTERN(2)) lmk_locked ( .clk( clk ), .rst( rst | reset_counters ), .pattern( 2'b10 ), .clk_cap(clk_sca_rd), .d( lmk_stat_ld ),  	 .q( lmk_locked_cnt ) ); 

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_stat (	
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s0_address ),
	.readdata		( s0_readdata ), 
	.read			( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 		( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_ctrl (
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s1_address ),
	.write			( s1_write ), 
	.writedata		( s1_writedata ),
	.byteenable     ( s1_byteenable ),
	.read			( s1_read  ), 
	.readdata		( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 			( ctrl )
);

endmodule
