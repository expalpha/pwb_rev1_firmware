module thermo2binary (
  d,
  q,
  val
);
  
parameter NUM_CH = 2;
  
localparam SZ_IN 	= 2**$clog2(NUM_CH);
localparam SZ_OUT = ($clog2(NUM_CH) < 1) ? 1 : $clog2(NUM_CH);
localparam N 		= ($clog2(NUM_CH) < 1) ? 1 : $clog2(NUM_CH);
  
input  wire [NUM_CH-1:0] d;
output wire [SZ_OUT-1:0] q;
output wire val;
  
wire [N:0][SZ_IN-1:0] c; // this may be larger than NUM_CH!

  // if d[0] isn't 1, nothing was found, so input is invalid
  // not a full test that the thermometer code is valid though!
  assign val = ~d[NUM_CH-1];
  
genvar n;
genvar j;
generate
  
// Short-circuit generation if we only have one channel
if(NUM_CH == 1) begin
	assign q[0] = 1'b0;
end else begin
  
	// assign first depth level output to initial inputs, or if value is above NUM_CH, set input to zero.
	// this is done to allow non-power-of-2 channel counts
	for(n=1; n<SZ_IN; n+=1) begin: gen_in
		if(n < NUM_CH) 
          assign c[0][n] = d[n-1];
		else
			assign c[0][n] = 1'b0;
	end
    
	// run through all the remaining depths
	for(j=1; j<N; j+=1) begin: gen_depth    
		// creating mux2's where appropriate
		for(n=1; n < (2**N)/(2**j); n+=1) begin: gen_mux2
			// this looks more complicated than it is
			// each 'middle' number of a given depth goes straight to an output (not done in this loop)
			// AND controls the muxes at that level
			// the mux inputs for each level are assigned to [n] or [n+middle_number], where n is 1 to middle_number-1
			assign c[j][n] = (c[j-1][ (2**N)/(2**j) ] == 1'b1) ? c[j-1][ n + (2**N)/(2**j) ] : c[j-1][n];
		end
	end
  
	// assign outputs to appropriate depth level output
	for(n=0; n<SZ_OUT; n+=1) begin: gen_out
		assign q[n] = c[ N-n-1 ][ (2**n)];
	end
  
end
  
endgenerate
endmodule
