module sca_protocol (
	// Wishbone/Avalon Interface
	clk, 
	rst, 
	
	s0_address, 
	s0_readdata, 
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_readdata, 
	s1_writedata,
	s1_byteenable,
	s1_write, 
	s1_readdatavalid, 
	s1_read,
	
	// Conduits	
    sc_din,
    sc_dout,
    sc_en,
    sc_ck
);

parameter CLOCK_PERIOD = 8;

localparam SZ_WIDTH = 32;

// Control Registers
localparam CTRL_CMD	       = 0;
localparam CTRL_MSB         = 1;
localparam CTRL_LSB         = 2;
localparam NUM_CTRL_REGS    = 3;

// Status Registers
localparam STAT_ACTIVE      = 0;
localparam STAT_MSB         = 1;
localparam STAT_LSB         = 2;
localparam NUM_STAT_REGS    = 3;

localparam SZ_STAT = $clog2(NUM_STAT_REGS);
localparam SZ_CTRL = $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire clk;
input wire rst;

// Conduits
output wire sc_din;
output wire sc_en;
output wire sc_ck;
input  wire sc_dout;

// Avalon 0 - Status
input  wire [SZ_ADDR_STAT-1:0]   s0_address;	// lower address bits
output wire [SZ_WIDTH-1:0]       s0_readdata;	// data bus input
input  wire                      s0_read;	// valid bus cycle input
output wire                      s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  wire [SZ_ADDR_CTRL-1:0]   s1_address;	// lower address bits
input  wire                      s1_write;	// write enable input
input  wire [SZ_WIDTH-1:0]       s1_writedata;	// data bus output
input  wire                      s1_read;	// valid bus cycle input
input  wire [3:0]                s1_byteenable;
output wire [SZ_WIDTH-1:0]       s1_readdata;	// data bus input
output wire                      s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0]	ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0]	status;

wire sca_reset;
wire [5:0] len;    // number of bits to read or write, depending on rd_wrn flag 
wire wr_n;            // (readd=1, write=0)
wire [6:0] addr;        // address bits to write at start
wire [37:0] wr_data; // data to be shifted out 
wire active;

wire [37:0] rd_data;

reg r_start_transmit;
reg r_start_transmit_pipe;

// Control
assign sca_reset    = ctrl[CTRL_CMD][31];
assign len          = ctrl[CTRL_CMD][13:8];
assign wr_n         = ctrl[CTRL_CMD][7];
assign addr         = ctrl[CTRL_CMD][6:0];
assign wr_data      = { ctrl[CTRL_MSB][5:0], ctrl[CTRL_LSB][31:0] };

// Status
assign status[STAT_ACTIVE] =  { {31{1'b0}}, active };
assign status[STAT_MSB]     = { {26{1'b0}}, rd_data[37:32] };
assign status[STAT_LSB]     = rd_data[31:0];


// Start a transmission if the CTRL_CMD register is written to
always@(posedge rst, posedge clk) begin 
    if(rst) begin 
        r_start_transmit <= 1'b0;
		  r_start_transmit_pipe <= 1'b0;
    end else begin 
        // start transmission
        if((r_start_transmit == 1'b0) && (s1_write == 1'b1) && (s1_address == CTRL_CMD)) begin 
            r_start_transmit <= 1'b1;
        end else begin 
            r_start_transmit <= 1'b0;
        end
		  r_start_transmit_pipe <= r_start_transmit;
    end 
end

sca_control #(
    .SZ_ADDR( 7 ),
    .SZ_SHIFT( 38 ),
    .CLOCK_PERIOD( CLOCK_PERIOD )
) u0 ( 
    .clk        ( clk ),
    .rst        ( rst | sca_reset ),
    .wr_n       ( wr_n ),
    .addr       ( addr ),
    .request    ( r_start_transmit_pipe ),
    .active     ( active ),
    .len        ( len ),
    .d          ( wr_data ),
    .q          ( rd_data ),
    
    .sc_ck      ( sc_ck ),
    .sc_en      ( sc_en ),
    .sc_din     ( sc_din ),
    .sc_dout    ( sc_dout )
);

avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) sca_stat (	
    .clk            ( clk ), 
    .rst            ( rst ), 
    .address        ( s0_address ),
    .readdata       ( s0_readdata ), 
    .read           ( s0_read ), 
    .readdatavalid  ( s0_readdatavalid ), 
    .status         ( status )
);

avalon_ctrl_slave #(
    .NUM_CTRL_REGS(NUM_CTRL_REGS),
    .SZ_WIDTH(SZ_WIDTH)
) sca_ctrl (
    .clk           ( clk ), 
    .rst           ( rst ), 
    .address       ( s1_address ),
    .write         ( s1_write ), 
    .writedata     ( s1_writedata ),
    .byteenable    ( s1_byteenable ),
    .read          ( s1_read  ), 
    .readdata      ( s1_readdata ), 
    .readdatavalid ( s1_readdatavalid ), 
    .ctrl          ( ctrl )
);

endmodule
