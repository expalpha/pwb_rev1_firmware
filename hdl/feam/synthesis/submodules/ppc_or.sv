module ppc_or (
	d,
	q_thermo,
	q_onehot
);

parameter NUM_CH = 2;

localparam LEVELS = $clog2(NUM_CH);

input wire [NUM_CH-1:0] d;
output wire [NUM_CH-1:0] q_thermo;
output wire [NUM_CH-1:0] q_onehot;

wire [LEVELS:0][NUM_CH-1:0] comb;

genvar n;
genvar j;
generate 

// Make sure we have enough channels to do work
if(NUM_CH > 1) begin 
	
	// First level is just the input from in
	for(n=0; n<NUM_CH; n+=1) begin: gen_ppc_start
		assign comb[0][n] = d[n];
	end
	
	// perform Parallel Partial Computation OR
	for(j=1; j<=LEVELS; j+=1) begin: gen_lvl
		for(n=0; n<NUM_CH; n+=1) begin: gen_ppc		  
			if(n > (((n - (n % (2**j)) - 1))+((2**j)/2))) begin
				// Verilog doesn't let us do this step-wise, so we have to find the value 
				assign comb[j][n] = comb[j-1][n] | comb[j-1][n - (n % (2**j)) + (((2**j)/2)-1)];
			end else begin
				// if no work is to be done, just connect to the last wire without
				assign comb[j][n] = comb[j-1][n];
			end
		
		end
	end
	
	assign q_thermo[NUM_CH-1:0] = comb[LEVELS][NUM_CH-1:0];

	// convert output from thermometer to one hot
	// ie: 1110 -> 0010
	assign q_onehot[NUM_CH-1:0] = comb[LEVELS][NUM_CH-1:0] ^ {comb[LEVELS][NUM_CH-2:0],1'b0};

end else begin
	// The algorithm breaks if NUM_CH == 1, so skip it and put a wire in if so
	assign q_thermo[0] = d[0];
	assign q_onehot[0] = d[0];
end

endgenerate

endmodule
