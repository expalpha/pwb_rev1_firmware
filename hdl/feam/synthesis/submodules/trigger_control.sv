module trigger_control (
	// Wishbone/Avalon Interface
	clk,
	rst,

	clk_trig, 
	rst_trig, 

	busy,
	busy_source,
	ena,
	trigger_request,

	trigger,
	trigger_delayed,
	trigger_source,
	
	s0_address, 
	s0_readdata,
	s0_read,
	s0_readdatavalid, 

	s1_address, 
	s1_readdata, 
	s1_writedata,
	s1_write, 
	s1_byteenable,
	s1_readdatavalid, 
	s1_read
);

parameter NUM_TRIGGER_SOURCES 	= 1;
parameter NUM_BUSY_SOURCES 		= 1;
parameter SZ_DELAY				= 16;
parameter SZ_COUNTER				= 32;

localparam SZ_TRIG_SRC			= (NUM_TRIGGER_SOURCES > 1) ? $clog2(NUM_TRIGGER_SOURCES) : 1;
localparam SZ_WIDTH 			= 32; // always NIOS 32bit bus size

// Control Registers
localparam GLOBAL_ENABLE		= 0;
localparam NUM_CTRL_BASE_REGS 	= 1;

localparam SRC_ENABLE			= 0;
localparam SRC_INVERT			= 1;
localparam SRC_DELAY			= 2;
localparam NUM_CTRL_SRC_REGS	= 3;

localparam NUM_CTRL_REGS		= NUM_CTRL_BASE_REGS + ( NUM_CTRL_SRC_REGS * NUM_TRIGGER_SOURCES ) + (NUM_BUSY_SOURCES);

// Status Registers		
localparam SRC_TOTAL_ACCEPTED	 = 0;
localparam SRC_TOTAL_DROPPED	 = 1;
localparam SRC_TOTAL_REQUESTED	 = 2;
localparam NUM_STAT_BASE_REGS	 = 3;

localparam SRC_REQUESTED		 = 0;
localparam SRC_ACCEPTED		     = 1;
localparam SRC_DROPPED		     = 2;
localparam SRC_DROPPED_BUSY		 = 3;
localparam SRC_DROPPED_TRIG		 = 4;
localparam SRC_DROPPED_DELAY	 = 5;
localparam SRC_DROPPED_PRIO		 = 6;

localparam NUM_STAT_SRC_REGS	 = 7;

localparam NUM_STAT_REGS 		= NUM_STAT_BASE_REGS + NUM_BUSY_SOURCES + ( NUM_STAT_SRC_REGS * NUM_TRIGGER_SOURCES );

localparam SZ_STAT		= $clog2(NUM_STAT_REGS);
localparam SZ_CTRL		= $clog2(NUM_CTRL_REGS);
localparam SZ_ADDR_STAT	= SZ_STAT;
localparam SZ_ADDR_CTRL	= SZ_CTRL;

// Clock and Resets
input wire clk;
input wire rst;

input wire clk_trig;
input wire rst_trig;

// Conduits
input wire busy;	// External BUSY signal (when high, triggers are DROPPED)
input wire ena; 	// External global enable (aka RUN bit)
input wire [NUM_BUSY_SOURCES-1:0] busy_source;
input wire [NUM_TRIGGER_SOURCES-1:0] trigger_request; // incoming trigger requests

output wire trigger;	// Trigger Signal out
output wire [SZ_DELAY-1:0] trigger_delayed;
output wire [SZ_TRIG_SRC-1:0] trigger_source;

// Avalon 0 - Status
input  [SZ_ADDR_STAT-1:0] 	s0_address;	// lower address bits
output [SZ_WIDTH-1:0] 		s0_readdata;	// data bus input
input        				s0_read;	// valid bus cycle input
output      				s0_readdatavalid;	// bus cycle acknowledge output

// Avalon 1 - Control 
input  [SZ_ADDR_CTRL-1:0] 	s1_address;	// lower address bits
input        				s1_write;	// write enable input
input  [SZ_WIDTH-1:0] 		s1_writedata;	// data bus output
input        				s1_read;	// valid bus cycle input
input  [3:0] 				s1_byteenable;
output [SZ_WIDTH-1:0] 		s1_readdata;	// data bus input
output      				s1_readdatavalid;	// bus cycle acknowledge output

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] ctrl;
wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] status;

// Conduits

wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_requested;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_accepted;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_dropped;

wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_dropped_busy;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_dropped_trigger;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_dropped_delay;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_COUNTER-1:0] src_dropped_priority;

wire [NUM_BUSY_SOURCES-1:0][SZ_COUNTER-1:0] busy_count;
wire [SZ_COUNTER-1:0] src_total_accepted;
wire [SZ_COUNTER-1:0] src_total_dropped;
wire [SZ_COUNTER-1:0] src_total_requested;

wire [NUM_TRIGGER_SOURCES-1:0] src_ena;
wire [NUM_TRIGGER_SOURCES-1:0] src_inv;
wire [NUM_TRIGGER_SOURCES-1:0][SZ_DELAY-1:0] src_dly;

wire global_ena;

genvar n;

// Assign controls 
synchronizer #( .SZ_DATA( 1 ) ) sync_global_ena ( .clk( clk_trig ), .rst( rst_trig ), .d( ctrl[GLOBAL_ENABLE][0] ), 	.q ( global_ena ));

generate 
for(n=0;n<NUM_TRIGGER_SOURCES; n+=1) begin: gen_ctrl_regs
	synchronizer #( .SZ_DATA( 1 ) ) sync_src_ena ( .clk( clk_trig ), .rst( rst_trig ), .d( ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_SRC_REGS)+SRC_ENABLE][0] ), 	.q ( src_ena[n] ));
	synchronizer #( .SZ_DATA( 1 ) ) sync_src_inv ( .clk( clk_trig ), .rst( rst_trig ), .d( ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_SRC_REGS)+SRC_INVERT][0] ), 	.q ( src_inv[n] ));
	synchronizer #( .SZ_DATA( SZ_DELAY ) ) sync_src_dly ( .clk( clk_trig ), .rst( rst_trig ), .d( ctrl[NUM_CTRL_BASE_REGS+(n*NUM_CTRL_SRC_REGS)+SRC_DELAY][SZ_DELAY-1:0] ), 	.q ( src_dly[n] ));
end
endgenerate

// Assign statuses
synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_total_accept 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_total_accepted ), 	.q ( status[SRC_TOTAL_ACCEPTED][SZ_COUNTER-1:0] ));
synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_total_drop 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_total_dropped ), 	.q ( status[SRC_TOTAL_DROPPED][SZ_COUNTER-1:0] ));
synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_total_request	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_total_requested ), 	.q ( status[SRC_TOTAL_REQUESTED][SZ_COUNTER-1:0] ));

generate
for(n=0; n<NUM_BUSY_SOURCES; n+=1) begin: gen_busy_stats_regs
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_busy_count 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( busy_count[n] ), 	.q ( status[NUM_STAT_BASE_REGS+n][SZ_COUNTER-1:0] ));
end
endgenerate

generate 
for(n=0; n<NUM_TRIGGER_SOURCES; n+=1) begin: gen_stat_regs
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_accept 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_accepted[n] ), 	.q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_ACCEPTED][SZ_COUNTER-1:0] ));
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_drop 		( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_dropped[n] ), 	.q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_DROPPED][SZ_COUNTER-1:0] ));
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_request	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_requested[n] ), .q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_REQUESTED][SZ_COUNTER-1:0] ));

	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_drop_busy 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_dropped_busy[n] ), 	.q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_DROPPED_BUSY][SZ_COUNTER-1:0] ));
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_drop_trig 	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_dropped_trigger[n] ), 	.q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_DROPPED_TRIG][SZ_COUNTER-1:0] ));
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_drop_delay	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_dropped_delay[n] ), .q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_DROPPED_DELAY][SZ_COUNTER-1:0] ));
	synchronizer_counter #( .SZ_WIDTH( SZ_COUNTER ) ) sync_drop_prio	( .clk( clk ), .rst( rst ), .d_clk( clk_trig ), .d( src_dropped_priority[n] ), .q ( status[NUM_STAT_BASE_REGS+NUM_BUSY_SOURCES+(n*NUM_STAT_SRC_REGS)+SRC_DROPPED_PRIO][SZ_COUNTER-1:0] ));
end
endgenerate

// Setup trigger sources
wire [NUM_TRIGGER_SOURCES-1:0] trigger_request_comb;

generate
for(n=0;n<NUM_TRIGGER_SOURCES;n+=1) begin: gen_trig_sources

	wire trigger_os;
		
	// and then a oneshot to ensure the signal is stable, and only fires once
	oneshot os_int ( 
		.clk( clk_trig ),	
		.rst( rst_trig ),	
		.d( (trigger_request[n] ^ src_inv[n]) & src_ena[n] ), // invert and enable trigger here, inbetween stages for easier timing
		.q( trigger_os ) 
	);
	
	// only allow triggers if the internal global ena and external ena are set
	assign trigger_request_comb[n] = trigger_os & global_ena & ena;
	
end
endgenerate

// Handles trigger requests and related counters
trigger_block #(
	.NUM_TRIGGER_SOURCES ( NUM_TRIGGER_SOURCES ),
	.NUM_BUSY_SOURCES( NUM_BUSY_SOURCES ),
	.SZ_COUNTER( SZ_COUNTER ),
	.SZ_DELAY ( SZ_DELAY )
) trigger_control (
	.clk				( clk_trig ),	// Clock
	.rst				( rst_trig ),	// Reset
	.delay				( src_dly ),	// Clock cycles to delay trigger request, zero is allowed
	.request			( trigger_request_comb ),	// Trigger requests
	.busy				( busy ),	// System to trigger is busy (cause drop)
	.busy_source		( busy_source ),
	.busy_count			( busy_count ),
	.requested_count	( src_requested ),	// Request counter per 
	.dropped_count		( src_dropped ),	// Dropped counter per
	.accepted_count		( src_accepted ),	// Accepted counter per

	.dropped_busy_count		( src_dropped_busy ),	// Dropped counter due to busy signal
	.dropped_trigger_count	( src_dropped_trigger ),	// Dropped counter due to trigger (already triggered and re-triggered too early)
	.dropped_delay_count		( src_dropped_delay ),	// Dropped counter due to delay (trigger occurred during delay with current trigger)
	.dropped_priority_count	( src_dropped_priority ),	// Dropped counter due to other trigger source claiming priority	
	
	.total_accepted		( src_total_accepted),	// Total accepted (to perform other totals, external summing must be done)
	.total_dropped 		( src_total_dropped ),
	.total_requested 	( src_total_requested ),
  	.trigger_delayed	( trigger_delayed ),	// Clock cycles that have passed since trigger request fired
	.trigger_source		( trigger_source ),	// Trigger request source that generated trigger
	.trigger			( trigger )		// Trigger System  
);
  
avalon_stat_slave #(
	.NUM_STAT_REGS(NUM_STAT_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_stat (	
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s0_address ),
	.readdata		( s0_readdata ), 
	.read			( s0_read ), 
	.readdatavalid	( s0_readdatavalid ), 
	.status 		( status )
);

avalon_ctrl_slave #(
	.NUM_CTRL_REGS(NUM_CTRL_REGS),
	.SZ_WIDTH(SZ_WIDTH)
) avl_ctrl (
	.clk			( clk ), 
	.rst			( rst ), 
	.address		( s1_address ),
	.write			( s1_write ), 
	.writedata		( s1_writedata ),
	.byteenable     ( s1_byteenable ),
	.read			( s1_read  ), 
	.readdata		( s1_readdata ), 
	.readdatavalid	( s1_readdatavalid ), 
	.ctrl 			( ctrl )
);

endmodule
