/*
 * AFTER_slowcontrol.h
 *
 *  Created on: Jan 23, 2017
 *      Author: bryerton
 */

#ifndef AFTER_SLOWCONTROL_H_
#define AFTER_SLOWCONTROL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define AFTER_REG_DUMMY		0x0
#define AFTER_REG_CONFIG_1	0x1
#define AFTER_REG_CONFIG_2	0x2
#define AFTER_REG_INJECT_1	0x3
#define AFTER_REG_INJECT_2	0x4
#define AFTER_REG_VERSION	0x6

#define AFTER_GAIN_RANGE_120fC	0
#define AFTER_GAIN_RANGE_240fC	1
#define AFTER_GAIN_RANGE_360fC	2
#define AFTER_GAIN_RANGE_600fC	3

#define AFTER_PEAKING_TIME_116ns	0
#define AFTER_PEAKING_TIME_200ns	1
#define AFTER_PEAKING_TIME_412ns	2
#define AFTER_PEAKING_TIME_505ns	3
#define AFTER_PEAKING_TIME_610ns	4
#define AFTER_PEAKING_TIME_695ns	5
#define AFTER_PEAKING_TIME_912ns	6
#define AFTER_PEAKING_TIME_993ns	7
#define AFTER_PEAKING_TIME_1054ns	8
#define AFTER_PEAKING_TIME_1134ns	9
#define AFTER_PEAKING_TIME_1343ns	10
#define AFTER_PEAKING_TIME_1421ns	11
#define AFTER_PEAKING_TIME_1546ns	12
#define AFTER_PEAKING_TIME_1626ns	13
#define AFTER_PEAKING_TIME_1834ns	14
#define AFTER_PEAKING_TIME_1912ns	15

#define AFTER_TEST_MODE_NONE	0
#define AFTER_TEST_MODE_CAL		1
#define AFTER_TEST_MODE_TEST	2
#define AFTER_TEST_MODE_FUNC	3

#define AFTER_DEBUG_MODE_STANDBY 	0
#define AFTER_DEBUG_MODE_CSA		1
#define AFTER_DEBUG_MODE_CR			2
#define AFTER_DEBUG_MODE_GAIN2		3

#define AFTER_CUR_RA_211UA_172_UA	0
#define AFTER_CUR_RA_274UA_211_UA	1
#define AFTER_CUR_RA_395UA_274UA	2
#define AFTER_CUR_RA_735UA_395UA	3

#define AFTER_CUR_BUF_132UA_482UA	0
#define AFTER_CUR_BUF_169UA_620UA	1
#define AFTER_CUR_BUF_239UA_813UA	2
#define AFTER_CUR_BUF_433UA_1610UA	3

typedef struct {
	uint8_t Icsa;
	uint8_t Gain;
	uint8_t Time;
	uint8_t Test;
	uint8_t Integrator_mode;
	uint8_t power_down_write;
	uint8_t power_down_read;
	uint8_t alternate_power;
} tAFTER_Reg_Config1;

typedef struct {
	uint8_t debug;
	uint8_t read_from_0;
	uint8_t test_digout;
	uint8_t en_mker_rst;
	uint8_t rst_lv_to_1;
	uint8_t boost_pw;
	uint8_t out_resync;
	uint8_t synchro_inv;
	uint8_t force_eout;
	uint8_t Cur_RA;
	uint8_t Cur_BUF;
} tAFTER_Reg_Config2;

typedef struct {
	uint8_t select_ch[72];
	uint8_t select_cfpn[4];
} tAFTER_Reg_Injection;

typedef struct {
	tAFTER_Reg_Config1 config1;
	tAFTER_Reg_Config2 config2;
	tAFTER_Reg_Injection injection;
	uint16_t version;
} tAFTER_Registers;

typedef void (*AFTER_write)(uint8_t addr, uint8_t len, uint64_t cmd);
typedef uint64_t (*AFTER_read)(uint8_t addr, uint8_t len);

// Helper functions for using Injection Registers
uint8_t AFTER_ConvertChToBit(uint8_t ch);
uint8_t AFTER_ConvertCFPNToBit(uint8_t cfpn);

void AFTER_WriteConfig1(AFTER_write wr, tAFTER_Reg_Config1* reg);
void AFTER_WriteConfig2(AFTER_write wr, tAFTER_Reg_Config2* reg);
void AFTER_WriteInjection(AFTER_write wr, tAFTER_Reg_Injection* reg);
void AFTER_ReadConfig1(AFTER_read rd, tAFTER_Reg_Config1* reg);
void AFTER_ReadConfig2(AFTER_read rd, tAFTER_Reg_Config2* reg);
void AFTER_ReadVersion(AFTER_read rd, uint16_t* reg);
void AFTER_ReadInjection(AFTER_read rd, tAFTER_Reg_Injection* reg);

void AFTER_WriteSettings(AFTER_write wr, tAFTER_Registers* settings);
void AFTER_ReadSettings(AFTER_read rd, tAFTER_Registers* settings);

#ifdef __cplusplus
}
#endif

#endif /* AFTER_SLOWCONTROL_H_ */
