/*
 * sfp.c
 *
 *  Created on: Jul 28, 2014
 *      Author: bryerton
 */

#include <i2c_opencores_regs.h>
#include <i2c_opencores.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "sfp.h"
#include "includes.h"
#include <ipport.h>

void SFP_GetInfo(unsigned int i2c_base, tSFPInfo* sfp_info) {
	alt_u8 temp;
	int16_t signed_temp16;
	uint16_t unsigned_temp16;

	if(!sfp_info) return;

	memset(sfp_info, 0, sizeof(tSFPInfo));

	sfp_info->transceiver_type = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_ID);
	sfp_info->connector = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_CONN);
	sfp_info->encoding = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_ENC);
	sfp_info->br_nominal = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_BR) * 100;
	sfp_info->rate_id = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_RATE_ID);

	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_WAVELENGTH, &sfp_info->wavelength, 2, 0);
	sfp_info->wavelength = htons(sfp_info->wavelength);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_VENDOR_NAME,(alt_u8*)sfp_info->vendor_name, 16, 1);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_VENDOR_OUI, (alt_u8*)sfp_info->vendor_oui, 3, 0);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_VENDOR_PN, (alt_u8*)sfp_info->vendor_pn, 16, 1);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_VENDOR_REV, (alt_u8*)sfp_info->vendor_rev, 4, 1);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_VENDOR_SN, (alt_u8*)sfp_info->vendor_sn, 16, 1);
	SFP_ReadBuff(i2c_base, 0xA0 >> 1, SFP_REG_DATECODE, (alt_u8*)sfp_info->datecode, 8, 1);

	temp = SFP_ReadByte(i2c_base, 0xA0 >> 1, SFP_REG_DM_TYPE);
	sfp_info->dmm_ena = (temp & 0x40) >> 6;
	sfp_info->int_cal = (temp & 0x20) >> 5;
	sfp_info->ext_cal = (temp & 0x10) >> 4;
	sfp_info->meas_type = (temp & 0x08) >> 3;
	sfp_info->addr_mode = (temp & 0x04) >> 2;

	// should we read the calibration settings?
	if(sfp_info->dmm_ena) {

		// by default, the calibration settings should be set to values that can be applied without changing the internal calibrated settings...
		if(sfp_info->ext_cal) {
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_RX_PWR_4, &sfp_info->cal_rx_pwr[4], 4, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_RX_PWR_3, &sfp_info->cal_rx_pwr[3], 4, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_RX_PWR_2, &sfp_info->cal_rx_pwr[2], 4, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_RX_PWR_1, &sfp_info->cal_rx_pwr[1], 4, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_RX_PWR_0, &sfp_info->cal_rx_pwr[0], 4, 0);

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TX_I_SLOPE, &sfp_info->cal_tx_i_slope, 2, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TX_I_OFFSET, &sfp_info->cal_tx_i_offset, 2, 0);

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TX_P_SLOPE, &sfp_info->cal_tx_p_slope, 2, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TX_P_OFFSET, &sfp_info->cal_tx_p_offset, 2, 0);

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TEMP_SLOPE, &sfp_info->cal_temp_slope, 2, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_TEMP_OFFSET, &sfp_info->cal_temp_offset, 2, 0);

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_VOLT_SLOPE, &sfp_info->cal_volt_slope, 2, 0);
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_CAL_VOLT_OFFSET, &sfp_info->cal_volt_offset, 2, 0);
		} else {
			// grab internal values and apply calibration settings to them
			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_TEMP, &signed_temp16, 2, 0);
			sfp_info->temp = htons(signed_temp16) * (1.0f/256.0f); // 1/256C units

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_VCC, &unsigned_temp16, 2, 0);
			sfp_info->vcc = htons(unsigned_temp16) * 0.000100f; // 100 uVolt units

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_TX_BIAS, &unsigned_temp16, 2, 0);
			sfp_info->tx_bias = htons(unsigned_temp16) * 0.002f; // 2uA units

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_TX_POWER, &unsigned_temp16, 2, 0);
			sfp_info->tx_power = htons(unsigned_temp16) * 0.1f; // 0.1uW units

			SFP_ReadBuff(i2c_base, 0xA2 >> 1, SFP_REG_RX_POWER, &unsigned_temp16, 2, 0);
			sfp_info->rx_power = htons(unsigned_temp16) * 0.1f; // 0.1uW units
		}

	}
}


alt_u8 SFP_ReadByte(alt_u32 i2c_base, alt_u8 address, alt_u8 reg) {
	alt_u8 data = 0;

	if(I2C_start(i2c_base, address, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {
		I2C_write(i2c_base,  reg, 0);
		I2C_start(i2c_base, address, I2C_OPENCORES_TXR_RD_MSK);
		data = I2C_read(i2c_base, 1);
	}

	return data;
}

alt_u8 SFP_WriteByte(alt_u32 i2c_base, alt_u8 address, alt_u8 reg, alt_u8 value) {
	if(I2C_start(i2c_base, address, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {
		I2C_write(i2c_base, reg, 0);
		I2C_write(i2c_base, value, 1);
	}
	return 0;
}

alt_u8 SFP_ReadBuff(alt_u32 i2c_base, alt_u8 address, alt_u8 reg, void* data, alt_u8 num_bytes, alt_u8 isString) {
	alt_u8 i;
	alt_u8 n;
	unsigned char c;

	// Skip if NULL ptr or nothing to transmit
	if((data == 0) || (num_bytes == 0)) { return 0; }

	n = 0;

	if(I2C_start(i2c_base, address, I2C_OPENCORES_TXR_WR_MSK) == I2C_ACK) {
		I2C_write(i2c_base,  reg, 0);
		I2C_start(i2c_base, address, I2C_OPENCORES_TXR_RD_MSK);
		for(i=0; i < num_bytes; ++i ) {
			// Enable stop bit on last byte to send
			c = I2C_read(i2c_base, (i == (num_bytes-1)));
			if((isString && isprint(c) && !isspace(c)) || !isString) {
				((char*)(data))[n++] = c;
			}
		}
	}

	// return actual number of bytes read
	return n;
}

