/*
 * task_esper.h
 *
 *  Created on: Nov 16, 2016
 *      Author: bryerton
 */

#ifndef TASK_ESPER_H_
#define TASK_ESPER_H_

#include "includes.h"

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"
#include "tcpport.h"
#include "net.h"

#define TASK_ESPER_STACKSIZE (65536*4)

INT8U create_task_esper(INT8U priority);

#endif /* TASK_ESPER_H_ */
