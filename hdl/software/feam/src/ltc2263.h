#ifndef LTC2263_H_
#define LTC2263_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define LTC2263_REG_RESET 					0x00
#define LTC2263_REG_FORMAT_AND_POWERDOWN	0x01
#define LTC2263_REG_OUTPUT_MODE				0x02
#define LTC2263_REG_TEST_PATTERN_MSB		0x03
#define LTC2263_REG_TEST_PATTERN_LSB		0x04

#define LTC2263_DUTY_CYCLE_STABILIZER_ON 	0
#define LTC2263_DUTY_CYCLE_STABILIZER_OFF 	1

#define LTC2263_DATA_OUTPUT_RANDOMIZER_ON	1
#define LTC2263_DATA_OUTPUT_RANDOMIZER_OFF	0

#define LTC2263_DATA_FORMAT_OFFSET_BINARY	0
#define LTC2263_DATA_FORMAT_TWOS_COMP		1

#define LTC2263_SLEEP_MODE_NORMAL			0
#define LTC2263_SLEEP_MODE_NAP_CHANNEL1		1
#define LTC2263_SLEEP_MODE_NAP_CHANNEL2		2
#define LTC2263_SLEEP_MODE_SLEEP			4

#define LTC2263_LVDS_CURRENT_3_5			0
#define LTC2263_LVDS_CURRENT_4_0			1
#define LTC2263_LVDS_CURRENT_4_5			2
#define LTC2263_LVDS_CURRENT_3_0			4
#define LTC2263_LVDS_CURRENT_2_5			5
#define LTC2263_LVDS_CURRENT_2_1			6
#define LTC2263_LVDS_CURRENT_1_75			7

#define LTC2263_LVDS_TERMINATION_OFF		0
#define LTC2263_LVDS_TERMINATION_ON			1

#define LTC2263_DIGITAL_OUTPUT_ENABLED		0
#define LTC2263_DIGITAL_OUTPUT_DISABLED		1

#define LTC2263_DIGITAL_OUTPUT_MODE_2LANE_16BIT	0
#define LTC2263_DIGITAL_OUTPUT_MODE_2LANE_14BIT	1
#define LTC2263_DIGITAL_OUTPUT_MODE_2LANE_12BIT 2
#define LTC2263_DIGITAL_OUTPUT_MODE_1LANE_14BIT	5
#define LTC2263_DIGITAL_OUTPUT_MODE_1LANE_12BIT	6
#define LTC2263_DIGITAL_OUTPUT_MODE_1LANE_16BIT	7

#define LTC2263_DIGITAL_TEST_PATTERN_OFF	0
#define LTC2263_DIGITAL_TEST_PATTERN_ON		1

typedef void (*ltc2263_write)(uint8_t addr, uint8_t data);
typedef uint8_t (*ltc2263_read)(uint8_t addr);

typedef struct {
	uint8_t adc_14_bit;
	uint8_t dcs_disable;
	uint8_t randomizer_en;
	uint8_t data_format;
	uint8_t sleep_mode;
	uint8_t lvds_current;
	uint8_t lvds_term_en;
	uint8_t output_disable;
	uint8_t output_mode;
	uint8_t test_en;
	uint16_t test_pattern;
} tLTC2263;

void LTC2263_Reset(ltc2263_write wr);
void LTC2263_SetFormatAndPower(ltc2263_write wr, uint8_t dcs_disable, uint8_t randomizer_en, uint8_t data_format, uint8_t sleep_mode);
void LTC2263_OutputMode(ltc2263_write wr, uint8_t lvds_current, uint8_t lvds_term_en, uint8_t output_disable, uint8_t output_mode);
void LTC2263_TestPattern(ltc2263_write wr, uint8_t test_en, uint16_t test_pattern);

void LTC2263_ReadSettings(ltc2263_read rd, tLTC2263* settings);
void LTC2263_WriteSettings(ltc2263_write wr, tLTC2263* settings);

#ifdef __cplusplus
}
#endif

#endif // LTC2263_H_
