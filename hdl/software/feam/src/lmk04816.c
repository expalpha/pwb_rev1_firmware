/*
 * lmk04816.c
 *
 *  Created on: Jan 21, 2017
 *      Author: bryerton
 */

#include <assert.h>
#include <string.h>
#include "lmk04816.h"

void LMK04816_Reset(lmk04816_write write) {
	write(0, 1 << 17); // reset is in bit 17
	write(0, 0); // reset is in bit 17
}

void LMK04816_Powerdown(lmk04816_write write, uint8_t val) {
	write(1, (val & 0x1) << 17); // power down is in bit 17
}

void LMK04816_WriteReg0(lmk04816_write wr, tLMK04816_Reg0* regmap) {
	wr(0,
		(((regmap->CLKout0_1_PD & 0x1) 	<< 31) |
		((regmap->CLKout1_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout0_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout0_1_DDLY & 0x3FF) 	<< 18) |
		((regmap->RESET & 0x0) 				<< 17) |
		((regmap->CLKout0_1_HS & 0x1) 		<< 16) |
		((regmap->CLKout0_1_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg1(lmk04816_write wr, tLMK04816_Reg1* regmap) {
	wr(1,
		(((regmap->CLKout2_3_PD & 0x1) 		<< 31) |
		((regmap->CLKout3_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout2_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout2_3_DDLY & 0x3FF) 	<< 18) |
		((regmap->POWERDOWN & 0x1) 			<< 17) |
		((regmap->CLKout2_3_HS & 0x1) 		<< 16) |
		((regmap->CLKout2_3_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg2(lmk04816_write wr, tLMK04816_Reg2* regmap) {
	wr(2,
		(((regmap->CLKout4_5_PD & 0x1) 		<< 31) |
		((regmap->CLKout5_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout4_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout4_5_DDLY & 0x3FF) 	<< 18) |
		((regmap->CLKout4_5_HS & 0x1) 		<< 16) |
		((regmap->CLKout4_5_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg3(lmk04816_write wr, tLMK04816_Reg3* regmap) {
	wr(3,
		(((regmap->CLKout6_7_PD & 0x1) 		<< 31) |
		((regmap->CLKout6_7_OSCin_Sel & 0x1)<< 30) |
		((regmap->CLKout7_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout6_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout6_7_DDLY & 0x3FF) 	<< 18) |
		((regmap->CLKout6_7_HS & 0x1) 		<< 16) |
		((regmap->CLKout6_7_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg4(lmk04816_write wr, tLMK04816_Reg4* regmap) {
	wr(4,
		(((regmap->CLKout8_9_PD & 0x1) 		<< 31) |
		((regmap->CLKout8_9_OSCin_Sel & 0x1)<< 30) |
		((regmap->CLKout9_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout8_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout8_9_DDLY & 0x3FF) 	<< 18) |
		((regmap->CLKout8_9_HS & 0x1) 		<< 16) |
		((regmap->CLKout8_9_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg5(lmk04816_write wr, tLMK04816_Reg5* regmap) {
	wr(5,
		(((regmap->CLKout10_11_PD & 0x1) 	<< 31) |
		((regmap->CLKout11_ADLY_SEL & 0x1) 	<< 29) |
		((regmap->CLKout10_ADLY_SEL & 0x1) 	<< 28) |
		((regmap->CLKout10_11_DDLY & 0x3FF) << 18) |
		((regmap->CLKout10_11_HS & 0x1) 	<< 16) |
		((regmap->CLKout10_11_DIV & 0x7FF) 	<< 5))
	);
}

void LMK04816_WriteReg6(lmk04816_write wr, tLMK04816_Reg6* regmap) {
	wr(6, (
		((regmap->CLKout3_TYPE & 0xF) 		<< 28) |
		((regmap->CLKout2_TYPE & 0xF) 		<< 24) |
		((regmap->CLKout1_TYPE & 0xF) 		<< 20) |
		((regmap->CLKout0_TYPE & 0xF) 		<< 16) |
		((regmap->CLKout2_3_ADLY & 0x1F) 	<< 11) |
		((regmap->CLKout0_1_ADLY & 0x1F) 	<< 5))
	);
}

void LMK04816_WriteReg7(lmk04816_write wr, tLMK04816_Reg7* regmap) {
	wr(7, (
		((regmap->CLKout7_TYPE & 0xF) 		<< 28) |
		((regmap->CLKout6_TYPE & 0xF) 		<< 24) |
		((regmap->CLKout5_TYPE & 0xF) 		<< 20) |
		((regmap->CLKout4_TYPE & 0xF) 		<< 16) |
		((regmap->CLKout6_7_ADLY & 0x1F) 	<< 11) |
		((regmap->CLKout4_5_ADLY & 0x1F) 	<< 5))
	);
}

void LMK04816_WriteReg8(lmk04816_write wr, tLMK04816_Reg8* regmap) {
	wr(8, (
		((regmap->CLKout11_TYPE & 0xF) 		<< 28) |
		((regmap->CLKout10_TYPE & 0xF) 		<< 24) |
		((regmap->CLKout9_TYPE & 0xF) 		<< 20) |
		((regmap->CLKout8_TYPE & 0xF) 		<< 16) |
		((regmap->CLKout10_11_ADLY & 0x1F) 	<< 11) |
		((regmap->CLKout8_9_ADLY & 0x1F) 	<< 5))
	);
}

void LMK04816_WriteReg10(lmk04816_write wr, tLMK04816_Reg10* regmap) {
	wr(10, (
		(1 << 31) | // not in the data sheet, but in the CodeLoader tool!
		(1 << 28) | // mandatory bit
		((regmap->OSCout0_TYPE & 0xF) 		<< 24) |
		((regmap->EN_OSCout0 & 0x1) 		<< 22) |
		((regmap->OSCout0_MUX & 0x1) 		<< 20) |
		((regmap->PD_OSCin & 0x1) 			<< 19) |
		((regmap->OSCout_DIV & 0x7) 		<< 16) |
		(1 << 14) | // mandatory bit
		((regmap->VCO_MUX & 0x1) 			<< 12) |
		((regmap->EN_FEEDBACK_MUX & 0x1)	<< 11) |
		((regmap->VCO_DIV & 0x7) 			<<  8) |
		((regmap->FEEDBACK_MUX & 0x7) 		<<  5))
	);
}

void LMK04816_WriteReg11(lmk04816_write wr, tLMK04816_Reg11* regmap) {
	wr(11, (
		((regmap->MODE & 0x1F) 				<< 27) |
		((regmap->EN_SYNC & 0x1) 			<< 26) |
		((regmap->NO_SYNC_CLKout10_11 & 0x1)<< 25) |
		((regmap->NO_SYNC_CLKout8_9 & 0x1) 	<< 24) |
		((regmap->NO_SYNC_CLKout6_7 & 0x1) 	<< 23) |
		((regmap->NO_SYNC_CLKout4_5 & 0x1) 	<< 22) |
		((regmap->NO_SYNC_CLKout2_3 & 0x1) 	<< 21) |
		((regmap->NO_SYNC_CLKout0_1 & 0x1) 	<< 20) |
		((regmap->SYNC_CLKin2_MUX & 0x3) 	<< 18) |
		((regmap->SYNC_QUAL & 0x1) 			<< 17) |
		((regmap->SYNC_POL_INV & 0x1)		<< 16) |
		((regmap->SYNC_EN_AUTO & 0x1) 		<< 15) |
		((regmap->SYNC_TYPE & 0x7) 			<< 12) |
		((regmap->EN_PLL2_XTAL & 0x1) 		<<  5))
	);
}

void LMK04816_WriteReg12(lmk04816_write wr, tLMK04816_Reg12* regmap) {
	wr(12, (
		((regmap->LD_MUX & 0x1F) 			<< 27) |
		((regmap->LD_TYPE & 0x7) 			<< 24) |
		((regmap->SYNC_PLL2_DLD & 0x1)		<< 23) |
		((regmap->SYNC_PLL1_DLD & 0x1) 		<< 22) |
		( 1 << 19) | // mandatory bit
		( 1 << 18) | // mandatory bit
		((regmap->EN_TRACK & 0x1) 			<< 8) |
		((regmap->HOLDOVER_MODE & 0x3) 		<< 6) |
		( 1 << 5)) // mandatory bit
	);
}

void LMK04816_WriteReg13(lmk04816_write wr, tLMK04816_Reg13* regmap) {
	wr(13, (
		((regmap->HOLDOVER_MUX & 0x1F) 		<< 27) |
		((regmap->HOLDOVER_TYPE & 0x7) 		<< 24) |
		((regmap->Status_CLKin1_MUX & 0x7)	<< 20) |
		((regmap->Status_CLKin0_TYPE & 0x7)	<< 16) |
		((regmap->DISABLE_DLD1_DET & 0x1)	<< 15) |
		((regmap->Status_CLKin0_MUX & 0x7) 	<< 12) |
		((regmap->CLKin_SELECT_MODE & 0x7) 	<< 9) |
		((regmap->CLKin_Sel_INV & 0x1)		<< 8) |
		((regmap->EN_CLKin2 & 0x1)			<< 7) |
		((regmap->EN_CLKin1 & 0x1)			<< 6) |
		((regmap->EN_CLKin0 & 0x1)			<< 5))
	);
}

void LMK04816_WriteReg14(lmk04816_write wr, tLMK04816_Reg14* regmap) {
	wr(14, (
		((regmap->LOS_TIMEOUT & 0x3) 		<< 30) |
		((regmap->EN_LOS & 0x1) 			<< 28) |
		((regmap->Status_CLKin1_TYPE & 0x7)	<< 24) |
		((regmap->CLKin2_BUF_TYPE & 0x1)	<< 22) |
		((regmap->CLKin1_BUF_TYPE & 0x1)	<< 21) |
		((regmap->CLKin0_BUF_TYPE & 0x1)	<< 20) |
		((regmap->DAC_HIGH_TRIP & 0x3F)		<< 14) |
		((regmap->DAC_LOW_TRIP & 0x3F)		<<  6) |
		((regmap->EN_VTUNE_RAIL_DET & 0x1)	<<  5))
	);
}

void LMK04816_WriteReg15(lmk04816_write wr, tLMK04816_Reg15* regmap) {
	wr(15, (
		((regmap->MAN_DAC & 0x3FF) 			<< 22) |
		((regmap->EN_MAN_DAC & 0x1) 		<< 20) |
		((regmap->HOLDOVER_DLD_CNT & 0x3FFF)<<  6) |
		((regmap->FORCE_HOLDOVER & 0x1)		<<  5))
	);
}

void LMK04816_WriteReg16(lmk04816_write wr, tLMK04816_Reg16* regmap) {
	wr(16, (
		((regmap->XTAL_LVL & 0x3) 			<< 30) |
		( 1 << 24) | // mandatory bit
		( 1 << 22) | // mandatory bit
		( 1 << 20) | // mandatory bit
		( 1 << 18) | // mandatory bit
		( 1 << 16) | // mandatory bit
		( 1 << 10))   // mandatory bit
	);
}

void LMK04816_WriteReg24(lmk04816_write wr, tLMK04816_Reg24* regmap) {
	wr(24, (
		((regmap->PLL2_C4_LF & 0xF) 		<< 28) |
		((regmap->PLL2_C3_LF & 0xF) 		<< 24) |
		((regmap->PLL2_R4_LF & 0x7) 		<< 20) |
		((regmap->PLL2_R3_LF & 0x7) 		<< 16) |
		((regmap->PLL1_N_DLY & 0x7) 		<< 12) |
		((regmap->PLL1_R_DLY & 0x7) 		<< 8) |
		((regmap->PLL1_WND_SIZE & 0x3) 		<< 6))
	);
}

void LMK04816_WriteReg25(lmk04816_write wr, tLMK04816_Reg25* regmap) {
	wr(25, (
		((regmap->DAC_CLK_DIV & 0x3FF) 		<< 22) |
		((regmap->PLL1_DLD_CNT & 0x3FFF) 	<< 6))
	);
}

void LMK04816_WriteReg26(lmk04816_write wr, tLMK04816_Reg26* regmap) {
	wr(26, (
		((regmap->PLL2_WND_SIZE & 0x3) 		<< 30) |
		((regmap->EN_PLL2_REF_2X & 0x1) 	<< 29) |
		((regmap->PLL2_CP_POL & 0x1) 		<< 28) |
		((regmap->PLL2_CP_GAIN & 0x3) 		<< 26) |
		( 1 << 25 ) |
		( 1 << 24 ) |
		( 1 << 23 ) |
		( 1 << 21 ) |
		((regmap->PLL2_DLD_CNT & 0x3FFF) 	<<  6) |
		((regmap->PLL2_CP_TRI & 0x1) 		<<  5))
	);
}

void LMK04816_WriteReg27(lmk04816_write wr, tLMK04816_Reg27* regmap) {
	wr(27, (
		((regmap->PLL1_CP_POL & 0x1) 		<< 28) |
		((regmap->PLL1_CP_GAIN & 0x3) 		<< 26) |
		((regmap->CLKin2_PreR_DIV & 0x3) 	<< 24) |
		((regmap->CLKin1_PreR_DIV & 0x3)	<< 22) |
		((regmap->CLKin0_PreR_DIV & 0x3)	<< 20) |
		((regmap->PLL1_R & 0x3FFF) 			<<  6) |
		((regmap->PLL1_CP_TRI & 0x1) 		<<  5))
	);
}

void LMK04816_WriteReg28(lmk04816_write wr, tLMK04816_Reg28* regmap) {
	wr(28, (
		((regmap->PLL2_R & 0xFFF)	 		<< 20) |
		((regmap->PLL1_N & 0x3FFF) 			<<  6))
	);
}

void LMK04816_WriteReg29(lmk04816_write wr, tLMK04816_Reg29* regmap) {
	wr(29, (
		((regmap->OSCin_FREQ & 0x7)	 		<< 24) |
		((regmap->PLL2_FAST_PDF & 0x1)		<< 23) |
		((regmap->PLL2_N_CAL & 0x3FFFF)		<<  5))
	);
}

void LMK04816_WriteReg30(lmk04816_write wr, tLMK04816_Reg30* regmap) {
	wr(30, (
		((regmap->PLL2_P & 0x7)	 			<< 24) |
		((regmap->PLL2_N & 0x3FFFF)			<<  5))
	);
}

void LMK04816_WriteReg31(lmk04816_write wr, tLMK04816_Reg31* regmap) {
	wr(31, (
		((regmap->READBACK_LE 	& 0x1)			<<  21) |
		((regmap->READBACK_ADDR & 0x1F)			<<  16) |
		((regmap->uWire_LOCK & 0x1)			<<  5))
	);
}

void LMK04816_ReadReg0(lmk04816_read rd, tLMK04816_Reg0* regmap) {
	uint32_t reg;

	reg= rd(0);

	regmap->CLKout0_1_PD 	 = (reg >> 31) & 0x1;
	regmap->CLKout1_ADLY_SEL = (reg >> 29) & 0x1;
	regmap->CLKout0_ADLY_SEL = (reg >> 28) & 0x1;
	regmap->CLKout0_1_DDLY 	 = (reg >> 18) & 0x3FF;
	regmap->RESET 			 = (reg >> 17) & 0x1;
	regmap->CLKout0_1_HS 	 = (reg >> 16) & 0x1;
	regmap->CLKout0_1_DIV 	 = (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg1(lmk04816_read rd, tLMK04816_Reg1* regmap) {
	uint32_t reg;

	reg= rd(1);

	regmap->CLKout2_3_PD 	 = (reg >> 31) & 0x1;
	regmap->CLKout3_ADLY_SEL = (reg >> 29) & 0x1;
	regmap->CLKout2_ADLY_SEL = (reg >> 28) & 0x1;
	regmap->CLKout2_3_DDLY 	 = (reg >> 18) & 0x3FF;
	regmap->POWERDOWN 		 = (reg >> 17) & 0x1;
	regmap->CLKout2_3_HS 	 = (reg >> 16) & 0x1;
	regmap->CLKout2_3_DIV 	 = (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg2(lmk04816_read rd, tLMK04816_Reg2* regmap) {
	uint32_t reg;

	reg= rd(2);

	regmap->CLKout4_5_PD 	 = (reg >> 31) & 0x1;
	regmap->CLKout5_ADLY_SEL = (reg >> 29) & 0x1;
	regmap->CLKout4_ADLY_SEL = (reg >> 28) & 0x1;
	regmap->CLKout4_5_DDLY 	 = (reg >> 18) & 0x3FF;
	regmap->CLKout4_5_HS 	 = (reg >> 16) & 0x1;
	regmap->CLKout4_5_DIV 	 = (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg3(lmk04816_read rd, tLMK04816_Reg3* regmap) {
	uint32_t reg;

	reg = rd(3);

	regmap->CLKout6_7_PD 		= (reg >> 31) & 0x1;
	regmap->CLKout6_7_OSCin_Sel = (reg >> 30) & 0x1;
	regmap->CLKout7_ADLY_SEL  	= (reg >> 29) & 0x1;
	regmap->CLKout6_ADLY_SEL 	= (reg >> 28) & 0x1;
	regmap->CLKout6_7_DDLY 		= (reg >> 18) & 0x3FF;
	regmap->CLKout6_7_HS 		= (reg >> 16) & 0x1;
	regmap->CLKout6_7_DIV 		= (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg4(lmk04816_read rd, tLMK04816_Reg4* regmap) {
	uint32_t reg;

	reg = rd(4);

	regmap->CLKout8_9_PD 		= (reg >> 31) & 0x1;
	regmap->CLKout8_9_OSCin_Sel	= (reg >> 30) & 0x1;
	regmap->CLKout9_ADLY_SEL	= (reg >> 29) & 0x1;
	regmap->CLKout8_ADLY_SEL	= (reg >> 28) & 0x1;
	regmap->CLKout8_9_DDLY		= (reg >> 18) & 0x3FF;
	regmap->CLKout8_9_HS		= (reg >> 16) & 0x1;
	regmap->CLKout8_9_DIV		= (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg5(lmk04816_read rd, tLMK04816_Reg5* regmap) {
	uint32_t reg;

	reg = rd(5);

	regmap->CLKout10_11_PD 		= (reg >> 31) & 0x1;
	regmap->CLKout11_ADLY_SEL	= (reg >> 29) & 0x1;
	regmap->CLKout10_ADLY_SEL 	= (reg >> 28) & 0x1;
	regmap->CLKout10_11_DDLY	= (reg >> 18) & 0x3FF;
	regmap->CLKout10_11_HS		= (reg >> 16) & 0x1;
	regmap->CLKout10_11_DIV		= (reg >>  5) & 0x7FF;
}

void LMK04816_ReadReg6(lmk04816_read rd, tLMK04816_Reg6* regmap) {
	uint32_t reg;

	reg = rd(6);

	regmap->CLKout3_TYPE		= (reg >> 28) & 0xF;
	regmap->CLKout2_TYPE 		= (reg >> 24) & 0xF;
	regmap->CLKout1_TYPE 		= (reg >> 20) & 0xF;
	regmap->CLKout0_TYPE 		= (reg >> 16) & 0xF;
	regmap->CLKout2_3_ADLY 		= (reg >> 11) & 0x1F;
	regmap->CLKout0_1_ADLY 		= (reg >>  5) & 0x1F;
}

void LMK04816_ReadReg7(lmk04816_read rd, tLMK04816_Reg7* regmap) {
	uint32_t reg;

	reg = rd(7);

	regmap->CLKout7_TYPE		= (reg >> 28) & 0xF;
	regmap->CLKout6_TYPE		= (reg >> 24) & 0xF;
	regmap->CLKout5_TYPE		= (reg >> 20) & 0xF;
	regmap->CLKout4_TYPE		= (reg >> 16) & 0xF;
	regmap->CLKout6_7_ADLY		= (reg >> 11) & 0x1F;
	regmap->CLKout4_5_ADLY		= (reg >>  5) & 0x1F;
}

void LMK04816_ReadReg8(lmk04816_read rd, tLMK04816_Reg8* regmap) {
	uint32_t reg;

	reg = rd(8);

	regmap->CLKout11_TYPE 		= (reg >> 28) & 0xF;
	regmap->CLKout10_TYPE		= (reg >> 24) & 0xF;
	regmap->CLKout9_TYPE 		= (reg >> 20) & 0xF;
	regmap->CLKout8_TYPE 		= (reg >> 16) & 0xF;
	regmap->CLKout10_11_ADLY 	= (reg >> 11) & 0x1F;
	regmap->CLKout8_9_ADLY		= (reg >>  5) & 0x1F;
}

void LMK04816_ReadReg10(lmk04816_read rd, tLMK04816_Reg10* regmap) {
	uint32_t reg;

	reg = rd(10);

	regmap->OSCout0_TYPE 		= (reg >> 24) & 0xF;
	regmap->EN_OSCout0 			= (reg >> 22) & 0x1;
	regmap->OSCout0_MUX			= (reg >> 20) & 0x1;
	regmap->PD_OSCin			= (reg >> 19) & 0x1;
	regmap->OSCout_DIV			= (reg >> 16) & 0x7;
	regmap->VCO_MUX 			= (reg >> 12) & 0x1;
	regmap->EN_FEEDBACK_MUX		= (reg >> 11) & 0x1;
	regmap->VCO_DIV 			= (reg >>  8) & 0x7;
	regmap->FEEDBACK_MUX 		= (reg >>  5) & 0x7;
}

void LMK04816_ReadReg11(lmk04816_read rd, tLMK04816_Reg11* regmap) {
	uint32_t reg;

	reg = rd(11);

	regmap->MODE				= (reg >> 27) & 0x1F;
	regmap->EN_SYNC				= (reg >> 26) & 0x1;
	regmap->NO_SYNC_CLKout10_11	= (reg >> 25) & 0x1;
	regmap->NO_SYNC_CLKout8_9	= (reg >> 24) & 0x1;
	regmap->NO_SYNC_CLKout6_7	= (reg >> 23) & 0x1;
	regmap->NO_SYNC_CLKout4_5 	= (reg >> 22) & 0x1;
	regmap->NO_SYNC_CLKout2_3 	= (reg >> 21) & 0x1;
	regmap->NO_SYNC_CLKout0_1	= (reg >> 20) & 0x1;
	regmap->SYNC_CLKin2_MUX		= (reg >> 18) & 0x3;
	regmap->SYNC_QUAL			= (reg >> 17) & 0x1;
	regmap->SYNC_POL_INV		= (reg >> 16) & 0x1;
	regmap->SYNC_EN_AUTO		= (reg >> 15) & 0x1;
	regmap->SYNC_TYPE			= (reg >> 12) & 0x7;
	regmap->EN_PLL2_XTAL		= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg12(lmk04816_read rd, tLMK04816_Reg12* regmap) {
	uint32_t reg;

	reg = rd(12);

	regmap->LD_MUX				= (reg >> 27) & 0x1F;
	regmap->LD_TYPE 			= (reg >> 24) & 0x7;
	regmap->SYNC_PLL2_DLD		= (reg >> 23) & 0x1;
	regmap->SYNC_PLL1_DLD 		= (reg >> 22) & 0x1;
	regmap->EN_TRACK			= (reg >>  8) & 0x1;
	regmap->HOLDOVER_MODE		= (reg >>  6) & 0x3;
}

void LMK04816_ReadReg13(lmk04816_read rd, tLMK04816_Reg13* regmap) {
	uint32_t reg;

	reg = rd(13);

	regmap->HOLDOVER_MUX 		= (reg >> 27) & 0x1F;
	regmap->HOLDOVER_TYPE 		= (reg >> 24) & 0x7;
	regmap->Status_CLKin1_MUX 	= (reg >> 20) & 0x7;
	regmap->Status_CLKin0_TYPE	= (reg >> 16) & 0x7;
	regmap->DISABLE_DLD1_DET	= (reg >> 15) & 0x1;
	regmap->Status_CLKin0_MUX	= (reg >> 12) & 0x7;
	regmap->CLKin_SELECT_MODE	= (reg >>  9) & 0x7;
	regmap->CLKin_Sel_INV		= (reg >>  8) & 0x1;
	regmap->EN_CLKin2 			= (reg >>  7) & 0x1;
	regmap->EN_CLKin1			= (reg >>  6) & 0x1;
	regmap->EN_CLKin0			= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg14(lmk04816_read rd, tLMK04816_Reg14* regmap) {
	uint32_t reg;

	reg = rd(14);

	regmap->LOS_TIMEOUT			= (reg >> 30) & 0x3;
	regmap->EN_LOS				= (reg >> 28) & 0x1;
	regmap->Status_CLKin1_TYPE	= (reg >> 24) & 0x7;
	regmap->CLKin2_BUF_TYPE		= (reg >> 22) & 0x1;
	regmap->CLKin1_BUF_TYPE		= (reg >> 21) & 0x1;
	regmap->CLKin0_BUF_TYPE 	= (reg >> 20) & 0x1;
	regmap->DAC_HIGH_TRIP 		= (reg >> 14) & 0x3F;
	regmap->DAC_LOW_TRIP		= (reg >>  6) & 0x3F;
	regmap->EN_VTUNE_RAIL_DET	= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg15(lmk04816_read rd, tLMK04816_Reg15* regmap) {
	uint32_t reg;

	reg = rd(15);

	regmap->MAN_DAC				= (reg >> 22) & 0x3FF;
	regmap->EN_MAN_DAC			= (reg >> 20) & 0x1;
	regmap->HOLDOVER_DLD_CNT 	= (reg >>  6) & 0x3FFF;
	regmap->FORCE_HOLDOVER		= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg16(lmk04816_read rd, tLMK04816_Reg16* regmap) {
	uint32_t reg;

	reg = rd(16);

	regmap->XTAL_LVL 			= (reg >> 30) & 0x3;
}

void LMK04816_ReadReg23(lmk04816_read rd, tLMK04816_Reg23* regmap) {
	uint32_t reg;

	reg = rd(23);

	regmap->DAC_CNT 			= (reg >> 14) & 0x3FF;
}

void LMK04816_ReadReg24(lmk04816_read rd, tLMK04816_Reg24* regmap) {
	uint32_t reg;

	reg = rd(24);

	regmap->PLL2_C4_LF			= (reg >> 28) & 0xF;
	regmap->PLL2_C3_LF			= (reg >> 24) & 0xF;
	regmap->PLL2_R4_LF			= (reg >> 20) & 0x7;
	regmap->PLL2_R3_LF			= (reg >> 16) & 0x7;
	regmap->PLL1_N_DLY			= (reg >> 12) & 0x7;
	regmap->PLL1_R_DLY			= (reg >>  8) & 0x7;
	regmap->PLL1_WND_SIZE		= (reg >>  6) & 0x3;
}

void LMK04816_ReadReg25(lmk04816_read rd, tLMK04816_Reg25* regmap) {
	uint32_t reg;

	reg = rd(25);

	regmap->DAC_CLK_DIV			= (reg >> 22) & 0x3FF;
	regmap->PLL1_DLD_CNT		= (reg >>  6) & 0x3FFF;
}

void LMK04816_ReadReg26(lmk04816_read rd, tLMK04816_Reg26* regmap) {
	uint32_t reg;

	reg = rd(26);

	regmap->PLL2_WND_SIZE		= (reg >> 30) & 0x3;
	regmap->EN_PLL2_REF_2X		= (reg >> 29) & 0x1;
	regmap->PLL2_CP_POL			= (reg >> 28) & 0x1;
	regmap->PLL2_CP_GAIN		= (reg >> 26) & 0x3;
	regmap->PLL2_DLD_CNT		= (reg >>  6) & 0x3FFF;
	regmap->PLL2_CP_TRI			= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg27(lmk04816_read rd, tLMK04816_Reg27* regmap) {
	uint32_t reg;

	reg = rd(27);

	regmap->PLL1_CP_POL			= (reg >> 28) & 0x1;
	regmap->PLL1_CP_GAIN		= (reg >> 26) & 0x3;
	regmap->CLKin2_PreR_DIV		= (reg >> 24) & 0x3;
	regmap->CLKin1_PreR_DIV		= (reg >> 22) & 0x3;
	regmap->CLKin0_PreR_DIV 	= (reg >> 20) & 0x3;
	regmap->PLL1_R				= (reg >>  6) & 0x3FFF;
	regmap->PLL1_CP_TRI			= (reg >>  5) & 0x1;
}

void LMK04816_ReadReg28(lmk04816_read rd, tLMK04816_Reg28* regmap) {
	uint32_t reg;

	reg = rd(28);

	regmap->PLL2_R				= (reg >> 20) & 0xFFF;
	regmap->PLL1_N				= (reg >>  6) & 0x3FFF;
}

void LMK04816_ReadReg29(lmk04816_read rd, tLMK04816_Reg29* regmap) {
	uint32_t reg;

	reg = rd(29);

	regmap->OSCin_FREQ 			= (reg >> 24) & 0x7;
	regmap->PLL2_FAST_PDF		= (reg >> 23) & 0x1;
	regmap->PLL2_N_CAL 			= (reg >>  5) & 0x3FFFF;
}

void LMK04816_ReadReg30(lmk04816_read rd, tLMK04816_Reg30* regmap) {
	uint32_t reg;

	reg = rd(30);

	regmap->PLL2_P				= (reg >> 24) & 0x7;
	regmap->PLL2_N				= (reg >>  5) & 0x3FFFF;
}

void LMK04816_ReadReg31(lmk04816_read rd, tLMK04816_Reg31* regmap) {
	uint32_t reg;

	reg = rd(12);

	regmap->READBACK_LE			= (reg >> 21) & 0x1;

	reg = rd(31);
	regmap->uWire_LOCK			= (reg >>  5) & 0x1;
	regmap->READBACK_ADDR		= 31;
}


void LMK04816_ReadSettings(lmk04816_read rd, tLMK04816_Registers* regmap) {
	LMK04816_ReadReg0(rd, &regmap->r0);
	LMK04816_ReadReg1(rd, &regmap->r1);
	LMK04816_ReadReg2(rd, &regmap->r2);
	LMK04816_ReadReg3(rd, &regmap->r3);
	LMK04816_ReadReg4(rd, &regmap->r4);
	LMK04816_ReadReg5(rd, &regmap->r5);
	LMK04816_ReadReg6(rd, &regmap->r6);
	LMK04816_ReadReg7(rd, &regmap->r7);
	LMK04816_ReadReg8(rd, &regmap->r8);
	LMK04816_ReadReg10(rd, &regmap->r10);
	LMK04816_ReadReg11(rd, &regmap->r11);
	LMK04816_ReadReg12(rd, &regmap->r12);
	LMK04816_ReadReg13(rd, &regmap->r13);
	LMK04816_ReadReg14(rd, &regmap->r14);
	LMK04816_ReadReg15(rd, &regmap->r15);
	//LMK04816_ReadReg16(rd, &regmap->r16);
	LMK04816_ReadReg23(rd, &regmap->r23);
	LMK04816_ReadReg24(rd, &regmap->r24);
	LMK04816_ReadReg25(rd, &regmap->r25);
	LMK04816_ReadReg26(rd, &regmap->r26);
	LMK04816_ReadReg27(rd, &regmap->r27);
	LMK04816_ReadReg28(rd, &regmap->r28);
	LMK04816_ReadReg29(rd, &regmap->r29);
	LMK04816_ReadReg30(rd, &regmap->r30);
	//LMK04816_ReadReg31(rd, &regmap->r31);
}

void LMK04816_WriteSettings(lmk04816_write wr, tLMK04816_Registers* regmap) {
	LMK04816_WriteReg0(wr, &regmap->r0);
	LMK04816_WriteReg1(wr, &regmap->r1);
	LMK04816_WriteReg2(wr, &regmap->r2);
	LMK04816_WriteReg3(wr, &regmap->r3);
	LMK04816_WriteReg4(wr, &regmap->r4);
	LMK04816_WriteReg5(wr, &regmap->r5);
	LMK04816_WriteReg6(wr, &regmap->r6);
	LMK04816_WriteReg7(wr, &regmap->r7);
	LMK04816_WriteReg8(wr, &regmap->r8);
	LMK04816_WriteReg10(wr, &regmap->r10);
	LMK04816_WriteReg11(wr, &regmap->r11);
	LMK04816_WriteReg12(wr, &regmap->r12);
	LMK04816_WriteReg13(wr, &regmap->r13);
	LMK04816_WriteReg14(wr, &regmap->r14);
	LMK04816_WriteReg15(wr, &regmap->r15);
	//LMK04816_WriteReg16(wr, &regmap->r16);
	LMK04816_WriteReg24(wr, &regmap->r24);
	LMK04816_WriteReg25(wr, &regmap->r25);
	LMK04816_WriteReg26(wr, &regmap->r26);
	LMK04816_WriteReg27(wr, &regmap->r27);
	LMK04816_WriteReg28(wr, &regmap->r28);
	LMK04816_WriteReg29(wr, &regmap->r29);
	LMK04816_WriteReg30(wr, &regmap->r30);
	LMK04816_WriteReg31(wr, &regmap->r31);
}

void LMK04816_WriteSyncSettings(lmk04816_write wr, tLMK04816_Registers* regmap) {
	LMK04816_WriteReg0(wr, &regmap->r0);
	LMK04816_WriteReg1(wr, &regmap->r1);
	LMK04816_WriteReg2(wr, &regmap->r2);
	LMK04816_WriteReg3(wr, &regmap->r3);
	LMK04816_WriteReg4(wr, &regmap->r4);
	LMK04816_WriteReg5(wr, &regmap->r5);
}

