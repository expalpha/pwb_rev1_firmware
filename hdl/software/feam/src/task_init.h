/*
 * task_init.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef TASK_INIT_H_
#define TASK_INIT_H_

/* Definition of Task Stacks */
#define TASK_STACKSIZE 8192

/* Definition of Task Priorities */
#define TASK_INIT_PRIORITY      1

#endif /* TASK_INIT_H_ */
