/*
 * sfp.h
 *
 *  Created on: Jul 28, 2014
 *      Author: bryerton
 */

#ifndef SFP_H_
#define SFP_H_

#include<stdint.h>

// A0h fields
#define SFP_REG_ID				0
#define SFP_REG_EID				1
#define SFP_REG_CONN			2
#define SFP_REG_TRAN			3 // 3-10
#define SFP_REG_ENC				11
#define SFP_REG_BR				12
#define SFP_REG_RATE_ID			13
#define SFP_REG_LINK_KM_9M		14
#define SFP_REG_LINK_100M_9M	15
#define SFP_REG_LINK_10M_50M	16
#define SFP_REG_LINK_10M_625M	17
#define	SFP_REG_LINK_COPPER		18
#define SFP_REG_LINK_10M_50M_2	19
#define SFP_REG_VENDOR_NAME		20 // 20-35
#define SFP_REG_VENDOR_OUI		37 // 37-39
#define SFP_REG_VENDOR_PN		40 // 40-55
#define SFP_REG_VENDOR_REV		56 // 56-59
#define SFP_REG_WAVELENGTH		60
#define SFP_REG_CC_BASE			63

#define SFP_REG_OPTIONS			64
#define SFP_REG_BR_MAX			66
#define SFP_REG_BR_MIN			67
#define SFP_REG_VENDOR_SN		68 // 68-83
#define SFP_REG_DATECODE		84 // 84-91
#define SFP_REG_DM_TYPE			92
#define SFP_REG_ENHANCED_OPT	93
#define SFP_REG_SFF_COMP		94
#define SFP_REG_CC_EXT			95

// A2h fields
#define SFP_REG_CAL_RX_PWR_4	56 // float
#define SFP_REG_CAL_RX_PWR_3	60 // float
#define SFP_REG_CAL_RX_PWR_2	64 // float
#define SFP_REG_CAL_RX_PWR_1	68 // float
#define SFP_REG_CAL_RX_PWR_0	72 // float
#define SFP_REG_CAL_TX_I_SLOPE	76 // uint16_t
#define SFP_REG_CAL_TX_I_OFFSET 78 // int16_t
#define SFP_REG_CAL_TX_P_SLOPE	80 // uint16_t
#define SFP_REG_CAL_TX_P_OFFSET	82 // int16_t
#define SFP_REG_CAL_TEMP_SLOPE	84 // uint16_t
#define SFP_REG_CAL_TEMP_OFFSET	86 // int16_t
#define SFP_REG_CAL_VOLT_SLOPE	88 // uint16_t
#define SFP_REG_CAL_VOLT_OFFSET	90 // int16_t

#define SFP_REG_TEMP			96
#define SFP_REG_VCC				98
#define SFP_REG_TX_BIAS			100
#define SFP_REG_TX_POWER		102
#define SFP_REG_RX_POWER		104

typedef struct {
	uint8_t transceiver_type;
	uint8_t  connector;
	uint8_t encoding;
	uint32_t br_nominal;
	uint8_t rate_id;
	uint16_t wavelength;
	char	vendor_name[16];
	uint8_t	vendor_oui[3];
	char	vendor_pn[16];
	char 	vendor_rev[4];
	char	vendor_sn[16];
	char	datecode[8];

	uint8_t dmm_ena;
	uint8_t int_cal;
	uint8_t ext_cal;
	uint8_t meas_type;
	uint8_t addr_mode;

	float temp;
	float vcc;
	float tx_bias;
	float tx_power;
	float rx_power;

	float cal_rx_pwr[5];
	uint16_t cal_tx_i_slope;
	uint16_t cal_tx_p_slope;
	uint16_t cal_temp_slope;
	uint16_t cal_volt_slope;

	int16_t cal_tx_i_offset;
	int16_t cal_tx_p_offset;
	int16_t cal_temp_offset;
	int16_t cal_volt_offset;
} tSFPInfo;

void SFP_GetInfo(unsigned int i2c_base, tSFPInfo* sfp_info);
alt_u8 SFP_ReadBuff(alt_u32 i2c_base, alt_u8 address, alt_u8 reg, void* data, alt_u8 num_bytes, alt_u8 isString);
alt_u8 SFP_ReadByte(alt_u32 i2c_base, alt_u8 address, alt_u8 reg);
alt_u8 SFP_WriteByte(alt_u32 i2c_base, alt_u8 address, alt_u8 reg, alt_u8 value);

#endif /* SFP_H_ */
