/*
 * storage_flash.c
 *
 *  Created on: Mar 25, 2017
 *      Author: admin
 */

#include <string.h>
#include "storage_epcq.h"

static eESPERResponse EPCQModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);
static const void* EPCQLoad(tESPERMID mid, const char* key, void* ctx);
static const void* EPCQSave(tESPERMID mid, const char* key, void* ctx);

static void Init(tESPERMID mid, tESPERStorageEPCQ* data);
static void Start(tESPERMID mid, tESPERStorageEPCQ* data);
static void Update(tESPERMID mid, tESPERStorageEPCQ* data);

tESPERStorage* EPCQStorage(const char* dev_name, uint32_t offset, uint32_t length, tESPERStorageEPCQ* ctx) {
	ctx->fd = alt_flash_open_dev(dev_name);
	ctx->dev = (alt_epcq_controller_dev*)ctx->fd;

	ctx->user_offset = offset;
	ctx->user_maxlen = length;

	strlcpy(ctx->device, dev_name, sizeof(ctx->device));
	ctx->base.ModuleHandler = EPCQModuleHandler;
	ctx->base.Load = EPCQLoad;
	ctx->base.Save = EPCQSave;
	ctx->base.ctx = ctx;

	return &ctx->base;
}

static eESPERResponse EPCQModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	tESPERStorageEPCQ* epcq_ctx;

	epcq_ctx = ((tESPERStorage*)ctx)->ctx;

	switch(state) {
	case ESPER_STATE_INIT:
		Init(mid, epcq_ctx);
		break;
	case ESPER_STATE_START:
		Start(mid, epcq_ctx);
		break;
	case ESPER_STATE_UPDATE:
		Update(mid, epcq_ctx);
		break;
	case ESPER_STATE_STOP:
		break;
	}
	return ESPER_RESP_OK;
}

static const void* EPCQLoad(tESPERMID mid, const char* key, void* ctx) {
	return 0;
}

static const void* EPCQSave(tESPERMID mid, const char* key, void* ctx) {
	return 0;
}

static void Init(tESPERMID mid, tESPERStorageEPCQ* ctx) {
	ESPER_CreateVarASCII(mid, "device",			ESPER_OPTION_RD, sizeof(ctx->device), ctx->device, 0, 0);
	ESPER_CreateVarUInt32(mid, "user_offset", 	ESPER_OPTION_RD, 1, &ctx->user_offset, 0, 0);
	ESPER_CreateVarUInt32(mid, "user_maxlen", 	ESPER_OPTION_RD, 1, &ctx->user_maxlen, 0, 0);
	if(ctx->dev) {
		ESPER_CreateVarUInt32(mid, "csr_base", 		ESPER_OPTION_RD, 1, &ctx->dev->csr_base, 0, 0);
		ESPER_CreateVarUInt32(mid, "data_base", 	ESPER_OPTION_RD, 1, &ctx->dev->data_base, 0, 0);
		ESPER_CreateVarUInt32(mid, "data_end", 		ESPER_OPTION_RD, 1, &ctx->dev->data_end, 0, 0);
		ESPER_CreateVarBool(mid, "is_epcs", 		ESPER_OPTION_RD, 1, (uint8_t*)&ctx->dev->is_epcs, 0, 0);
		ESPER_CreateVarUInt32(mid, "num_sectors", 	ESPER_OPTION_RD, 1, &ctx->dev->number_of_sectors, 0, 0);
		ESPER_CreateVarUInt32(mid, "page_size", 	ESPER_OPTION_RD, 1, &ctx->dev->page_size, 0, 0);
		ESPER_CreateVarUInt32(mid, "sector_size", 	ESPER_OPTION_RD, 1, &ctx->dev->sector_size, 0, 0);
		ESPER_CreateVarUInt32(mid, "silicon_id", 	ESPER_OPTION_RD, 1, &ctx->dev->silicon_id, 0, 0);
		ESPER_CreateVarUInt32(mid, "size_in_bytes", ESPER_OPTION_RD, 1, &ctx->dev->size_in_bytes, 0, 0);
	}
}

static void Start(tESPERMID mid, tESPERStorageEPCQ* ctx) {

}

static void Update(tESPERMID mid,tESPERStorageEPCQ* ctx) {

}
