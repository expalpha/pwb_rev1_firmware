/*
 * task_esper.c
 *
 *  Created on: Nov 16, 2016
 *      Author: bryerton
 */

#include <system.h>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "includes.h"

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"
#include "tcpport.h"
#include "net.h"

#include <drivers/inc/board_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/sca_protocol.h>
#include <drivers/inc/24aa02e48.h>

#include "task_esper.h"

#include <esper.h>
#include <esper_nios.h>
#include "modules/mod_remote_update.h"
#include "modules/mod_sfp.h"
#include "modules/mod_ltc2263.h"
#include "modules/mod_sca.h"
#include "modules/mod_lmk04816.h"
#include "modules/mod_sp.h"
#include "modules/mod_uart.h"
#include "modules/mod_http.h"
#include "modules/mod_offload.h"
#include "modules/mod_offload_sata.h"
#include "modules/mod_board.h"
#include "modules/mod_gxb.h"
#include "modules/mod_ethernet.h"
#include "modules/mod_trigger.h"

#include "storage_epcq.h"

// Hack to remove error, thanks altera
u_long inet_addr(char FAR * str);

#define ESPER_MAX_MODULES	64
#define ESPER_MAX_VARS		1024
#define ESPER_MAX_ATTRS		1024

#define REMOTE_UPGRADE_APP_PAGE 0x01000000

static tESPERModule g_modules[ESPER_MAX_MODULES];
static tESPERVar	g_vars[ESPER_MAX_VARS];
static tESPERAttr	g_attrs[ESPER_MAX_ATTRS];

static tESPERModuleBoard 		mod_board_ctx;
static tESPERModuleSFP 			mod_sfp_ctx;
static tESPERModuleLTC2263 		mod_adc_ctx[2];
static tESPERModuleSCA 			mod_sca_ctx[4];
static tESPERModuleLMK04816		mod_cc_ctx;
static tESPERModuleSP 			mod_sp_ctx;
static tESPERModuleEthernet 	mod_eth_ctx;
static tESPERModuleUDPOffload 	mod_udp_ctx;
static tESPERModuleUDPOffload 	mod_offload_sata_ctx;
static tESPERModuleRemoteUpdate mod_remote_ctx;
static tESPERModuleHTTP 		mod_http_ctx;
static tESPERModuleTrigger 		mod_trigger_ctx;
static tESPERModuleGXB			mod_gxb_ctx;
static tESPERStorageEPCQ 		storage_epcq;

static uint8_t* GetMACAddr(void);

static void ltc2263_spi0_write(uint8_t addr, uint8_t reg);
static uint8_t ltc2263_spi0_read(uint8_t addr);
static void ltc2263_spi1_write(uint8_t addr, uint8_t reg);
static uint8_t ltc2263_spi1_read(uint8_t addr);
static void cc_spi_write(uint8_t addr, uint32_t cmd);
static uint32_t cc_spi_read(uint8_t addr);

static uint64_t after_read0(uint8_t addr, uint8_t len);
static uint64_t after_read1(uint8_t addr, uint8_t len);
static uint64_t after_read2(uint8_t addr, uint8_t len);
static uint64_t after_read3(uint8_t addr, uint8_t len);

static void after_write0(uint8_t addr, uint8_t len, uint64_t cmd);
static void after_write1(uint8_t addr, uint8_t len, uint64_t cmd);
static void after_write2(uint8_t addr, uint8_t len, uint64_t cmd);
static void after_write3(uint8_t addr, uint8_t len, uint64_t cmd);

#include "altera_eth_tse_regs.h"
#include "system.h"

static void doctor_mac_settings()
{
   //
   // setup networking for use with the sata link to another PWB:
   //
   // - turn off ALTERA_TSEMAC_CMD_TX_ADDR_INS_MSK - it replaces the MAC address of the other PWB with our MAC address
   // - turn off MAC address filter (promisc mode) - MAC address filter kills packets destined to the other PWB
   // - instead of turning off the MAC address filter, we should write the MAC address of the other PWB into our secondary MAC address register
   //   but we do not know the MAC address of the other PWB yet.
   // - turn off IP forwarding - it will spew packets destined to the other PWB back into the network causing a storm of duplicate packets
   //

   ip_mib.ipForwarding = 2; // turn off IP forwarding since we will be seeing packets that are not for us and we should not be regurgitating them back into the network.

   //printf("MAC register REV:        0x%08x\n", (unsigned int)IORD_ALTERA_TSEMAC_REV(ETH_TSE_BASE));
   uint32_t cmd_config = IORD_ALTERA_TSEMAC_CMD_CONFIG(ETH_TSE_BASE);
   uint32_t cmd_config_wr = cmd_config;
   cmd_config_wr &= (~ALTERA_TSEMAC_CMD_TX_ADDR_INS_MSK);
   cmd_config_wr |= ALTERA_TSEMAC_CMD_PROMIS_EN_MSK;
   //printf("MAC register CMD_CONFIG: 0x%08x -> 0x%08x\n", (unsigned int)cmd_config, (unsigned int)cmd_config_wr);
   IOWR_ALTERA_TSEMAC_CMD_CONFIG(ETH_TSE_BASE, cmd_config_wr);

   printf("networking setup for use with sata pwb cross-link!\n");
}

TK_OBJECT(to_taskesper);
TK_ENTRY(task_esper);

INT8U create_task_esper(INT8U priority) {
	static struct inet_taskinfo task_info;

	INT8U err;

	task_info.entry = task_esper;
	task_info.name = "esper";
	task_info.priority = priority;
	task_info.stacksize = TASK_ESPER_STACKSIZE;
	task_info.tk_ptr = &to_taskesper;

	// Create HTTP network task
	err = TK_NEWTASK(&task_info);

	return err;
}

void task_esper(void* pdata) {
        printf("task_esper!\n");

	doctor_mac_settings();

	printf("after doctoring ethernet MAC settings!\n");

	// Instantiate the clock cleaner module first, so we have all the clocks stable before the board
	mod_cc_ctx.uwire_write = cc_spi_write;
	mod_cc_ctx.uwire_read = cc_spi_read;
	mod_cc_ctx.lmk_stat_ld = (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_LD);
	mod_cc_ctx.lmk_stat_hold = (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_HOLD);
	mod_cc_ctx.lmk_sync_clkin2 = (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_SYNC_CLKIN2);
	mod_cc_ctx.lmk_stat_clkin1 = (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_CLKIN1);
	mod_cc_ctx.lmk_lock_counter = (uint32_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_LOCK_CNT);

	// ADC modules enable SCA power as well, must go before SCAs
	//mod_adc_ctx[0].io_adc_rand_en = (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_ADC_RAND_ENA);
	mod_adc_ctx[0].spi_read = ltc2263_spi0_read;
	mod_adc_ctx[0].spi_write = ltc2263_spi0_write;

	//mod_adc_ctx[1].io_adc_rand_en = (uint8_t*)(GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_ADC_RAND_ENA)+1);
	mod_adc_ctx[1].spi_read = ltc2263_spi1_read;
	mod_adc_ctx[1].spi_write = ltc2263_spi1_write;

        // enable 12-bit randomizer vs 14-bit randomizer
	io_adc_rand_en = (uint32_t*)(GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_ADC_RAND_ENA));

	// Setup Avalon-bus locations for backup link
	mod_gxb_ctx.ctrl_base = GXB_MODULE_LINK_CONTROL_BASE;
	mod_gxb_ctx.stat_base = GXB_MODULE_LINK_STATUS_BASE;

	ESPER_Init("PWB", g_modules, g_vars, g_attrs,  ESPER_MAX_MODULES, ESPER_MAX_VARS, ESPER_MAX_ATTRS, EPCQStorage(EPCQ_CONFIG_AVL_MEM_NAME, 0, 0, &storage_epcq));

	ESPER_CreateModule("clockcleaner",  "Clockcleaner", 		0, LMK04816ModuleHandler, 		LMK04816ModuleInit(&mod_cc_ctx));
	ESPER_CreateModule("board",			"Board", 				0, BoardModuleHandler, 			BoardModuleInit(&mod_board_ctx));
	ESPER_CreateModule("update",		"Update", 				0, RemoteUpdateModuleHandler, 	RemoteUpdateModuleInit(REMOTE_UPDATE_NAME, &mod_remote_ctx));
	ESPER_CreateModule("adc0", 			"ADC0", 				0, ModuleLTC2263Handler, 		ModuleLTC2263Init(&mod_adc_ctx[0]));
	ESPER_CreateModule("adc1", 			"ADC1", 				1, ModuleLTC2263Handler, 		ModuleLTC2263Init(&mod_adc_ctx[1]));
	ESPER_CreateModule("sca0", 			"SCA A", 				0, SCAModuleHandler, 			SCAModuleInit(after_write0, after_read0, &mod_sca_ctx[0]));
	ESPER_CreateModule("sca1", 			"SCA B", 				1, SCAModuleHandler, 			SCAModuleInit(after_write1, after_read1, &mod_sca_ctx[1]));
	ESPER_CreateModule("sca2", 			"SCA C", 				2, SCAModuleHandler, 			SCAModuleInit(after_write2, after_read2, &mod_sca_ctx[2]));
	ESPER_CreateModule("sca3", 			"SCA D", 				3, SCAModuleHandler, 			SCAModuleInit(after_write3, after_read3, &mod_sca_ctx[3]));
	ESPER_CreateModule("trigger",		"Trigger Sources", 		0, ModuleTriggerHandler,		ModuleTriggerInit(TRIGGER_CONTROL_CONTROL_BASE, TRIGGER_CONTROL_STATUS_BASE, TRIGGER_CONTROL_CONTROL_NUM_TRIG_SOURCES, TRIGGER_CONTROL_CONTROL_NUM_BUSY_SOURCES, &mod_trigger_ctx));
	ESPER_CreateModule("signalproc",	"Signal Processing",	0, ModuleSPHandler, 			ModuleSPInit(SIGPROC_CONTROL_BASE, SIGPROC_STATUS_BASE, &mod_sp_ctx));
	ESPER_CreateModule("ethernet", 		"Ethernet Stats", 		0, ModuleEthernetHandler, 		ModuleEthernetInit(ETH_TSE_BASE, &mod_eth_ctx));
	ESPER_CreateModule("sfp", 			"SFP Module", 			0, ModuleSFPHandler, 			ModuleSFPInit(I2C_SFP_BASE | 0x80000000, &mod_sfp_ctx));
	ESPER_CreateModule("offload",		"UDP Offloader", 		0, ModuleUDPOffloadHandler,		ModuleUDPOffloadInit(UDP_STREAM_SCA_0_BASE, GetMACAddr(), inet_addr("192.168.1.1"), 50006, 1001, &mod_udp_ctx));
	ESPER_CreateModule("offload_sata",		"SATA UDP Offloader", 		0, ModuleUDPOffloadSataHandler,		ModuleUDPOffloadSataInit(UDP_STREAM_SATA_BASE, GetMACAddr(), inet_addr("192.168.1.1"), 50006, 1001, &mod_offload_sata_ctx));
	ESPER_CreateModule("http", 			"Web Server", 			0, ModuleHTTPHandler, 			ModuleHTTPInit("ALPHAg PWB", ALTERA_RO_ZIPFS_NAME, 80, &mod_http_ctx));
	ESPER_CreateModule("link", 			"Backup Link", 			0,	ModuleGXBHandler,			ModuleGXBInit(&mod_gxb_ctx));

	ESPER_Start();
	while(ESPER_Update() == ESPER_RESP_OK) {
		OSTimeDly(1);
	}

	ESPER_Stop();
}


static uint8_t* GetMACAddr(void) {
	static uint8_t mac_addr[6];
	static uint8_t init;
	if(!init) {
		init = 1;
		EEPROM_24AA02E48_GetEUI48(I2C_MAC_BASE, (tEUI48*)&mac_addr);
	}

	return mac_addr;
}


static void ltc2263_spi0_write(uint8_t addr, uint8_t reg) {
	alt_u8 tx_data[2];

	tx_data[0] = addr & 0x7F;
	tx_data[1] = reg;
	alt_avalon_spi_command(SPI_ADC1_BASE, 0, 2, tx_data, 0, 0, 0);
}

static uint8_t ltc2263_spi0_read(uint8_t addr) {
 	alt_u8 tx_data[1];
 	alt_u8 rx_data[1];

	tx_data[0] = 0x80 | (addr & 0x7F);
	alt_avalon_spi_command(SPI_ADC1_BASE, 0, 1, tx_data, 1, rx_data, 0);

	return rx_data[0];
}

static void ltc2263_spi1_write(uint8_t addr, uint8_t reg) {
	alt_u8 tx_data[2];

	tx_data[0] = addr & 0x7F;
	tx_data[1] = reg;
	alt_avalon_spi_command(SPI_ADC2_BASE, 0, 2, tx_data, 0, 0, 0);
}

static uint8_t ltc2263_spi1_read(uint8_t addr) {
 	alt_u8 tx_data[1];
 	alt_u8 rx_data[1];

	tx_data[0] = 0x80 | (addr & 0x7F);
	alt_avalon_spi_command(SPI_ADC2_BASE, 0, 1, tx_data, 1, rx_data, 0);

	return rx_data[0];
}

static void cc_spi_write(uint8_t addr, uint32_t cmd) {
	alt_u8 n;
	alt_u8 tx_data[4];

	// Double-write to ensure the command succeeds (datasheet suggested workaround for R0-R4 special write issues)
	for(n=0; n<2; n++) {
		tx_data[0] = (cmd >> 24) & 0xFF;
		tx_data[1] = (cmd >> 16) & 0xFF;
		tx_data[2] = (cmd >>  8) & 0xFF;
		tx_data[3] = (cmd & 0xE0) | (addr & 0x1F);
		alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
		usleep(1);
	}
}

static uint32_t cc_spi_read(uint8_t addr) {
	alt_u8 tx_data[4];
	alt_u8 rx_data[4];

	tx_data[0] = 0;
	tx_data[1] = (addr & 0x1F);
	tx_data[2] = 0;
	tx_data[3] = 0x1F;
	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
	usleep(1);

	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 0, 0, 4, rx_data, 0);
	usleep(1);

	// Due to the timing of the response from the LMK04816, the read bits are come in one bit later than expected, shift everything by one to compensate
	// And the register address is added back in to match what is shown in TIs CodeLoader and Clock Design Tool
	return ((((rx_data[0] << 24) | (rx_data[1] << 16) | (rx_data[2] << 8) | (rx_data[3])) << 1) & 0xFFFFFFE0) | (addr & 0x1F);
}

/*
static AFTER_write after_write[] = {
	after_write0,
	after_write1,
	after_write2,
	after_write3
};

static AFTER_read after_read[] = {
	after_read0,
	after_read1,
	after_read2,
	after_read3
};
*/

static uint64_t after_read0(uint8_t addr, uint8_t len) {	return SCA_Read(SCA_0_CONTROL_BASE, SCA_0_STATUS_BASE, addr, len); }
static uint64_t after_read1(uint8_t addr, uint8_t len) {	return SCA_Read(SCA_1_CONTROL_BASE, SCA_1_STATUS_BASE, addr, len); }
static uint64_t after_read2(uint8_t addr, uint8_t len) {	return SCA_Read(SCA_2_CONTROL_BASE, SCA_2_STATUS_BASE, addr, len); }
static uint64_t after_read3(uint8_t addr, uint8_t len) {	return SCA_Read(SCA_3_CONTROL_BASE, SCA_3_STATUS_BASE, addr, len); }

static void after_write0(uint8_t addr, uint8_t len, uint64_t cmd) { SCA_Write(SCA_0_CONTROL_BASE, SCA_0_STATUS_BASE, addr, len, cmd); }
static void after_write1(uint8_t addr, uint8_t len, uint64_t cmd) { SCA_Write(SCA_1_CONTROL_BASE, SCA_1_STATUS_BASE, addr, len, cmd); }
static void after_write2(uint8_t addr, uint8_t len, uint64_t cmd) { SCA_Write(SCA_2_CONTROL_BASE, SCA_2_STATUS_BASE, addr, len, cmd); }
static void after_write3(uint8_t addr, uint8_t len, uint64_t cmd) { SCA_Write(SCA_3_CONTROL_BASE, SCA_3_STATUS_BASE, addr, len, cmd); }
