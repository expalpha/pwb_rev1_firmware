/*
 * esper_nios.h
 *
 *  Created on: May 26, 2017
 *      Author: admin
 */

#ifndef ESPER_NIOS_H_
#define ESPER_NIOS_H_

#define GET_REG_OFFSET(base,reg) (((uint8_t*)(base) + ((reg)*4)))

#endif /* ESPER_NIOS_H_ */
