/*
 * mod_ethernet.c
 *
 *  Created on: Dec 9, 2016
 *      Author: admin
 */

#include <altera_eth_tse_regs.h>
#include "mod_ethernet.h"

static eESPERResponse Init(tESPERMID mid, tESPERModuleEthernet* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleEthernet* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleEthernet* ctx);

static void ReadTSEStats(uint32_t tse_base, tTSEStats* tse_stats);

tESPERModuleEthernet* ModuleEthernetInit(uint32_t tse_base, tESPERModuleEthernet* ctx) {
	if(!ctx) return 0;

	ctx->tse_base = tse_base;

	return ctx;
}

eESPERResponse ModuleEthernetHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleEthernet*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleEthernet*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleEthernet*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleEthernet* ctx){
	tESPERVID vid;

	vid = ESPER_CreateVarUInt32(mid, "fmtxok", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aFramesTransmittedOK, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Frames Transmitted OK");

	vid = ESPER_CreateVarUInt32(mid, "fmrxok", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aFramesReceivedOK, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Frames Transmitted OK");

	vid = ESPER_CreateVarUInt32(mid, "fmcheckseqerr", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aFramesCheckSequenceErrors, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Frame Check Sequence Errors");

	vid = ESPER_CreateVarUInt32(mid, "alignmentErr", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aAlignmentErrors, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Alignment Errors");

	vid = ESPER_CreateVarUInt32(mid, "octetstxok", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aOctetsTransmittedOK, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Octets Transmitted OK");

	vid = ESPER_CreateVarUInt32(mid, "octetsrxok", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aOctetsReceivedOK, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Octets Received OK");

	vid = ESPER_CreateVarUInt32(mid, "txpmacctrlfm", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aTxPAUSEMACCtrlFrames, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "TX Pause MAC Ctrl Frames");

	vid = ESPER_CreateVarUInt32(mid, "rxpmacctrlfm", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.aRxPAUSEMACCtrlFrames, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "RX Pause MAC Ctrl Frames");

	vid = ESPER_CreateVarUInt32(mid, "inerrors", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifInErrors, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "In Errors");

	vid = ESPER_CreateVarUInt32(mid, "outerrors", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifOutErrors, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Out Errors");

	vid = ESPER_CreateVarUInt32(mid, "inucastpkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifInUcastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "In Unicast Packets");

	vid = ESPER_CreateVarUInt32(mid, "inmulticastpkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifInMulticastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "In Multicast Packets");

	vid = ESPER_CreateVarUInt32(mid, "inbroadcastpkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifInBroadcastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "In Broadcast Packets");

	vid = ESPER_CreateVarUInt32(mid, "outdiscards", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifOutDiscards, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Out Discards");

	vid = ESPER_CreateVarUInt32(mid, "outucastpkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifOutUcastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Out Unicast Packets");

	vid = ESPER_CreateVarUInt32(mid, "outmulticastpkt", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifOutMulticastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Out Multicast Packets");

	vid = ESPER_CreateVarUInt32(mid, "outbroadcastpkt", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.ifOutBroadcastPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Out Broadcast Packets");

	vid = ESPER_CreateVarUInt32(mid, "dropevent", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsDropEvent, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Dropped Events");

	vid = ESPER_CreateVarUInt32(mid, "octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsOctets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Octets");

	vid = ESPER_CreateVarUInt32(mid, "etherstatspkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Packets");

	vid = ESPER_CreateVarUInt32(mid, "usizepkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsUndersizePkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Undersized Packets");

	vid = ESPER_CreateVarUInt32(mid, "etherstatsopkts", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsOversizePkts, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Oversized Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts64octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts64Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "64 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts65octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts65to127Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "65-127 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts128octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts128to255Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "128-255 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts256octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts256to511Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "256-511 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts512octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts512to1023Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "512-1023 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts1024octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts1024to1518Octets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "1024-1518 Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "pkts1519octets", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsPkts1519toXOctets, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "1519-X Octet Packets");

	vid = ESPER_CreateVarUInt32(mid, "jabbers", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsJabbers, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Jabbers");

	vid = ESPER_CreateVarUInt32(mid, "fragments", 	ESPER_OPTION_RD, 1, &ctx->tse_stats.etherStatsFragments, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Fragments");



	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleEthernet* ctx) {
	ReadTSEStats(ctx->tse_base, &ctx->tse_stats);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleEthernet* ctx){
	ReadTSEStats(ctx->tse_base, &ctx->tse_stats);
	ESPER_TouchModule(mid);

	return ESPER_RESP_OK;
}

static void ReadTSEStats(uint32_t tse_base, tTSEStats* tse_stats) {
	tse_stats->aFramesTransmittedOK			  	= IORD_ALTERA_TSEMAC_A_FRAMES_TX_OK							  (tse_base);
	tse_stats->aFramesReceivedOK                = IORD_ALTERA_TSEMAC_A_FRAMES_RX_OK							  (tse_base);
	tse_stats->aFramesCheckSequenceErrors       = IORD_ALTERA_TSEMAC_A_FRAME_CHECK_SEQ_ERRS					  (tse_base);
	tse_stats->aAlignmentErrors                 = IORD_ALTERA_TSEMAC_A_ALIGNMENT_ERRS                           (tse_base);
	tse_stats->aOctetsTransmittedOK             = IORD_ALTERA_TSEMAC_A_OCTETS_TX_OK                             (tse_base);
	tse_stats->aOctetsReceivedOK                = IORD_ALTERA_TSEMAC_A_OCTETS_RX_OK                             (tse_base);
	tse_stats->aTxPAUSEMACCtrlFrames            = IORD_ALTERA_TSEMAC_A_TX_PAUSE_MAC_CTRL_FRAMES                 (tse_base);
	tse_stats->aRxPAUSEMACCtrlFrames            = IORD_ALTERA_TSEMAC_A_RX_PAUSE_MAC_CTRL_FRAMES                 (tse_base);
	tse_stats->ifInErrors                       = IORD_ALTERA_TSEMAC_IF_IN_ERRORS                               (tse_base);
	tse_stats->ifOutErrors                      = IORD_ALTERA_TSEMAC_IF_OUT_ERRORS                              (tse_base);
	tse_stats->ifInUcastPkts                    = IORD_ALTERA_TSEMAC_IF_IN_UCAST_PKTS                           (tse_base);
	tse_stats->ifInMulticastPkts                = IORD_ALTERA_TSEMAC_IF_IN_MULTICAST_PKTS                       (tse_base);
	tse_stats->ifInBroadcastPkts                = IORD_ALTERA_TSEMAC_IF_IN_BROADCAST_PKTS                       (tse_base);
	tse_stats->ifOutDiscards                    = IORD_ALTERA_TSEMAC_IF_OUT_DISCARDS                            (tse_base);
	tse_stats->ifOutUcastPkts                   = IORD_ALTERA_TSEMAC_IF_OUT_UCAST_PKTS                          (tse_base);
	tse_stats->ifOutMulticastPkts               = IORD_ALTERA_TSEMAC_IF_IN_MULTICAST_PKTS                       (tse_base);
	tse_stats->ifOutBroadcastPkts               = IORD_ALTERA_TSEMAC_IF_IN_BROADCAST_PKTS                       (tse_base);
	tse_stats->etherStatsDropEvent              = IORD_ALTERA_TSEMAC_ETHER_STATS_DROP_EVENTS                    (tse_base);
	tse_stats->etherStatsOctets                 = IORD_ALTERA_TSEMAC_ETHER_STATS_OCTETS                         (tse_base);
	tse_stats->etherStatsPkts                   = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS                           (tse_base);
	tse_stats->etherStatsUndersizePkts          = IORD_ALTERA_TSEMAC_ETHER_STATS_UNDERSIZE_PKTS                 (tse_base);
	tse_stats->etherStatsOversizePkts           = IORD_ALTERA_TSEMAC_ETHER_STATS_OVERSIZE_PKTS                  (tse_base);
	tse_stats->etherStatsPkts64Octets           = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_64_OCTETS                 (tse_base);
	tse_stats->etherStatsPkts65to127Octets      = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_65_TO_127_OCTETS          (tse_base);
	tse_stats->etherStatsPkts128to255Octets     = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_128_TO_255_OCTETS         (tse_base);
	tse_stats->etherStatsPkts256to511Octets     = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_256_TO_511_OCTETS         (tse_base);
	tse_stats->etherStatsPkts512to1023Octets    = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_512_TO_1023_OCTETS        (tse_base);
	tse_stats->etherStatsPkts1024to1518Octets   = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_1024_TO_1518_OCTETS       (tse_base);
	tse_stats->etherStatsPkts1519toXOctets      = IORD_ALTERA_TSEMAC_ETHER_STATS_PKTS_1519_TO_X_OCTETS          (tse_base);
	tse_stats->etherStatsJabbers                = IORD_ALTERA_TSEMAC_ETHER_STATS_JABBERS                        (tse_base);
	tse_stats->etherStatsFragments              = IORD_ALTERA_TSEMAC_ETHER_STATS_FRAGMENTS                      (tse_base);
}

