/*
 * adc32.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <esper.h>
#include <drivers/inc/board_regs.h>
#include "mod_ltc2263.h"

uint32_t *io_adc_rand_en = NULL;

#define GET_REG_OFFSET(base,reg) (((uint8_t*)(base) + ((reg)*4)))

static void Init(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* data);
static void Start(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* data);
static void Update(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* data);

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

eESPERResponse ModuleLTC2263Handler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		Init(mid, gid, (tESPERModuleLTC2263*)ctx);
		break;
	case ESPER_STATE_START:
		Start(mid, gid, (tESPERModuleLTC2263*)ctx);
		break;
	case ESPER_STATE_UPDATE:
		Update(mid, gid, (tESPERModuleLTC2263*)ctx);
		break;
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

tESPERModuleLTC2263* ModuleLTC2263Init(tESPERModuleLTC2263* ctx) {
	if(!ctx) return 0;
	if(!ctx->spi_write) return 0;
	if(!ctx->spi_read) return 0;

	return ctx;
}

static void Init(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* ctx) {
	tESPERVID vid;

	vid = ESPER_CreateVarBool(mid, "adc_14_bit", 		ESPER_OPTION_WR_RD, 1, &ctx->regmap.adc_14_bit, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC is 14 bit");

	vid = ESPER_CreateVarBool(mid, "dcs_disable",		ESPER_OPTION_WR_RD, 1, &ctx->regmap.dcs_disable, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Disable Duty Cycle Stabilizer");

	vid = ESPER_CreateVarBool(mid, "rand_enable", 		ESPER_OPTION_WR_RD, 1, &ctx->regmap.randomizer_en, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Output Randomizer");

	vid = ESPER_CreateVarUInt8(mid, "data_format", 		ESPER_OPTION_WR_RD, 1, &ctx->regmap.data_format, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Data Format");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Offset Binary", LTC2263_DATA_FORMAT_OFFSET_BINARY);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Two's compliment", LTC2263_DATA_FORMAT_TWOS_COMP);

	vid = ESPER_CreateVarUInt8(mid, "sleep_mode", 		ESPER_OPTION_WR_RD, 1, &ctx->regmap.sleep_mode, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Sleep/Nap Mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Normal", LTC2263_SLEEP_MODE_NORMAL);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Channel 1 Nap", LTC2263_SLEEP_MODE_NAP_CHANNEL1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Channel 2 Nap", LTC2263_SLEEP_MODE_NAP_CHANNEL2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Sleep", LTC2263_SLEEP_MODE_SLEEP);

	vid = ESPER_CreateVarUInt8(mid, "lvds_current",		ESPER_OPTION_WR_RD, 1, &ctx->regmap.lvds_current, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "LVDS Output Current");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1.75 mA", LTC2263_LVDS_CURRENT_1_75);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2.1 mA", LTC2263_LVDS_CURRENT_2_1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2.5 mA", LTC2263_LVDS_CURRENT_2_5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "3.0 mA", LTC2263_LVDS_CURRENT_3_0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "3.5 mA", LTC2263_LVDS_CURRENT_3_5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4.0 mA", LTC2263_LVDS_CURRENT_4_0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4.5 mA", LTC2263_LVDS_CURRENT_4_5);

	vid = ESPER_CreateVarBool(mid, "lvds_term",			ESPER_OPTION_WR_RD, 1, &ctx->regmap.lvds_term_en, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "LVDS Termination");

	vid = ESPER_CreateVarBool(mid, "output_disable",	ESPER_OPTION_WR_RD, 1, &ctx->regmap.output_disable, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Disable LVDS Output");

	vid = ESPER_CreateVarUInt8(mid, "output_mode",		ESPER_OPTION_WR_RD, 1, &ctx->regmap.output_mode, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "LVDS Output Mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "2 Lane, 16-bit", LTC2263_DIGITAL_OUTPUT_MODE_2LANE_16BIT);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2 Lane, 14-bit", LTC2263_DIGITAL_OUTPUT_MODE_2LANE_14BIT);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2 Lane, 12-bit", LTC2263_DIGITAL_OUTPUT_MODE_2LANE_12BIT);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1 Lane, 14-bit", LTC2263_DIGITAL_OUTPUT_MODE_1LANE_14BIT);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1 Lane, 12-bit", LTC2263_DIGITAL_OUTPUT_MODE_1LANE_12BIT);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1 Lane, 16-bit", LTC2263_DIGITAL_OUTPUT_MODE_1LANE_16BIT);

	vid = ESPER_CreateVarBool(mid, "test_enable",		ESPER_OPTION_WR_RD, 1, &ctx->regmap.test_en, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Test Mode");

	vid = ESPER_CreateVarUInt16(mid, "test_pattern",	ESPER_OPTION_WR_RD, 1, &ctx->regmap.test_pattern, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Test Pattern");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");
}

static void Start(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* ctx) {
	LTC2263_Reset( ctx->spi_write);
	usleep(100000);
	ctx->regmap.adc_14_bit = 0;
	ctx->regmap.dcs_disable = LTC2263_DUTY_CYCLE_STABILIZER_ON;
	ctx->regmap.randomizer_en = LTC2263_DATA_OUTPUT_RANDOMIZER_ON;
	ctx->regmap.lvds_current = LTC2263_LVDS_CURRENT_3_5;
	ctx->regmap.lvds_term_en	= LTC2263_LVDS_TERMINATION_OFF;
	ctx->regmap.data_format = LTC2263_DATA_FORMAT_TWOS_COMP;
	ctx->regmap.output_disable = LTC2263_DIGITAL_OUTPUT_ENABLED;
	ctx->regmap.output_mode = LTC2263_DIGITAL_OUTPUT_MODE_2LANE_16BIT;
	ctx->regmap.sleep_mode = LTC2263_SLEEP_MODE_NORMAL;
	ctx->regmap.test_en = LTC2263_DIGITAL_TEST_PATTERN_OFF;
	ctx->regmap.test_pattern = 0x0000;

	// If the randomizer is on, this bit must be set to XOR the data in the SERDES
	if(ctx->regmap.randomizer_en == LTC2263_DATA_OUTPUT_RANDOMIZER_ON) {
           if (ctx->regmap.adc_14_bit == 0) {
              *io_adc_rand_en = 0x0001; // 12-bit randomizer
           } else {
              *io_adc_rand_en = 0x0100; // 14-bit randomizer
           }
	} else {
              *io_adc_rand_en = 0x0000; // randomizer turned off
	}

	LTC2263_WriteSettings( ctx->spi_write, &ctx->regmap );


	// Reset counters after ADCs are configured
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_RESET_COUNTERS) = 1;
	usleep(1);
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_RESET_COUNTERS) = 0;
}

static void Update(tESPERMID mid, tESPERGID gid, tESPERModuleLTC2263* ctx) {

}

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleLTC2263* adc_ctx = (tESPERModuleLTC2263*)ctx;

	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		LTC2263_WriteSettings( adc_ctx->spi_write, &adc_ctx->regmap );
		// If the randomizer is on, this bit must be set to XOR the data in the SERDES
		if(adc_ctx->regmap.randomizer_en == LTC2263_DATA_OUTPUT_RANDOMIZER_ON) {
                   if (adc_ctx->regmap.adc_14_bit == 0) {
                      *io_adc_rand_en = 0x0001; // 12-bit randomizer
                   } else {
                      *io_adc_rand_en = 0x0100; // 14-bit randomizer
                   }
		} else {
                      *io_adc_rand_en = 0x0000; // randomizer turned off
		}
		break;
	default:
		break;
	}

	return 1;
}
