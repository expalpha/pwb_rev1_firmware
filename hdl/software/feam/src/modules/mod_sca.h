/*
 * mod_sca.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MOD_SCA_H_
#define MOD_SCA_H_

#include "../AFTER_slowcontrol.h"

typedef struct {
	uint8_t verify_write;
	uint8_t verify_ok;
	uint32_t write_counter;
	AFTER_write sca_write;
	AFTER_read  sca_read;
	tAFTER_Registers sca_regmap;
} tESPERModuleSCA;

tESPERModuleSCA* SCAModuleInit(AFTER_write sca_write, AFTER_read sca_read, tESPERModuleSCA* ctx);
eESPERResponse SCAModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);


#endif /* MOD_SCA_H_ */
