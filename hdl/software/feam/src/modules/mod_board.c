/*
 * mod_board.c
 *
 *  Created on: Apr 2, 2017
 *      Author: admin
 */

#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <esper_nios.h>
#include "mod_board.h"
#include <drivers/inc/board_regs.h>
#include <altera_avalon_sysid_qsys.h>
#include <altera_avalon_sysid_qsys_regs.h>
#include <build_number.h>
#include <ltc2983/ltc2983.h>
#include <ltc2983/ltc2983_constants.h>
#include <drivers/inc/24aa02e48.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <custom_altera_avalon_spi.h>

static eESPERResponse Init(tESPERMID mid, tESPERModuleBoard* data);
static eESPERResponse Start(tESPERMID mid, tESPERModuleBoard* data);
static eESPERResponse Update(tESPERMID mid, tESPERModuleBoard* data);

static uint16_t mv2_write(uint8_t addr, uint8_t data);
static uint8_t mv2_read(uint8_t addr);

static uint8_t ADCPwrHandle(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static uint8_t MV2Handle(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static void InitLTC2983(void);

void XMV2_WriteSettings(tESPERModuleBoard* ctx)
{
   MV2_WriteSettings(mv2_write, &ctx->mv2);
   mv2_settings rmv2;
   MV2_ReadSettings(mv2_read, &rmv2);
   ctx->mv2_verify_ok = (0 == memcmp(&ctx->mv2, &rmv2, sizeof(mv2_settings)));
}

tESPERModuleBoard* BoardModuleInit(tESPERModuleBoard* ctx) {
	if(!ctx) return 0;

	strlcpy(ctx->elf_build_str, VERSION_STR, SYS_VAR_BUILD_STR_LEN);
	strlcpy(ctx->elf_build_user, BUILT_BY_USER_STR, SYS_VAR_BUILD_USER_LEN);
	strlcpy(ctx->elf_build_email, BUILT_BY_EMAIL_STR, SYS_VAR_BUILD_EMAIL_LEN);
	ctx->elf_buildnumber = BUILD_NUMBER;
	ctx->elf_buildtime = BUILD_TIMESTAMP;
	strlcpy(ctx->git_branch, GIT_BRANCH_STR, SYS_VAR_BUILD_GIT_BRANCH_LEN);
	strlcpy(ctx->git_hash, GIT_HASH_STR, SYS_VAR_BUILD_GIT_HASH_LEN);
	ctx->sw_qsys_sysid = SYSID_ID;
	ctx->sw_qsys_timestamp = SYSID_TIMESTAMP;
	ctx->hw_qsys_sysid = IORD_ALTERA_AVALON_SYSID_QSYS_ID(SYSID_BASE);
	ctx->hw_qsys_timestamp = IORD_ALTERA_AVALON_SYSID_QSYS_TIMESTAMP(SYSID_BASE);
	ctx->hw_sw_qsys_match = (alt_avalon_sysid_qsys_test() == 0) ? 1 : 0;

	MV2_ReadSettings(mv2_read, &ctx->mv2);

	ctx->mv2_enabled = 0;
	ctx->mv2_verify_ok = 0;
        ctx->mv2_counter = 0;
        ctx->mv2_skip_counter = 0;

	ctx->mv2_data[0] = 0;
	ctx->mv2_data[1] = 0;
	ctx->mv2_data[2] = 0;
	ctx->mv2_data[3] = 0;
	ctx->mv2_data[4] = 0;

	ctx->mv2.ma = 0;
	ctx->mv2.hc = 1;
	ctx->mv2.emr = 1;
	ctx->mv2.inv = 0;
	ctx->mv2.lmr = 0;
	ctx->mv2.os = 0;
	ctx->mv2.ra = 0;
	ctx->mv2.re = 3;
	ctx->mv2.tc = 1;
	ctx->mv2.po = 1;
	ctx->mv2.lp = 1;
	ctx->mv2.sp = 1;

	MV2_WriteSettings(mv2_write, &ctx->mv2);
	//XMV2_WriteSettings(ctx);

	return ctx;
}

eESPERResponse BoardModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {

	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleBoard*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleBoard* ctx) {
	tESPERVID vid;

	vid = ESPER_CreateVarASCII(mid, "elf_build_str", ESPER_OPTION_RD, SYS_VAR_BUILD_STR_LEN, ctx->elf_build_str, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Build String");

	//vid = ESPER_CreateVarASCII(mid, "elf_build_user", ESPER_OPTION_RD, SYS_VAR_BUILD_USER_LEN, ctx->elf_build_user, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "ELF Build User");

	//vid = ESPER_CreateVarASCII(mid, "elf_build_email", ESPER_OPTION_RD, SYS_VAR_BUILD_EMAIL_LEN, ctx->elf_build_email, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Email");

	//vid = ESPER_CreateVarUInt32(mid, "elf_buildnum", ESPER_OPTION_RD, 1, &ctx->elf_buildnumber, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Number");

	vid = ESPER_CreateVarUInt32(mid, "elf_buildtime", ESPER_OPTION_RD, 1, &ctx->elf_buildtime, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Build Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarUInt32(mid, "elf_timestamp", ESPER_OPTION_RD, 1, &ctx->elf_buildtime, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ELF Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarASCII(mid, "git_branch", ESPER_OPTION_RD, SYS_VAR_BUILD_GIT_BRANCH_LEN, ctx->git_branch, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "GIT Branch");

	vid = ESPER_CreateVarASCII(mid, "git_hash", ESPER_OPTION_RD, SYS_VAR_BUILD_GIT_HASH_LEN, ctx->git_hash, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "GIT Commit Hash");

	vid = ESPER_CreateVarUInt32(mid, "quartus_buildtime", ESPER_OPTION_RD, 1, &ctx->quartus_buildtime, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_FPGA_TIMESTAMP), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Quartus Build Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarUInt32(mid, "sof_timestamp", ESPER_OPTION_RD, 1, &ctx->quartus_buildtime, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_FPGA_TIMESTAMP), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SOF Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sw_qsys_sysid", ESPER_OPTION_RD, 1, &ctx->sw_qsys_sysid, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SW QSYS Sysid");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sw_qsys_ts", ESPER_OPTION_RD, 1, &ctx->sw_qsys_timestamp, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SW QSYS Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarUInt32(mid, "hw_qsys_sysid", ESPER_OPTION_RD, 1, &ctx->hw_qsys_sysid, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW QSYS Sysid");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "hw_qsys_ts", ESPER_OPTION_RD, 1, &ctx->hw_qsys_timestamp, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW QSYS Timestamp");
	ESPER_CreateAttrNull(mid, vid, "format", "timestamp");

	vid = ESPER_CreateVarBool(mid, "hw_sw_match", ESPER_OPTION_RD, 1, &ctx->hw_sw_qsys_match, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "HW/SW Match");

	vid = ESPER_CreateVarASCII(mid, "mac_addr", ESPER_OPTION_RD, 17, ctx->mac_addr_str, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MAC Address");

	vid = ESPER_CreateVarBool(mid, "invert_ext_trig", 	ESPER_OPTION_WR_RD, 1, &ctx->invert_ext_trig, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_INVERT_EXT_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Invert External Trigger");

	vid = ESPER_CreateVarBool(mid, "adc_pwr_en", 	ESPER_OPTION_WR_RD, 2, &ctx->adc_pwr_en[0], (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_ADC_PWR_EN), ADCPwrHandle);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC/SCA Power");

	vid = ESPER_CreateVarBool(mid, "ext_test_pulse", 		ESPER_OPTION_WR_RD, 1, &ctx->ext_test_pulse, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_EXT_TEST_PULSE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Test board pulse");

	vid = ESPER_CreateVarBool(mid, "reset_counters", 		ESPER_OPTION_WR_RD, 1, &ctx->reset_counters, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_RESET_COUNTERS), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Reset Counters");

	vid = ESPER_CreateVarBool(mid, "reset_nios", 		ESPER_OPTION_WR_RD, 1, &ctx->use_sata_gxb, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_USE_SATA_GXB), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Reset QSYS and NIOS CPU");

	vid = ESPER_CreateVarBool(mid, "serdes_locked",		ESPER_OPTION_RD, 1, &ctx->serdes_locked, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_ADC_LOCKED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC SERDES Locked");

	vid = ESPER_CreateVarUInt32(mid, "serdes_lock_cnt",		ESPER_OPTION_RD, 2, &ctx->cnt_locked[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_ADC_LOCKED_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SERDES Locked Counter");

	vid = ESPER_CreateVarBool(mid, "serdes_aligned",		ESPER_OPTION_RD, 1, &ctx->serdes_aligned, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_ADC_ALIGNED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC SERDES Aligned");

	vid = ESPER_CreateVarUInt32(mid, "serdes_align_cnt",		ESPER_OPTION_RD, 2, &ctx->cnt_aligned[0], (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_ADC_ALIGNED_CNT0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SERDES Aligned Counter");

	vid = ESPER_CreateVarUInt64(mid, "chip_id", 			ESPER_OPTION_RD, 1, &ctx->chipid, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CHIPID_LO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "FPGA Chip ID");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "freq_serdes1",		ESPER_OPTION_RD, 1, &ctx->freq_serdes0, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_SERDES0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC0 SERDES");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_serdes2",		ESPER_OPTION_RD, 1, &ctx->freq_serdes1, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_SERDES1), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC1 SERDES");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_io",			ESPER_OPTION_RD, 1, &ctx->freq_io, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_IO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "IO Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_sca_wr",		ESPER_OPTION_RD, 1, &ctx->freq_sca_wr, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_SCA_WR), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Write Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_sca_rd",		ESPER_OPTION_RD, 1, &ctx->freq_sca_rd, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_SCA_RD), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Read Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_sfp",			ESPER_OPTION_RD, 1, &ctx->freq_sfp, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_ETH), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Recovered SFP Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarUInt32(mid, "freq_sata",			ESPER_OPTION_RD, 1, &ctx->freq_sata, (uint8_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_XCVR), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Recovered Backup Link Frequency");
	ESPER_CreateAttrNull(mid, vid, "format", "freq");

	vid = ESPER_CreateVarFloat32(mid, "temp_sca_a", ESPER_OPTION_RD, 1, &ctx->temp_scaA, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA A Temp");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "temp_sca_b", ESPER_OPTION_RD, 1, &ctx->temp_scaB, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA B Temp");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "temp_sca_c", ESPER_OPTION_RD, 1, &ctx->temp_scaC, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA C Temp");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "temp_sca_d", ESPER_OPTION_RD, 1, &ctx->temp_scaD, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA D Temp");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "temp_board", ESPER_OPTION_RD, 1, &ctx->temp_board, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Board Temp");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "C");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "i_sca12", ESPER_OPTION_RD, 1, &ctx->i_sca12, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA12 Current");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "I");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "i_sca34", ESPER_OPTION_RD, 1, &ctx->i_sca34, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA34 Current");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "I");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "i_p2", ESPER_OPTION_RD, 1, &ctx->i_p2, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "2V Current");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "I");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "i_p5", ESPER_OPTION_RD, 1, &ctx->i_p5, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "5V Current");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "I");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "v_sca12", ESPER_OPTION_RD, 1, &ctx->v_sca12, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA12 Voltage");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "V");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "v_sca34", ESPER_OPTION_RD, 1, &ctx->v_sca34, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA34 Voltage");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "V");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "v_p2", ESPER_OPTION_RD, 1, &ctx->v_p2, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "2V Voltage");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "V");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarFloat32(mid, "v_p5", ESPER_OPTION_RD, 1, &ctx->v_p5, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "5V Voltage");
	ESPER_CreateAttrASCII(mid, vid, "si", "unit", "V");
	ESPER_CreateAttrSInt8(mid, vid, "si", "prefix", 0);
	ESPER_CreateAttrUInt8(mid, vid, "si", "precision", 2); // number of decimal places

	vid = ESPER_CreateVarUInt32(mid, "raw_data", ESPER_OPTION_RD, 13, ctx->raw, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "raw data");
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarBool(mid, "mv2_enable", ESPER_OPTION_WR_RD, 1, &ctx->mv2_enabled, 0, MV2Handle); // setup to resend settings on enable/disable
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Enabled");

	vid = ESPER_CreateVarBool(mid, "mv2_verify_ok", ESPER_OPTION_RD, 1, &ctx->mv2_verify_ok, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Verify Ok");

	vid = ESPER_CreateVarUInt8(mid, "mv2_range", ESPER_OPTION_WR_RD, 1, &ctx->mv2.ra, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Range");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 100mT", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 300mT", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 1 T", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "+/- 3 T", 3);

	vid = ESPER_CreateVarUInt8(mid, "mv2_res", ESPER_OPTION_WR_RD, 1, &ctx->mv2.re, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Resolution");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "14 bits / 3 kHz", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "15 bits / 1.5 kHz", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "16 bits / 0.75 kHz", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "16 bits / 0.375 kHz", 3);

	vid = ESPER_CreateVarUInt8(mid, "mv2_axis", ESPER_OPTION_WR_RD, 1, &ctx->mv2.ma, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Measurement Axis");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "3 Axis", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "X Axis", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Y Axis", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Z Axis", 3);

	vid = ESPER_CreateVarBool(mid, "mv2_lowpow", ESPER_OPTION_WR_RD, 1, &ctx->mv2.lp, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Low Power");

	vid = ESPER_CreateVarBool(mid, "mv2_highclk", ESPER_OPTION_WR_RD, 1, &ctx->mv2.hc, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 High Clock");

	vid = ESPER_CreateVarBool(mid, "mv2_invert", ESPER_OPTION_WR_RD, 1, &ctx->mv2.inv, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Invert");

	vid = ESPER_CreateVarBool(mid, "mv2_emr", ESPER_OPTION_WR_RD, 1, &ctx->mv2.emr, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Ext. Measurement Range");

	vid = ESPER_CreateVarBool(mid, "mv2_lmr", ESPER_OPTION_WR_RD, 1, &ctx->mv2.lmr, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Large Measurement Range");

	vid = ESPER_CreateVarUInt8(mid, "mv2_tc", ESPER_OPTION_WR_RD, 1, &ctx->mv2.tc, 0, MV2Handle);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Temperature Comp.");
	ESPER_CreateAttrUInt8(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt8(mid, vid, "limit", "max", 15);

	vid = ESPER_CreateVarUInt16(mid, "mv2_xaxis", ESPER_OPTION_RD, 1, &ctx->mv2_xaxis, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 X Axis");

	vid = ESPER_CreateVarUInt16(mid, "mv2_yaxis", ESPER_OPTION_RD, 1, &ctx->mv2_yaxis, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Y Axis");

	vid = ESPER_CreateVarUInt16(mid, "mv2_zaxis", ESPER_OPTION_RD, 1, &ctx->mv2_zaxis, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Z Axis");

	vid = ESPER_CreateVarUInt16(mid, "mv2_taxis", ESPER_OPTION_RD, 1, &ctx->mv2_taxis, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 T Axis");

	vid = ESPER_CreateVarUInt16(mid, "mv2_data", ESPER_OPTION_RD, 5, ctx->mv2_data, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Data");

	vid = ESPER_CreateVarUInt32(mid, "mv2_counter", ESPER_OPTION_RD, 1, &ctx->mv2_counter, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "MV2 Counter");

	vid = ESPER_CreateVarBool(mid, "tpulse_trig", ESPER_OPTION_WR_RD, 1, &ctx->test_pulse_trigger, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_EXT_TEST_PULSE_ON_TRIGGER), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Test Pulse On Trigger");

	vid = ESPER_CreateVarBool(mid, "tgate_ena", ESPER_OPTION_WR_RD, 1, &ctx->test_gate_ena, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_EXT_GATE_ENA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Gate on Test Pulse");

	vid = ESPER_CreateVarUInt16(mid, "tpulse_width", ESPER_OPTION_WR_RD, 1, &ctx->test_pulse_width, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_EXT_TEST_PULSE_WIDTH), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Test Pulse Width");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 65535);

	vid = ESPER_CreateVarUInt16(mid, "tgate_width", ESPER_OPTION_WR_RD, 1, &ctx->test_gate_width, (uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_EXT_GATE_WIDTH), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Gate Width");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 65535);

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleBoard* ctx) {

	// Turn on both to start
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "adc_pwr_en"), 0, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "adc_pwr_en"), 1, 1);

	InitLTC2983();

	EEPROM_24AA02E48_GetEUI48(I2C_MAC_BASE, (tEUI48*)&ctx->mac_addr);

	*(uint32_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_MAC_ADDR_LO) = ctx->mac_addr[2] | (ctx->mac_addr[3] << 8 ) | (ctx->mac_addr[4] << 16 ) | (ctx->mac_addr[5] << 24 );
	*(uint32_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_MAC_ADDR_HI) = ctx->mac_addr[0] | (2 << 8 );

	sprintf(ctx->mac_addr_str, "%02X:%02X:%02X:%02X:%02X:%02X", ctx->mac_addr[0], ctx->mac_addr[1], ctx->mac_addr[2], ctx->mac_addr[3], ctx->mac_addr[4], ctx->mac_addr[5] );
	ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mac_addr"));

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "tgate_ena"), 0, 1);
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "tpulse_width"), 0, 256);
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "tgate_width"), 0, 256);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleBoard* ctx) {
	uint32_t channel_assignment_data;
	uint16_t axis_value;
	static uint8_t temp_voltage;

        if(ctx->mv2_enabled) {
           ctx->mv2_skip_counter++;
           if ((ctx->mv2_skip_counter%5) == 0) {
			axis_value = MV2_ReadAxis( mv2_write, SPI_MV2_MAGNETOMETER_AXIS_X, &ctx->mv2);
			ctx->mv2_taxis = axis_value;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_taxis"));

			axis_value = MV2_ReadAxis( mv2_write, SPI_MV2_MAGNETOMETER_AXIS_Y, &ctx->mv2);
			ctx->mv2_xaxis = axis_value;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_xaxis"));

			axis_value = MV2_ReadAxis( mv2_write, SPI_MV2_MAGNETOMETER_AXIS_Z, &ctx->mv2);
			ctx->mv2_yaxis = axis_value;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_yaxis"));

			axis_value = MV2_ReadAxis( mv2_write, SPI_MV2_MAGNETOMETER_AXIS_T, &ctx->mv2);
			ctx->mv2_zaxis = axis_value;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_zaxis"));

                        ctx->mv2_data[0] = ctx->mv2_xaxis;
                        ctx->mv2_data[1] = ctx->mv2_yaxis;
                        ctx->mv2_data[2] = ctx->mv2_zaxis;
                        ctx->mv2_data[3] = ctx->mv2_taxis;
                        ctx->mv2_data[4] = ctx->mv2_counter;

			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_data"));

                        ctx->mv2_counter++;

			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_counter"));
           }
        }

	// Update these once a second
	if(!ltc2983_is_converting(SPI_TEMP_BASE, 0)) {

		if(!temp_voltage) {
                   ctx->temp_scaA = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 5, TEMPERATURE, ctx->raw+0);      // Ch 4: RTD PT-100
                   ctx->temp_scaB = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 3, TEMPERATURE, ctx->raw+1);      // Ch 6: RTD PT-100
                   ctx->temp_scaC = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 7, TEMPERATURE, ctx->raw+2);      // Ch 8: RTD PT-100
                   ctx->temp_scaD = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 9, TEMPERATURE, ctx->raw+3);      // Ch 10: RTD PT-100
                   ctx->temp_board = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 11, TEMPERATURE, ctx->raw+4);    // Ch 12: RTD PT-100
                   
                   ctx->i_p5 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 14, VOLTAGE, ctx->raw+5) * 40000;      // Ch 14: RTD PT-100
                   ctx->i_p2 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 16, VOLTAGE, ctx->raw+6) * 10000;      // Ch 16: RTD PT-100
                   ctx->i_sca12 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 18, VOLTAGE, ctx->raw+7) * 10000;   // Ch 18: RTD PT-100
                   ctx->i_sca34 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 20, VOLTAGE, ctx->raw+8) * 10000;   // Ch 20: RTD PT-100
                   
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "temp_sca_a"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "temp_sca_b"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "temp_sca_c"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "temp_sca_d"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "temp_board"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "i_sca12"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "i_sca34"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "i_p2"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "i_p5"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "raw_data"));

			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF5, 0x0A); // channel 20,18
			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF6, 0xA0); // channel 16,14,11,9
			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF7, 0x00); // channel 7,5,3

			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_SINGLE_ENDED;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 14, channel_assignment_data);
			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_SINGLE_ENDED;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 16, channel_assignment_data);
			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_SINGLE_ENDED;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 18, channel_assignment_data);
			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_SINGLE_ENDED;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 20, channel_assignment_data);

			ltc2983_ch_start_conversion(SPI_TEMP_BASE, 0, 0); // all masked channels

			temp_voltage = 1;
		} else {
                   ctx->v_p5 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 14, VOLTAGE, ctx->raw+9) * 2;      // Ch 14: RTD PT-100
                   ctx->v_p2 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 16, VOLTAGE, ctx->raw+10);         // Ch 16: RTD PT-100
                   ctx->v_sca12 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 18, VOLTAGE, ctx->raw+11) * 2;  // Ch 18: RTD PT-100
                   ctx->v_sca34 = ltc2983_ch_read_conversion(SPI_TEMP_BASE, 0, 20, VOLTAGE, ctx->raw+12) * 2;  // Ch 20: RTD PT-100

			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "v_p5"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "v_p2"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "v_sca12"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "v_sca34"));
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "raw_data"));

			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_DIFFERENTIAL;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 14, channel_assignment_data);

			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC |	DIRECT_ADC_DIFFERENTIAL;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 16, channel_assignment_data);

			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_DIFFERENTIAL;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 18, channel_assignment_data);

			channel_assignment_data = SENSOR_TYPE__DIRECT_ADC | DIRECT_ADC_DIFFERENTIAL;
			ltc2983_ch_assign(SPI_TEMP_BASE, 0, 20, channel_assignment_data);

			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF5, 0x0A); // channel 20,18
			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF6, 0xA5); // channel 16,14,11,9
			ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF7, 0x54); // channel 7,5,3

			ltc2983_ch_start_conversion(SPI_TEMP_BASE, 0, 0); // all masked channels

			temp_voltage = 0;
		}
	}

	return ESPER_RESP_OK;
}

static uint8_t ADCPwrHandle(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
    uint32_t byte_count;
    uint32_t byte_offset;

    byte_offset = offset * ESPER_GetTypeSize(var->info.type);
    byte_count = num_elements * ESPER_GetTypeSize(var->info.type);

    switch(request) {
    case ESPER_REQUEST_INIT:
         // If we have IO, load it into data by default
        // Loading from DISK/FLASH/NETWORK will occur later, as the Handler may require other variables to exist, but this is safe to do
        if(var->io) {
            memcpy(var->data + byte_offset, (void*)var->io + byte_offset, byte_count);
        }

        if(var->info.type == ESPER_TYPE_ASCII) {
            ((char*)var->data)[var->info.max_elements_per_request-1] = '\0';
            var->info.num_elements = strlen(var->data);
        }
        break;

    case ESPER_REQUEST_REFRESH:
    case ESPER_REQUEST_READ_PRE:
        // Update FROM IO to ESPER if we notice a difference between the two, and update the ts+wc info
        if(var->io) {
            if(memcmp(var->data + byte_offset, (void*)var->io + byte_offset, byte_count) != 0) {
                memcpy(var->data + byte_offset, (void*)var->io + byte_offset, byte_count);
                ESPER_TouchVar(mid, vid);
            }
        }
        break;

    case ESPER_REQUEST_WRITE_PRE:
        break;
    case ESPER_REQUEST_WRITE_POST:
        // Update the IO after if we've been sent new ESPER data
        if(var->io) {
            // Perform the write regardless, in-case the fabric is looking for a write
            memcpy((void*)var->io + byte_offset, var->data + byte_offset, byte_count);
        }

        // Give the ADCs a chance to power up
        usleep(250000);
		*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 1;
		usleep(100);
		*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 0;

        if(var->info.type == ESPER_TYPE_ASCII) {
            ((char*)var->data)[var->info.max_elements_per_request-1] = '\0';
            var->info.num_elements = strlen(var->data);
        }
        break;
    }

    return 1;
}

static uint8_t MV2Handle(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleBoard* board_ctx = (tESPERModuleBoard*)ctx;

    switch(request) {
    case ESPER_REQUEST_INIT:
        break;

    case ESPER_REQUEST_REFRESH:
    case ESPER_REQUEST_READ_PRE:
        break;
    case ESPER_REQUEST_WRITE_PRE:
        break;
    case ESPER_REQUEST_WRITE_POST:
        //MV2_WriteSettings(mv2_write, &board_ctx->mv2);
    	XMV2_WriteSettings(board_ctx);
        ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "mv2_verify_ok"));
        break;
    }

    return 1;
}

static void InitLTC2983(void) {
	uint32_t channel_assignment_data;

	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_TEMP_RSTN) = 0;
	usleep(200000);
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_TEMP_RSTN) = 1;
	usleep(200000); // 100ms startup time, we'll give it twice that

	// ----- Channel 2: Assign Sense Resistor -----
	channel_assignment_data = SENSOR_TYPE__SENSE_RESISTOR |	(uint32_t) 0x1F4000 << SENSE_RESISTOR_VALUE_LSB;		// sense resistor - value: 2000.
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 2, channel_assignment_data);

	// ----- Channel 3: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 3, channel_assignment_data);

	// ----- Channel 5: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 5, channel_assignment_data);

	// ----- Channel 7: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 7, channel_assignment_data);

	// ----- Channel 9: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 9, channel_assignment_data);

	// ----- Channel 11: Assign RTD PT-100 -----
	channel_assignment_data =
			SENSOR_TYPE__RTD_PT_100 |
			RTD_RSENSE_CHANNEL__2 |
			RTD_NUM_WIRES__2_WIRE |
			RTD_EXCITATION_MODE__NO_ROTATION_SHARING |
                        //RTD_EXCITATION_CURRENT__1MA |
			RTD_EXCITATION_CURRENT__500UA |
			RTD_STANDARD__AMERICAN;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 11, channel_assignment_data);

	// ----- Channel 14: Assign Direct ADC -----
	channel_assignment_data =
			SENSOR_TYPE__DIRECT_ADC |
			DIRECT_ADC_DIFFERENTIAL;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 14, channel_assignment_data);

	// ----- Channel 16: Assign Direct ADC -----
	channel_assignment_data =
			SENSOR_TYPE__DIRECT_ADC |
			DIRECT_ADC_DIFFERENTIAL;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 16, channel_assignment_data);

	// ----- Channel 18: Assign Direct ADC -----
	channel_assignment_data =
			SENSOR_TYPE__DIRECT_ADC |
			DIRECT_ADC_DIFFERENTIAL;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 18, channel_assignment_data);

	// ----- Channel 20: Assign Direct ADC -----
	channel_assignment_data =
			SENSOR_TYPE__DIRECT_ADC |
			DIRECT_ADC_DIFFERENTIAL;
	ltc2983_ch_assign(SPI_TEMP_BASE, 0, 20, channel_assignment_data);

	// Set global parameters
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF0, TEMP_UNIT__C | REJECTION__50_60_HZ);
	// Set any extra delay between conversions (in this case, 0*100us)
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xFF, 0);
	// setup multiple channel masking for multiple read
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF5, 0x0A); // channel 20,18
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF6, 0xA5); // channel 16,14,11,9
	ltc2983_transfer_byte(SPI_TEMP_BASE, 0, WRITE_TO_RAM, 0xF7, 0x54); // channel 7,5,3

	// start first conversion
	ltc2983_ch_start_conversion(SPI_TEMP_BASE, 0, 0); // all masked channels
}

static uint16_t mv2_write(uint8_t addr, uint8_t data) {
	alt_u16 tx_data[1];
	alt_u16 rx_data[1];

	usleep(500);

	rx_data[0] = 0;
	tx_data[0] = ((0x2C | (addr & 0x03)) << 8) | data;

	custom_alt_avalon_spi_command(SPI_EXTERNAL_BASE, 0, 1, tx_data, 1, rx_data, 0);

	return rx_data[0];
}

static uint8_t mv2_read(uint8_t addr) {
	alt_u16 tx_data[1];
	alt_u16 rx_data[1];

	usleep(500);

	tx_data[0] = (0x1C | (addr & 0x03)) << 8;

	custom_alt_avalon_spi_command(SPI_EXTERNAL_BASE, 0, 1, tx_data, 1, rx_data, 0);

	return rx_data[0] & 0xFF;
}
