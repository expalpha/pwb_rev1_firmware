/*
 * mod_sp.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MOD_SP_H_
#define MOD_SP_H_

#include <esper.h>
#include <system.h>

typedef struct {
	uint32_t sp_ctrl_base;
	uint32_t sp_stat_base;

	uint8_t force_run;
	uint8_t force_rst;
	uint8_t trig_out;
	uint8_t hold_ena;
	uint8_t start_delay[4];
	uint8_t ext_trig_ena;
	uint8_t ext_trig_inv;
	uint8_t sfp_trig_ena;
	uint8_t test_pulse_ena[4];
	uint8_t test_pulse_interval_ena;
	uint32_t test_pulse_interval;
	uint16_t test_pulse_width[4];

	uint32_t event_drop_full;
	uint32_t event_drop_busy;
	uint32_t event_drop_err;
	uint32_t* mem_slot[SIGPROC_CONTROL_NUM_MEM_SLOTS];

	uint8_t sca_enable[4];
	uint16_t sca_samples_to_read;
	uint8_t run_status;
	uint8_t hold_status;
	uint64_t ts_run;
	uint64_t ts_start;
	uint64_t ts_trig;
	uint32_t trig_accepted;
	uint32_t trig_dropped;

   //uint8_t sca_a_ch_enable[79];
   //uint8_t sca_b_ch_enable[79];
   //uint8_t sca_c_ch_enable[79];
   //uint8_t sca_d_ch_enable[79];

   uint32_t sca_a_ch_enable_bitmap[3];
   uint32_t sca_b_ch_enable_bitmap[3];
   uint32_t sca_c_ch_enable_bitmap[3];
   uint32_t sca_d_ch_enable_bitmap[3];

   //uint8_t sca_a_ch_force[79];
   //uint8_t sca_b_ch_force[79];
   //uint8_t sca_c_ch_force[79];
   //uint8_t sca_d_ch_force[79];

   uint32_t sca_a_ch_force_bitmap[3];
   uint32_t sca_b_ch_force_bitmap[3];
   uint32_t sca_c_ch_force_bitmap[3];
   uint32_t sca_d_ch_force_bitmap[3];

   //int16_t sca_a_ch_threshold[79];
   //int16_t sca_b_ch_threshold[79];
   //int16_t sca_c_ch_threshold[79];
   //int16_t sca_d_ch_threshold[79];
   
   uint32_t sca_a_ch_ctrl;
   uint32_t sca_b_ch_ctrl;
   uint32_t sca_c_ch_ctrl;
   uint32_t sca_d_ch_ctrl;
   
	uint32_t trig_cnt_int;
	uint32_t trig_cnt_ext;
	uint32_t trig_cnt_sfp;
	uint8_t test_mode;
	uint8_t auto_mode;
} tESPERModuleSP;

tESPERModuleSP* ModuleSPInit(uint32_t sp_ctrl_base, uint32_t sp_stat_base, tESPERModuleSP* ctx);
eESPERResponse ModuleSPHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_SP_H_ */
