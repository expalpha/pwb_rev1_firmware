/*
 * mod_sca.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include "../AFTER_slowcontrol.h"
#include <esper.h>
#include <string.h>
#include "mod_sca.h"

static eESPERResponse Init(tESPERMID mid, tESPERModuleSCA* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleSCA* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleSCA* ctx);

static void AFTER_WriteAll(tESPERModuleSCA* ctx);

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

tESPERModuleSCA* SCAModuleInit(AFTER_write sca_write, AFTER_read sca_read, tESPERModuleSCA* ctx) {
	if(!ctx) return 0;
	if(!sca_write) return 0;
	if(!sca_read) return 0;

	ctx->sca_write = sca_write;
	ctx->sca_read = sca_read;

	return ctx;
}

eESPERResponse SCAModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleSCA*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleSCA*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleSCA*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleSCA* ctx) {
	tESPERVID vid;

	uint32_t k;

	AFTER_ReadSettings(ctx->sca_read, &ctx->sca_regmap);

	ctx->sca_regmap.config1.Icsa = 0;
	ctx->sca_regmap.config1.Gain = 0;
	ctx->sca_regmap.config1.Time = 0;
	ctx->sca_regmap.config1.Test = 0;
	ctx->sca_regmap.config1.Integrator_mode = 0;
	ctx->sca_regmap.config1.power_down_read = 0;
	ctx->sca_regmap.config1.power_down_write = 0;
	ctx->sca_regmap.config1.alternate_power = 0;
	AFTER_WriteConfig1(ctx->sca_write, &ctx->sca_regmap.config1);

	ctx->sca_regmap.config2.debug = 0;
	ctx->sca_regmap.config2.read_from_0 = 0;
	ctx->sca_regmap.config2.test_digout = 0;
	ctx->sca_regmap.config2.en_mker_rst = 1;
	ctx->sca_regmap.config2.rst_lv_to_1 = 0;
	ctx->sca_regmap.config2.boost_pw = 0;
	ctx->sca_regmap.config2.out_resync = 0;
	ctx->sca_regmap.config2.synchro_inv = 0;
	ctx->sca_regmap.config2.force_eout = 0;
	ctx->sca_regmap.config2.Cur_RA = 2;
	ctx->sca_regmap.config2.Cur_BUF = 3;
	AFTER_WriteConfig2(ctx->sca_write, &ctx->sca_regmap.config2);

	for(k=0; k<4; k++) {
		ctx->sca_regmap.injection.select_cfpn[k] = 0;
	}

	for(k=0; k<72; k++) {
		ctx->sca_regmap.injection.select_ch[k] = 1;
	}

	AFTER_WriteAll(ctx);

	vid = ESPER_CreateVarUInt16(mid, "version", ESPER_OPTION_RD, 1, &ctx->sca_regmap.version, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Version");

	//vid = ESPER_CreateVarBool(mid, "sca_slow_wr", 	ESPER_OPTION_RD, 1, &ctx->verify_write, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "SCA Slow Control WIP");

	vid = ESPER_CreateVarBool(mid, "verify_ok", 	ESPER_OPTION_RD, 1, &ctx->verify_ok, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Config Read after Write test");
        
	//vid = ESPER_CreateVarUInt32(mid, "write_counter", 	ESPER_OPTION_RD, 1, &ctx->write_counter, 0, 0);
	//ESPER_CreateAttrNull(mid, vid, "name", "SCA Config Write counter");

	vid = ESPER_CreateVarUInt8(mid, "i_csa", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.Icsa, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Nominal CSA Bias Current");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "x1", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "x2", 1);

	vid = ESPER_CreateVarUInt8(mid, "gain", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.Gain, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Charge Range");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "120 fC", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "240 fC", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "360 fC", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "600 fC", 3);

	vid = ESPER_CreateVarUInt8(mid, "time", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.Time, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Peaking time of Shaper");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "116 ns", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "200 ns", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "412 ns", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "505 ns", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "610 ns", 4);
	ESPER_CreateAttrUInt8(mid, vid, "option", "695 ns", 5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "912 ns", 6);
	ESPER_CreateAttrUInt8(mid, vid, "option", "993 ns", 7);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1054 ns", 8);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1134 ns", 9);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1343 ns", 10);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1421 ns", 11);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1546 ns", 12);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1626 ns", 13);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1834 ns", 14);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1912 ns", 15);

	vid = ESPER_CreateVarUInt8(mid, "test", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.Test, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Test mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "nothing", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "calibration", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "test", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "functionality", 3);

	vid = ESPER_CreateVarBool(mid, "integrator", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.Integrator_mode, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Independent 1us Integration");

	vid = ESPER_CreateVarBool(mid, "pd_read", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.power_down_read, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Power down Read");

	vid = ESPER_CreateVarBool(mid, "pd_write", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.power_down_write, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Power down Write");

	vid = ESPER_CreateVarBool(mid, "alt_power", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config1.alternate_power, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Power down Read/Write");

	vid = ESPER_CreateVarUInt8(mid, "debug", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.debug, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Debug (Spy) Mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Standby", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CSA", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CR", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Gain2", 3);

	vid = ESPER_CreateVarBool(mid, "read_from_0", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.read_from_0, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Start from Column 0");

	vid = ESPER_CreateVarBool(mid, "test_digout", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.test_digout, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Test pattern Out");

	// Read-only, this is being used to detect the start of the SCA read
	vid = ESPER_CreateVarBool(mid, "ena_rst_marker", 	ESPER_OPTION_RD, 1, &ctx->sca_regmap.config2.en_mker_rst, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Digital Marker in Reset");

	// Read-only, this is being used to detect the start of the SCA read
	vid = ESPER_CreateVarBool(mid, "rst_lv_to_1", 	ESPER_OPTION_RD, 1, &ctx->sca_regmap.config2.rst_lv_to_1, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Set Reset Marker High");

	vid = ESPER_CreateVarBool(mid, "boost_pw", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.boost_pw, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Increase GAIN-2 Current 20%");

	vid = ESPER_CreateVarBool(mid, "out_resync", 	ESPER_OPTION_RD, 1, &ctx->sca_regmap.config2.out_resync, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Slow Control Resync");

	vid = ESPER_CreateVarBool(mid, "synchro_inv", 	ESPER_OPTION_RD, 1, &ctx->sca_regmap.config2.synchro_inv, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Slow Control Edge");

	vid = ESPER_CreateVarBool(mid, "force_eout", 	ESPER_OPTION_RD, 1, &ctx->sca_regmap.config2.force_eout, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Slow Control Inhibit");

	vid = ESPER_CreateVarUInt8(mid, "cur_ra", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.Cur_RA, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Line Readout Bias");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "211uA/172uA", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "274uA/211uA", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "395uA/274uA", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "735uA/395uA", 3);

	vid = ESPER_CreateVarUInt8(mid, "cur_buf", 	ESPER_OPTION_WR_RD, 1, &ctx->sca_regmap.config2.Cur_BUF, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Group/Output Buffer Current");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "132uA/482uA", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "169uA/620uA", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "239uA/831uA", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "433uA/1.610mA", 3);

	vid = ESPER_CreateVarBool(mid, "select_ch", 	ESPER_OPTION_WR_RD, 72, ctx->sca_regmap.injection.select_ch, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "CH Test Pulse Injection");

	vid = ESPER_CreateVarBool(mid, "select_fpn", 	ESPER_OPTION_WR_RD, 4, ctx->sca_regmap.injection.select_cfpn, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "FPN Test Pulse Injection");

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleSCA* ctx) {
	AFTER_WriteAll(ctx);

	return ESPER_RESP_OK;
}

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		AFTER_WriteAll((tESPERModuleSCA*)ctx);
		break;
	default:
		break;
	}

	return 1;
}

static void AFTER_WriteAll(tESPERModuleSCA* ctx) {
	ctx->verify_write = 1;
        ctx->write_counter++;

	AFTER_WriteConfig1(ctx->sca_write, &ctx->sca_regmap.config1);
	AFTER_WriteConfig2(ctx->sca_write, &ctx->sca_regmap.config2);
	AFTER_WriteInjection(ctx->sca_write, &ctx->sca_regmap.injection);
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleSCA* ctx) {
	tAFTER_Registers regmap;

	if(ctx->verify_write) {
                //ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "sca_slow_wr"));
		AFTER_ReadSettings(ctx->sca_read, &regmap);

		if(memcmp(&regmap, &ctx->sca_regmap, sizeof(tAFTER_Registers)) == 0) {
			ctx->verify_write = 0;
			//ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "sca_slow_wr"));
                        ctx->verify_ok = 1;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "verify_ok"));
		} else {
                        // AFTER_WriteAll(ctx); // do not go into infinite loop writing to a defective SCA
			ctx->verify_write = 0;
			//ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "sca_slow_wr"));
                        ctx->verify_ok = 0;
			ESPER_TouchVar(mid, ESPER_GetVarIdByKey(mid, "verify_ok"));
		}
	}

	return ESPER_RESP_OK;
}

