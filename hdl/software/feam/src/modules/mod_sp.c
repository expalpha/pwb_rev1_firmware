/*
 * mod_sp.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <sys/types.h>
#include <alt_iniche_dev.h>
#include "io.h"
#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <string.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <drivers/inc/sigproc_regs.h>
#include "mod_sp.h"
#include <esper_nios.h>
#include <stdlib.h>
#include <malloc.h> // memalign()

// Hack to remove error, thanks altera
//u_long inet_addr(char FAR * str);

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleSP* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleSP* ctx);

tESPERModuleSP* ModuleSPInit(uint32_t sp_ctrl_base, uint32_t sp_stat_base, tESPERModuleSP* ctx) {
	if(!ctx) return 0;

	ctx->sp_ctrl_base = sp_ctrl_base;
	ctx->sp_stat_base = sp_stat_base;
	ctx->hold_ena = 1;

	return ctx;
}

eESPERResponse ModuleSPHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleSP*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleSP*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleSP*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleSP* ctx) {
	tESPERVID vid;

	vid = ESPER_CreateVarBool(mid, "force_run",  		ESPER_OPTION_WR_RD, 1, &ctx->force_run, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_FORCE_RUN), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Force Run");

	vid = ESPER_CreateVarBool(mid, "force_rst",  		ESPER_OPTION_WR_RD, 1, &ctx->force_rst, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_FORCE_RESET), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Force Reset");

	vid = ESPER_CreateVarBool(mid, "test_mode", ESPER_OPTION_WR_RD, 1, &ctx->test_mode, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TEST_MODE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Test Mode");

	vid = ESPER_CreateVarBool(mid, "auto_mode", ESPER_OPTION_WR_RD, 1, &ctx->auto_mode, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_AUTO_MODE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Auto Mode");

	vid = ESPER_CreateVarBool(mid, "sca_enable", ESPER_OPTION_WR_RD, 4, ctx->sca_enable, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_ENA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable SCA");

	vid = ESPER_CreateVarUInt16(mid, "sca_samples",	ESPER_OPTION_WR_RD, 1, &ctx->sca_samples_to_read, (uint16_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_SAMPLES), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Samples to Read");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 511);

	vid = ESPER_CreateVarBool(mid, "trig_out",  		ESPER_OPTION_WR_RD, 1, &ctx->trig_out, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TRIG_OUT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Out");

	vid = ESPER_CreateVarUInt8(mid, "start_delay",	ESPER_OPTION_WR_RD, 4, ctx->start_delay, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_START_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Start Delay");
    ESPER_CreateAttrUInt8(mid, vid, "limit", "min", 0);
    ESPER_CreateAttrUInt8(mid, vid, "limit", "max", 255);

	vid = ESPER_CreateVarBool(mid, "test_pulse_ena",	ESPER_OPTION_WR_RD, 4, &ctx->test_pulse_ena[0], (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TEST_PULSE_ENA), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Internal Test Pulse");

	vid = ESPER_CreateVarBool(mid, "test_pulse_interval_ena",	ESPER_OPTION_WR_RD, 1, &ctx->test_pulse_interval_ena, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TEST_PULSE_ENA_INTERVAL), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Internal Test Pulse Interval Trigger");

	vid = ESPER_CreateVarUInt32(mid, "test_pulse_interval", ESPER_OPTION_WR_RD, 1, &ctx->test_pulse_interval, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TEST_PULSE_INTERVAL), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Test Pulse Interval");

	vid = ESPER_CreateVarUInt16(mid, "test_pulse_wdt", ESPER_OPTION_WR_RD, 4, &ctx->test_pulse_width[0], (uint16_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_TEST_PULSE_WIDTH0_1), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Test Pulse Width");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 65535);

	vid = ESPER_CreateVarBool(mid, "run_status",  	ESPER_OPTION_RD, 1, &ctx->run_status, (uint8_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_RUN),0);
	ESPER_CreateAttrNull(mid, vid, "name", "Run Status");

	vid = ESPER_CreateVarUInt64(mid, "ts_run", 		ESPER_OPTION_RD, 1, &ctx->ts_run, (uint64_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_STAT_TS_RUN_LO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Run Timestamp");

	vid = ESPER_CreateVarUInt32(mid, "event_drop_full",  	ESPER_OPTION_RD, 1, &ctx->event_drop_full, (uint32_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_EVENT_FIFO_FULL),0);
	ESPER_CreateAttrNull(mid, vid, "name", "Event Dropped Full");

	vid = ESPER_CreateVarUInt32(mid, "event_drop_busy",  	ESPER_OPTION_RD, 1, &ctx->event_drop_busy, (uint32_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_EVENT_FIFO_BUSY),0);
	ESPER_CreateAttrNull(mid, vid, "name", "Event Dropped Busy");

	vid = ESPER_CreateVarUInt32(mid, "event_drop_err",  	ESPER_OPTION_RD, 1, &ctx->event_drop_err, (uint32_t*)GET_REG_OFFSET(ctx->sp_stat_base, SIGPROC_REG_EVENT_FIFO_ERR),0);
	ESPER_CreateAttrNull(mid, vid, "name", "Event Dropped Error");

	vid = ESPER_CreateVarUInt32(mid, "sca_a_ch_ctrl", ESPER_OPTION_WR_RD, 1, &ctx->sca_a_ch_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_CTRL),0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_b_ch_ctrl", ESPER_OPTION_WR_RD, 1, &ctx->sca_b_ch_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_CTRL),0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_c_ch_ctrl", ESPER_OPTION_WR_RD, 1, &ctx->sca_c_ch_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_CTRL),0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_d_ch_ctrl", ESPER_OPTION_WR_RD, 1, &ctx->sca_d_ch_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_CTRL),0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_a_ch_enable_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_a_ch_enable_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_ENA_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_b_ch_enable_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_b_ch_enable_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_ENA_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_c_ch_enable_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_c_ch_enable_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_ENA_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_d_ch_enable_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_d_ch_enable_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_ENA_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_a_ch_force_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_a_ch_force_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_FORCE_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_b_ch_force_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_b_ch_force_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_FORCE_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_c_ch_force_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_c_ch_force_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_FORCE_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

	vid = ESPER_CreateVarUInt32(mid, "sca_d_ch_force_bitmap", ESPER_OPTION_WR_RD, 3, ctx->sca_d_ch_force_bitmap, (uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_FORCE_M0), 0);
	ESPER_CreateAttrNull(mid, vid, "format", "hex");

#if 0
	vid = ESPER_CreateVarBool(mid, "sca_a_ch_enable", ESPER_OPTION_WR_RD, 79, ctx->sca_a_ch_enable, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_ENA0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA A Channel Enable");

	vid = ESPER_CreateVarBool(mid, "sca_b_ch_enable", ESPER_OPTION_WR_RD, 79, ctx->sca_b_ch_enable, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_ENA0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA B Channel Enable");

	vid = ESPER_CreateVarBool(mid, "sca_c_ch_enable", ESPER_OPTION_WR_RD, 79, ctx->sca_c_ch_enable, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_ENA0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA C Channel Enable");

	vid = ESPER_CreateVarBool(mid, "sca_d_ch_enable", ESPER_OPTION_WR_RD, 79, ctx->sca_d_ch_enable, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_ENA0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA D Channel Enable");

	// Force Settings
	vid = ESPER_CreateVarBool(mid, "sca_a_ch_force", ESPER_OPTION_WR_RD, 79, ctx->sca_a_ch_force, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_FORCE0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA A Channel Force");

	vid = ESPER_CreateVarBool(mid, "sca_b_ch_force", ESPER_OPTION_WR_RD, 79, ctx->sca_b_ch_force, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_FORCE0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA B Channel Force");

	vid = ESPER_CreateVarBool(mid, "sca_c_ch_force", ESPER_OPTION_WR_RD, 79, ctx->sca_c_ch_force, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_FORCE0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA C Channel Force");

	vid = ESPER_CreateVarBool(mid, "sca_d_ch_force", ESPER_OPTION_WR_RD, 79, ctx->sca_d_ch_force, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_FORCE0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA D Channel Force");

	// Threshold settings
	vid = ESPER_CreateVarSInt16(mid, "sca_a_ch_threshold", ESPER_OPTION_WR_RD, 79, ctx->sca_a_ch_threshold, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_A_CH_THRES0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA A Channel Threshold");
	ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
	ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);

	vid = ESPER_CreateVarSInt16(mid, "sca_b_ch_threshold", ESPER_OPTION_WR_RD, 79, ctx->sca_b_ch_threshold, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_B_CH_THRES0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA B Channel Threshold");
	ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
	ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);

	vid = ESPER_CreateVarSInt16(mid, "sca_c_ch_threshold", ESPER_OPTION_WR_RD, 79, ctx->sca_c_ch_threshold, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_C_CH_THRES0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA C Channel Threshold");
	ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
	ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);

	vid = ESPER_CreateVarSInt16(mid, "sca_d_ch_threshold", ESPER_OPTION_WR_RD, 79, ctx->sca_d_ch_threshold, (uint8_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_SCA_D_CH_THRES0), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA D Channel Threshold");
	ESPER_CreateAttrSInt16(mid, vid, "limit", "min", -2048);
	ESPER_CreateAttrSInt16(mid, vid, "limit", "max", 2047);
#endif

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleSP* ctx) {
	uint32_t n;
	uint32_t data;

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "force_rst"), 0, 1);

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "auto_mode"), 0, 1);

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_enable"), 0, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_enable"), 1, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_enable"), 2, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_enable"), 3, 1);

	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "sca_samples"), 0, 511);

	// Default test pulse interval to off
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "test_pulse_interval_ena"), 0, 0);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "test_pulse_interval"), 0, 125000000);

	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "test_pulse_wdt"), 0, 128);
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "test_pulse_wdt"), 1, 128);
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "test_pulse_wdt"), 2, 128);
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "test_pulse_wdt"), 3, 128);

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "test_pulse_ena"), 0, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "test_pulse_ena"), 1, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "test_pulse_ena"), 2, 1);
	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "test_pulse_ena"), 3, 1);

	ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "start_delay"), 0, 21);
	ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "start_delay"), 1, 21);
	ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "start_delay"), 2, 21);
	ESPER_WriteVarUInt8(mid, ESPER_GetVarIdByKey(mid, "start_delay"), 3, 21);

	// Default channels to on
	for(n=0; n<3; n++) {
		ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "sca_a_ch_enable_bitmap"), n, 0xFFFFFFFF);
		ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "sca_b_ch_enable_bitmap"), n, 0xFFFFFFFF);
		ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "sca_c_ch_enable_bitmap"), n, 0xFFFFFFFF);
		ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "sca_d_ch_enable_bitmap"), n, 0xFFFFFFFF);
	}

#if 0
	// Default channels to on
	for(n=0; n<79; n++) {
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_a_ch_enable"), n, 1);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_b_ch_enable"), n, 1);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_c_ch_enable"), n, 1);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_d_ch_enable"), n, 1);
	}

	// Don't force channels, we have thresholding
	for(n=0; n<79; n++) {
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_a_ch_force"), n, 0);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_b_ch_force"), n, 0);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_c_ch_force"), n, 0);
		ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "sca_d_ch_force"), n, 0);
	}


	for(n=0; n<79; n++) {
		ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "sca_a_ch_threshold"), n, 1000);
		ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "sca_b_ch_threshold"), n, 1000);
		ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "sca_c_ch_threshold"), n, 1000);
		ESPER_WriteVarSInt16(mid, ESPER_GetVarIdByKey(mid, "sca_d_ch_threshold"), n, 1000);
	}
#endif

        int slot_size = 512*79*4*2; // Number of SCA cells (511) x Number of SCA Channels (79=72 CH + 4 FPN + 3 RESET) x Number of SCAs (4), 16-bit values per cell (2 bytes)
	for(n=0; n<SIGPROC_CONTROL_NUM_MEM_SLOTS; n++) {
           //ctx->mem_slot[n] = calloc(511*79*4, 2); // Number of SCA cells (511) x Number of SCA Channels (79=72 CH + 4 FPN + 3 RESET) x Number of SCAs (4), 16-bit values per cell (2 bytes)
           char* ptr = malloc(slot_size+16);
           char* ptra = ptr;
           // most stupid way to align pointer to 128 bit (16 bytes).
           // memalign() does not work - returns strange addresses...
           while ((uint32_t)ptra & 0xF) {
              ptra++;
              //printf("ptra %p\n", ptra);
           }
           // memory addresses must be aligned to 128 bit because
           // DDR reader uses 128-bit reads that must be aligned.
           // unaligned addresses will be trunctated resulting
           // in data shifting by 1 ADC time bin between the DDR writer and reader.
           ctx->mem_slot[n] = (uint32_t*)ptra;
           //ctx->mem_slot[n] = memalign(slot_size, 128/8); // align on 128 bits (16 bytes)
           //printf("mem_ready %d slot %p, ptr %p, ptra %p!\n", (int)n, ctx->mem_slot[n], ptr, ptra);
           *(uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_MEM_SLOT0+n) = (uint32_t)ctx->mem_slot[n];
	}

        printf("mem_ready %d slots!\n", SIGPROC_CONTROL_NUM_MEM_SLOTS);

	data = 1;
	memcpy(GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_MEM_READY), &data, 1);

        //printf("mem_ready read back 0x%08x!\n", *(uint32_t*)GET_REG_OFFSET(ctx->sp_ctrl_base, SIGPROC_REG_CTRL_MEM_READY));

	ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "force_rst"), 0, 0);
	//ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "force_run"), 0, 1);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleSP* ctx) {
	
	return ESPER_RESP_OK;
}
