/*
 * mod_sp.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <sys/types.h>
#include <drivers/inc/trigger_control_regs.h>
#include <drivers/inc/trigger_control.h>
#include "mod_trigger.h"
#include <esper_nios.h>
#include <stdlib.h>


static eESPERResponse Init(tESPERMID mid, tESPERModuleTrigger* ctx);
static eESPERResponse Start(tESPERMID mid, tESPERModuleTrigger* ctx);
static eESPERResponse Update(tESPERMID mid, tESPERModuleTrigger* ctx);

tESPERModuleTrigger* ModuleTriggerInit(uint32_t trig_ctrl_base, uint32_t trig_stat_base, uint32_t num_trig_src, uint32_t num_busy_src, tESPERModuleTrigger* ctx) {
	if(!ctx) return 0;

	ctx->trig_ctrl_base = trig_ctrl_base;
	ctx->trig_stat_base = trig_stat_base;

	ctx->num_trig_src = num_trig_src;
	ctx->num_busy_src = num_busy_src;
	ctx->global_ena = 0;

	ctx->busy_source = calloc(num_busy_src, sizeof(uint32_t));

	ctx->src_requested = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_accepted = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_dropped = calloc(num_trig_src ,sizeof(uint32_t));

	ctx->src_dropped_busy = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_dropped_delay = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_dropped_priority = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_dropped_trigger = calloc(num_trig_src ,sizeof(uint32_t));

	ctx->src_enable = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_invert = calloc(num_trig_src ,sizeof(uint32_t));
	ctx->src_delay = calloc(num_trig_src ,sizeof(uint32_t));

	return ctx;
}

eESPERResponse ModuleTriggerHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {
	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleTrigger*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleTrigger*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleTrigger*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleTrigger* ctx) {
	tESPERVID vid;
	// uint32_t n;

	vid = ESPER_CreateVarBool(mid, "enable_all",  		ESPER_OPTION_WR_RD, 1, &ctx->global_ena, (uint8_t*)GET_TRIGCTRL_REG_CTRL_OFFSET(ctx->trig_ctrl_base, TRIG_CTRL_REG_GLOBAL_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable All");

	vid = ESPER_CreateVarUInt32(mid, "total_requested", ESPER_OPTION_RD, 1, &ctx->total_requested, (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_REG_TOTAL_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Request Count");

	vid = ESPER_CreateVarUInt32(mid, "total_accepted", ESPER_OPTION_RD, 1, &ctx->total_accepted, (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_REG_TOTAL_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Accept Count");

	vid = ESPER_CreateVarUInt32(mid, "total_dropped", ESPER_OPTION_RD, 1, &ctx->total_dropped, (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_REG_TOTAL_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Trigger Dropped Count");

	vid = ESPER_CreateVarUInt32(mid, "num_trig_src", ESPER_OPTION_RD, 1, &ctx->num_trig_src, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Num Trigger Sources");

	vid = ESPER_CreateVarUInt32(mid, "num_busy_src", ESPER_OPTION_RD, 1, &ctx->num_busy_src, 0, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Num Busy Sources");

	// Busy Sources
	vid = ESPER_CreateVarUInt32(mid, "busy_mem_rdy", ESPER_OPTION_RD, 1, &ctx->busy_source[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_NUM_BASE_STAT_REGS), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Busy - Mem Ready");

	vid = ESPER_CreateVarUInt32(mid, "busy_sca_wr_fill", ESPER_OPTION_RD, 1, &ctx->busy_source[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_NUM_BASE_STAT_REGS+1), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Busy - SCA Write Filled");

	vid = ESPER_CreateVarUInt32(mid, "busy_evt_fifo_full", ESPER_OPTION_RD, 1, &ctx->busy_source[2], (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_NUM_BASE_STAT_REGS+2), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Busy - Event FIFO Full");

	vid = ESPER_CreateVarUInt32(mid, "busy_master", ESPER_OPTION_RD, 1, &ctx->busy_source[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_OFFSET(ctx->trig_stat_base, TRIG_CTRL_NUM_BASE_STAT_REGS+3), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Busy - Master");
/*
	for(n=0; n<ctx->num_trig_src; n++) {
		vid = ESPER_CreateVarBool(mid, "trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[n], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, n, TRIG_CTRL_REG_SRC_ENABLE), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Enable");

		vid = ESPER_CreateVarBool(mid, "trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[n], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, n, TRIG_CTRL_REG_SRC_INVERT), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Invert");

		vid = ESPER_CreateVarUInt32(mid, "trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[n], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, n, TRIG_CTRL_REG_SRC_DELAY), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Delay");
		ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
		ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

		vid = ESPER_CreateVarUInt32(mid, "trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[n], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, n, TRIG_CTRL_REG_SRC_REQUESTED), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Request Count");

		vid = ESPER_CreateVarUInt32(mid, "trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[n], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, n, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Accept Count");

		vid = ESPER_CreateVarUInt32(mid, "trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[n], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, n, TRIG_CTRL_REG_SRC_DROPPED), 0);
		ESPER_CreateAttrNull(mid, vid, "name", "Trigger Drop Count");
	}
*/
	// External Trigger Source
	vid = ESPER_CreateVarBool(mid, "ext_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[0], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 0, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Enable");

	vid = ESPER_CreateVarBool(mid, "ext_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[0], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 0, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Invert");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[0], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 0, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Request Count");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Accept Count");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Drop Count");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_drop_busy", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_busy[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_DROPPED_BUSY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Drop Busy");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_drop_trig", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_trigger[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_DROPPED_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Drop Trig");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_drop_dly", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_delay[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_DROPPED_DLY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Drop Delay");

	vid = ESPER_CreateVarUInt32(mid, "ext_trig_drop_prio", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_priority[0], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 0, TRIG_CTRL_REG_SRC_DROPPED_PRIO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Trigger Drop Priority");

	// Manual Trigger (fired by NIOS/ESPER)
	vid = ESPER_CreateVarBool(mid, "man_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[1], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 1, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Enable");

	/*
	vid = ESPER_CreateVarBool(mid, "man_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[1], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 1, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Invert");
	*/

	vid = ESPER_CreateVarUInt32(mid, "man_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[1], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 1, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "man_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Request Count");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Accept Count");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Drop Count");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_drop_busy", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_busy[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_DROPPED_BUSY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Drop Busy");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_drop_trig", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_trigger[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_DROPPED_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Drop Trig");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_drop_dly", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_delay[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_DROPPED_DLY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Drop Delay");

	vid = ESPER_CreateVarUInt32(mid, "man_trig_drop_prio", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_priority[1], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 1, TRIG_CTRL_REG_SRC_DROPPED_PRIO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual Trigger Drop Priority");


	// Periodic Trigger
	/*
	vid = ESPER_CreateVarBool(mid, "period_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[2], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 2, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Enable");

	vid = ESPER_CreateVarBool(mid, "period_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[2], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 2, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Invert");

	vid = ESPER_CreateVarUInt32(mid, "period_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[2], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 2, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "period_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[2], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, 2, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Request Count");

	vid = ESPER_CreateVarUInt32(mid, "period_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[2], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, 2, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Accept Count");

	vid = ESPER_CreateVarUInt32(mid, "period_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[2], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, 2, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Periodic Trigger Drop Count");
	 */

	// Internal Pulse Trigger
	vid = ESPER_CreateVarBool(mid, "intp_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[3], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 3, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Enable");

	/*
	vid = ESPER_CreateVarBool(mid, "intp_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[3], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 3, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Invert");
	*/

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[3], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 3, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Request");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Accept");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Drop");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_drop_busy", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_busy[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_DROPPED_BUSY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Drop Busy");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_drop_trig", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_trigger[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_DROPPED_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Drop Trig");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_drop_dly", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_delay[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_DROPPED_DLY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Drop Delay");

	vid = ESPER_CreateVarUInt32(mid, "intp_trig_drop_prio", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_priority[3], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 3, TRIG_CTRL_REG_SRC_DROPPED_PRIO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Internal Pulse Trigger Drop Priority");


	// External Pulse Trigger
	vid = ESPER_CreateVarBool(mid, "extp_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[4], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 4, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Enable");

	vid = ESPER_CreateVarBool(mid, "extp_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[4], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 4, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Invert");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[4], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 4, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Request");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Accept");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Drop");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_drop_busy", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_busy[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_DROPPED_BUSY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Drop Busy");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_drop_trig", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_trigger[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_DROPPED_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Drop Trig");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_drop_dly", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_delay[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_DROPPED_DLY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Drop Delay");

	vid = ESPER_CreateVarUInt32(mid, "extp_trig_drop_prio", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_priority[4], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 4, TRIG_CTRL_REG_SRC_DROPPED_PRIO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "External Pulse Trigger Drop Priority");


	// Link Trigger
	vid = ESPER_CreateVarBool(mid, "link_trig_ena",	ESPER_OPTION_WR_RD, 1, &ctx->src_enable[5], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 5, TRIG_CTRL_REG_SRC_ENABLE), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Trigger Enable");

	/*
	vid = ESPER_CreateVarBool(mid, "udp_trig_inv",	ESPER_OPTION_WR_RD, 1, &ctx->src_invert[5], (uint8_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 5, TRIG_CTRL_REG_SRC_INVERT), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "UDP Trigger Invert");
	*/

	vid = ESPER_CreateVarUInt32(mid, "link_trig_delay",	ESPER_OPTION_WR_RD, 1, &ctx->src_delay[5], (uint32_t*)GET_TRIGCTRL_REG_CTRL_SRC_OFFSET(ctx->trig_ctrl_base, 5, TRIG_CTRL_REG_SRC_DELAY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Trigger Delay");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 4294967295);

	vid = ESPER_CreateVarUInt32(mid, "link_trig_requested", ESPER_OPTION_RD, 1, &ctx->src_requested[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_REQUESTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Trigger Request Count");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_accepted", ESPER_OPTION_RD, 1, &ctx->src_accepted[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_ACCEPTED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Trigger Accept Count");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_dropped", 	ESPER_OPTION_RD, 1, &ctx->src_dropped[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_DROPPED), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Trigger Drop Count");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_drop_busy", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_busy[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_DROPPED_BUSY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Pulse Trigger Drop Busy");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_drop_trig", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_trigger[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_DROPPED_TRIG), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Pulse Trigger Drop Trig");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_drop_dly", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_delay[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_DROPPED_DLY), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Pulse Trigger Drop Delay");

	vid = ESPER_CreateVarUInt32(mid, "link_trig_drop_prio", 	ESPER_OPTION_RD, 1, &ctx->src_dropped_priority[5], (uint32_t*)GET_TRIGCTRL_REG_STAT_SRC_OFFSET(ctx->trig_stat_base, ctx->num_busy_src, 5, TRIG_CTRL_REG_SRC_DROPPED_PRIO), 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Link Pulse Trigger Drop Priority");

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleTrigger* ctx) {

        //ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "enable_all"), 0, 1);
	//ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "ext_trig_ena"), 0, 1);
	//ESPER_WriteVarBool(mid, ESPER_GetVarIdByKey(mid, "intp_trig_ena"), 0, 1);
	ESPER_WriteVarUInt32(mid, ESPER_GetVarIdByKey(mid, "intp_trig_delay"), 0, 400);


	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleTrigger* ctx) {

	return ESPER_RESP_OK;
}
