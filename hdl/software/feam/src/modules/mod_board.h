/*
 * mod_board.h
 *
 *  Created on: Apr 2, 2017
 *      Author: admin
 */

#ifndef MOD_BOARD_H_
#define MOD_BOARD_H_

#include <unistd.h>
#include <esper.h>
#include <drivers/inc/mv2_magnetometer.h>

#define SYS_VAR_NAME_LEN 32
#define SYS_VAR_BUILD_USER_LEN 64
#define SYS_VAR_BUILD_EMAIL_LEN 64
#define SYS_VAR_BUILD_STR_LEN 64
#define SYS_VAR_BUILD_GIT_BRANCH_LEN 256
#define SYS_VAR_BUILD_GIT_HASH_LEN 64

typedef struct {
	uint32_t quartus_buildtime;
	uint32_t elf_buildtime;
	uint32_t elf_buildnumber;
	char elf_build_str[SYS_VAR_BUILD_STR_LEN];
	char elf_build_user[SYS_VAR_BUILD_USER_LEN];
	char elf_build_email[SYS_VAR_BUILD_EMAIL_LEN];
	char git_branch[SYS_VAR_BUILD_GIT_BRANCH_LEN];
	char git_hash[SYS_VAR_BUILD_GIT_HASH_LEN];
	uint32_t sw_qsys_sysid;
	uint32_t sw_qsys_timestamp;
	uint32_t hw_qsys_sysid;
	uint32_t hw_qsys_timestamp;
	uint8_t	 hw_sw_qsys_match;
	uint8_t  mac_addr[6];
	char 	 mac_addr_str[18];

	uint16_t mv2_xaxis;
	uint16_t mv2_yaxis;
	uint16_t mv2_zaxis;
	uint16_t mv2_taxis;
	uint8_t  mv2_enabled;
	uint16_t mv2_data[5];
        uint32_t mv2_counter;
        uint32_t mv2_skip_counter;

	uint64_t chipid;
	uint8_t serdes_locked;
	uint8_t serdes_aligned;
	uint8_t adc_pwr_en[2];
	uint8_t reset_counters;
	uint8_t ext_test_pulse;
	uint8_t use_sata_gxb;
	// uint8_t gxb_reset;

	float temp_scaA;
	float temp_scaB;
	float temp_scaC;
	float temp_scaD;
	float temp_board;
	float i_sca12;
	float v_sca12;
	float i_sca34;
	float v_sca34;
	float i_p2;
	float v_p2;
	float i_p5;
	float v_p5;

        uint32_t raw[13];

	uint16_t test_pulse_width;
	uint8_t test_gate_ena;
	uint8_t test_pulse_trigger;
	uint16_t test_gate_width;

	uint32_t freq_serdes0;
	uint32_t freq_serdes1;
	uint32_t freq_io;
	uint32_t freq_sca_wr;
	uint32_t freq_sca_rd;
	uint32_t freq_sfp;
	uint32_t freq_sata;
	uint32_t cnt_locked[2];
	uint32_t cnt_aligned[2];
	uint32_t lmk_locked;
	uint8_t  invert_ext_trig;

	mv2_settings mv2;
	uint8_t mv2_verify_ok;
} tESPERModuleBoard;

eESPERResponse BoardModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);
tESPERModuleBoard* BoardModuleInit(tESPERModuleBoard* ctx);

#endif /* MOD_BOARD_H_ */
