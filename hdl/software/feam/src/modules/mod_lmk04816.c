/*
 * mod_cc.c
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <drivers/inc/board_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <drivers/inc/altera_avalon_pio_regs.h>
#include <esper_nios.h>
#include "mod_lmk04816.h"

static eESPERResponse Init(tESPERMID mid, tESPERModuleLMK04816* data);
static eESPERResponse Start(tESPERMID mid, tESPERModuleLMK04816* data);
static eESPERResponse Update(tESPERMID mid, tESPERModuleLMK04816* data);

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);
static uint8_t SyncHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx);

tESPERModuleLMK04816* LMK04816ModuleInit(tESPERModuleLMK04816* ctx) {
	if(!ctx) return 0;
	if(!ctx->uwire_write) return 0;
	if(!ctx->uwire_read) return 0;

	return ctx;
}

eESPERResponse LMK04816ModuleHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {

	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleLMK04816*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleLMK04816*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleLMK04816*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleLMK04816* ctx) {
	tESPERVID vid;

	// Get current configuration
	LMK04816_ReadSettings(ctx->uwire_read, &ctx->regmap);

	vid = ESPER_CreateVarBool(mid, "plls_locked", ESPER_OPTION_RD, 1, &ctx->plls_locked, ctx->lmk_stat_ld, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 & PLL2 Locked");

	vid = ESPER_CreateVarUInt32(mid, "lmk_lock_cnt",	ESPER_OPTION_RD, 1, &ctx->lock_counter, ctx->lmk_lock_counter, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 & PLL2 Locked Counter");

	vid = ESPER_CreateVarBool(mid, "holdover_on",	ESPER_OPTION_RD, 1, &ctx->holdover_on, ctx->lmk_stat_hold, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Holdover Mode Active");

	vid = ESPER_CreateVarBool(mid, "sata_sel", 	ESPER_OPTION_RD, 1, &ctx->clk1_los, ctx->lmk_stat_clkin1, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "SATA Clock Selected");

	vid = ESPER_CreateVarBool(mid, "osc_sel",	ESPER_OPTION_RD, 1, &ctx->clk2_los, ctx->lmk_sync_clkin2, 0);
	ESPER_CreateAttrNull(mid, vid, "name", "Oscillator Clock Selected");

	// Map clock cleaner to ESPER registers
	vid = ESPER_CreateVarUInt8(mid, "holdover_mode", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r12.HOLDOVER_MODE, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Holdover Mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Disable", 0x01);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Enable", 0x02);

	vid = ESPER_CreateVarBool(mid, "disable_dld1", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r13.DISABLE_DLD1_DET, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Disable DLD1 Detect");

	vid = ESPER_CreateVarUInt8(mid, "clkin_sel", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r13.CLKin_SELECT_MODE, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "CLKin Select Mode");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin0 (external)", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin1 (backup/sata)", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "CLKin2 (oscillator)", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Auto", 4);

	vid = ESPER_CreateVarBool(mid, "manual_dac_en", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r15.EN_MAN_DAC, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Enable Manual DAC");

	vid = ESPER_CreateVarUInt16(mid, "manual_dac", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r15.MAN_DAC, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Manual DAC Value");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 0);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 1023);

	vid = ESPER_CreateVarBool(mid, "force_holdover", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r15.FORCE_HOLDOVER, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Force Holdover");

	vid = ESPER_CreateVarUInt16(mid, "holdover_cnt", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r15.HOLDOVER_DLD_CNT, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "Holdover DLD Count");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 16383);

	vid = ESPER_CreateVarUInt8(mid, "clkin0_pre_div", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.CLKin0_PreR_DIV, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "CLKin0 Pre R Divider");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "8", 3);

	vid = ESPER_CreateVarUInt8(mid, "clkin1_pre_div", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.CLKin1_PreR_DIV, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "CLKin1 Pre R Divider");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "8", 3);

	vid = ESPER_CreateVarUInt8(mid, "clkin2_pre_div", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.CLKin2_PreR_DIV, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "CLKin2 Pre R Divider");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "1", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "2", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "4", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "8", 3);

	vid = ESPER_CreateVarUInt16(mid, "pll1_r", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.PLL1_R, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 R");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 16383);

	vid = ESPER_CreateVarUInt32(mid, "pll1_n", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r28.PLL1_N, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 N");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 16383);

	vid = ESPER_CreateVarUInt8(mid, "pll1_wnd_size", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r24.PLL1_WND_SIZE, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 Window Size");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "5.5ns", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "10ns", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "18.6ns", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "40ns", 3);

	vid = ESPER_CreateVarUInt16(mid, "pll1_dld_cnt", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r25.PLL1_DLD_CNT, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 DLD Count");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 16383);

	vid = ESPER_CreateVarUInt8(mid, "pll1_cp_pol", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.PLL1_CP_POL, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 CP Polarity");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Negative Slope", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Positive Slope", 1);

	vid = ESPER_CreateVarUInt8(mid, "pll1_cp_gain", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r27.PLL1_CP_GAIN, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL1 CP Gain");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "100uA", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "200uA", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "400uA", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1600uA", 3);

	vid = ESPER_CreateVarUInt16(mid, "pll2_r", ESPER_OPTION_RD, 1, &ctx->regmap.r28.PLL2_R, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 R");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 4095);

	vid = ESPER_CreateVarUInt32(mid, "pll2_n", ESPER_OPTION_RD, 1, &ctx->regmap.r30.PLL2_N, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 N");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 262143);

	vid = ESPER_CreateVarUInt32(mid, "pll2_n_cal", ESPER_OPTION_RD, 1, &ctx->regmap.r29.PLL2_N_CAL, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 N_CAL");
	ESPER_CreateAttrUInt32(mid, vid, "limit", "min", 1);
	ESPER_CreateAttrUInt32(mid, vid, "limit", "max", 262143);

	vid = ESPER_CreateVarUInt8(mid, "pll2_n_pre", ESPER_OPTION_RD, 1, &ctx->regmap.r30.PLL2_P, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 N Pre-divider");
	ESPER_CreateAttrUInt8(mid, vid, "limit", "min", 2);
	ESPER_CreateAttrUInt8(mid, vid, "limit", "max", 8);

	vid = ESPER_CreateVarUInt8(mid, "pll2_wnd_size", ESPER_OPTION_RD, 1, &ctx->regmap.r26.PLL2_WND_SIZE, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 Window Size");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "reserved", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "reserved", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "3.7ns", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "reserved", 3);

	vid = ESPER_CreateVarUInt8(mid, "pll2_cp_pol", ESPER_OPTION_RD, 1, &ctx->regmap.r26.PLL2_CP_POL, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 CP Polarity");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "Negative Slope", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "Positive Slope", 1);

	vid = ESPER_CreateVarUInt8(mid, "pll2_cp_gain", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r26.PLL2_CP_GAIN, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "PLL2 CP Gain");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "100uA", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "400uA", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1600uA", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "3200uA", 3);
	/*

	vid = ESPER_CreateVarUInt8(mid, "adc_adly", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r6.CLKout0_1_ADLY, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Analog Delay");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "500ps", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "525ps", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "550ps", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "575ps", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "600ps", 4);
	ESPER_CreateAttrUInt8(mid, vid, "option", "625ps", 5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "650ps", 6);
	ESPER_CreateAttrUInt8(mid, vid, "option", "675ps", 7);
	ESPER_CreateAttrUInt8(mid, vid, "option", "700ps", 8);
	ESPER_CreateAttrUInt8(mid, vid, "option", "725ps", 9);
	ESPER_CreateAttrUInt8(mid, vid, "option", "750ps", 10);
	ESPER_CreateAttrUInt8(mid, vid, "option", "775ps", 11);
	ESPER_CreateAttrUInt8(mid, vid, "option", "800ps", 12);
	ESPER_CreateAttrUInt8(mid, vid, "option", "825ps", 13);
	ESPER_CreateAttrUInt8(mid, vid, "option", "850ps", 14);
	ESPER_CreateAttrUInt8(mid, vid, "option", "875ps", 15);
	ESPER_CreateAttrUInt8(mid, vid, "option", "900ps", 16);
	ESPER_CreateAttrUInt8(mid, vid, "option", "925ps", 17);
	ESPER_CreateAttrUInt8(mid, vid, "option", "950ps", 18);
	ESPER_CreateAttrUInt8(mid, vid, "option", "975ps", 19);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1000ps", 20);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1025ps", 21);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1050ps", 22);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1075ps", 23);

	vid = ESPER_CreateVarUInt8(mid, "sca_adly", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r7.CLKout4_5_ADLY, 0, Handler);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Analog Delay");
	ESPER_CreateAttrNull(mid, vid, "format", "select");
	ESPER_CreateAttrUInt8(mid, vid, "option", "500ps", 0);
	ESPER_CreateAttrUInt8(mid, vid, "option", "525ps", 1);
	ESPER_CreateAttrUInt8(mid, vid, "option", "550ps", 2);
	ESPER_CreateAttrUInt8(mid, vid, "option", "575ps", 3);
	ESPER_CreateAttrUInt8(mid, vid, "option", "600ps", 4);
	ESPER_CreateAttrUInt8(mid, vid, "option", "625ps", 5);
	ESPER_CreateAttrUInt8(mid, vid, "option", "650ps", 6);
	ESPER_CreateAttrUInt8(mid, vid, "option", "675ps", 7);
	ESPER_CreateAttrUInt8(mid, vid, "option", "700ps", 8);
	ESPER_CreateAttrUInt8(mid, vid, "option", "725ps", 9);
	ESPER_CreateAttrUInt8(mid, vid, "option", "750ps", 10);
	ESPER_CreateAttrUInt8(mid, vid, "option", "775ps", 11);
	ESPER_CreateAttrUInt8(mid, vid, "option", "800ps", 12);
	ESPER_CreateAttrUInt8(mid, vid, "option", "825ps", 13);
	ESPER_CreateAttrUInt8(mid, vid, "option", "850ps", 14);
	ESPER_CreateAttrUInt8(mid, vid, "option", "875ps", 15);
	ESPER_CreateAttrUInt8(mid, vid, "option", "900ps", 16);
	ESPER_CreateAttrUInt8(mid, vid, "option", "925ps", 17);
	ESPER_CreateAttrUInt8(mid, vid, "option", "950ps", 18);
	ESPER_CreateAttrUInt8(mid, vid, "option", "975ps", 19);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1000ps", 20);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1025ps", 21);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1050ps", 22);
	ESPER_CreateAttrUInt8(mid, vid, "option", "1075ps", 23);
	*/


	/*

	vid = ESPER_CreateVarUInt16(mid, "adc_ddelay", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r0.CLKout0_1_DDLY, 0, SyncHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Digital Delay");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 5);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 522);

	vid = ESPER_CreateVarBool(mid, "adc_ddlay_hs", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r0.CLKout0_1_HS, 0, SyncHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "ADC Digital Delay Half-step");
	 */

	vid = ESPER_CreateVarUInt16(mid, "sca_ddelay", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r2.CLKout4_5_DDLY, 0, SyncHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Digital Delay");
	ESPER_CreateAttrUInt16(mid, vid, "limit", "min", 5);
	ESPER_CreateAttrUInt16(mid, vid, "limit", "max", 522);

	/*
	vid = ESPER_CreateVarBool(mid, "sca_ddlay_hs", ESPER_OPTION_WR_RD, 1, &ctx->regmap.r2.CLKout4_5_HS, 0, SyncHandler);
	ESPER_CreateAttrNull(mid, vid, "name", "SCA Digital Delay Half-step");
	*/

	// Make sure these settings are active
	LMK04816_WriteSettings(ctx->uwire_write, &ctx->regmap);
	usleep(1000000);

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleLMK04816* ctx) {
	ESPER_WriteVarUInt16(mid, ESPER_GetVarIdByKey(mid, "sca_ddelay"), 0, 5);

	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleLMK04816* ctx) {

	return ESPER_RESP_OK;
}

static uint8_t Handler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleLMK04816* context;

	context = (tESPERModuleLMK04816*)(ctx);

	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		LMK04816_WriteSettings(context->uwire_write, &context->regmap);
		break;
	default:
		break;
	}

	return 1;
}

static uint8_t SyncHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleLMK04816* context;

	context = (tESPERModuleLMK04816*)(ctx);

	switch(request) {
	case ESPER_REQUEST_WRITE_POST:
		LMK04816_WriteSyncSettings(context->uwire_write, &context->regmap);
		*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 1;
		usleep(10);
		*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 0;
		usleep(10);
		break;
	default:
		break;
	}

	return 1;
}

