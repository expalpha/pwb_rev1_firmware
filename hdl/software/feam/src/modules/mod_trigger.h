/*
 * mod_trigger.h
 *
 *  Created on: Nov 14, 2016
 *      Author: bryerton
 */

#ifndef MOD_TRIGGER_H_
#define MOD_TRIGGER_H_

#include <esper.h>

typedef struct {
	uint32_t trig_ctrl_base;
	uint32_t trig_stat_base;

	uint8_t global_ena;
	uint32_t total_accepted;
	uint32_t total_dropped;
	uint32_t total_requested;
	uint32_t num_trig_src;
	uint32_t num_busy_src;

	uint8_t *src_enable;
	uint8_t *src_invert;
	uint32_t *src_delay;

	uint32_t *busy_source;

	uint32_t *src_requested;
	uint32_t *src_accepted;
	uint32_t *src_dropped;
	uint32_t *src_dropped_busy;
	uint32_t *src_dropped_delay;
	uint32_t *src_dropped_priority;
	uint32_t *src_dropped_trigger;
} tESPERModuleTrigger;

tESPERModuleTrigger* ModuleTriggerInit(uint32_t trig_ctrl_base, uint32_t trig_stat_base, uint32_t num_trig_src, uint32_t num_busy_src, tESPERModuleTrigger* ctx);
eESPERResponse ModuleTriggerHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);

#endif /* MOD_TRIGGER_H_ */
