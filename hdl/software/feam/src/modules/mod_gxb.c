/*
 * mod_gxb.c
 *
 *  Created on: Apr 3, 2017
 *      Author: admin
 */


#include <assert.h>
#include <system.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mod_gxb.h"
#include <esper_nios.h>
#include <drivers/inc/gxb_module_regs.h>

static eESPERResponse Init(tESPERMID mid, tESPERModuleGXB* data);
static eESPERResponse Start(tESPERMID mid, tESPERModuleGXB* data);
static eESPERResponse Update(tESPERMID mid, tESPERModuleGXB* data);


tESPERModuleGXB* ModuleGXBInit(tESPERModuleGXB* ctx) {
	if(!ctx) return 0;

	return ctx;
}

eESPERResponse ModuleGXBHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx) {

	switch(state) {
	case ESPER_STATE_INIT:
		return Init(mid, (tESPERModuleGXB*)ctx);
	case ESPER_STATE_START:
		return Start(mid, (tESPERModuleGXB*)ctx);
	case ESPER_STATE_UPDATE:
		return Update(mid, (tESPERModuleGXB*)ctx);
	case ESPER_STATE_STOP:
		break;
	}

	return ESPER_RESP_OK;
}

#include "altera_eth_tse_regs.h"
#include "system.h"

static void start_stop_eth(int do_stop)
{
   //
   // enable or disable ethernet transmitter and receiver
   //
   // - turn off ALTERA_TSEMAC_CMD_TX_ADDR_INS_MSK - it replaces the MAC address of the other PWB with our MAC address
   // - turn off MAC address filter (promisc mode) - MAC address filter kills packets destined to the other PWB
   // - instead of turning off the MAC address filter, we should write the MAC address of the other PWB into our secondary MAC address register
   //   but we do not know the MAC address of the other PWB yet.
   //

   uint32_t cmd_config = IORD_ALTERA_TSEMAC_CMD_CONFIG(ETH_TSE_BASE);

   uint32_t cmd_config_wr = cmd_config;

   if (do_stop) {
      // stop ethernet
      cmd_config_wr &= ~(ALTERA_TSEMAC_CMD_TX_ENA_MSK);
      cmd_config_wr &= ~(ALTERA_TSEMAC_CMD_RX_ENA_MSK);
   } else {
      // start ethernet
      cmd_config_wr |= ALTERA_TSEMAC_CMD_TX_ENA_MSK;
      cmd_config_wr |= ALTERA_TSEMAC_CMD_RX_ENA_MSK;
   }

   //printf("MAC register CMD_CONFIG: 0x%08x -> 0x%08x\n", (unsigned int)cmd_config, (unsigned int)cmd_config_wr);

   IOWR_ALTERA_TSEMAC_CMD_CONFIG(ETH_TSE_BASE, cmd_config_wr);
}

static uint8_t StopEthHandler(tESPERMID mid, tESPERVID vid, tESPERVar* var, eESPERRequest request, uint32_t offset, uint32_t num_elements, void* ctx) {
	tESPERModuleGXB* context;

	context = (tESPERModuleGXB*)ctx;

	switch(request) {
		case ESPER_REQUEST_INIT:
		case ESPER_REQUEST_REFRESH:
			break;
		case ESPER_REQUEST_WRITE_PRE:
			break;
		 case ESPER_REQUEST_WRITE_POST:
			if(context->stop_eth) {
                           //printf("stop the ethernet!\n");
                           start_stop_eth(1);
			} else {
                           //printf("start the ethernet!\n");
                           start_stop_eth(0);
			}
			break;
		case ESPER_REQUEST_READ_PRE:
			break;
	}

	return 1;
}

static eESPERResponse Init(tESPERMID mid, tESPERModuleGXB* ctx) {
   tESPERVID vid;
	vid = ESPER_CreateVarBool(mid, "loopback_en", 	ESPER_OPTION_WR_RD, 1, &ctx->loopback_en, (uint8_t*)GET_REG_OFFSET(ctx->ctrl_base, GXB_MOD_REG_CTRL_SERIAL_LPBK_EN), 0);
	vid = ESPER_CreateVarBool(mid, "rx_invert", 		ESPER_OPTION_WR_RD, 1, &ctx->rx_invert, (uint8_t*)GET_REG_OFFSET(ctx->ctrl_base, GXB_MOD_REG_CTRL_RX_INVERT), 0);
	vid = ESPER_CreateVarBool(mid, "reset_counters", 	ESPER_OPTION_WR_RD, 1, &ctx->reset_counters, (uint8_t*)GET_REG_OFFSET(ctx->ctrl_base, GXB_MOD_REG_CTRL_RESET_COUNTERS), 0);
	vid = ESPER_CreateVarUInt32(mid, "link_ctrl",	        ESPER_OPTION_WR_RD, 1, &ctx->link_ctrl, (uint32_t*)GET_REG_OFFSET(ctx->ctrl_base, GXB_MOD_REG_CTRL_LINK_CTRL), 0);
        ESPER_CreateAttrNull(mid, vid, "format", "hex");
	vid = ESPER_CreateVarBool(mid, "stop_eth", 	ESPER_OPTION_WR_RD, 1, &ctx->stop_eth, 0, StopEthHandler);

	ESPER_CreateVarBool(mid, "pll_locked",		ESPER_OPTION_RD, 1, &ctx->pll_locked,     (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_PLL_LOCKED), 0);
	ESPER_CreateVarBool(mid, "is_locked_dat",	ESPER_OPTION_RD, 1, &ctx->is_locked_dat,  (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_IS_RX_LOCKED_TO_DATA), 0);
	ESPER_CreateVarBool(mid, "is_locked_ref",	ESPER_OPTION_RD, 1, &ctx->is_locked_ref,  (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_IS_RX_LOCKED_TO_REF), 0);
	ESPER_CreateVarBool(mid, "signal_detect",	ESPER_OPTION_RD, 1, &ctx->signal_detect,  (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_SIGNAL_DETECT), 0);
	ESPER_CreateVarBool(mid, "rx_err_detect",	ESPER_OPTION_RD, 1, &ctx->rx_err_detect,  (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_RX_ERR_DETECT), 0);
	ESPER_CreateVarBool(mid, "rx_pattern_detect",	ESPER_OPTION_RD, 1, &ctx->rx_pattern_det, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_RX_PATTERN_DETECT), 0);
	ESPER_CreateVarBool(mid, "rx_err_parity",	ESPER_OPTION_RD, 1, &ctx->rx_err_parity,  (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_RX_ERR_DISPARITY), 0);
	ESPER_CreateVarBool(mid, "rx_sync_status",	ESPER_OPTION_RD, 1, &ctx->rx_sync_status, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_RX_SYNC_STATUS), 0);
	ESPER_CreateVarBool(mid, "link_status",		ESPER_OPTION_RD, 1, &ctx->link_status,    (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_LINK_STATUS), 0);

	ESPER_CreateVarUInt32(mid, "cnt_pll_lock",	ESPER_OPTION_RD, 1, &ctx->cnt_pll_lock,       (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_PLL_LOCKED), 0);
	ESPER_CreateVarUInt32(mid, "cnt_lock_dat",	ESPER_OPTION_RD, 1, &ctx->cnt_lock_dat,       (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_IS_LOCKED_TO_DATA), 0);
	ESPER_CreateVarUInt32(mid, "cnt_lock_ref",	ESPER_OPTION_RD, 1, &ctx->cnt_lock_ref,       (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_IS_LOCKED_TO_REF), 0);
	ESPER_CreateVarUInt32(mid, "cnt_signal_det",	ESPER_OPTION_RD, 1, &ctx->cnt_signal_det,     (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_SIGNAL_DETECT), 0);
	ESPER_CreateVarUInt32(mid, "cnt_rx_err_det",	ESPER_OPTION_RD, 1, &ctx->cnt_rx_err_det,     (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_RX_ERR_DETECT), 0);
	ESPER_CreateVarUInt32(mid, "cnt_rx_err_par",	ESPER_OPTION_RD, 1, &ctx->cnt_rx_err_par,     (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_RX_ERR_DISPARITY), 0);
	ESPER_CreateVarUInt32(mid, "cnt_rx_pattern_det",ESPER_OPTION_RD, 1, &ctx->cnt_rx_pattern_det, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_RX_PATTERN_DETECT), 0);
	ESPER_CreateVarUInt32(mid, "cnt_rx_sync_stat",	ESPER_OPTION_RD, 1, &ctx->cnt_rx_sync_stat,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_RX_SYNC_STATUS), 0);

	ESPER_CreateVarUInt32(mid, "cnt_link_stat",	ESPER_OPTION_RD, 1, &ctx->cnt_link_stat, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_LINK_STATUS), 0);

	ESPER_CreateVarUInt32(mid, "cnt_badk_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_badk_rx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_BADK_RX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_trig_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_trig_rx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_TRIG_RX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_stop_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_stop_rx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_STOP_RX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_ovfl_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_ovfl_rx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_OVFL_RX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_octets_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_octets_rx, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_OCTETS_RX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_pkts_rx",	ESPER_OPTION_RD, 1, &ctx->cnt_pkts_rx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_PKTS_RX), 0);

	ESPER_CreateVarUInt32(mid, "cnt_trig_tx",	ESPER_OPTION_RD, 1, &ctx->cnt_trig_tx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_TRIG_TX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_stop_tx",	ESPER_OPTION_RD, 1, &ctx->cnt_stop_tx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_STOP_TX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_octets_tx",	ESPER_OPTION_RD, 1, &ctx->cnt_octets_tx, (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_OCTETS_TX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_pkts_tx",	ESPER_OPTION_RD, 1, &ctx->cnt_pkts_tx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_PKTS_TX), 0);
	ESPER_CreateVarUInt32(mid, "cnt_flow_tx",	ESPER_OPTION_RD, 1, &ctx->cnt_flow_tx,   (uint8_t*)GET_REG_OFFSET(ctx->stat_base, GXB_MOD_REG_STAT_CNT_FLOW_TX), 0);

	return ESPER_RESP_OK;
}

static eESPERResponse Start(tESPERMID mid, tESPERModuleGXB* ctx) {
	return ESPER_RESP_OK;
}

static eESPERResponse Update(tESPERMID mid, tESPERModuleGXB* ctx) {
	return ESPER_RESP_OK;
}

//end
