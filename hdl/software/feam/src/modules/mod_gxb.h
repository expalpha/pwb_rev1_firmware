/*
 * mod_gxb.h
 *
 *  Created on: Apr 3, 2017
 *      Author: admin
 */

#ifndef MOD_GXB_H_
#define MOD_GXB_H_

#include <unistd.h>
#include <esper.h>

typedef struct {
	uint32_t ctrl_base;
	uint32_t stat_base;

	uint8_t loopback_en;
	uint8_t reset_counters;
        uint32_t link_ctrl;
        uint8_t stop_eth;

	uint8_t pll_locked;
	uint8_t is_locked_dat;
	uint8_t is_locked_ref;
	uint8_t signal_detect;
	uint8_t rx_err_detect;
	uint8_t rx_err_parity;
	uint8_t rx_sync_status;
	uint8_t link_status;
	uint8_t rx_invert;
	uint8_t rx_pattern_det;

	uint32_t cnt_pll_lock;
	uint32_t cnt_lock_dat;
	uint32_t cnt_lock_ref;
	uint32_t cnt_signal_det;
	uint32_t cnt_rx_err_det;
	uint32_t cnt_rx_err_par;
	uint32_t cnt_rx_sync_stat;
	uint32_t cnt_rx_pattern_det;

	uint32_t cnt_link_stat;

	uint32_t cnt_badk_rx;
	uint32_t cnt_trig_rx;
	uint32_t cnt_stop_rx;
	uint32_t cnt_ovfl_rx;
	uint32_t cnt_octets_rx;
	uint32_t cnt_pkts_rx;

	uint32_t cnt_trig_tx;
	uint32_t cnt_stop_tx;
	uint32_t cnt_pkts_tx;
	uint32_t cnt_octets_tx;
	uint32_t cnt_flow_tx;

} tESPERModuleGXB;

eESPERResponse ModuleGXBHandler(tESPERMID mid, tESPERGID gid, eESPERState state, void* ctx);
tESPERModuleGXB* ModuleGXBInit(tESPERModuleGXB* ctx);

#endif /* MOD_GXB_H_ */
