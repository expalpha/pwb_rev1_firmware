/*
 * AFTER_slowcontrol.c
 *
 *  Created on: Jan 23, 2017
 *      Author: bryerton
 */

#include <assert.h>
#include "AFTER_slowcontrol.h"

// Helper functions for using Injection Registers
uint8_t AFTER_ConvertChToBit(uint8_t ch) {
	if((ch>0) && (ch<13)) { // 1-12
		ch = 38 - ch;
	} else if((ch>12) && (ch<25)) { // 13-24
		ch = 37 - ch;
	} else if((ch>24) && (ch<37)) { // 25-36
		ch = 36 - ch;
	} else if((ch>36) && (ch<49)) { // 37-48
		ch = ch - 37;
	} else if((ch>48) && (ch<61)) { // 49-60
		ch = ch - 36;
	} else if((ch>60) && (ch<73)) { // 61-72
		ch = ch - 35;
	} else {
		ch = 0;
	}

	return ch;
}

uint8_t AFTER_ConvertCFPNToBit(uint8_t cfpn) {
	switch(cfpn) {
	case 1:
		return 25;
	case 2:
		return 12;
	case 3:
		return 12;
	case 4:
		return 25;
	default:
		return 0;
	}

	return 0;
}

void AFTER_WriteConfig1(AFTER_write wr, tAFTER_Reg_Config1* reg) {
	uint64_t cmd;

	assert(wr != 0);
	assert(reg != 0);

	if(!wr) return;
	if(!reg) return;

	cmd = (reg->Icsa & 0x1) 					   |
		  ((reg->Gain & 0x3) 				<<  1) |
		  ((reg->Time & 0xF) 				<<  3) |
		  ((reg->Test & 0x3) 				<<  7) |
		  ((reg->Integrator_mode & 0x1)		<<  9) |
		  ((reg->power_down_write & 0x1) 	<< 13) |
		  ((reg->power_down_read & 0x1) 	<< 14) |
		  ((reg->alternate_power & 0x1) 	<< 15);

	// For us, the write starts at bit 38
	cmd = cmd << 22;

	wr(AFTER_REG_CONFIG_1, 16, cmd);
}

void AFTER_WriteConfig2(AFTER_write wr, tAFTER_Reg_Config2* reg) {
	uint64_t cmd;

	assert(wr != 0);
	assert(reg != 0);

	if(!wr) return;
	if(!reg) return;

	cmd = (reg->debug & 0x3) |
		  ((reg->read_from_0 & 0x1) <<  3) |
		  ((reg->test_digout & 0x1) <<  4) |
		  ((reg->en_mker_rst & 0x1) <<  6) |
		  ((reg->rst_lv_to_1 & 0x1) <<  7) |
		  ((reg->boost_pw & 0x1) 	<<  8) |
		  ((reg->out_resync & 0x1) 	<<  9) |
		  ((reg->synchro_inv & 0x1) << 10) |
		  ((reg->force_eout & 0x1) 	<< 11) |
		  ((reg->Cur_RA & 0x3) 		<< 12) |
		  ((reg->Cur_BUF & 0x3)		<< 14);

	// For us, the write starts at bit 38
	cmd = cmd << 22;

	wr(AFTER_REG_CONFIG_2, 16, cmd);
}

void AFTER_WriteInjection(AFTER_write wr, tAFTER_Reg_Injection* reg) {
	uint8_t n;
	uint64_t cmd;

	assert(wr != 0);
	assert(reg != 0);

	if(!wr) return;
	if(!reg) return;

	// Off-by-one offset between our register vs the SCA select_chN
	cmd = 0;

	for(n=0; n<36; n++) {
		cmd |= (uint64_t)reg->select_ch[n] << AFTER_ConvertChToBit(n+1);
	}

	for(n=0; n<2; n++) {
		cmd |= (uint64_t)reg->select_cfpn[n] << AFTER_ConvertCFPNToBit(n+1);
	}
	wr(AFTER_REG_INJECT_1, 38, cmd);

	cmd = 0;
	for(n=36; n<72; n++) {
		cmd |= (uint64_t)reg->select_ch[n] << AFTER_ConvertChToBit(n+1);
	}

	for(n=2; n<4; n++) {
		cmd |= (uint64_t)reg->select_cfpn[n] << AFTER_ConvertCFPNToBit(n+1);
	}
	wr(AFTER_REG_INJECT_2, 38, cmd);
}

void AFTER_ReadConfig1(AFTER_read rd, tAFTER_Reg_Config1* reg) {
	uint64_t cmd;

	assert(rd != 0);
	assert(reg != 0);

	if(!rd) return;
	if(!reg) return;

	cmd = rd(AFTER_REG_CONFIG_1, 16);

	reg->Icsa = cmd & 0x1;
	reg->Gain = (cmd >> 1) & 0x3;
	reg->Time = (cmd >> 3) & 0xF;
	reg->Test = (cmd >> 7) & 0x3;
	reg->Integrator_mode = (cmd >> 9) & 0x1;
	reg->power_down_write = (cmd >> 13) & 0x1;
	reg->power_down_read = (cmd >> 14) & 0x1;
	reg->alternate_power = (cmd >> 15) & 0x1;
}

void AFTER_ReadConfig2(AFTER_read rd, tAFTER_Reg_Config2* reg) {
	uint64_t cmd;

	assert(rd != 0);
	assert(reg != 0);

	if(!rd) return;
	if(!reg) return;

	cmd = rd(AFTER_REG_CONFIG_2, 16);

	reg->debug = cmd & 0x3;
	reg->read_from_0 = (cmd >> 3) & 0x1;
	reg->test_digout = (cmd >> 4) & 0x1;
	reg->en_mker_rst = (cmd >> 6) & 0x1;
	reg->rst_lv_to_1 = (cmd >> 7) & 0x1;
	reg->boost_pw 	 = (cmd >> 8) & 0x1;
	reg->out_resync  = (cmd >> 9) & 0x1;
	reg->synchro_inv = (cmd >> 10) & 0x1;
	reg->force_eout  = (cmd >> 11) & 0x1;
	reg->Cur_RA 	 = (cmd >> 12) & 0x3;
	reg->Cur_BUF 	 = (cmd >> 14) & 0x3;
}

void AFTER_ReadVersion(AFTER_read rd, uint16_t* reg) {
	uint64_t cmd;

	assert(rd != 0);
	assert(reg != 0);

	if(!rd) return;
	if(!reg) return;

	cmd = rd(AFTER_REG_VERSION, 16);

	*reg = cmd & 0xFFFF;
}

void AFTER_ReadInjection(AFTER_read rd, tAFTER_Reg_Injection* reg) {
	uint8_t n;
	uint64_t cmd;

	assert(rd != 0);
	assert(reg != 0);

	if(!rd) return;
	if(!reg) return;

	cmd = rd(AFTER_REG_INJECT_1, 38);

	for(n=0; n<36; n++) {
		reg->select_ch[n] = (cmd >> AFTER_ConvertChToBit(n+1)) & 0x1;
	}

	for(n=0; n<2; n++) {
		reg->select_cfpn[n] = (cmd >> AFTER_ConvertCFPNToBit(n+1)) & 0x1;
	}

	cmd = rd(AFTER_REG_INJECT_2, 38);

	for(n=36; n<72; n++) {
		reg->select_ch[n] = (cmd >> AFTER_ConvertChToBit(n+1)) & 0x1;
	}

	for(n=2; n<4; n++) {
		reg->select_cfpn[n] = (cmd >> AFTER_ConvertCFPNToBit(n+1)) & 0x1;
	}
}

void AFTER_WriteSettings(AFTER_write wr, tAFTER_Registers* settings) {
	assert(wr != 0);
	assert(settings != 0);

	if(!wr) return;
	if(!settings) return;

	AFTER_WriteConfig1(wr, &settings->config1);
	AFTER_WriteConfig2(wr, &settings->config2);
	AFTER_WriteInjection(wr, &settings->injection);
}

void AFTER_ReadSettings(AFTER_read rd, tAFTER_Registers* settings) {
	assert(rd != 0);
	assert(settings != 0);

	if(!rd) return;
	if(!settings) return;

	AFTER_ReadConfig1(rd, &settings->config1);
	AFTER_ReadConfig2(rd, &settings->config2);
	AFTER_ReadInjection(rd, &settings->injection);
	AFTER_ReadVersion(rd, &settings->version);
}


