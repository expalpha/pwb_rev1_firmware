/*
 * storage_flash.h
 *
 *  Created on: Mar 25, 2017
 *      Author: admin
 */

#ifndef STORAGE_EPCQ_H_
#define STORAGE_EPCQ_H_

#include <esper/esper.h>
#include <drivers/inc/altera_epcq_controller_regs.h>
#include <drivers/inc/altera_epcq_controller.h>
#include <sys/alt_flash.h>

typedef struct {
	char device[32];
	alt_flash_fd* fd;
	alt_epcq_controller_dev* dev;
	uint32_t user_offset; // offset in flash memory
	uint32_t user_maxlen; // maximum storage area allowed

	tESPERStorage base;
} tESPERStorageEPCQ;

tESPERStorage* EPCQStorage(const char* dev, uint32_t offset, uint32_t length, tESPERStorageEPCQ* ctx);

#endif /* STORAGE_FLASH_H_ */
