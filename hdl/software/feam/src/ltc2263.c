/*
 * ltc2263.c
 *
 *  Created on: Jan 22, 2017
 *      Author: bryerton
 */

#include <assert.h>
#include "ltc2263.h"

/*
// Example SPI read and write functions for Altera SPI driver
// Replace SPI_LTC2263_BASE with appropriate base address of ADC SPI

static uint8_t ltc2263_spi_read(uint8_t addr) {
 	alt_u8 tx_data[1];
 	alt_u8 rx_data[1];

	tx_data[0] = 0x80 | (addr & 0x7F);
	alt_avalon_spi_command(SPI_LTC2263_BASE, 0, 1, tx_data, 1, rx_data, 0);

	return rx_data[0];
}

static void ltc2263_spi_write(uint8_t addr, uint8_t reg) {
	alt_u8 tx_data[2];

	tx_data[0] = addr & 0x7F;
	tx_data[1] = reg;
	alt_avalon_spi_command(SPI_LTC2263_BASE, 0, 2, tx_data, 0, 0, 0);
}

*/

void LTC2263_Reset(ltc2263_write wr) {
	wr(LTC2263_REG_RESET, 0x80);
}

void LTC2263_SetFormatAndPower(ltc2263_write wr, uint8_t dcs_off, uint8_t rand_on, uint8_t twos_comp, uint8_t sleep) {
	// Fix and spread out the sleep/nap2/nap1 bits
	sleep = (sleep & 0x7) << 2;
	if(sleep & 0x4) {
		sleep |= 1;
		sleep &= ~(0x4);
	}

	wr(LTC2263_REG_FORMAT_AND_POWERDOWN,
		((dcs_off & 0x1) 	<< 7) |
		((rand_on & 0x1) 	<< 6) |
		((twos_comp & 0x1)	<< 5) |
		sleep
	);
}

void LTC2263_OutputMode(ltc2263_write wr, uint8_t lvds_current, uint8_t termination, uint8_t output_disable, uint8_t output_mode) {
	wr(LTC2263_REG_OUTPUT_MODE,
		((lvds_current & 0x7) 	<< 5) |
		((termination & 0x1) 	<< 4) |
		((output_disable & 0x1) << 3) |
		((output_mode & 0x7) 		)
	);
}

void LTC2263_TestPattern(ltc2263_write wr, uint8_t test_on, uint16_t test_pattern) {
	wr(LTC2263_REG_TEST_PATTERN_MSB,
		((test_on & 0x1) << 7) |
		((test_pattern & 0x3FFF) >> 8)
	);

	wr(LTC2263_REG_TEST_PATTERN_LSB, test_pattern & 0xFF);
}

void LTC2263_ReadSettings(ltc2263_read rd, tLTC2263* settings) {
	uint8_t reg;

	assert(settings != 0);

	if(!settings) return;

	reg = rd(LTC2263_REG_FORMAT_AND_POWERDOWN);
	settings->dcs_disable 		= (reg >> 7) & 0x1;
	settings->randomizer_en 	= (reg >> 6) & 0x1;
	settings->data_format		= (reg >> 5) & 0x1;
	settings->sleep_mode		= ((reg >> 2) & 0x6) | (reg & 0x1); // recombine sleep/nap2/nap1

	reg = rd(LTC2263_REG_OUTPUT_MODE);
	settings->lvds_current 		= (reg >> 5) & 0x7;
	settings->lvds_term_en 		= (reg >> 4) & 0x1;
	settings->output_disable 	= (reg >> 3) & 0x1;
	settings->output_mode		= reg & 0x7;

	reg = rd(LTC2263_REG_TEST_PATTERN_MSB);
	settings->test_en 			= (reg >> 7) & 0x1;
	settings->test_pattern		= (reg & 0x3F) << 8;

	reg = rd(LTC2263_REG_TEST_PATTERN_LSB);
	settings->test_pattern |= reg;

}

void LTC2263_WriteSettings(ltc2263_write wr, tLTC2263* settings) {
	assert(settings != 0);

	if(!settings) return;

	LTC2263_SetFormatAndPower(wr, settings->dcs_disable, settings->randomizer_en, settings->data_format, settings->sleep_mode);
	LTC2263_OutputMode(wr, settings->lvds_current, settings->lvds_term_en, settings->output_disable, settings->output_mode);
	LTC2263_TestPattern(wr, settings->test_en, settings->test_pattern);
}

