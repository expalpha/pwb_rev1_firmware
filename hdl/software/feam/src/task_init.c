#include <stdio.h>
#include <unistd.h>
#include <system.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include <altera_avalon_pio_regs.h>
#include <drivers/inc/board_regs.h>
#include "includes.h"
#include "build_number.h"
#include <esper.h>
#include <drivers/inc/altera_remote_update_regs.h>

#include "task_init.h"
#include "task_esper.h"
#include "lmk04816.h"

#define GET_REG_OFFSET(base,reg) (((uint8_t*)(base) + ((reg)*4)))

static void cc_spi_write(uint8_t addr, uint32_t cmd);
static uint32_t cc_spi_read(uint8_t addr);
static void InitLMK04816(void);
static void task_init(void* pdata);

static OS_STK task_init_stk[TASK_STACKSIZE];

#include "dhcpclnt.h"

int main(void) {
	OSTaskCreateExt(task_init,
		NULL,
		(void *)&task_init_stk[TASK_STACKSIZE-1],
		TASK_INIT_PRIORITY,
		TASK_INIT_PRIORITY,
		task_init_stk,
		TASK_STACKSIZE,
		NULL,
		0);

	OSStart();

	return 0;
}

// we overwrite the altera jtag uart "write" function
// because there is no way to prevent the stock version
// from waiting forever if output fifo is full and
// nios2-terminal is not connected. K.O. Aug 2020.

#include "altera_avalon_jtag_uart_regs.h"
#include "altera_avalon_jtag_uart.h"

int altera_avalon_jtag_uart_write(altera_avalon_jtag_uart_state* sp, 
  const char * ptr, int count, int flags)
{
  unsigned int base = sp->base;

  const char * end = ptr + count;

  //while (ptr < end)
  //  if ((IORD_ALTERA_AVALON_JTAG_UART_CONTROL(base) & ALTERA_AVALON_JTAG_UART_CONTROL_WSPACE_MSK) != 0)
  //    IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  while (ptr < end)
     IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  return count;
}

static void task_init(void* pdata) {
	printf("\nPWB Revision 1 Firmware...\n");
        usleep(500000);
        setbuf(stdout, NULL);

	printf("%s\n", VERSION_STR);
	printf("Git Hash: %s\n", GIT_HASH_STR);
	printf("Git Branch [Tag]: %s [%s]\n", GIT_BRANCH_STR, GIT_TAG_STR);
	printf("Built by: %s (%s)\n", BUILT_BY_USER_STR, BUILT_BY_EMAIL_STR);
        
	printf("Watchdog enable: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE));
	printf("Watchdog timeout: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE));

        if (0) {
           // test for jtag uart fifo overflow. without my workaround,
           // program will stop and wait until nios-terminal is connected.
           // with the workaround, fifo over flow is ignored, additional
           // characters are lost. K.O. Aug 2020.
           for (int i=0; i<400; i++) {
              uint32_t reg0 = IORD_ALTERA_AVALON_JTAG_UART_DATA(JTAGUART_0_BASE);
              uint32_t reg1 = IORD_ALTERA_AVALON_JTAG_UART_CONTROL(JTAGUART_0_BASE);
              printf("jtag uart regs: data 0x%lx ctrl 0x%lx, loop 0x%x\n", reg0, reg1, i);
           }
        }

        // reset the watchdog to avoid fpga reboot during many attempts to start
        // this software in the eclipse debugger, where it keep crashing
        // during the clock cleaner reset. K.O.
        IOWR_ALTERA_RU_RESET_TIMER(REMOTE_UPDATE_BASE, 1);

        //uint32_t current_page = IORD_ALTERA_RU_PAGE_SELECT(REMOTE_UPDATE_BASE);
        // Enable Watchdog if the Application page is loaded and remote configuration mode is set
        //if((current_page != 0x0) && (current_page != 0xffffff00)) {
        //#define WATCHDOG_TIMEOUT_PERIOD ((12500000u * 30u) >> 17)
        //#define WATCHDOG_TIMEOUT_PERIOD ((12500000u * 1u) >> 17)
        //#define WATCHDOG_TIMEOUT_PERIOD (1)
        //   printf("Setting Watchdog Timeout 0%08x\n", WATCHDOG_TIMEOUT_PERIOD);
        //   IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE, WATCHDOG_TIMEOUT_PERIOD);
        //   printf("Enabling Watchdog\n");
        //   IOWR_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE, ALTERA_RU_WATCHDOG_ENABLE);
        //
        // printf("Watchdog enable: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_ENABLE(REMOTE_UPDATE_BASE));
        // printf("Watchdog timeout: 0x%08x\n", IORD_ALTERA_RU_WATCHDOG_TIMEOUT(REMOTE_UPDATE_BASE));
        //}

	// Initialize the LMK04816 Clock cleaner
	InitLMK04816();

	// Initialize Altera NicheStack TCP/IP Stack - Nios II Edition specific code.
	alt_iniche_init();

	// Start the Iniche-specific network tasks and initialize the network devices
	netmain();

	printf("after netmain, waiting for iniche_net_ready!\n");
	printf("waiting for dhcp reply!\n");
	printf("\n");

	// Wait for the network stack to be ready before proceeding
	while (!iniche_net_ready) {
           static int t = 0;
           if (dhc_states[0].tries != t) {
              //printf("dhcp tries: %d -> %d\n", t, dhc_states[0].tries);
              t = dhc_states[0].tries;
              if (dhc_states[0].tries > 1) {
                 dhc_states[0].tries = 0; // see iniche/src/net/dhcpclnt.c
              }
           }

           IOWR_ALTERA_RU_RESET_TIMER(REMOTE_UPDATE_BASE, 1);
           TK_SLEEP(1);
        }

	printf("have iniche_net_ready, dhc_alldone is %d, dhc_states[0].state is %d!\n", dhc_alldone(), dhc_states[0].state);
        //usleep(500000);

        // NB: DHCP timeout is 2 minutes, hardwired in the loop over (cticks - dhcp_started)
        // in dhc_setup() in iniche/src/misclib/dhcsetup.c

        if (dhc_states[0].state == 0) {
           printf("Error: cannot get an IP address, resetting the CPU!\n");
           usleep(500000);
           
           // https://www.intel.com/content/www/us/en/programmable/support/support-resources/knowledge-base/solutions/rd05062005_584.html
           NIOS2_WRITE_STATUS(0); 
           NIOS2_WRITE_IENABLE(0); 
           ((void (*) (void)) NIOS2_RESET_ADDR) ();

           // NB: tested. this reset works. K.O.
           // NB: this reset does not work in the eclipse debugger, shows stuck in alt_tick(). K.O.

           // CPU did not reset?!?
           while(1) { };
        }

        //while (1) {
        //   extern void dhc_setup(void); // not in any header file. see iniche/src/misclib/dhcsetup.c
        //   extern UDPCONN dhc_conn; // see iniche/src/net/dhcpclnt.c
        //   printf("dhc_conn %p, dhc_states[0].state %d, .ipaddr 0x%08lx\n", dhc_conn, dhc_states[0].state, (unsigned long)dhc_states[0].ipaddr);
        //   printf("calling dhc_setup()!\n");
        //   usleep(500000);
        //   if (dhc_conn) {
        //      udp_close(dhc_conn);
        //      dhc_conn = NULL;
        //   }
        //   dhc_setup();
        //}

	//printf("IP address 0x%08lx!\n", ((NET)(netlist.q_head))->n_ipaddr);

	create_task_esper(TK_APP_TPRIO);

	// Done, delete the task
	OSTaskDel(OS_PRIO_SELF);

	while(1); /* Correct Program Flow never gets here. */
}

static void InitLMK04816(void) {
	tLMK04816_Registers regmap;

	printf("Initialing LMK04816 Clock Cleaner\n");
	printf("...calling clock cleaner reset ...\n");

	LMK04816_Reset(cc_spi_write);

	printf("...reset done\n");

	// Configure device

        // after reset, clock cleaner MICROWIRE readback is
        // routed to the wrong pin. We cannot read clock cleaner
        // register until we program it... K.O.
	//LMK04816_ReadSettings(cc_spi_read, &regmap);
        memset(&regmap, 0, sizeof(regmap));

	//printf("...read settings done\n");
	//usleep(500000);

	regmap.r0.RESET = 0;

	// Channel Configuration
	regmap.r0.CLKout0_1_PD = 0;
	regmap.r1.CLKout2_3_PD = 0;
	regmap.r2.CLKout4_5_PD = 0;
	regmap.r3.CLKout6_7_PD = 0; // powered up to use CLKout6 for feedback
	regmap.r4.CLKout8_9_PD = 1;
	regmap.r5.CLKout10_11_PD = 0;

	// All channels should use the VCO (not OSCin)
	regmap.r3.CLKout6_7_OSCin_Sel = 0; // 0 - VCO, 1- OSCin
	regmap.r4.CLKout8_9_OSCin_Sel = 0; // 0 - VCO, 1- OSCin

	regmap.r0.CLKout0_ADLY_SEL = 0;
	regmap.r0.CLKout1_ADLY_SEL = 0;
	regmap.r1.CLKout2_ADLY_SEL = 0;
	regmap.r1.CLKout3_ADLY_SEL = 0;
	regmap.r2.CLKout4_ADLY_SEL = 0; // 1
	regmap.r2.CLKout5_ADLY_SEL = 0; // 1
	regmap.r3.CLKout6_ADLY_SEL = 0; // 1
	regmap.r3.CLKout7_ADLY_SEL = 0; // 1
	regmap.r4.CLKout8_ADLY_SEL = 0;
	regmap.r4.CLKout9_ADLY_SEL = 0;
	regmap.r5.CLKout10_ADLY_SEL = 0;
	regmap.r5.CLKout11_ADLY_SEL = 0;

	regmap.r6.CLKout0_1_ADLY = 0;
	regmap.r7.CLKout4_5_ADLY = 0;
	regmap.r7.CLKout6_7_ADLY = 0;

	regmap.r0.CLKout0_1_DDLY = 5; // 0.400ns per unit, 14*0.4ns = 5.6ns delay relative to the SCA clock
	regmap.r1.CLKout2_3_DDLY = 5;
	regmap.r2.CLKout4_5_DDLY = 15;
	regmap.r3.CLKout6_7_DDLY = 5;
	regmap.r4.CLKout8_9_DDLY = 5;
	regmap.r5.CLKout10_11_DDLY = 5;

	regmap.r0.CLKout0_1_HS = 0;
	regmap.r1.CLKout2_3_HS = 0;
	regmap.r2.CLKout4_5_HS = 0;
	regmap.r3.CLKout6_7_HS = 1; // qualifying clock, distribution path is >= 1.8GHz (2.4GHz) and CLKout6_7_DIV is EVEN, so HS must = 1
	regmap.r4.CLKout8_9_HS = 0;
	regmap.r5.CLKout10_11_HS = 0;
	// 200 = 12.5 MHz
	// 125 = 20 MHz
	// 100 = 25 MHz
	// 40  = 62.5 MHz
	// 20  = 125 MHz
	regmap.r0.CLKout0_1_DIV = 100;		// 20 MHz SCA RD CLK 34 + 12 = 125, 12.5MHz,
	regmap.r1.CLKout2_3_DIV = 40;		// 62.5 MHz clock into the FPGA for SCA WRITE control lines... was DIV 20 for 125 MHz XCVR / LVDS
	regmap.r2.CLKout4_5_DIV = 100;		// 20 MHz ADC CLK MON , ADC Clock
	regmap.r3.CLKout6_7_DIV = 200;		// 12.5 MHz for feedback path / 0-delay mode
	regmap.r4.CLKout8_9_DIV = 0;  		// Off
	regmap.r5.CLKout10_11_DIV = 40; 	// 62.5 MHz SCA WR CLK

	regmap.r6.CLKout0_TYPE = 1; // 1 - LVDS
	regmap.r6.CLKout1_TYPE = 1;
	regmap.r6.CLKout2_TYPE = 1;
	regmap.r6.CLKout3_TYPE = 1;
	regmap.r7.CLKout4_TYPE = 1;
	regmap.r7.CLKout5_TYPE = 1;
	regmap.r7.CLKout6_TYPE = 0; // off
	regmap.r7.CLKout7_TYPE = 0; // off
	regmap.r8.CLKout8_TYPE = 0; // off
	regmap.r8.CLKout9_TYPE = 0; // off
	regmap.r8.CLKout10_TYPE = 1;
	regmap.r8.CLKout11_TYPE = 1;

	regmap.r10.OSCout0_TYPE = 1;
	regmap.r10.EN_OSCout0 = 1;
	regmap.r10.OSCout0_MUX = 0; // Bypass divider
	regmap.r10.PD_OSCin = 0;
	regmap.r10.OSCout_DIV = 2;
	regmap.r10.VCO_DIV = 1;

	regmap.r10.VCO_MUX = 0;
	regmap.r10.EN_FEEDBACK_MUX = 1; // 0, normal, 1 - 0DELAY
	regmap.r10.FEEDBACK_MUX = 3; // 0 - CLKout0 (ADC - 20/25MHz), 1 - CLKout2 (SCA Write Clock 50/62.5Mhz), 5- CLKout10 (12.5)
	regmap.r11.MODE = 2; // 0, normal, 2 - PLL1 + PLL2 + Internal VCO

	regmap.r11.EN_SYNC = 1;
	regmap.r11.NO_SYNC_CLKout0_1 = 0;
	regmap.r11.NO_SYNC_CLKout2_3 = 0;
	regmap.r11.NO_SYNC_CLKout4_5 = 0;
	regmap.r11.NO_SYNC_CLKout6_7 = 1; // must be one when using 0-delay mode and this the qualifying clock
	regmap.r11.NO_SYNC_CLKout8_9 = 1;
	regmap.r11.NO_SYNC_CLKout10_11 = 0;

	regmap.r11.SYNC_QUAL = 1; // Qualify sync with FEEDBACK_MUX clock (CLKout6/7 for us!)
	regmap.r11.SYNC_POL_INV = 0; // Active high sync
	regmap.r11.SYNC_EN_AUTO = 1;

	regmap.r11.EN_PLL2_XTAL = 0;
	regmap.r12.LD_MUX = 3; // 3 - PLL1 & PLL2 DLD. 3 should work for our purposes (full lock detected)
	regmap.r12.LD_TYPE = 3; // 3 - Output (push-pull)

	regmap.r12.SYNC_PLL1_DLD = 0; // useless since we are using SYNC_QUAL, leave at 0
	regmap.r12.SYNC_PLL2_DLD = 0;

	regmap.r12.EN_TRACK = 1;
	regmap.r12.HOLDOVER_MODE = 2; // 1 - Disable Holdover, 2 - Enable Holdover mode

	regmap.r13.HOLDOVER_MUX = 4	; // 4 - holdover status output
	regmap.r13.HOLDOVER_TYPE = 3; // 3 - Output (push-pull)

	regmap.r13.Status_CLKin0_MUX = 2; // 6 - uWire readback
	regmap.r13.Status_CLKin0_TYPE = 0; // 0 - input, 3 - Output (push-pull)

	regmap.r13.Status_CLKin1_MUX = 2; // 1 - CLKin1 LOS, 2 - CLKin1 Selected, 3 - uWire Readback
	regmap.r14.Status_CLKin1_TYPE = 0; // 0 - input, 3 - Output (push-pull)*-

	regmap.r11.SYNC_CLKin2_MUX = 3; // 0 - Low, 1 - CLKin2 LOS, 2 - CLKin2 Selected, 3 - uWire Readback
	regmap.r11.SYNC_TYPE = 3; // 3 - Output (push-pull), we shall use the uWire to generate a sync pulse!

	regmap.r13.DISABLE_DLD1_DET = 0; // must be 0 for holdover to work
	regmap.r13.CLKin_SELECT_MODE = 2; // // 0-2 = CLKin0-2, 0-External. 1-Backup link, 2-Local Oscillator, 3-pin select, 4-auto (will default to ethernet if available)
	regmap.r13.CLKin_Sel_INV = 0;

	regmap.r13.EN_CLKin2 = 1;
	regmap.r13.EN_CLKin1 = 1;
	regmap.r13.EN_CLKin0 = 1;

	regmap.r14.LOS_TIMEOUT = 1;
	regmap.r14.EN_LOS = 1;
	regmap.r16.XTAL_LVL = 0;

	regmap.r14.CLKin0_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS
	regmap.r14.CLKin1_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS
	regmap.r14.CLKin2_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS

	regmap.r15.MAN_DAC = 512; // range is 0-1023
	regmap.r15.EN_MAN_DAC = 0; // 0 - automatic, 1 - manual
	regmap.r15.FORCE_HOLDOVER = 0;
	regmap.r14.DAC_LOW_TRIP = 0;
	regmap.r14.DAC_HIGH_TRIP = 0;

	regmap.r15.HOLDOVER_DLD_CNT = 1000; // range is 1 - 16383
	regmap.r25.DAC_CLK_DIV = 10;
	regmap.r26.EN_PLL2_REF_2X = 1;

	regmap.r27.PLL1_R = 10;
	regmap.r28.PLL1_N = 2;
	regmap.r24.PLL1_WND_SIZE = 2;
	regmap.r25.PLL1_DLD_CNT = 1000;
	regmap.r27.PLL1_CP_POL = 1;
	regmap.r27.PLL1_CP_GAIN = 3;

	// divide the external clocks down to 62.5MHz
	// 0 - divide by 1, 1 - divide by 2, 2 - divide by 4, 3 - divide by 8
	regmap.r27.CLKin0_PreR_DIV = 0; // "External 125" aka SATA, often 62.5 MHz
	regmap.r27.CLKin1_PreR_DIV = 0; // "Recovered Ethernet" aka SFP, gets divided down to get out the FPGA
	regmap.r27.CLKin2_PreR_DIV = 1; // 0 - divide by 1, 1 - divide by 2, 2 - divide by 4, 3 - divide by 8

	regmap.r28.PLL2_R = 4;
	regmap.r30.PLL2_N = 25;
	regmap.r29.PLL2_N_CAL = 25;
	regmap.r30.PLL2_P = 2;
	regmap.r26.PLL2_CP_POL = 0;
	regmap.r26.PLL2_CP_GAIN = 0;
	regmap.r26.PLL2_WND_SIZE = 2; // required value of 2

	regmap.r29.OSCin_FREQ = 1;
	regmap.r29.PLL2_FAST_PDF = 0;

	regmap.r31.READBACK_ADDR = 31;
	regmap.r31.READBACK_LE = 0;
	regmap.r31.uWire_LOCK = 1;

	LMK04816_WriteSettings(cc_spi_write, &regmap);
	usleep(1000000);
	//printf("...write settings done\n");
	//usleep(500000);


	printf("Resetting SERDES... ");
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 1;
	usleep(10);
	*(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 0;
	usleep(10000);
	printf("Done\n");
}

static void cc_spi_write(uint8_t addr, uint32_t cmd) {
	alt_u8 n;
	alt_u8 tx_data[4];

	// Double-write to ensure the command succeeds (datasheet suggested workaround for R0-R4 special write issues)
	for(n=0; n<2; n++) {
		tx_data[0] = (cmd >> 24) & 0xFF;
		tx_data[1] = (cmd >> 16) & 0xFF;
		tx_data[2] = (cmd >>  8) & 0xFF;
		tx_data[3] = (cmd & 0xE0) | (addr & 0x1F);
		alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
		usleep(1);
	}
}

static uint32_t cc_spi_read(uint8_t addr) {
	alt_u8 tx_data[4];
	alt_u8 rx_data[4];

	tx_data[0] = 0;
	tx_data[1] = (addr & 0x1F);
	tx_data[2] = 0;
	tx_data[3] = 0x1F;
	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
	usleep(1);
	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 0, 0, 4, rx_data, 0);
	usleep(1);

	// Due to the timing of the response from the LMK04816, the read bits are come in one bit later than expected, shift everything by one to compensate
	// And the register address is added back in to match what is shown in TIs CodeLoader and Clock Design Tool
	return ((((rx_data[0] << 24) | (rx_data[1] << 16) | (rx_data[2] << 8) | (rx_data[3])) << 1) & 0xFFFFFFE0) | (addr & 0x1F);
}
