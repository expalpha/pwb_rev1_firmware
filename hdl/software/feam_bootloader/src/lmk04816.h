/*
 * lmk04816.h
 *
 *  Created on: Jan 21, 2017
 *      Author: bryerton
 */

#ifndef LMK04816_H_
#define LMK04816_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
	uint8_t CLKout0_1_PD;
	uint8_t CLKout1_ADLY_SEL;
	uint8_t CLKout0_ADLY_SEL;
	uint16_t CLKout0_1_DDLY;
	uint8_t RESET;
	uint8_t CLKout0_1_HS;
	uint16_t CLKout0_1_DIV;
} tLMK04816_Reg0;

typedef struct {
	uint8_t CLKout2_3_PD;
	uint8_t CLKout3_ADLY_SEL;
	uint8_t CLKout2_ADLY_SEL;
	uint16_t CLKout2_3_DDLY;
	uint8_t POWERDOWN;
	uint8_t CLKout2_3_HS;
	uint16_t CLKout2_3_DIV;
} tLMK04816_Reg1;

typedef struct {
	uint8_t CLKout4_5_PD;
	uint8_t CLKout5_ADLY_SEL;
	uint8_t CLKout4_ADLY_SEL;
	uint16_t CLKout4_5_DDLY;
	uint8_t CLKout4_5_HS;
	uint16_t CLKout4_5_DIV;
} tLMK04816_Reg2;

typedef struct {
	uint8_t CLKout6_7_PD;
	uint8_t CLKout6_7_OSCin_Sel;
	uint8_t CLKout7_ADLY_SEL;
	uint8_t CLKout6_ADLY_SEL;
	uint16_t CLKout6_7_DDLY;
	uint8_t CLKout6_7_HS;
	uint16_t CLKout6_7_DIV;
} tLMK04816_Reg3;

typedef struct {
	uint8_t CLKout8_9_PD;
	uint8_t CLKout8_9_OSCin_Sel;
	uint8_t CLKout9_ADLY_SEL;
	uint8_t CLKout8_ADLY_SEL;
	uint16_t CLKout8_9_DDLY;
	uint8_t CLKout8_9_HS;
	uint16_t CLKout8_9_DIV;
} tLMK04816_Reg4;

typedef struct {
	uint8_t CLKout10_11_PD;
	uint8_t CLKout11_ADLY_SEL;
	uint8_t CLKout10_ADLY_SEL;
	uint16_t CLKout10_11_DDLY;
	uint8_t CLKout10_11_HS;
	uint16_t CLKout10_11_DIV;
} tLMK04816_Reg5;

typedef struct {
	uint8_t CLKout3_TYPE;
	uint8_t CLKout2_TYPE;
	uint8_t CLKout1_TYPE;
	uint8_t CLKout0_TYPE;
	uint8_t CLKout2_3_ADLY;
	uint8_t CLKout0_1_ADLY;
} tLMK04816_Reg6;

typedef struct {
	uint8_t CLKout4_TYPE;
	uint8_t CLKout5_TYPE;
	uint8_t CLKout6_TYPE;
	uint8_t CLKout7_TYPE;
	uint8_t CLKout4_5_ADLY;
	uint8_t CLKout6_7_ADLY;
} tLMK04816_Reg7;

typedef struct {
	uint8_t CLKout8_TYPE;
	uint8_t CLKout9_TYPE;
	uint8_t CLKout10_TYPE;
	uint8_t CLKout11_TYPE;
	uint8_t CLKout8_9_ADLY;
	uint8_t CLKout10_11_ADLY;
} tLMK04816_Reg8;

typedef struct {
	uint8_t OSCout0_TYPE;
	uint8_t EN_OSCout0;
	uint8_t OSCout0_MUX;
	uint8_t PD_OSCin;
	uint8_t OSCout_DIV;
	uint8_t VCO_MUX;
	uint8_t EN_FEEDBACK_MUX;
	uint8_t VCO_DIV;
	uint8_t FEEDBACK_MUX;
} tLMK04816_Reg10;

typedef struct {
	uint8_t MODE;
	uint8_t EN_SYNC;
	uint8_t NO_SYNC_CLKout0_1;
	uint8_t NO_SYNC_CLKout2_3;
	uint8_t NO_SYNC_CLKout4_5;
	uint8_t NO_SYNC_CLKout6_7;
	uint8_t NO_SYNC_CLKout8_9;
	uint8_t NO_SYNC_CLKout10_11;
	uint8_t SYNC_CLKin2_MUX;
	uint8_t SYNC_QUAL;
	uint8_t SYNC_POL_INV;
	uint8_t SYNC_EN_AUTO;
	uint8_t SYNC_TYPE;
	uint8_t EN_PLL2_XTAL;
} tLMK04816_Reg11;

typedef struct {
	uint8_t LD_MUX;
	uint8_t LD_TYPE;
	uint8_t SYNC_PLL2_DLD;
	uint8_t SYNC_PLL1_DLD;
	uint8_t EN_TRACK;
	uint8_t HOLDOVER_MODE;
} tLMK04816_Reg12;

typedef struct {
	uint8_t HOLDOVER_MUX;
	uint8_t HOLDOVER_TYPE;
	uint8_t Status_CLKin1_MUX;
	uint8_t Status_CLKin0_TYPE;
	uint8_t DISABLE_DLD1_DET;
	uint8_t Status_CLKin0_MUX;
	uint8_t CLKin_SELECT_MODE;
	uint8_t CLKin_Sel_INV;
	uint8_t EN_CLKin2;
	uint8_t EN_CLKin1;
	uint8_t EN_CLKin0;
} tLMK04816_Reg13;

typedef struct {
	uint8_t LOS_TIMEOUT;
	uint8_t EN_LOS;
	uint8_t Status_CLKin1_TYPE;
	uint8_t CLKin2_BUF_TYPE;
	uint8_t CLKin1_BUF_TYPE;
	uint8_t CLKin0_BUF_TYPE;
	uint8_t DAC_HIGH_TRIP;
	uint8_t DAC_LOW_TRIP;
	uint8_t EN_VTUNE_RAIL_DET;
} tLMK04816_Reg14;

typedef struct {
	uint16_t MAN_DAC;
	uint8_t EN_MAN_DAC;
	uint16_t HOLDOVER_DLD_CNT;
	uint8_t FORCE_HOLDOVER;
} tLMK04816_Reg15;

typedef struct {
	uint8_t XTAL_LVL;
} tLMK04816_Reg16;

typedef struct {
	uint16_t DAC_CNT;
} tLMK04816_Reg23;

typedef struct {
	uint8_t PLL2_C4_LF;
	uint8_t PLL2_C3_LF;
	uint8_t PLL2_R4_LF;
	uint8_t PLL2_R3_LF;
	uint8_t PLL1_N_DLY;
	uint8_t PLL1_R_DLY;
	uint8_t PLL1_WND_SIZE;
} tLMK04816_Reg24;

typedef struct {
	uint16_t DAC_CLK_DIV;
	uint16_t PLL1_DLD_CNT;
} tLMK04816_Reg25;

typedef struct {
	uint8_t PLL2_WND_SIZE;
	uint8_t EN_PLL2_REF_2X;
	uint8_t PLL2_CP_POL;
	uint8_t PLL2_CP_GAIN;
	uint16_t PLL2_DLD_CNT;
	uint8_t PLL2_CP_TRI;
} tLMK04816_Reg26;

typedef struct {
	uint8_t PLL1_CP_POL;
	uint8_t PLL1_CP_GAIN;
	uint8_t CLKin2_PreR_DIV;
	uint8_t CLKin1_PreR_DIV;
	uint8_t CLKin0_PreR_DIV;
	uint16_t PLL1_R;
	uint8_t PLL1_CP_TRI;
} tLMK04816_Reg27;

typedef struct {
	uint16_t PLL2_R;
	uint32_t PLL1_N;
} tLMK04816_Reg28;

typedef struct {
	uint8_t OSCin_FREQ;
	uint8_t PLL2_FAST_PDF;
	uint32_t PLL2_N_CAL;
} tLMK04816_Reg29;

typedef struct {
	uint8_t PLL2_P;
	uint32_t PLL2_N;
} tLMK04816_Reg30;

typedef struct {
	uint8_t READBACK_ADDR;
	uint8_t READBACK_LE;
	uint8_t uWire_LOCK;
} tLMK04816_Reg31;

typedef struct {
 	tLMK04816_Reg0  r0;
	tLMK04816_Reg1  r1;
	tLMK04816_Reg2  r2;
	tLMK04816_Reg3  r3;
	tLMK04816_Reg4  r4;
	tLMK04816_Reg5  r5;
	tLMK04816_Reg6  r6;
	tLMK04816_Reg7  r7;
	tLMK04816_Reg8  r8;
	tLMK04816_Reg10 r10;
	tLMK04816_Reg11 r11;
	tLMK04816_Reg12 r12;
	tLMK04816_Reg13 r13;
	tLMK04816_Reg14 r14;
	tLMK04816_Reg15 r15;
	tLMK04816_Reg16 r16;
	tLMK04816_Reg23 r23;
	tLMK04816_Reg24 r24;
	tLMK04816_Reg25 r25;
	tLMK04816_Reg26 r26;
	tLMK04816_Reg27 r27;
	tLMK04816_Reg28 r28;
	tLMK04816_Reg29 r29;
	tLMK04816_Reg30 r30;
	tLMK04816_Reg31 r31;
} tLMK04816_Registers;


typedef void (*lmk04816_write)(uint8_t addr, uint32_t cmd);
typedef uint32_t (*lmk04816_read)(uint8_t addr);

void LMK04816_Reset(lmk04816_write wr);
void LMK04816_Powerdown(lmk04816_write wr, uint8_t val);

void LMK04816_WriteReg0(lmk04816_write wr, tLMK04816_Reg0* regmap);
void LMK04816_WriteReg1(lmk04816_write wr, tLMK04816_Reg1* regmap);
void LMK04816_WriteReg2(lmk04816_write wr, tLMK04816_Reg2* regmap);
void LMK04816_WriteReg3(lmk04816_write wr, tLMK04816_Reg3* regmap);
void LMK04816_WriteReg4(lmk04816_write wr, tLMK04816_Reg4* regmap);
void LMK04816_WriteReg5(lmk04816_write wr, tLMK04816_Reg5* regmap);
void LMK04816_WriteReg6(lmk04816_write wr, tLMK04816_Reg6* regmap);
void LMK04816_WriteReg7(lmk04816_write wr, tLMK04816_Reg7* regmap);
void LMK04816_WriteReg8(lmk04816_write wr, tLMK04816_Reg8* regmap);
void LMK04816_WriteReg10(lmk04816_write wr, tLMK04816_Reg10* regmap);
void LMK04816_WriteReg11(lmk04816_write wr, tLMK04816_Reg11* regmap);
void LMK04816_WriteReg12(lmk04816_write wr, tLMK04816_Reg12* regmap);
void LMK04816_WriteReg13(lmk04816_write wr, tLMK04816_Reg13* regmap);
void LMK04816_WriteReg14(lmk04816_write wr, tLMK04816_Reg14* regmap);
void LMK04816_WriteReg15(lmk04816_write wr, tLMK04816_Reg15* regmap);
void LMK04816_WriteReg16(lmk04816_write wr, tLMK04816_Reg16* regmap);
void LMK04816_WriteReg24(lmk04816_write wr, tLMK04816_Reg24* regmap);
void LMK04816_WriteReg25(lmk04816_write wr, tLMK04816_Reg25* regmap);
void LMK04816_WriteReg26(lmk04816_write wr, tLMK04816_Reg26* regmap);
void LMK04816_WriteReg27(lmk04816_write wr, tLMK04816_Reg27* regmap);
void LMK04816_WriteReg28(lmk04816_write wr, tLMK04816_Reg28* regmap);
void LMK04816_WriteReg29(lmk04816_write wr, tLMK04816_Reg29* regmap);
void LMK04816_WriteReg30(lmk04816_write wr, tLMK04816_Reg30* regmap);
void LMK04816_WriteReg31(lmk04816_write wr, tLMK04816_Reg31* regmap);

void LMK04816_ReadReg0(lmk04816_read rd, tLMK04816_Reg0* regmap);
void LMK04816_ReadReg1(lmk04816_read rd, tLMK04816_Reg1* regmap);
void LMK04816_ReadReg2(lmk04816_read rd, tLMK04816_Reg2* regmap);
void LMK04816_ReadReg3(lmk04816_read rd, tLMK04816_Reg3* regmap);
void LMK04816_ReadReg4(lmk04816_read rd, tLMK04816_Reg4* regmap);
void LMK04816_ReadReg5(lmk04816_read rd, tLMK04816_Reg5* regmap);
void LMK04816_ReadReg6(lmk04816_read rd, tLMK04816_Reg6* regmap);
void LMK04816_ReadReg7(lmk04816_read rd, tLMK04816_Reg7* regmap);
void LMK04816_ReadReg8(lmk04816_read rd, tLMK04816_Reg8* regmap);
void LMK04816_ReadReg10(lmk04816_read rd, tLMK04816_Reg10* regmap);
void LMK04816_ReadReg11(lmk04816_read rd, tLMK04816_Reg11* regmap);
void LMK04816_ReadReg12(lmk04816_read rd, tLMK04816_Reg12* regmap);
void LMK04816_ReadReg13(lmk04816_read rd, tLMK04816_Reg13* regmap);
void LMK04816_ReadReg14(lmk04816_read rd, tLMK04816_Reg14* regmap);
void LMK04816_ReadReg15(lmk04816_read rd, tLMK04816_Reg15* regmap);
void LMK04816_ReadReg16(lmk04816_read rd, tLMK04816_Reg16* regmap);
void LMK04816_ReadReg23(lmk04816_read rd, tLMK04816_Reg23* regmap);
void LMK04816_ReadReg24(lmk04816_read rd, tLMK04816_Reg24* regmap);
void LMK04816_ReadReg25(lmk04816_read rd, tLMK04816_Reg25* regmap);
void LMK04816_ReadReg26(lmk04816_read rd, tLMK04816_Reg26* regmap);
void LMK04816_ReadReg27(lmk04816_read rd, tLMK04816_Reg27* regmap);
void LMK04816_ReadReg28(lmk04816_read rd, tLMK04816_Reg28* regmap);
void LMK04816_ReadReg29(lmk04816_read rd, tLMK04816_Reg29* regmap);
void LMK04816_ReadReg30(lmk04816_read rd, tLMK04816_Reg30* regmap);
void LMK04816_ReadReg31(lmk04816_read rd, tLMK04816_Reg31* regmap);

void LMK04816_ReadSettings(lmk04816_read rd, tLMK04816_Registers* regmap);
void LMK04816_WriteSettings(lmk04816_write wr, tLMK04816_Registers* regmap);
void LMK04816_WriteSyncSettings(lmk04816_write wr, tLMK04816_Registers* regmap);

#ifdef __cplusplus
}
#endif

#endif /* LMK04816_H_ */
