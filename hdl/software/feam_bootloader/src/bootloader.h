/*
 * advance_boot_copier.h
 *
 *  Created on: Jun 23, 2016
 *      Author: bryerton
 */

#ifndef ADVANCE_BOOT_COPIER_H_
#define ADVANCE_BOOT_COPIER_H_

#include "alt_types.h"



#define GET_REG_OFFSET(base,reg) (((alt_u8*)(base) + ((reg)*4)))

/*
 * Define whether we are booting from flash or from on-chip RAM
 */
// Do not edit these defines
#define BOOT_FROM_CFI_FLASH -1
#define BOOT_CFI_FROM_ONCHIP_ROM -2
#define BOOT_EPCS_FROM_ONCHIP_ROM -3
#define CFI -10
#define EPCS -11

/*
 * Some CRC error codes for readability.
 */
#define CRC_VALID 0
#define SIGNATURE_INVALID 1
#define HEADER_CRC_INVALID 2
#define DATA_CRC_INVALID 3

#define MEM_FLASH_MATCH 0
#define MEM_FLASH_MISMATCH 1

/*
 * Size of buffer for processing flash contents
 */
#define FLASH_BUFFER_LENGTH 256

// Watchdog Timeout Period, the clock is 12MHz, but it only uses the upper 12 bits of the 29 bit counter
// NOTE: If this is changed, please update the alt_printf() text for the watchdog timeout, as the alt_printf() doesn't support decimal printing, so the value is hard-coded
//#define WATCHDOG_TIMEOUT_PERIOD (12500000u * 5u) >> 17

#define WATCHDOG_RESET(dev) if(dev) { IOWR_ALTERA_RU_RESET_TIMER(dev->base, 1); }

/*
 * Flash device name to use
 */
#define FLASH_DEV_NAME EPCQ_CONFIG_AVL_MEM_NAME

/*
 *  Edit this define to control the boot method.
 *  Options are:
 *        BOOT_FROM_CFI_FLASH
 *        BOOT_CFI_FROM_ONCHIP_ROM
 *        BOOT_EPCS_FROM_ONCHIP_ROM
 */
#define BOOT_METHOD BOOT_EPCS_FROM_ONCHIP_ROM

/*
 * These defines locate our two possible application images at specifically
 * these two offsets in the flash memory.  If you edit these defines, ensure
 * when programming the images to flash that you program them to the new
 * locations you define here.
 */
#if BOOT_METHOD == BOOT_FROM_CFI_FLASH

#	define BOOT_IMAGE_1_OFFSET  ( 0x00240000 )
#	define BOOT_IMAGE_2_OFFSET  ( 0x00440000 )
#	define BOOT_IMAGE_1_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_1_OFFSET )
#	define BOOT_IMAGE_2_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_2_OFFSET )

#elif BOOT_METHOD == BOOT_CFI_FROM_ONCHIP_ROM

#	define BOOT_IMAGE_1_OFFSET  ( 0x00240000 )
#	define BOOT_IMAGE_2_OFFSET  ( 0x00440000 )
#	define BOOT_IMAGE_1_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_1_OFFSET )
#	define BOOT_IMAGE_2_ADDR  ( EXT_FLASH_BASE + BOOT_IMAGE_2_OFFSET )

#elif BOOT_METHOD == BOOT_EPCS_FROM_ONCHIP_ROM

// In an EPCS, the data is not present in the CPU's memory map, so we just
// specify the image addresses as offsets within the EPCS.  The CopyFromFlash
// routine will work out the details later.
// It is important to carefully choose the offsets at which to place boot images
// in EPCS devices, ensuring they do not overlap FPGA configuration data. These
// offsets should be sufficient for a Cyclone II 2C35 development board.
#	define BOOT_IMAGE  ( 0x00600000 )

#endif // BOOT_METHOD
/*
 * Don't edit these defines
 */
#if (BOOT_METHOD == BOOT_FROM_CFI_FLASH || BOOT_METHOD == BOOT_CFI_FROM_ONCHIP_ROM)
# define FLASH_TYPE CFI
#elif (BOOT_METHOD == BOOT_EPCS_FROM_ONCHIP_ROM)
# define FLASH_TYPE EPCS
#endif // BOOT_METHOD

/*
 * The boot images stored in flash memory, have a specific header
 * attached to them.  This is the structure of that header.  The
 * perl script "make_header.pl", included with this example is
 *  used to add this header to your boot image.
 */
typedef struct {
  alt_u32 signature;
  alt_u32 version;
  alt_u32 timestamp;
  alt_u32 data_length;
  alt_u32 data_crc;
  alt_u32 res1;
  alt_u32 res2;
  alt_u32 header_crc;
} my_flash_header_type;

void* CopyFromFlash( alt_u32 src, void * dest, size_t num );
int CompareFromFlash( alt_u32 src, void * dest, size_t num );
alt_u32 LoadFlashImage (alt_u32 image_addr);
alt_u32 FlashCalcCRC32(alt_u32 flash_addr, int bytes);
int ValidateFlashImage(alt_u32 image_addr);
int ValidateDDRImage(alt_u32 image_addr);
void JumpFromBootCopier(void target(void));
void print_uint(unsigned int n);
void print_int(int n);

int jtag_present(void);

#endif /* ADVANCE_BOOT_COPIER_H_ */
