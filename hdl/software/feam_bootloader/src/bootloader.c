#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <system.h>
#include <sys/alt_stdio.h>
#include <alt_types.h>
#include <sys/alt_alarm.h>
#include <sys/alt_cache.h>
#include <sys/alt_dev.h>
#include <sys/alt_irq.h>
#include <sys/alt_sys_init.h>
#include <sys/alt_flash.h>
#include <priv/alt_file.h>
#include <drivers/inc/board_regs.h>
#include <drivers/inc/24aa02e48.h>
#include <drivers/inc/altera_remote_update_regs.h>
#include <drivers/inc/altera_remote_update.h>
#include <drivers/inc/altera_epcq_controller_regs.h>
#include <drivers/inc/altera_epcq_controller.h>
#include <drivers/inc/altera_avalon_jtag_uart_regs.h>
#include <drivers/inc/altera_avalon_spi.h>
#include <drivers/inc/altera_avalon_spi_regs.h>
#include "bootloader.h"
#include "memtest.h"
#include "build_number.h"
#include "lmk04816.h"

static void cc_spi_write(uint8_t addr, uint32_t cmd);
static uint32_t cc_spi_read(uint8_t addr);
static void InitLMK04816(int force_holdover, int clkin_pinselect);

/*
 * The following statement defines "main()" so that when the Nios II SBT4E
 * debugger is set to break at "main()", it will break at the appropriate
 * place in this program, which does not contain a function called "main()".
 */
int main(void) __attribute__ ((weak, alias ("alt_main")));

/*
int jtag_present(void) {
	return ((IORD_ALTERA_AVALON_JTAG_UART_CONTROL(ALT_STDIN_BASE) & ALTERA_AVALON_JTAG_UART_CONTROL_AC_MSK) != 0);
}
*/

// we overwrite the altera jtag uart "write" function
// because there is no way to prevent the stock version
// from waiting forever if output fifo is full and
// nios2-terminal is not connected. K.O. Aug 2020.

#include "altera_avalon_jtag_uart_regs.h"
#include "altera_avalon_jtag_uart.h"

int altera_avalon_jtag_uart_write(altera_avalon_jtag_uart_state* sp, 
  const char * ptr, int count, int flags)
{
  unsigned int base = sp->base;

  const char * end = ptr + count;

  //while (ptr < end)
  //  if ((IORD_ALTERA_AVALON_JTAG_UART_CONTROL(base) & ALTERA_AVALON_JTAG_UART_CONTROL_WSPACE_MSK) != 0)
  //    IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  while (ptr < end)
     IOWR_ALTERA_AVALON_JTAG_UART_DATA(base, *ptr++);

  return count;
}

void report_ddr()
{
   alt_printf("Reading DDR RAM CSRs\n");
   for (int i=0; i<=8; i++) {
      alt_u32 v = IORD(DDR_CSR_BASE, i);
      alt_printf("CSR[0x%x] = 0x%x\n", i, v);
   }
}

//
// this does not do anything:
//
//void reset_ddr()
//{
//   alt_printf("Reset DDR RAM!\n");
//   alt_printf("Status...\n");
//   report_ddr();
//   IOWR(DDR_CSR_BASE, 4, 1);
//   alt_printf("Status after reset...\n");
//   report_ddr();
//}

int wait_ddr()
{
   alt_printf("Waiting for DDR RAM calibration...\n");
   //report_ddr();
   alt_u32 xcsr4 = 0;
   for (int i=0; i<100; i++) {
      alt_u32 csr4 = IORD(DDR_CSR_BASE, 4);
      alt_u32 good = (csr4 & (1<<24));
      alt_u32 fail = (csr4 & (1<<25));
      alt_u32 pll_locked = (csr4 & (1<<26));
      if (!pll_locked) {
         if (csr4 != xcsr4) {
            alt_printf("csr4: 0x%x - no pll_locked...loop 0x%x\n", csr4, i);
            xcsr4 = csr4;
         }
      } else if (good) {
         alt_printf("csr4: 0x%x - pll_locked and calibration good...loop 0x%x\n", csr4, i);
         alt_printf("DDR RAM calibration success!\n");
         return 0;
      } else if (fail) {
         alt_printf("csr4: 0x%x - pll_locked and calibration failed...loop 0x%x\n", csr4, i);
         alt_printf("DDR RAM calibration failed! memtest and load image from flash will fail or hang!\n");
         return 1;
      } else {
         if (csr4 != xcsr4) {
            alt_printf("csr4: 0x%x - pll_locked and waiting...loop 0x%x\n", csr4, i);
            xcsr4 = csr4;
         }
      }
      usleep(100000);
   }

   alt_printf("Timeout waiting for DDR RAM calibration!\n");
   report_ddr();
   return 1;
}

static int try(volatile alt_u32* addr, alt_u32 v, int print)
{
   *addr = v;
   alt_u32 r = *addr;
   if (r == v) {
      if (print) {
         alt_printf("Addr 0x%x: value 0x%x ok!\n", addr, v);
      }
      return 0;
   }

   alt_printf("Addr 0x%x: wrote 0x%x, read 0x%x, mismatch 0x%x\n", addr, v, r, v^r);
   return -1;
}

static int memtest_ko(int print)
{
   alt_printf("memtest_ko() running...\n");

   int result = 0;
   alt_u32 *addr = DDR_AVL_0_BASE;
   alt_printf("reading DDR RAM at address 0x%x...\n", addr);
   alt_u32 v = addr[0];
   alt_printf("success reading DDR RAM at address 0x%x, data: 0x%x\n", addr, v);
   alt_printf("bit pattern tests...\n");
   result |= try(addr, 0, print);
   result |= try(addr, 0xffffffff, print);
   result |= try(addr, 0xaaaaaaaa, print);
   result |= try(addr, 0x55555555, print);
   for (int i=0; i<32; i++) {
      result |= try(addr, (1<<i), print);
      result |= try(addr, ~(1<<i), print);
   }

   alt_printf("address and refresh test...\n");

   for (int i=0; i<32; i++) {
      result |= try(addr + i, 0, print);
      result |= try(addr + i, 0xFFFFFFFF, print);
   }

   for (int loop=0; loop<2; loop++) {
      //alt_printf("Loop 0x%x\n", loop);
      for (int i=0; i<32; i++) {
         addr[i] = (i<<24)|(i<<16)|(i<<8)|i;
      }

      if (loop != 0) {
         alt_printf("refresh test...\n");
         usleep(1000000);
      }
      
      for (int i=0; i<32; i++) {
         alt_u32 w = (i<<24)|(i<<16)|(i<<8)|i;
         alt_u32 r = addr[i];
         if (r != w) {
            alt_printf("Addr 0x%x: wrote 0x%x, read 0x%x, mismatch 0x%x\n", addr+i, w, r, w^r);
            result |= 1;
         } else {
            if (print) {
               alt_printf("Addr 0x%x: wrote 0x%x, ok\n", addr+i, w);
            }
         }
      }
   }

   alt_printf("address test...\n");

   int count = 0;

   for (int i=0; i<0x83fc8/4; i++) {
      addr[i] = i;
   }

   for (int i=0; i<0x83fc8/4; i++) {
      alt_u32 w = i;
      alt_u32 r = addr[i];
      if (r != w) {
         count++;
         result |= 1;

         if (count <= 20) {
            alt_printf("Addr 0x%x: wrote 0x%x, read 0x%x, mismatch 0x%x\n", i, w, r, w^r);
            if (count == 20) {
               alt_printf("...and maybe more mismatches...\n");
            }
         }
      } else {
         //alt_printf("Addr 0x%x: wrote 0x%x, ok\n", i, w);
      }
   }

   if (count > 0) {
      alt_printf("address test: 0x%x mismatches\n", count);
   }

   if (result)
      alt_printf("memtest_ko() fail!\n");
   else
      alt_printf("memtest_ko() success!\n");

   return result;
}

static int run_memtest_ko()
{
   int result = memtest_ko(0);
   if  (result) {
      alt_printf("rerunning memtest_ko() with more reporting...\n");
      result = memtest_ko(1);
   }
   return result;
}

static void PrintBoardLmk()
{
   int clock_rcvd_eth = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_ETH);
   int clock_rcvd_sata = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_XCVR);
   int lmk_sync_clkin2 = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_SYNC_CLKIN2);
   int lmk_stat_clkin1 = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_CLKIN1);
   int lmk_stat_hold = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_HOLD);
   int lmk_stat_ld = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_LD);
   int lmk_lock_cnt = IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_LOCK_CNT);
   
   alt_printf("clocks status: ");
   if (lmk_sync_clkin2)
      alt_printf("CLKin2 ");
   if (lmk_stat_clkin1)
      alt_printf("CLKin1 ");
   if (lmk_stat_hold)
      alt_printf("HOLD ");
   else
      alt_printf("no-HOLD ");
   if (lmk_stat_ld)
      alt_printf("LD ");
   else
      alt_printf("no-LD ");
   alt_printf("lock_count: 0x%x, freq: eth 0x%x, sata 0x%x\n", lmk_lock_cnt, clock_rcvd_eth, clock_rcvd_sata);
}

static void ReportLMK04816(void) {
   alt_printf("LMK04816 status:\n");
   int reg23 = cc_spi_read(23);
   int dac = ((reg23>>5)>>14)&0x3FF;
   if (reg23 == 0x17) {
      alt_printf("LMK04816 not initialized for reading registers, readback stuck low\n");
   } else if (reg23 == 0xfffffff7) {
      alt_printf("LMK04816 not initialized for reading registers, readback stuck high\n");
   } else {
      tLMK04816_Registers regmap;
      LMK04816_ReadSettings(cc_spi_read, &regmap);
      //alt_printf("regmap.r0:  0x%x\n", cc_spi_read(0));
      //alt_printf("regmap.r1:  0x%x\n", cc_spi_read(1));
      //alt_printf("regmap.r12: 0x%x\n", cc_spi_read(12));
      alt_printf("regmap.r13.CLKin_SELECT_MODE: 0x%x\n", regmap.r13.CLKin_SELECT_MODE);
      alt_printf("regmap.r15.FORCE_HOLDOVER: 0x%x\n", regmap.r15.FORCE_HOLDOVER);
      //alt_printf("regmap.r13: 0x%x\n", cc_spi_read(13));
      alt_printf("regmap.r23: 0x%x, DAC 0x%x\n", reg23, dac);
   }
#if 0
   alt_printf("\n");
   alt_printf("board.clock_io: 0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_IO));
   alt_printf("board.clock_rcvd_eth:  0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_ETH));
   alt_printf("board.clock_rcvd_sata: 0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_CLOCK_RCVD_XCVR));
   alt_printf("board.lmk_sync_clkin2: 0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_SYNC_CLKIN2));
   alt_printf("board.lmk_stat_clkin1: 0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_CLKIN1));
   alt_printf("board.lmk_stat_hold:   0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_HOLD));
   alt_printf("board.lmk_stat_ld:     0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_STAT_LD));
   alt_printf("board.lmk_lock_cnt:    0x%x\n", IORD(BOARD_STATUS_BASE, BOARD_REG_STAT_LMK_LOCK_CNT));
   alt_printf("\n");
#endif
   PrintBoardLmk();
}

#if 0
static void WaitLMK04816(int count)
{
   for (int i=0; i<count; i++) {
      PrintBoardLmk();
      usleep(1000000);
   }
}
#endif

#if 0
static void ResetNios()
{
   alt_printf("ResetNios!\n");
   IOWR(BOARD_CONTROL_BASE, BOARD_REG_CTRL_USE_SATA_GXB, 0);
   IOWR(BOARD_CONTROL_BASE, BOARD_REG_CTRL_USE_SATA_GXB, 1);
   IOWR(BOARD_CONTROL_BASE, BOARD_REG_CTRL_USE_SATA_GXB, 0);
   alt_printf("ResetNios failed!\n");
}
#endif

//
// this does nothing
//
//void ResetSERDES()
//{
//   alt_printf("Resetting SERDES...\n");
//   *(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 1;
//   usleep(10);
//   *(uint8_t*)GET_REG_OFFSET(BOARD_CONTROL_BASE, BOARD_REG_CTRL_SERDES_RST) = 0;
//   usleep(10000);
//   alt_printf("Done\n");
//}

static void AdcPwrEnable()
{
   alt_printf("Power up the wing board...\n");
   IOWR(BOARD_CONTROL_BASE, BOARD_REG_CTRL_ADC_PWR_EN, 0x0101);
}

void Fail(altera_remote_update_state* rem_update_dev)
{
   alt_printf("Fatal failure, cannot continue, after delay, FPGA reconfigure in factory mode...\n");
   int end = 0x5a;
   for (int loop=0; loop<0x5a; loop++) {
      alt_printf("sleep 10 sec...0x%x...0x%x\n", loop, end-loop);
      for (int i=0; i<10; i++) {
         usleep(1000000);
      }
   }
   alt_printf("FPGA reconfigure in factory mode...\n");
   altera_remote_update_trigger_reconfig(rem_update_dev, ALTERA_RU_RECONFIG_MODE_FACTORY, 0, 0); \
   alt_printf("FPGA reconfigure in factory mode did not happen. Halt...\n"); \
   while (1) { usleep(1000000); };
   // DOES NOT RETURN
}

/*****************************************************************************
 *  Function: alt_main
 *
 *  Purpose: This is our boot copier's entry point. We are implementing
 *  this as an alt_main() instead of a main(), so that we can better control
 *  the drivers that load and the system resources that are enabled.  Since
 *  code size may be a consideration, this method allows us to keep the
 *  memory requirements small.
 *
 *****************************************************************************/
int alt_main(void) {
	static alt_u32 entry_point;
	static 	alt_u32 current_page;
	static alt_u32 image_location;
	static alt_flash_fd* fd;
	static int result;
	static alt_u8 sfp_module_detected;
	static alt_u8 sfp_los;
	static alt_u32 reg;
	static alt_u32 bit;

	//alt_u8 mod_id;
	//unsigned char mac_addr[6];
	//int n;

	// Initialize relevant drivers

	// In order to allow interrupts to occur while the boot copier executes we initialize the main irq handler.
	alt_irq_init(ALT_IRQ_BASE);

	// Now we initialize the drivers that we require.
	alt_sys_init();

	//alt_io_redirect (ALT_STDOUT, ALT_STDIN, ALT_STDERR);

	/*
	 * Pick a flash image to load.  The criteria for picking an image are
	 * discussed the text of the application note, and also in the code comments
	 * preceding the function "PickFlashImage()" found in this file.
	 */

	/*
	 * Now we're going to try to load the application into memory, this will
	 * likely overwrite our current exception handler, so before we do that
	 * we'll disable interrupts and not turn them back on again.
	 *
	 * It's also important minimize your reliance on the ".rwdata", ".bss" and
	 * stack sections.  Since all of these sections can exist in the exception
	 * memory they are all subject to being overwritten.  You can inspect how
	 * much of the ".rwdata" and ".bss" sections the bootcopier uses by
	 * looking at the disassembly for the bootcopier.  The disassembly can be
	 * generated by running "nios2-elf-objdump.exe" with the -d option from a
	 * Nios II Command Shell.
	 */
	alt_irq_disable_all();

	alt_printf("bootloader started!\n");

        ReportLMK04816();
        //WaitLMK04816(30);
	InitLMK04816(1, 0);
        //ResetSERDES();
        //ReportLMK04816();
        //WaitLMK04816();
        ReportLMK04816();

        //reset_ddr();
        wait_ddr();
        //report_ddr();

	alt_printf("\n");
	alt_printf("PWB Revision 1 Boot Loader\n");
	alt_printf("%s\n", VERSION_STR);
	alt_printf("Git Hash: %s\n", GIT_HASH_STR);
	alt_printf("Git Branch [Tag]: %s [%s]\n", GIT_BRANCH_STR, GIT_TAG_STR);
	alt_printf("Built by: %s (%s)\n", BUILT_BY_USER_STR, BUILT_BY_EMAIL_STR);
        
	// Find the flash device
	alt_printf("Opening Flash Device %s...\n", FLASH_DEV_NAME);
	fd = alt_flash_open_dev((FLASH_DEV_NAME));
	if(fd) {
		alt_printf("Success\n");

		// Attempt to Lock the EPCQ if flash device was found
		alt_printf("Locking all sectors of epcq flash...\n");
		result = alt_epcq_controller_lock(fd, 0x1F); // lock out entire EPCQ256
		if(result == 0) {
			alt_printf("Success\n");
		} else if (result == -ETIME) {
			alt_printf("Timed out!\n");
		} else {
			alt_printf("Lock Failed!\n");
		}
	} else {
		alt_printf("Failed\n");
	}

	// Get Current page_select
	current_page = 0;
	alt_printf("Opening Remote Update Module %s...\n", REMOTE_UPDATE_NAME);
	altera_remote_update_state* rem_update_dev = altera_remote_update_open(REMOTE_UPDATE_NAME);
	if(rem_update_dev) {
		alt_printf("Success\n");

		alt_printf("Reconfig Trigger Conditions:\n");
		reg = IORD_ALTERA_RU_RECONFIG_TRIGGER_CONDITIONS(rem_update_dev->base);
		alt_printf("\tWatchdog Timeout: ");
		bit = ((reg & (1 << 4)) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("\tnCONFIG Asserted: ");
		bit = ((reg & (1 << 3)) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("\tReset Request: ");
		bit = ((reg & (1 << 2)) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("\tnSTATUS Asserted: ");
		bit = ((reg & (1 << 1)) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("\tCRC Error: ");
		bit = ((reg & (1 << 0)) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("\tWatchdog Enabled: ");
		reg = IORD_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base);
		bit = ((reg & ALTERA_RU_WATCHDOG_ENABLE_MASK) != 0) ? 1 : 0;
		if(bit) {
			alt_printf("True\n");
		} else {
			alt_printf("False\n");
		}

		alt_printf("Current Mode: ");
		reg = IORD_ALTERA_RU_CONFIG_MODE(rem_update_dev->base);
		bit = reg & ALTERA_RU_RECONFIG_MODE_MASK;
		if(bit == ALTERA_RU_RECONFIG_MODE_FACTORY ) {
			alt_printf("Factory\n");
		} else if (bit == ALTERA_RU_RECONFIG_MODE_APP) {
			alt_printf("Application\n");
		} else {
			alt_printf("Unknown\n");
		}

		current_page = IORD_ALTERA_RU_PAGE_SELECT(rem_update_dev->base);

		// Enable Watchdog if the Application page is loaded and remote configuration mode is set
		if((current_page != 0x0) && (current_page != 0xffffff00)) {
			alt_printf("Application Image Selected at 0x%x\n", current_page);

                        // watchdog setting cannot be changed when in application mode. KO Aug 2020.
			//alt_printf("Setting Watchdog Timeout Period to 0x%x\n", WATCHDOG_TIMEOUT_PERIOD);
			//IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(rem_update_dev->base, WATCHDOG_TIMEOUT_PERIOD);
                        //
			//alt_printf("Enabling Watchdog\n");
			//IOWR_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base, ALTERA_RU_WATCHDOG_ENABLE);
		} else {
			if(current_page == 0xffffff00) {
				alt_printf("!!FPGA is not in Remote Configuration mode!!\nPlease configure the project in Quartus to use Remote Configuration\n");
				current_page = 0;
			}

			alt_printf(("Factory Image Selected at 0x0\n"));

                        //
                        // watchdog does not work in factory mode
                        // and cannot be changed when in application mode,
                        // so the following code does nothing. K.O. Aug 2020.
                        //
                        // actual watchdog enabling is done in mod_remote_update.c before
                        // rebooting from factory page to user page. K.O. Aug 2020.
                        //
                        //if (0) {
                        //   // The watchdog does not work in factory mode. See
                        //   // https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_altremote.pdf
                        //   // KO Aug 2020.
                        //   alt_printf("Setting Watchdog Timeout Period to 0x%x\n", (WATCHDOG_TIMEOUT_PERIOD));
                        //   IOWR_ALTERA_RU_WATCHDOG_TIMEOUT(rem_update_dev->base, (WATCHDOG_TIMEOUT_PERIOD));
                        //
                        //   alt_printf("Enabling Watchdog\n");
                        //   IOWR_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base, ALTERA_RU_WATCHDOG_ENABLE);
                        //} else {
                        //   // Disable Watchdog
                        //   alt_printf("Disabling Watchdog in Factory mode to allow for debugging\n");
                        //   IOWR_ALTERA_RU_WATCHDOG_ENABLE(rem_update_dev->base, 0);
                        //}
		}
	} else {
		alt_printf("Failed\n");
		current_page = 0;
	}

	WATCHDOG_RESET(rem_update_dev);

        // report status of SFP module and fiber signal

	sfp_module_detected = *(alt_u8*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_SFP_MODDET);
	alt_printf("SFP Module Status: ");
	if(!sfp_module_detected) {
		alt_printf("Found\n");
	} else {
		alt_printf("SFP module not found!\n");
	}

	sfp_los= *(alt_u8*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_SFP_LOS);
	alt_printf("SFP LOS Status: ");
	if(!sfp_los) {
		alt_printf("OK\n");
	} else {
		alt_printf("BAD. No SFP or no fiber connected!\n");
	}

        // check that DDR memory is ready to be used

        int ddr_memory_is_good = 1;

        //reset_ddr();
        int ddr_fail = wait_ddr();
        if (ddr_fail) {
           report_ddr();
           ReportLMK04816();
           alt_printf("DDR memory calibration failure!\n");
           //Fail(rem_update_dev);
           ddr_memory_is_good = 0;
        }

        //
        // enable power to the wing boards so Daryl&co can debug
        // the hardware happily
        //

        AdcPwrEnable();

        // random delay to avoid all PWBs booting at the same time,
        // specifically, all PWBs running memtest at the same time
        // and browning-out the power supply. K.O. Aug 2020.

        if (current_page == 0) {
           WATCHDOG_RESET(rem_update_dev);
           uint32_t chipid_lo = *(uint32_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CHIPID_LO);
           uint32_t chipid_hi = *(uint32_t*)GET_REG_OFFSET(BOARD_STATUS_BASE, BOARD_REG_STAT_CHIPID_HI);
           alt_printf("Chip ID hi 0x%x, low 0x%x\n", chipid_hi, chipid_lo);
           int delay = chipid_lo & 0xF;
           alt_printf("Delay 0x%x\n", delay);
           for (int i=0; i<delay; i++) {
              //alt_printf("Sleep 0x%x!\n", i);
              usleep(1000000);
              WATCHDOG_RESET(rem_update_dev);
              usleep(1000000);
              WATCHDOG_RESET(rem_update_dev);
           }
        }

        if (0) {
           // test for jtag uart fifo overflow. without my workaround,
           // program will stop and wait until nios-terminal is connected.
           // with the workaround, fifo over flow is ignored, additional
           // characters are lost. K.O. Aug 2020.
           for (int i=0; i<400; i++) {
              uint32_t reg0 = IORD_ALTERA_AVALON_JTAG_UART_DATA(JTAGUART_0_BASE);
              uint32_t reg1 = IORD_ALTERA_AVALON_JTAG_UART_CONTROL(JTAGUART_0_BASE);
              alt_printf("jtag uart regs: data 0x%x ctrl 0x%x, loop 0x%x\n", reg0, reg1, i);
           }
        }

	image_location = current_page + BOOT_IMAGE;
	alt_printf("Firmware image address in flash: 0x%x\n", image_location);

	alt_printf("Checking Flash Image...\n");
	WATCHDOG_RESET(rem_update_dev);

        int flash_image_is_good = 0;

        if (fd) {
           result = ValidateFlashImage(image_location);
           
           switch(result) {
           case CRC_VALID:
              alt_printf("Flash image good!\n"); 
              flash_image_is_good = 1;
              break;
           case SIGNATURE_INVALID:
              alt_printf("Flash image signature not found\n"); 
              break;
           case HEADER_CRC_INVALID:
              alt_printf("Flash image header CRC invalid\n");
              break;				
           case DATA_CRC_INVALID:
              alt_printf("Flash image data CRC invalid\n"); 
              break;
           default:
              alt_printf(("CRC Check Failed\n"));
              break;
           }
        } else {
           alt_printf("Flash image not available\n");
        }

        WATCHDOG_RESET(rem_update_dev);
        
        alt_printf("Clock status...\n");
        ReportLMK04816();

        if (!ddr_memory_is_good) {
           alt_printf("DDR memory calibration failure, cannot continue!\n");
           Fail(rem_update_dev);
        }
        
        // Run Mem Test only we are on the factory page
        if (current_page == 0) {
           result = run_memtest_ko();
           
           if (result != 0) {
              alt_printf("memtest_ko failed!\n");
              // Go back to the first page in the EPCQ, if we aren't already there
              Fail(rem_update_dev);
           }

           result = memtest();
           
           if (result != 0) {
              alt_printf("Memory Check Failed\n");

              // Go back to the first page in the EPCQ, if we aren't already there
              Fail(rem_update_dev);
           }
           
           alt_printf("Memory Check Passed\n");

           result = run_memtest_ko();
           
           if (result != 0) {
              alt_printf("memtest_ko failed!\n");
              // Go back to the first page in the EPCQ, if we aren't already there
              Fail(rem_update_dev);
           }
        }

	// If flash memory not available, give up.
	if (!flash_image_is_good) {
           alt_printf("Flash image is no good, cannot continue!\n");
           Fail(rem_update_dev);
	}

        alt_printf("Loading Image From Flash...\n");
        
        WATCHDOG_RESET(rem_update_dev);
        entry_point = LoadFlashImage(image_location);
        
        WATCHDOG_RESET(rem_update_dev);
        alt_printf("Done\n", entry_point);
        
        // Validate Image now that it's in DDR and make sure the entry point is found
        alt_printf("Comparing Flash Image to Loaded Image in DDR...\n");
        
        WATCHDOG_RESET(rem_update_dev);
        
        result = ValidateDDRImage(image_location);
        
        if (result != MEM_FLASH_MATCH) {
           Fail(rem_update_dev);
        }
        
        WATCHDOG_RESET(rem_update_dev);
        
        if (entry_point < 0) {  // load the image
           alt_printf("No Entry Point Found\n");
           Fail(rem_update_dev);
        }
	
        alt_printf("Jumping to Entry Point Located at 0x%x\n\n", entry_point);
        usleep(1000000);

        // Jump to the entry point of the application
        JumpFromBootCopier((void (*)(void)) (entry_point));
        
	// We should never get here
	exit(0);
}

/*****************************************************************************
 *  Function: JumpFromBootCopier
 *
 *  Purpose: This routine shuts down the boot copier and jumps somewhere else.
 *  The place to jump is passed in as a function pointer named "target".
 *
 *****************************************************************************/
void JumpFromBootCopier(void target(void)) {
	/*
	 * If you have any outstanding I/O or system resources that needed to be
	 * cleanly disabled before leaving the boot copier program, then this is
	 * the place to do that.
	 *
	 * In this example we only need to ensure the state of the Nios II cpu is
	 * equivalent to reset.  If we disable interrupts, and flush the caches,
	 * then the program we jump to should receive the cpu just as it would
	 * coming out of a hardware reset.
	 */
	alt_irq_disable_all();
	alt_dcache_flush_all();
	alt_icache_flush_all();

	/*
	 * The cpu state is as close to reset as we can get it, so we jump to the new
	 * application.
	 */
	target();

	/*
	 * In the odd event that the program we jump to decides to return, we should
	 * probably just jump back to the reset vector. We pass in the reset address
	 * as a function pointer.
	 */

	// Wait 0.5 seconds
	//usleep(500000);

	// Jump back to the reset address
	JumpFromBootCopier((void (*)(void)) (NIOS2_RESET_ADDR));
}

/*****************************************************************************
 *  Function: CopyFromFlash
 *
 *  Purpose:  This subroutine copies data from a flash memory to a buffer
 *  The function uses the appropriate copy routine for the flash that is
 *  defined by FLASH_TYPE.  EPCS devices can't simply be read from using
 *  memcpy().
 *
 *****************************************************************************/
void* CopyFromFlash(alt_u32 src, void * dest, size_t num) {
	static alt_flash_fd* fd;

	fd = alt_flash_open_dev(FLASH_DEV_NAME);

# if( FLASH_TYPE == CFI )

	memcpy( dest, src, num );

# elif( FLASH_TYPE == EPCS )

	// If we're dealing with EPCS, "src" has already been defined for us as
	// an offset into the EPCS, not an absolute address.
	alt_read_flash(fd, src, dest, num);
# endif //FLASH_TYPE

        //alt_printf("CopyFromFlash 0x%x to 0x%x+0x%x values 0x%x 0x%x 0x%x 0x%x\n", src, dest, num, ((alt_u32*)dest)[0], ((alt_u32*)dest)[1], ((alt_u32*)dest)[2], ((alt_u32*)dest)[3]);

	return (dest);
}

/*****************************************************************************
 *  Function: CompareFromFlash
 *
 *  Purpose:  This subroutine copies data from a flash memory to a buffer
 *  The function uses the appropriate copy routine for the flash that is
 *  defined by FLASH_TYPE.  EPCS devices can't simply be read from using
 *  memcpy().
 *
 *****************************************************************************/
int CompareFromFlash(alt_u32 src, void * dest, size_t num) {
	static alt_flash_fd* fd;
	static size_t bytes_to_check;
	static char mem_buff[FLASH_BUFFER_LENGTH];

	fd = alt_flash_open_dev(FLASH_DEV_NAME);

	// If we're dealing with EPCS, "src" has already been defined for us as
	// an offset into the EPCS, not an absolute address.

	while(num > 0) {
		bytes_to_check = (FLASH_BUFFER_LENGTH < num) ? FLASH_BUFFER_LENGTH : num;

		// Load mem_buff from flash
		# if( FLASH_TYPE == CFI )
			memcpy( mem_buff, src, bytes_to_check );
		# elif( FLASH_TYPE == EPCS )
			alt_read_flash(fd, src, mem_buff, bytes_to_check);
		#endif

                //alt_printf("CompareFromFlash 0x%x to 0x%x+0x%x membuf 0x%x ddr 0x%x\n", src, dest, num, *(alt_u32*)mem_buff, *(alt_u32*)dest);

		// Compare mem_buff with contents of DDR
		if(memcmp(mem_buff, dest, bytes_to_check) != 0) {
                   alt_printf("DDR mismatch at 0x%x size 0x%x!\n", dest, bytes_to_check);
                   int count = 0;
                   for (int i=0; i<bytes_to_check; i+=4) {
                      alt_u32 vflash = *(alt_u32*)(((char*)mem_buff)+i);
                      alt_u32 vddr   = *(alt_u32*)(((char*)dest)+i);
                      if (vflash != vddr) {
                         alt_printf("DDR mismatch at +0x%x: flash 0x%x ddr 0x%x, diff 0x%x\n", i, vflash, vddr, vflash ^ vddr);
                         if (count > 20) {
                            alt_printf("and maybe more...\n");
                            break;
                         }
                      }
                   }
                   return -1;
		}

		dest += bytes_to_check; // move pointer in DDR along
		num -= bytes_to_check; // note that we've processed a number of bytes
		src += bytes_to_check; // move pointer in Flash along
	}

	return 0;
}

/*****************************************************************************
 *  Function: LoadFlashImage
 *
 *  Purpose:  This subroutine loads an image from flash into the Nios II
 *  memory map.  It decodes boot records in the format produced from the
 *  elf2flash utility, and loads the image as directed by those records.
 *  The format of the boot record is described in the text of the application
 *  note.
 *
 *  The input operand, "image" is expected to be the image selector indicating
 *  which flash image, 1 or 2, should be loaded.
 *
 *****************************************************************************/
alt_u32 LoadFlashImage(alt_u32 image_addr) {
	static alt_u32 next_flash_byte;
	static alt_u32 length;
	static alt_u32 address;

	/*
	 * Load the image pointer based on the value of "image"
	 * The boot image header is 32 bytes long, so we add an offset of 32.
	 */
	next_flash_byte = image_addr + 32;

	/*
	 * Flash images are not guaranteed to be word-aligned within the flash
	 * memory, so a word-by-word copy loop should not be used.
	 *
	 * The "memcpy()" function works well to copy non-word-aligned data, and
	 * it is relatively small, so that's what we'll use.
	 */

	// Get the first 4 bytes of the boot record, which should be a length record
	CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
	next_flash_byte += 4;

	// Now loop until we get jump record, or a halt record
	while ((length != 0) && (length != 0xffffffff)) {
		// Get the next 4 bytes of the boot record, which should be an address
		// record
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		alt_printf("Copy 0x%x+0x%x\n", address, length);

		// Copy the next "length" bytes to "address"
		CopyFromFlash(next_flash_byte, (void*) (address), (size_t) (length));
		next_flash_byte += length;

		// Get the next 4 bytes of the boot record, which now should be another
		// length record
		CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
		next_flash_byte += 4;
	}

	// "length" was read as either 0x0 or 0xffffffff, which means we are done
	// copying.
	if (length == 0xffffffff) {
		// We read a HALT record, so return a -1
		return -1;
	} else // length == 0x0
	{
		// We got a jump record, so read the next 4 bytes for the entry address
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		// Return the entry point address
		return address;
	}
}

/*****************************************************************************
 *  Function: ValidateFlashImage
 *
 *  Purpose:  This routine validates a flash image based upon three criteria:
 *            1.) It contains the correct flash image signature
 *            2.) A CRC check of the image header
 *            3.) A CRC check of the image data (payload)
 *
 *  Since it's inefficient to read individual bytes from EPCS, and since
 *  we don't really want to expend RAM to buffer the entire image, we compromise
 *  in the case of EPCS, and create a medium-size buffer, who's size is
 *  adjustable by the user.
 *
 *****************************************************************************/
int ValidateFlashImage(alt_u32 image_addr) {
	static my_flash_header_type temp_header __attribute__((aligned(4)));
	static alt_u32 crc;

	/*
	 * Again, we don't assume the image is word aligned, so we copy the header
	 * from flash to a word aligned buffer.
	 */
	CopyFromFlash(image_addr, &temp_header, 32);

	alt_printf("\tFound Flash Signature 0x%x\n", temp_header.signature);

	// Check the signature first
	if (temp_header.signature == 0xa5a5a5a5) {
		// Signature is good, validate the header crc
		alt_printf("\tExpected Flash Header CRC 0x%x... ", temp_header.header_crc);
		crc = FlashCalcCRC32(image_addr, 28);
		alt_printf("Found Flash Header CRC 0x%x\n", crc);
		if (temp_header.header_crc != crc) {
			// Header crc is not valid
			return HEADER_CRC_INVALID;
		} else {
			// header crc is valid, now validate the data crc
			alt_printf("\tExpected Flash Data CRC 0x%x... ", temp_header.data_crc);
			crc = FlashCalcCRC32(image_addr + 32, temp_header.data_length);
			alt_printf("Found Flash Data CRC 0x%x\n", crc);
			if (temp_header.data_crc == crc) {
				// data crc validates, the image is good
				return CRC_VALID;
			} else {
				// data crc is not valid
				return DATA_CRC_INVALID;
			}
		}
	} else {
		// bad signature, return 1
		return SIGNATURE_INVALID;
	}
}


/*****************************************************************************
 *  Function: ValidateDDRImage
 *
 *  Purpose:  This routine validates a DDR image based upon three criteria:
 *            1.) It contains the correct flash image signature
 *            2.) A CRC check of the image header
 *            3.) A CRC check of the image data (payload)
 *
 *  Since it's inefficient to read individual bytes from EPCS, and since
 *  we don't really want to expend RAM to buffer the entire image, we compromise
 *  in the case of EPCS, and create a medium-size buffer, who's size is
 *  adjustable by the user.
 *
 *****************************************************************************/
int ValidateDDRImage(alt_u32 image_addr) {
	static alt_u32 next_flash_byte;
	static alt_u32 length;
	static alt_u32 address;
	static int is_valid;

	/*
	 * Load the image pointer based on the value of "image"
	 * The boot image header is 32 bytes long, so we add an offset of 32.
	 */
	next_flash_byte = image_addr + 32;

	/*
	 * Flash images are not guaranteed to be word-aligned within the flash
	 * memory, so a word-by-word copy loop should not be used.
	 *
	 * The "memcpy()" function works well to copy non-word-aligned data, and
	 * it is relatively small, so that's what we'll use.
	 */

	// Get the first 4 bytes of the boot record, which should be a length record
	CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
	next_flash_byte += 4;

	is_valid = MEM_FLASH_MATCH;

	// Now loop until we get jump record, or a halt record
	while ((length != 0) && (length != 0xffffffff) && (is_valid == MEM_FLASH_MATCH)) {
		// Get the next 4 bytes of the boot record, which should be an address
		// record
		CopyFromFlash(next_flash_byte, (void*) (&address), (size_t) (4));
		next_flash_byte += 4;

		// Compare the next "length" bytes to what's stored at "address"
		//CopyFromFlash(next_flash_byte, (void*) (address), (size_t) (length));
		if(CompareFromFlash(next_flash_byte, (void*)(address), (size_t)(length)) != 0) {
			is_valid = MEM_FLASH_MISMATCH;
		}
		next_flash_byte += length;

		// Get the next 4 bytes of the boot record, which now should be another
		// length record
		CopyFromFlash(next_flash_byte, (void*) (&length), (size_t) (4));
		next_flash_byte += 4;
	}

	if(is_valid != MEM_FLASH_MATCH) {
		alt_printf("!!Flash/DDR Mismatch at 0x%x!!\n", address);
	} else {
		alt_printf("Matched\n");
	}

	return is_valid;
}

/*****************************************************************************
 *  Function: FlashCalcCRC32
 *
 *  Purpose:  This subroutine calcuates a reflected CRC32 on data located
 *  flash.  The routine buffers flash contents locally in order
 *  to support EPCS flash as well as CFI
 *
 *****************************************************************************/
alt_u32 FlashCalcCRC32(alt_u32 flash_addr, int bytes) {
	static alt_u32 crcval;
	static int i;
	static int buf_index;
	static int copy_length;
	static alt_u8 cval;
	static char flash_buffer[FLASH_BUFFER_LENGTH];

	crcval = 0xffffffff;

	while (bytes != 0) {
		copy_length = (FLASH_BUFFER_LENGTH < bytes) ? FLASH_BUFFER_LENGTH : bytes;
		CopyFromFlash(flash_addr, flash_buffer, copy_length);
		for (buf_index = 0; buf_index < copy_length; buf_index++) {
			cval = flash_buffer[buf_index];
			crcval ^= cval;
			for (i = 8; i > 0; i--) {
				crcval = (crcval & 0x00000001) ? ((crcval >> 1) ^ 0xEDB88320) : (crcval >> 1);
			}
			bytes--;
		}
		flash_addr += FLASH_BUFFER_LENGTH;
	}
	return crcval;
}


void print_uint(unsigned int n) {
    if (n / 10 != 0) { print_uint(n / 10); }
    alt_putchar((n % 10) + '0');
}

void print_int(int n) {
    if (n < 0) {
        alt_putchar('-');
        n = -n;
    }
    print_uint((unsigned int) n);
}


static void InitLMK04816(int force_holdover, int clkin_pinselect) {
	tLMK04816_Registers regmap;

	alt_printf("Initialing LMK04816 Clock Cleaner...\n");

	LMK04816_Reset(cc_spi_write);

        // after reset, clock cleaner MICROWIRE readback is
        // routed to the wrong pin. We cannot read clock cleaner
        // register until we program it... K.O.
	//LMK04816_ReadSettings(cc_spi_read, &regmap);
        memset(&regmap, 0, sizeof(regmap));

	// Configure device

	regmap.r0.RESET = 0;

	// Channel Configuration
	regmap.r0.CLKout0_1_PD = 0;
	regmap.r1.CLKout2_3_PD = 0;
	regmap.r2.CLKout4_5_PD = 0;
	regmap.r3.CLKout6_7_PD = 0; // powered up to use CLKout6 for feedback
	regmap.r4.CLKout8_9_PD = 1;
	regmap.r5.CLKout10_11_PD = 0;

	// All channels should use the VCO (not OSCin)
	regmap.r3.CLKout6_7_OSCin_Sel = 0; // 0 - VCO, 1- OSCin
	regmap.r4.CLKout8_9_OSCin_Sel = 0; // 0 - VCO, 1- OSCin

	regmap.r0.CLKout0_ADLY_SEL = 0;
	regmap.r0.CLKout1_ADLY_SEL = 0;
	regmap.r1.CLKout2_ADLY_SEL = 0;
	regmap.r1.CLKout3_ADLY_SEL = 0;
	regmap.r2.CLKout4_ADLY_SEL = 0; // 1
	regmap.r2.CLKout5_ADLY_SEL = 0; // 1
	regmap.r3.CLKout6_ADLY_SEL = 0; // 1
	regmap.r3.CLKout7_ADLY_SEL = 0; // 1
	regmap.r4.CLKout8_ADLY_SEL = 0;
	regmap.r4.CLKout9_ADLY_SEL = 0;
	regmap.r5.CLKout10_ADLY_SEL = 0;
	regmap.r5.CLKout11_ADLY_SEL = 0;

	regmap.r6.CLKout0_1_ADLY = 0;
	regmap.r7.CLKout4_5_ADLY = 0;
	regmap.r7.CLKout6_7_ADLY = 0;

	regmap.r0.CLKout0_1_DDLY = 5; // 0.400ns per unit, 14*0.4ns = 5.6ns delay relative to the SCA clock
	regmap.r1.CLKout2_3_DDLY = 5;
	regmap.r2.CLKout4_5_DDLY = 15;
	regmap.r3.CLKout6_7_DDLY = 5;
	regmap.r4.CLKout8_9_DDLY = 5;
	regmap.r5.CLKout10_11_DDLY = 5;

	regmap.r0.CLKout0_1_HS = 0;
	regmap.r1.CLKout2_3_HS = 0;
	regmap.r2.CLKout4_5_HS = 0;
	regmap.r3.CLKout6_7_HS = 1; // qualifying clock, distribution path is >= 1.8GHz (2.4GHz) and CLKout6_7_DIV is EVEN, so HS must = 1
	regmap.r4.CLKout8_9_HS = 0;
	regmap.r5.CLKout10_11_HS = 0;
	// 200 = 12.5 MHz
	// 125 = 20 MHz
	// 100 = 25 MHz
	// 40  = 62.5 MHz
	// 20  = 125 MHz
	regmap.r0.CLKout0_1_DIV = 100;		// SCA RD CLK 34 + 12 = 125, 25MHz,
	regmap.r1.CLKout2_3_DIV = 40;		// 62.5 MHz clock into the FPGA for SCA WRITE control lines... was DIV 20 for 125 MHz XCVR / LVDS
	regmap.r2.CLKout4_5_DIV = 100;		// ADC CLK MON , ADC Clock, 25MHz
	regmap.r3.CLKout6_7_DIV = 200;		// 12.5 MHz for feedback path / 0-delay mode
	regmap.r4.CLKout8_9_DIV = 0;  		// Off
	regmap.r5.CLKout10_11_DIV = 40; 	// 62.5 MHz SCA WR CLK

	regmap.r6.CLKout0_TYPE = 1; // 1 - LVDS
	regmap.r6.CLKout1_TYPE = 1;
	regmap.r6.CLKout2_TYPE = 1;
	regmap.r6.CLKout3_TYPE = 1;
	regmap.r7.CLKout4_TYPE = 1;
	regmap.r7.CLKout5_TYPE = 1;
	regmap.r7.CLKout6_TYPE = 0; // off
	regmap.r7.CLKout7_TYPE = 0; // off
	regmap.r8.CLKout8_TYPE = 0; // off
	regmap.r8.CLKout9_TYPE = 0; // off
	regmap.r8.CLKout10_TYPE = 1;
	regmap.r8.CLKout11_TYPE = 1;

	regmap.r10.OSCout0_TYPE = 1;
	regmap.r10.EN_OSCout0 = 1;
	regmap.r10.OSCout0_MUX = 0; // 0 = Bypass divider, 1 = Divided
	regmap.r10.PD_OSCin = 0;
	regmap.r10.OSCout_DIV = 2;
	regmap.r10.VCO_DIV = 1;

	regmap.r10.VCO_MUX = 0;
	regmap.r10.EN_FEEDBACK_MUX = 1; // 0, normal, 1 - 0DELAY
	regmap.r10.FEEDBACK_MUX = 3; // 0 - CLKout0 (ADC - 20/25MHz), 1 - CLKout2 (SCA Write Clock 50/62.5Mhz), 2 - CLKout4, 3 - CLKout6, 4 - CLKout8, 5- CLKout10 (12.5)
	regmap.r11.MODE = 2; // 0, normal, 2 - PLL1 + PLL2 + Internal VCO

	regmap.r11.EN_SYNC = 1;
	regmap.r11.NO_SYNC_CLKout0_1 = 0;
	regmap.r11.NO_SYNC_CLKout2_3 = 0;
	regmap.r11.NO_SYNC_CLKout4_5 = 0;
	regmap.r11.NO_SYNC_CLKout6_7 = 1; // must be one when using 0-delay mode and this the qualifying clock
	regmap.r11.NO_SYNC_CLKout8_9 = 1;
	regmap.r11.NO_SYNC_CLKout10_11 = 0;

	regmap.r11.SYNC_QUAL = 1; // Qualify sync with FEEDBACK_MUX clock (CLKout6/7 for us!)
	regmap.r11.SYNC_POL_INV = 0; // Active high sync
	regmap.r11.SYNC_EN_AUTO = 1;

	regmap.r11.EN_PLL2_XTAL = 0;
	regmap.r12.LD_MUX = 3; // 3 - PLL1 & PLL2 DLD. 3 should work for our purposes (full lock detected)
	regmap.r12.LD_TYPE = 3; // 3 - Output (push-pull)

	regmap.r12.SYNC_PLL1_DLD = 0; // useless since we are using SYNC_QUAL, leave at 0
	regmap.r12.SYNC_PLL2_DLD = 0;

	regmap.r12.EN_TRACK = 1;
	regmap.r12.HOLDOVER_MODE = 2; // 1 - Disable Holdover, 2 - Enable Holdover mode

	regmap.r13.HOLDOVER_MUX = 4	; // 4 - holdover status output
	regmap.r13.HOLDOVER_TYPE = 3; // 3 - Output (push-pull)

        // pin select mode:
        // STATUS_CLKin1 STATUS_CLKin0 Active clock
        // 0 0 CLKin0 - external clock
        // 0 1 CLKin1 - sata link clock
        // 1 0 CLKin2 - internal oscillator
        // 1 1 holdover

	regmap.r13.Status_CLKin0_MUX = 2; // 1 - CLKin0 LOS, 2 - CLKin0 selected, 6 - uWire readback
	regmap.r13.Status_CLKin0_TYPE = 0; // 0-input, 3-Output (push-pull)

	regmap.r13.Status_CLKin1_MUX = 2; // 1 - CLKin1 LOS, 2 - CLKin1 Selected, 3 - uWire Readback
	regmap.r14.Status_CLKin1_TYPE = 0; // 0-input, 3-Output (push-pull)

	regmap.r11.SYNC_CLKin2_MUX = 3; // 0 - Low, 1 - CLKin2 LOS, 2 - CLKin2 Selected, 3 - uWire Readback
	regmap.r11.SYNC_TYPE = 3; // 0 - input, 3 - Output (push-pull), we shall use the uWire to generate a sync pulse!

	regmap.r13.DISABLE_DLD1_DET = 0; // must be 0 for holdover to work
        if (clkin_pinselect) {
           alt_printf("CLKin_SELECT_MODE in pin select mode...\n");
           regmap.r13.CLKin_SELECT_MODE = 3; // 0-2 = CLKin0-2, 0-External. 1-Backup link, 2-Local Oscillator, 3-pin select, 4-auto (will default to ethernet if available)
        } else {
           regmap.r13.CLKin_SELECT_MODE = 2; // 0-2 = CLKin0-2, 0-External. 1-Backup link, 2-Local Oscillator, 3-pin select, 4-auto (will default to ethernet if available)
        }
	regmap.r13.CLKin_Sel_INV = 0;

	regmap.r13.EN_CLKin2 = 1;
	regmap.r13.EN_CLKin1 = 1;
	regmap.r13.EN_CLKin0 = 1;

	regmap.r14.LOS_TIMEOUT = 1;
	regmap.r14.EN_LOS = 1;
	regmap.r16.XTAL_LVL = 0;

	regmap.r14.CLKin0_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS
	regmap.r14.CLKin1_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS
	regmap.r14.CLKin2_BUF_TYPE = 0; // 0 - bipolar, 1 - CMOS

        if (force_holdover) { // forced holdover mode
           alt_printf("In forced HOLDOVER mode...\n");
           regmap.r15.MAN_DAC = 512; // range is 0-1023
           regmap.r15.EN_MAN_DAC = 1; // 0 - automatic, 1 - manual
           regmap.r15.FORCE_HOLDOVER = 1;
           regmap.r14.DAC_LOW_TRIP = 0;
           regmap.r14.DAC_HIGH_TRIP = 0;
        } else {
           regmap.r15.MAN_DAC = 512; // range is 0-1023
           regmap.r15.EN_MAN_DAC = 0; // 0 - automatic, 1 - manual
           regmap.r15.FORCE_HOLDOVER = 0;
           regmap.r14.DAC_LOW_TRIP = 0;
           regmap.r14.DAC_HIGH_TRIP = 0;
        }

	regmap.r15.HOLDOVER_DLD_CNT = 1000; // range is 1 - 16383
	regmap.r25.DAC_CLK_DIV = 10;
	regmap.r26.EN_PLL2_REF_2X = 1;

        // PLL1 frequency calculation:
        // clkin side: 62.5 MHz / 10 = 6.25 MHz
        // feedback side: CLKout6 12.5 MHz / 2 = 6.25 MHz

	regmap.r27.PLL1_R = 10;
	regmap.r28.PLL1_N = 2;
	regmap.r24.PLL1_WND_SIZE = 2;
	regmap.r25.PLL1_DLD_CNT = 10000;
	regmap.r27.PLL1_CP_POL = 1;
	regmap.r27.PLL1_CP_GAIN = 3; // changed from 1 to 3 from main firmware

	// divide the external clocks down to 62.5MHz
	// 0 - divide by 1, 1 - divide by 2, 2 - divide by 4, 3 - divide by 8
	regmap.r27.CLKin0_PreR_DIV = 0; // "External 125" aka SATA, often 62.5 MHz
	regmap.r27.CLKin1_PreR_DIV = 0; // "Recovered Ethernet" aka SFP, gets divided down to get out the FPGA
	regmap.r27.CLKin2_PreR_DIV = 1; // 0 - divide by 1, 1 - divide by 2, 2 - divide by 4, 3 - divide by 8

	regmap.r28.PLL2_R = 4;
	regmap.r30.PLL2_N = 25;
	regmap.r29.PLL2_N_CAL = 25;
	regmap.r30.PLL2_P = 2;
	regmap.r26.PLL2_CP_POL = 0;
	regmap.r26.PLL2_CP_GAIN = 0;
	regmap.r26.PLL2_WND_SIZE = 2; // required value of 2

	regmap.r29.OSCin_FREQ = 1;
	regmap.r29.PLL2_FAST_PDF = 0;

	regmap.r31.READBACK_ADDR = 31;
	regmap.r31.READBACK_LE = 0;
	regmap.r31.uWire_LOCK = 1;

	LMK04816_WriteSettings(cc_spi_write, &regmap);

	alt_printf("Done\n");
}

static void cc_spi_write(uint8_t addr, uint32_t cmd) {
	alt_u8 n;
	alt_u8 tx_data[4];

	// Double-write to ensure the command succeeds (datasheet suggested workaround for R0-R4 special write issues)
	for(n=0; n<2; n++) {
		tx_data[0] = (cmd >> 24) & 0xFF;
		tx_data[1] = (cmd >> 16) & 0xFF;
		tx_data[2] = (cmd >>  8) & 0xFF;
		tx_data[3] = (cmd & 0xE0) | (addr & 0x1F);
		alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
		usleep(1);
	}
}

static uint32_t cc_spi_read(uint8_t addr) {
	alt_u8 tx_data[4];
	alt_u8 rx_data[4];

	tx_data[0] = 0;
	tx_data[1] = (addr & 0x1F);
	tx_data[2] = 0;
	tx_data[3] = 0x1F;
	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 4, tx_data, 0, 0, 0);
	usleep(1);
	alt_avalon_spi_command(SPI_CLOCKCLEANER_BASE, 0, 0, 0, 4, rx_data, 0);
	usleep(1);

	// Due to the timing of the response from the LMK04816, the read bits are come in one bit later than expected, shift everything by one to compensate
	// And the register address is added back in to match what is shown in TIs CodeLoader and Clock Design Tool
	return ((((rx_data[0] << 24) | (rx_data[1] << 16) | (rx_data[2] << 8) | (rx_data[3])) << 1) & 0xFFFFFFE0) | (addr & 0x1F);
}

