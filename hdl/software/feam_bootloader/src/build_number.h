#ifndef BUILD_NUMBER_STR
#define BUILD_NUMBER_STR "517"
#endif

#ifndef BUILD_NUMBER
#define BUILD_NUMBER 517
#endif

#ifndef BUILD_TIMESTAMP
#define BUILD_TIMESTAMP 1679962960
#endif

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 2
#endif

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif

#ifndef VERSION_STR
#define VERSION_STR "Ver 2.0  Build 517 - Mon 27 Mar 2023 05:22:40 PM PDT"
#endif

#ifndef VERSION_STR_SHORT
#define VERSION_STR_SHORT "2.0.517"
#endif

#ifndef GIT_HASH_STR
#define GIT_HASH_STR "cf767385d6705b8da468cbdfd7c7b784d45a5ad5"
#endif

#ifndef GIT_BRANCH_STR
#define GIT_BRANCH_STR "alphag"
#endif

#ifndef GIT_TAG_STR
#define GIT_TAG_STR ""
#endif

#ifndef BUILT_BY_USER_STR
#define BUILT_BY_USER_STR "First Last"
#endif

#ifndef BUILT_BY_EMAIL_STR
#define BUILT_BY_EMAIL_STR "noreply@example.com"
#endif

