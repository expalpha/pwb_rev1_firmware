/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_0' in SOPC Builder design 'feam'
 * SOPC Builder design path: ../../feam.sopcinfo
 *
 * Generated: Mon Mar 27 17:22:45 PDT 2023
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x22002820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 100000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x1f
#define ALT_CPU_DCACHE_BYPASS_MASK 0x80000000
#define ALT_CPU_DCACHE_LINE_SIZE 32
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_DCACHE_SIZE 16384
#define ALT_CPU_EXCEPTION_ADDR 0x00000020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 100000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_EXTRA_EXCEPTION_INFO
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 32
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_ICACHE_SIZE 16384
#define ALT_CPU_INITDA_SUPPORTED
#define ALT_CPU_INST_ADDR_WIDTH 0x1f
#define ALT_CPU_NAME "nios2_0"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 0
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_PERIPHERAL_REGION_BASE 0x20000000
#define ALT_CPU_PERIPHERAL_REGION_PRESENT
#define ALT_CPU_PERIPHERAL_REGION_SIZE 0x20000000
#define ALT_CPU_RESET_ADDR 0x40000000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x22002820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 100000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x1f
#define NIOS2_DCACHE_BYPASS_MASK 0x80000000
#define NIOS2_DCACHE_LINE_SIZE 32
#define NIOS2_DCACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_SIZE 16384
#define NIOS2_EXCEPTION_ADDR 0x00000020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_EXTRA_EXCEPTION_INFO
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_ICACHE_SIZE 16384
#define NIOS2_INITDA_SUPPORTED
#define NIOS2_INST_ADDR_WIDTH 0x1f
#define NIOS2_NUM_OF_SHADOW_REG_SETS 0
#define NIOS2_OCI_VERSION 1
#define NIOS2_PERIPHERAL_REGION_BASE 0x20000000
#define NIOS2_PERIPHERAL_REGION_PRESENT
#define NIOS2_PERIPHERAL_REGION_SIZE 0x20000000
#define NIOS2_RESET_ADDR 0x40000000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_SGDMA
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_EPCQ_CONTROLLER
#define __ALTERA_ETH_TSE
#define __ALTERA_MEM_IF_DDR3_EMIF
#define __ALTERA_NIOS2_GEN2
#define __ALTERA_REMOTE_UPDATE
#define __ALT_XCVR_RECONFIG
#define __BOARD
#define __CUSTOM_PATTERN_CHECKER
#define __CUSTOM_PATTERN_GENERATOR
#define __FABRIC_UDP_STREAM
#define __GXB_MODULE
#define __I2C_OPENCORES
#define __ONE_TO_TWO_ST_DEMUX
#define __PRBS_PATTERN_CHECKER
#define __PRBS_PATTERN_GENERATOR
#define __RAM_TEST_CONTROLLER
#define __SCA_PROTOCOL
#define __SIGPROC
#define __TRIGGER_CONTROL
#define __TWO_TO_ONE_ST_MUX


/*
 * SCA_0_control configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_0_control sca_protocol
#define SCA_0_CONTROL_BASE 0x22003880
#define SCA_0_CONTROL_IRQ -1
#define SCA_0_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_0_CONTROL_NAME "/dev/SCA_0_control"
#define SCA_0_CONTROL_SPAN 16
#define SCA_0_CONTROL_TYPE "sca_protocol"


/*
 * SCA_0_status configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_0_status sca_protocol
#define SCA_0_STATUS_BASE 0x22003840
#define SCA_0_STATUS_IRQ -1
#define SCA_0_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_0_STATUS_NAME "/dev/SCA_0_status"
#define SCA_0_STATUS_SPAN 16
#define SCA_0_STATUS_TYPE "sca_protocol"


/*
 * SCA_1_control configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_1_control sca_protocol
#define SCA_1_CONTROL_BASE 0x22003890
#define SCA_1_CONTROL_IRQ -1
#define SCA_1_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_1_CONTROL_NAME "/dev/SCA_1_control"
#define SCA_1_CONTROL_SPAN 16
#define SCA_1_CONTROL_TYPE "sca_protocol"


/*
 * SCA_1_status configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_1_status sca_protocol
#define SCA_1_STATUS_BASE 0x22003850
#define SCA_1_STATUS_IRQ -1
#define SCA_1_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_1_STATUS_NAME "/dev/SCA_1_status"
#define SCA_1_STATUS_SPAN 16
#define SCA_1_STATUS_TYPE "sca_protocol"


/*
 * SCA_2_control configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_2_control sca_protocol
#define SCA_2_CONTROL_BASE 0x220038a0
#define SCA_2_CONTROL_IRQ -1
#define SCA_2_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_2_CONTROL_NAME "/dev/SCA_2_control"
#define SCA_2_CONTROL_SPAN 16
#define SCA_2_CONTROL_TYPE "sca_protocol"


/*
 * SCA_2_status configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_2_status sca_protocol
#define SCA_2_STATUS_BASE 0x22003860
#define SCA_2_STATUS_IRQ -1
#define SCA_2_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_2_STATUS_NAME "/dev/SCA_2_status"
#define SCA_2_STATUS_SPAN 16
#define SCA_2_STATUS_TYPE "sca_protocol"


/*
 * SCA_3_control configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_3_control sca_protocol
#define SCA_3_CONTROL_BASE 0x220038b0
#define SCA_3_CONTROL_IRQ -1
#define SCA_3_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_3_CONTROL_NAME "/dev/SCA_3_control"
#define SCA_3_CONTROL_SPAN 16
#define SCA_3_CONTROL_TYPE "sca_protocol"


/*
 * SCA_3_status configuration
 *
 */

#define ALT_MODULE_CLASS_SCA_3_status sca_protocol
#define SCA_3_STATUS_BASE 0x22003870
#define SCA_3_STATUS_IRQ -1
#define SCA_3_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCA_3_STATUS_NAME "/dev/SCA_3_status"
#define SCA_3_STATUS_SPAN 16
#define SCA_3_STATUS_TYPE "sca_protocol"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone V"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtaguart_0"
#define ALT_STDERR_BASE 0x220038c8
#define ALT_STDERR_DEV jtaguart_0
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtaguart_0"
#define ALT_STDIN_BASE 0x220038c8
#define ALT_STDIN_DEV jtaguart_0
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtaguart_0"
#define ALT_STDOUT_BASE 0x220038c8
#define ALT_STDOUT_DEV jtaguart_0
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "feam"


/*
 * altera_iniche configuration
 *
 */

#define DHCP_CLIENT
#define INCLUDE_TCP
#define INICHE_DEFAULT_IF "NOT_USED"
#define IP_FRAGMENTS


/*
 * altera_ro_zipfs configuration
 *
 */

#define ALTERA_RO_ZIPFS_BASE 0x24000000
#define ALTERA_RO_ZIPFS_NAME "/mnt/rozipfs"
#define ALTERA_RO_ZIPFS_OFFSET 0x800000


/*
 * board_control configuration
 *
 */

#define ALT_MODULE_CLASS_board_control board
#define BOARD_CONTROL_BASE 0x26000080
#define BOARD_CONTROL_CLOCK_FREQ 12500000u
#define BOARD_CONTROL_IRQ -1
#define BOARD_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOARD_CONTROL_NAME "/dev/board_control"
#define BOARD_CONTROL_SPAN 64
#define BOARD_CONTROL_TYPE "board"


/*
 * board_status configuration
 *
 */

#define ALT_MODULE_CLASS_board_status board
#define BOARD_STATUS_BASE 0x26000000
#define BOARD_STATUS_CLOCK_FREQ 12500000u
#define BOARD_STATUS_IRQ -1
#define BOARD_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOARD_STATUS_NAME "/dev/board_status"
#define BOARD_STATUS_SPAN 128
#define BOARD_STATUS_TYPE "board"


/*
 * ddr configuration as viewed by sgdma_rx_m_write
 *
 */

#define SGDMA_RX_M_WRITE_DDR_BASE 0x0
#define SGDMA_RX_M_WRITE_DDR_IRQ -1
#define SGDMA_RX_M_WRITE_DDR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SGDMA_RX_M_WRITE_DDR_NAME "/dev/ddr"
#define SGDMA_RX_M_WRITE_DDR_SPAN 536870912
#define SGDMA_RX_M_WRITE_DDR_TYPE "altera_mem_if_ddr3_emif"


/*
 * ddr configuration as viewed by sgdma_tx_m_read
 *
 */

#define SGDMA_TX_M_READ_DDR_BASE 0x0
#define SGDMA_TX_M_READ_DDR_IRQ -1
#define SGDMA_TX_M_READ_DDR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SGDMA_TX_M_READ_DDR_NAME "/dev/ddr"
#define SGDMA_TX_M_READ_DDR_SPAN 536870912
#define SGDMA_TX_M_READ_DDR_TYPE "altera_mem_if_ddr3_emif"


/*
 * ddr_avl_0 configuration
 *
 */

#define ALT_MODULE_CLASS_ddr_avl_0 altera_mem_if_ddr3_emif
#define DDR_AVL_0_BASE 0x0
#define DDR_AVL_0_IRQ -1
#define DDR_AVL_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DDR_AVL_0_NAME "/dev/ddr_avl_0"
#define DDR_AVL_0_SPAN 536870912
#define DDR_AVL_0_TYPE "altera_mem_if_ddr3_emif"


/*
 * ddr_csr configuration
 *
 */

#define ALT_MODULE_CLASS_ddr_csr altera_mem_if_ddr3_emif
#define DDR_CSR_BASE 0x28000000
#define DDR_CSR_IRQ -1
#define DDR_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DDR_CSR_NAME "/dev/ddr_csr"
#define DDR_CSR_SPAN 262144
#define DDR_CSR_TYPE "altera_mem_if_ddr3_emif"


/*
 * epcq_config_avl_csr configuration
 *
 */

#define ALT_MODULE_CLASS_epcq_config_avl_csr altera_epcq_controller
#define EPCQ_CONFIG_AVL_CSR_BASE 0x26000180
#define EPCQ_CONFIG_AVL_CSR_FLASH_TYPE "EPCQ256"
#define EPCQ_CONFIG_AVL_CSR_IRQ 12
#define EPCQ_CONFIG_AVL_CSR_IRQ_INTERRUPT_CONTROLLER_ID 0
#define EPCQ_CONFIG_AVL_CSR_IS_EPCS 0
#define EPCQ_CONFIG_AVL_CSR_NAME "/dev/epcq_config_avl_csr"
#define EPCQ_CONFIG_AVL_CSR_NUMBER_OF_SECTORS 512
#define EPCQ_CONFIG_AVL_CSR_PAGE_SIZE 256
#define EPCQ_CONFIG_AVL_CSR_SECTOR_SIZE 65536
#define EPCQ_CONFIG_AVL_CSR_SPAN 32
#define EPCQ_CONFIG_AVL_CSR_SUBSECTOR_SIZE 4096
#define EPCQ_CONFIG_AVL_CSR_TYPE "altera_epcq_controller"


/*
 * epcq_config_avl_mem configuration
 *
 */

#define ALT_MODULE_CLASS_epcq_config_avl_mem altera_epcq_controller
#define EPCQ_CONFIG_AVL_MEM_BASE 0x24000000
#define EPCQ_CONFIG_AVL_MEM_FLASH_TYPE "EPCQ256"
#define EPCQ_CONFIG_AVL_MEM_IRQ -1
#define EPCQ_CONFIG_AVL_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define EPCQ_CONFIG_AVL_MEM_IS_EPCS 0
#define EPCQ_CONFIG_AVL_MEM_NAME "/dev/epcq_config_avl_mem"
#define EPCQ_CONFIG_AVL_MEM_NUMBER_OF_SECTORS 512
#define EPCQ_CONFIG_AVL_MEM_PAGE_SIZE 256
#define EPCQ_CONFIG_AVL_MEM_SECTOR_SIZE 65536
#define EPCQ_CONFIG_AVL_MEM_SPAN 33554432
#define EPCQ_CONFIG_AVL_MEM_SUBSECTOR_SIZE 4096
#define EPCQ_CONFIG_AVL_MEM_TYPE "altera_epcq_controller"


/*
 * eth_tse configuration
 *
 */

#define ALT_MODULE_CLASS_eth_tse altera_eth_tse
#define ETH_TSE_BASE 0x22003000
#define ETH_TSE_ENABLE_MACLITE 0
#define ETH_TSE_FIFO_WIDTH 32
#define ETH_TSE_IRQ -1
#define ETH_TSE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ETH_TSE_IS_MULTICHANNEL_MAC 0
#define ETH_TSE_MACLITE_GIGE 0
#define ETH_TSE_MDIO_SHARED 0
#define ETH_TSE_NAME "/dev/eth_tse"
#define ETH_TSE_NUMBER_OF_CHANNEL 1
#define ETH_TSE_NUMBER_OF_MAC_MDIO_SHARED 1
#define ETH_TSE_PCS 1
#define ETH_TSE_PCS_ID 0
#define ETH_TSE_PCS_SGMII 1
#define ETH_TSE_RECEIVE_FIFO_DEPTH 4096
#define ETH_TSE_REGISTER_SHARED 0
#define ETH_TSE_RGMII 0
#define ETH_TSE_SPAN 1024
#define ETH_TSE_TRANSMIT_FIFO_DEPTH 4096
#define ETH_TSE_TYPE "altera_eth_tse"
#define ETH_TSE_USE_MDIO 0


/*
 * gxb_module_link_control configuration
 *
 */

#define ALT_MODULE_CLASS_gxb_module_link_control gxb_module
#define GXB_MODULE_LINK_CONTROL_BASE 0x21000140
#define GXB_MODULE_LINK_CONTROL_IRQ -1
#define GXB_MODULE_LINK_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define GXB_MODULE_LINK_CONTROL_NAME "/dev/gxb_module_link_control"
#define GXB_MODULE_LINK_CONTROL_SPAN 16
#define GXB_MODULE_LINK_CONTROL_TYPE "gxb_module"


/*
 * gxb_module_link_status configuration
 *
 */

#define ALT_MODULE_CLASS_gxb_module_link_status gxb_module
#define GXB_MODULE_LINK_STATUS_BASE 0x21000000
#define GXB_MODULE_LINK_STATUS_IRQ -1
#define GXB_MODULE_LINK_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define GXB_MODULE_LINK_STATUS_NAME "/dev/gxb_module_link_status"
#define GXB_MODULE_LINK_STATUS_SPAN 128
#define GXB_MODULE_LINK_STATUS_TYPE "gxb_module"


/*
 * gxb_reconfig configuration
 *
 */

#define ALT_MODULE_CLASS_gxb_reconfig alt_xcvr_reconfig
#define GXB_RECONFIG_BASE 0x22003600
#define GXB_RECONFIG_IRQ -1
#define GXB_RECONFIG_IRQ_INTERRUPT_CONTROLLER_ID -1
#define GXB_RECONFIG_NAME "/dev/gxb_reconfig"
#define GXB_RECONFIG_SPAN 512
#define GXB_RECONFIG_TYPE "alt_xcvr_reconfig"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK SYS_TIMER
#define ALT_TIMESTAMP_CLK SYS_TIMESTAMP


/*
 * i2c_mac configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_mac i2c_opencores
#define I2C_MAC_BASE 0x260001a0
#define I2C_MAC_BUS_FREQ 100000u
#define I2C_MAC_CLOCK_FREQ 12500000u
#define I2C_MAC_IRQ 11
#define I2C_MAC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_MAC_NAME "/dev/i2c_mac"
#define I2C_MAC_SPAN 32
#define I2C_MAC_TYPE "i2c_opencores"


/*
 * i2c_sfp configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_sfp i2c_opencores
#define I2C_SFP_BASE 0x260001c0
#define I2C_SFP_BUS_FREQ 100000u
#define I2C_SFP_CLOCK_FREQ 12500000u
#define I2C_SFP_IRQ 10
#define I2C_SFP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_SFP_NAME "/dev/i2c_sfp"
#define I2C_SFP_SPAN 32
#define I2C_SFP_TYPE "i2c_opencores"


/*
 * jtaguart_0 configuration
 *
 */

#define ALT_MODULE_CLASS_jtaguart_0 altera_avalon_jtag_uart
#define JTAGUART_0_BASE 0x220038c8
#define JTAGUART_0_IRQ 0
#define JTAGUART_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAGUART_0_NAME "/dev/jtaguart_0"
#define JTAGUART_0_READ_DEPTH 2048
#define JTAGUART_0_READ_THRESHOLD 8
#define JTAGUART_0_SPAN 8
#define JTAGUART_0_TYPE "altera_avalon_jtag_uart"
#define JTAGUART_0_WRITE_DEPTH 8192
#define JTAGUART_0_WRITE_THRESHOLD 8


/*
 * memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr custom_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_BASE 0x22001420
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_NAME "/dev/memory_checker_pattern_checker_subsystem_custom_pattern_checker_csr"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_SPAN 16
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_CSR_TYPE "custom_pattern_checker"


/*
 * memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access custom_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_BASE 0x22001000
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_NAME "/dev/memory_checker_pattern_checker_subsystem_custom_pattern_checker_pattern_access"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_SPAN 1024
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_CUSTOM_PATTERN_CHECKER_PATTERN_ACCESS_TYPE "custom_pattern_checker"


/*
 * memory_checker_pattern_checker_subsystem_one_to_two_st_demux configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_one_to_two_st_demux one_to_two_st_demux
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_BASE 0x22001400
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_NAME "/dev/memory_checker_pattern_checker_subsystem_one_to_two_st_demux"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_SPAN 8
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_ONE_TO_TWO_ST_DEMUX_TYPE "one_to_two_st_demux"


/*
 * memory_checker_pattern_checker_subsystem_prbs_pattern_checker configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_checker_subsystem_prbs_pattern_checker prbs_pattern_checker
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_BASE 0x22001440
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_IRQ -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_NAME "/dev/memory_checker_pattern_checker_subsystem_prbs_pattern_checker"
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_SPAN 32
#define MEMORY_CHECKER_PATTERN_CHECKER_SUBSYSTEM_PRBS_PATTERN_CHECKER_TYPE "prbs_pattern_checker"


/*
 * memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr custom_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_BASE 0x22000400
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_NAME "/dev/memory_checker_pattern_generator_subsystem_custom_pattern_generator_csr"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_SPAN 16
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_CSR_TYPE "custom_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access custom_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_BASE 0x22000000
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_NAME "/dev/memory_checker_pattern_generator_subsystem_custom_pattern_generator_pattern_access"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_SPAN 1024
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_CUSTOM_PATTERN_GENERATOR_PATTERN_ACCESS_TYPE "custom_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_prbs_pattern_generator configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_prbs_pattern_generator prbs_pattern_generator
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_BASE 0x22000420
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_NAME "/dev/memory_checker_pattern_generator_subsystem_prbs_pattern_generator"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_SPAN 32
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_PRBS_PATTERN_GENERATOR_TYPE "prbs_pattern_generator"


/*
 * memory_checker_pattern_generator_subsystem_two_to_one_st_mux configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_pattern_generator_subsystem_two_to_one_st_mux two_to_one_st_mux
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_BASE 0x22000440
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_IRQ -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_NAME "/dev/memory_checker_pattern_generator_subsystem_two_to_one_st_mux"
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_SPAN 8
#define MEMORY_CHECKER_PATTERN_GENERATOR_SUBSYSTEM_TWO_TO_ONE_ST_MUX_TYPE "two_to_one_st_mux"


/*
 * memory_checker_ram_test_controller configuration
 *
 */

#define ALT_MODULE_CLASS_memory_checker_ram_test_controller ram_test_controller
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_BASE 0x22000800
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_CLOCK_FREQUENCY_IN_HZ 100000000
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_BLOCK_SIZE 1024
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_TIMER_RESOLUTION 10
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_DEFAULT_TRAIL_DISTANCE 1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_IRQ -1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_NAME "/dev/memory_checker_ram_test_controller"
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_SPAN 32
#define MEMORY_CHECKER_RAM_TEST_CONTROLLER_TYPE "ram_test_controller"


/*
 * ram_descriptor configuration
 *
 */

#define ALT_MODULE_CLASS_ram_descriptor altera_avalon_onchip_memory2
#define RAM_DESCRIPTOR_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define RAM_DESCRIPTOR_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define RAM_DESCRIPTOR_BASE 0x22003400
#define RAM_DESCRIPTOR_CONTENTS_INFO ""
#define RAM_DESCRIPTOR_DUAL_PORT 1
#define RAM_DESCRIPTOR_GUI_RAM_BLOCK_TYPE "AUTO"
#define RAM_DESCRIPTOR_INIT_CONTENTS_FILE "feam_ram_descriptor"
#define RAM_DESCRIPTOR_INIT_MEM_CONTENT 0
#define RAM_DESCRIPTOR_INSTANCE_ID "NONE"
#define RAM_DESCRIPTOR_IRQ -1
#define RAM_DESCRIPTOR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_DESCRIPTOR_NAME "/dev/ram_descriptor"
#define RAM_DESCRIPTOR_NON_DEFAULT_INIT_FILE_ENABLED 0
#define RAM_DESCRIPTOR_RAM_BLOCK_TYPE "AUTO"
#define RAM_DESCRIPTOR_READ_DURING_WRITE_MODE "DONT_CARE"
#define RAM_DESCRIPTOR_SINGLE_CLOCK_OP 0
#define RAM_DESCRIPTOR_SIZE_MULTIPLE 1
#define RAM_DESCRIPTOR_SIZE_VALUE 512
#define RAM_DESCRIPTOR_SPAN 512
#define RAM_DESCRIPTOR_TYPE "altera_avalon_onchip_memory2"
#define RAM_DESCRIPTOR_WRITABLE 1


/*
 * ram_nios configuration
 *
 */

#define ALT_MODULE_CLASS_ram_nios altera_avalon_onchip_memory2
#define RAM_NIOS_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define RAM_NIOS_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define RAM_NIOS_BASE 0x40000000
#define RAM_NIOS_CONTENTS_INFO ""
#define RAM_NIOS_DUAL_PORT 0
#define RAM_NIOS_GUI_RAM_BLOCK_TYPE "AUTO"
#define RAM_NIOS_INIT_CONTENTS_FILE "feam_ram_nios"
#define RAM_NIOS_INIT_MEM_CONTENT 1
#define RAM_NIOS_INSTANCE_ID "NONE"
#define RAM_NIOS_IRQ -1
#define RAM_NIOS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_NIOS_NAME "/dev/ram_nios"
#define RAM_NIOS_NON_DEFAULT_INIT_FILE_ENABLED 0
#define RAM_NIOS_RAM_BLOCK_TYPE "AUTO"
#define RAM_NIOS_READ_DURING_WRITE_MODE "DONT_CARE"
#define RAM_NIOS_SINGLE_CLOCK_OP 0
#define RAM_NIOS_SIZE_MULTIPLE 1
#define RAM_NIOS_SIZE_VALUE 32768
#define RAM_NIOS_SPAN 32768
#define RAM_NIOS_TYPE "altera_avalon_onchip_memory2"
#define RAM_NIOS_WRITABLE 1


/*
 * remote_update configuration
 *
 */

#define ALT_MODULE_CLASS_remote_update altera_remote_update
#define REMOTE_UPDATE_BASE 0x26000160
#define REMOTE_UPDATE_IRQ -1
#define REMOTE_UPDATE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define REMOTE_UPDATE_NAME "/dev/remote_update"
#define REMOTE_UPDATE_SPAN 32
#define REMOTE_UPDATE_TYPE "altera_remote_update"


/*
 * sgdma_rx configuration
 *
 */

#define ALT_MODULE_CLASS_sgdma_rx altera_avalon_sgdma
#define SGDMA_RX_ADDRESS_WIDTH 32
#define SGDMA_RX_ALWAYS_DO_MAX_BURST 1
#define SGDMA_RX_ATLANTIC_CHANNEL_DATA_WIDTH 4
#define SGDMA_RX_AVALON_MM_BYTE_REORDER_MODE 0
#define SGDMA_RX_BASE 0x210000c0
#define SGDMA_RX_BURST_DATA_WIDTH 8
#define SGDMA_RX_BURST_TRANSFER 0
#define SGDMA_RX_BYTES_TO_TRANSFER_DATA_WIDTH 16
#define SGDMA_RX_CHAIN_WRITEBACK_DATA_WIDTH 32
#define SGDMA_RX_COMMAND_FIFO_DATA_WIDTH 104
#define SGDMA_RX_CONTROL_DATA_WIDTH 8
#define SGDMA_RX_CONTROL_SLAVE_ADDRESS_WIDTH 0x4
#define SGDMA_RX_CONTROL_SLAVE_DATA_WIDTH 32
#define SGDMA_RX_DESCRIPTOR_READ_BURST 0
#define SGDMA_RX_DESC_DATA_WIDTH 32
#define SGDMA_RX_HAS_READ_BLOCK 0
#define SGDMA_RX_HAS_WRITE_BLOCK 1
#define SGDMA_RX_IN_ERROR_WIDTH 0
#define SGDMA_RX_IRQ 4
#define SGDMA_RX_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SGDMA_RX_NAME "/dev/sgdma_rx"
#define SGDMA_RX_OUT_ERROR_WIDTH 0
#define SGDMA_RX_READ_BLOCK_DATA_WIDTH 32
#define SGDMA_RX_READ_BURSTCOUNT_WIDTH 4
#define SGDMA_RX_SPAN 64
#define SGDMA_RX_STATUS_TOKEN_DATA_WIDTH 24
#define SGDMA_RX_STREAM_DATA_WIDTH 32
#define SGDMA_RX_SYMBOLS_PER_BEAT 4
#define SGDMA_RX_TYPE "altera_avalon_sgdma"
#define SGDMA_RX_UNALIGNED_TRANSFER 0
#define SGDMA_RX_WRITE_BLOCK_DATA_WIDTH 32
#define SGDMA_RX_WRITE_BURSTCOUNT_WIDTH 4


/*
 * sgdma_tx configuration
 *
 */

#define ALT_MODULE_CLASS_sgdma_tx altera_avalon_sgdma
#define SGDMA_TX_ADDRESS_WIDTH 32
#define SGDMA_TX_ALWAYS_DO_MAX_BURST 1
#define SGDMA_TX_ATLANTIC_CHANNEL_DATA_WIDTH 4
#define SGDMA_TX_AVALON_MM_BYTE_REORDER_MODE 0
#define SGDMA_TX_BASE 0x21000100
#define SGDMA_TX_BURST_DATA_WIDTH 8
#define SGDMA_TX_BURST_TRANSFER 0
#define SGDMA_TX_BYTES_TO_TRANSFER_DATA_WIDTH 16
#define SGDMA_TX_CHAIN_WRITEBACK_DATA_WIDTH 32
#define SGDMA_TX_COMMAND_FIFO_DATA_WIDTH 104
#define SGDMA_TX_CONTROL_DATA_WIDTH 8
#define SGDMA_TX_CONTROL_SLAVE_ADDRESS_WIDTH 0x4
#define SGDMA_TX_CONTROL_SLAVE_DATA_WIDTH 32
#define SGDMA_TX_DESCRIPTOR_READ_BURST 0
#define SGDMA_TX_DESC_DATA_WIDTH 32
#define SGDMA_TX_HAS_READ_BLOCK 1
#define SGDMA_TX_HAS_WRITE_BLOCK 0
#define SGDMA_TX_IN_ERROR_WIDTH 0
#define SGDMA_TX_IRQ 3
#define SGDMA_TX_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SGDMA_TX_NAME "/dev/sgdma_tx"
#define SGDMA_TX_OUT_ERROR_WIDTH 0
#define SGDMA_TX_READ_BLOCK_DATA_WIDTH 32
#define SGDMA_TX_READ_BURSTCOUNT_WIDTH 4
#define SGDMA_TX_SPAN 64
#define SGDMA_TX_STATUS_TOKEN_DATA_WIDTH 24
#define SGDMA_TX_STREAM_DATA_WIDTH 32
#define SGDMA_TX_SYMBOLS_PER_BEAT 4
#define SGDMA_TX_TYPE "altera_avalon_sgdma"
#define SGDMA_TX_UNALIGNED_TRANSFER 0
#define SGDMA_TX_WRITE_BLOCK_DATA_WIDTH 32
#define SGDMA_TX_WRITE_BURSTCOUNT_WIDTH 4


/*
 * sigproc_control configuration
 *
 */

#define ALT_MODULE_CLASS_sigproc_control sigproc
#define SIGPROC_CONTROL_BASE 0x20000000
#define SIGPROC_CONTROL_IRQ -1
#define SIGPROC_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SIGPROC_CONTROL_NAME "/dev/sigproc_control"
#define SIGPROC_CONTROL_NUM_MEM_SLOTS 32u
#define SIGPROC_CONTROL_SPAN 2048
#define SIGPROC_CONTROL_TYPE "sigproc"


/*
 * sigproc_status configuration
 *
 */

#define ALT_MODULE_CLASS_sigproc_status sigproc
#define SIGPROC_STATUS_BASE 0x20000980
#define SIGPROC_STATUS_IRQ -1
#define SIGPROC_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SIGPROC_STATUS_NAME "/dev/sigproc_status"
#define SIGPROC_STATUS_NUM_MEM_SLOTS 32u
#define SIGPROC_STATUS_SPAN 32
#define SIGPROC_STATUS_TYPE "sigproc"


/*
 * spi_adc1 configuration
 *
 */

#define ALT_MODULE_CLASS_spi_adc1 altera_avalon_spi
#define SPI_ADC1_BASE 0x260000e0
#define SPI_ADC1_CLOCKMULT 1
#define SPI_ADC1_CLOCKPHASE 0
#define SPI_ADC1_CLOCKPOLARITY 0
#define SPI_ADC1_CLOCKUNITS "Hz"
#define SPI_ADC1_DATABITS 8
#define SPI_ADC1_DATAWIDTH 16
#define SPI_ADC1_DELAYMULT "1.0E-9"
#define SPI_ADC1_DELAYUNITS "ns"
#define SPI_ADC1_EXTRADELAY 0
#define SPI_ADC1_INSERT_SYNC 1
#define SPI_ADC1_IRQ 8
#define SPI_ADC1_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_ADC1_ISMASTER 1
#define SPI_ADC1_LSBFIRST 0
#define SPI_ADC1_NAME "/dev/spi_adc1"
#define SPI_ADC1_NUMSLAVES 1
#define SPI_ADC1_PREFIX "spi_"
#define SPI_ADC1_SPAN 32
#define SPI_ADC1_SYNC_REG_DEPTH 3
#define SPI_ADC1_TARGETCLOCK 3125000u
#define SPI_ADC1_TARGETSSDELAY "0.0"
#define SPI_ADC1_TYPE "altera_avalon_spi"


/*
 * spi_adc2 configuration
 *
 */

#define ALT_MODULE_CLASS_spi_adc2 altera_avalon_spi
#define SPI_ADC2_BASE 0x260000c0
#define SPI_ADC2_CLOCKMULT 1
#define SPI_ADC2_CLOCKPHASE 0
#define SPI_ADC2_CLOCKPOLARITY 0
#define SPI_ADC2_CLOCKUNITS "Hz"
#define SPI_ADC2_DATABITS 8
#define SPI_ADC2_DATAWIDTH 16
#define SPI_ADC2_DELAYMULT "1.0E-9"
#define SPI_ADC2_DELAYUNITS "ns"
#define SPI_ADC2_EXTRADELAY 0
#define SPI_ADC2_INSERT_SYNC 1
#define SPI_ADC2_IRQ 9
#define SPI_ADC2_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_ADC2_ISMASTER 1
#define SPI_ADC2_LSBFIRST 0
#define SPI_ADC2_NAME "/dev/spi_adc2"
#define SPI_ADC2_NUMSLAVES 1
#define SPI_ADC2_PREFIX "spi_"
#define SPI_ADC2_SPAN 32
#define SPI_ADC2_SYNC_REG_DEPTH 3
#define SPI_ADC2_TARGETCLOCK 3125000u
#define SPI_ADC2_TARGETSSDELAY "0.0"
#define SPI_ADC2_TYPE "altera_avalon_spi"


/*
 * spi_clockcleaner configuration
 *
 */

#define ALT_MODULE_CLASS_spi_clockcleaner altera_avalon_spi
#define SPI_CLOCKCLEANER_BASE 0x26000100
#define SPI_CLOCKCLEANER_CLOCKMULT 1
#define SPI_CLOCKCLEANER_CLOCKPHASE 0
#define SPI_CLOCKCLEANER_CLOCKPOLARITY 0
#define SPI_CLOCKCLEANER_CLOCKUNITS "Hz"
#define SPI_CLOCKCLEANER_DATABITS 8
#define SPI_CLOCKCLEANER_DATAWIDTH 16
#define SPI_CLOCKCLEANER_DELAYMULT "1.0E-9"
#define SPI_CLOCKCLEANER_DELAYUNITS "ns"
#define SPI_CLOCKCLEANER_EXTRADELAY 0
#define SPI_CLOCKCLEANER_INSERT_SYNC 1
#define SPI_CLOCKCLEANER_IRQ 7
#define SPI_CLOCKCLEANER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_CLOCKCLEANER_ISMASTER 1
#define SPI_CLOCKCLEANER_LSBFIRST 0
#define SPI_CLOCKCLEANER_NAME "/dev/spi_clockcleaner"
#define SPI_CLOCKCLEANER_NUMSLAVES 1
#define SPI_CLOCKCLEANER_PREFIX "spi_"
#define SPI_CLOCKCLEANER_SPAN 32
#define SPI_CLOCKCLEANER_SYNC_REG_DEPTH 2
#define SPI_CLOCKCLEANER_TARGETCLOCK 3125000u
#define SPI_CLOCKCLEANER_TARGETSSDELAY "0.0"
#define SPI_CLOCKCLEANER_TYPE "altera_avalon_spi"


/*
 * spi_external configuration
 *
 */

#define ALT_MODULE_CLASS_spi_external altera_avalon_spi
#define SPI_EXTERNAL_BASE 0x26000140
#define SPI_EXTERNAL_CLOCKMULT 1
#define SPI_EXTERNAL_CLOCKPHASE 0
#define SPI_EXTERNAL_CLOCKPOLARITY 0
#define SPI_EXTERNAL_CLOCKUNITS "Hz"
#define SPI_EXTERNAL_DATABITS 16
#define SPI_EXTERNAL_DATAWIDTH 16
#define SPI_EXTERNAL_DELAYMULT "1.0E-9"
#define SPI_EXTERNAL_DELAYUNITS "ns"
#define SPI_EXTERNAL_EXTRADELAY 0
#define SPI_EXTERNAL_INSERT_SYNC 1
#define SPI_EXTERNAL_IRQ 5
#define SPI_EXTERNAL_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_EXTERNAL_ISMASTER 1
#define SPI_EXTERNAL_LSBFIRST 0
#define SPI_EXTERNAL_NAME "/dev/spi_external"
#define SPI_EXTERNAL_NUMSLAVES 1
#define SPI_EXTERNAL_PREFIX "spi_"
#define SPI_EXTERNAL_SPAN 32
#define SPI_EXTERNAL_SYNC_REG_DEPTH 2
#define SPI_EXTERNAL_TARGETCLOCK 156250u
#define SPI_EXTERNAL_TARGETSSDELAY "0.0"
#define SPI_EXTERNAL_TYPE "altera_avalon_spi"


/*
 * spi_temp configuration
 *
 */

#define ALT_MODULE_CLASS_spi_temp altera_avalon_spi
#define SPI_TEMP_BASE 0x26000120
#define SPI_TEMP_CLOCKMULT 1
#define SPI_TEMP_CLOCKPHASE 0
#define SPI_TEMP_CLOCKPOLARITY 0
#define SPI_TEMP_CLOCKUNITS "Hz"
#define SPI_TEMP_DATABITS 8
#define SPI_TEMP_DATAWIDTH 16
#define SPI_TEMP_DELAYMULT "1.0E-9"
#define SPI_TEMP_DELAYUNITS "ns"
#define SPI_TEMP_EXTRADELAY 0
#define SPI_TEMP_INSERT_SYNC 1
#define SPI_TEMP_IRQ 6
#define SPI_TEMP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_TEMP_ISMASTER 1
#define SPI_TEMP_LSBFIRST 0
#define SPI_TEMP_NAME "/dev/spi_temp"
#define SPI_TEMP_NUMSLAVES 1
#define SPI_TEMP_PREFIX "spi_"
#define SPI_TEMP_SPAN 32
#define SPI_TEMP_SYNC_REG_DEPTH 2
#define SPI_TEMP_TARGETCLOCK 156250u
#define SPI_TEMP_TARGETSSDELAY "0.0"
#define SPI_TEMP_TYPE "altera_avalon_spi"


/*
 * sys_timer configuration
 *
 */

#define ALT_MODULE_CLASS_sys_timer altera_avalon_timer
#define SYS_TIMER_ALWAYS_RUN 0
#define SYS_TIMER_BASE 0x22003800
#define SYS_TIMER_COUNTER_SIZE 32
#define SYS_TIMER_FIXED_PERIOD 0
#define SYS_TIMER_FREQ 100000000
#define SYS_TIMER_IRQ 1
#define SYS_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYS_TIMER_LOAD_VALUE 99999
#define SYS_TIMER_MULT 0.001
#define SYS_TIMER_NAME "/dev/sys_timer"
#define SYS_TIMER_PERIOD 1
#define SYS_TIMER_PERIOD_UNITS "ms"
#define SYS_TIMER_RESET_OUTPUT 0
#define SYS_TIMER_SNAPSHOT 1
#define SYS_TIMER_SPAN 32
#define SYS_TIMER_TICKS_PER_SEC 1000
#define SYS_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define SYS_TIMER_TYPE "altera_avalon_timer"


/*
 * sys_timestamp configuration
 *
 */

#define ALT_MODULE_CLASS_sys_timestamp altera_avalon_timer
#define SYS_TIMESTAMP_ALWAYS_RUN 0
#define SYS_TIMESTAMP_BASE 0x22003820
#define SYS_TIMESTAMP_COUNTER_SIZE 32
#define SYS_TIMESTAMP_FIXED_PERIOD 0
#define SYS_TIMESTAMP_FREQ 100000000
#define SYS_TIMESTAMP_IRQ 2
#define SYS_TIMESTAMP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYS_TIMESTAMP_LOAD_VALUE 99999
#define SYS_TIMESTAMP_MULT 0.001
#define SYS_TIMESTAMP_NAME "/dev/sys_timestamp"
#define SYS_TIMESTAMP_PERIOD 1
#define SYS_TIMESTAMP_PERIOD_UNITS "ms"
#define SYS_TIMESTAMP_RESET_OUTPUT 0
#define SYS_TIMESTAMP_SNAPSHOT 1
#define SYS_TIMESTAMP_SPAN 32
#define SYS_TIMESTAMP_TICKS_PER_SEC 1000
#define SYS_TIMESTAMP_TIMEOUT_PULSE_OUTPUT 0
#define SYS_TIMESTAMP_TYPE "altera_avalon_timer"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x220038c0
#define SYSID_ID 1095517765
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1679959176
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * trigger_control_control configuration
 *
 */

#define ALT_MODULE_CLASS_trigger_control_control trigger_control
#define TRIGGER_CONTROL_CONTROL_BASE 0x20000900
#define TRIGGER_CONTROL_CONTROL_IRQ -1
#define TRIGGER_CONTROL_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TRIGGER_CONTROL_CONTROL_NAME "/dev/trigger_control_control"
#define TRIGGER_CONTROL_CONTROL_NUM_BUSY_SOURCES 4u
#define TRIGGER_CONTROL_CONTROL_NUM_TRIG_SOURCES 6u
#define TRIGGER_CONTROL_CONTROL_SPAN 128
#define TRIGGER_CONTROL_CONTROL_TYPE "trigger_control"


/*
 * trigger_control_status configuration
 *
 */

#define ALT_MODULE_CLASS_trigger_control_status trigger_control
#define TRIGGER_CONTROL_STATUS_BASE 0x20000800
#define TRIGGER_CONTROL_STATUS_IRQ -1
#define TRIGGER_CONTROL_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TRIGGER_CONTROL_STATUS_NAME "/dev/trigger_control_status"
#define TRIGGER_CONTROL_STATUS_NUM_BUSY_SOURCES 4u
#define TRIGGER_CONTROL_STATUS_NUM_TRIG_SOURCES 6u
#define TRIGGER_CONTROL_STATUS_SPAN 256
#define TRIGGER_CONTROL_STATUS_TYPE "trigger_control"


/*
 * ucosii configuration
 *
 */

#define OS_ARG_CHK_EN 1
#define OS_CPU_HOOKS_EN 1
#define OS_DEBUG_EN 1
#define OS_EVENT_NAME_SIZE 32
#define OS_FLAGS_NBITS 16
#define OS_FLAG_ACCEPT_EN 1
#define OS_FLAG_DEL_EN 1
#define OS_FLAG_EN 1
#define OS_FLAG_NAME_SIZE 32
#define OS_FLAG_QUERY_EN 1
#define OS_FLAG_WAIT_CLR_EN 1
#define OS_LOWEST_PRIO 20
#define OS_MAX_EVENTS 60
#define OS_MAX_FLAGS 20
#define OS_MAX_MEM_PART 60
#define OS_MAX_QS 20
#define OS_MAX_TASKS 10
#define OS_MBOX_ACCEPT_EN 1
#define OS_MBOX_DEL_EN 1
#define OS_MBOX_EN 1
#define OS_MBOX_POST_EN 1
#define OS_MBOX_POST_OPT_EN 1
#define OS_MBOX_QUERY_EN 1
#define OS_MEM_EN 1
#define OS_MEM_NAME_SIZE 32
#define OS_MEM_QUERY_EN 1
#define OS_MUTEX_ACCEPT_EN 1
#define OS_MUTEX_DEL_EN 1
#define OS_MUTEX_EN 1
#define OS_MUTEX_QUERY_EN 1
#define OS_Q_ACCEPT_EN 1
#define OS_Q_DEL_EN 1
#define OS_Q_EN 1
#define OS_Q_FLUSH_EN 1
#define OS_Q_POST_EN 1
#define OS_Q_POST_FRONT_EN 1
#define OS_Q_POST_OPT_EN 1
#define OS_Q_QUERY_EN 1
#define OS_SCHED_LOCK_EN 1
#define OS_SEM_ACCEPT_EN 1
#define OS_SEM_DEL_EN 1
#define OS_SEM_EN 1
#define OS_SEM_QUERY_EN 1
#define OS_SEM_SET_EN 1
#define OS_TASK_CHANGE_PRIO_EN 1
#define OS_TASK_CREATE_EN 1
#define OS_TASK_CREATE_EXT_EN 1
#define OS_TASK_DEL_EN 1
#define OS_TASK_IDLE_STK_SIZE 512
#define OS_TASK_NAME_SIZE 32
#define OS_TASK_PROFILE_EN 1
#define OS_TASK_QUERY_EN 1
#define OS_TASK_STAT_EN 1
#define OS_TASK_STAT_STK_CHK_EN 1
#define OS_TASK_STAT_STK_SIZE 512
#define OS_TASK_SUSPEND_EN 1
#define OS_TASK_SW_HOOK_EN 1
#define OS_TASK_TMR_PRIO 0
#define OS_TASK_TMR_STK_SIZE 512
#define OS_THREAD_SAFE_NEWLIB 1
#define OS_TICKS_PER_SEC SYS_TIMER_TICKS_PER_SEC
#define OS_TICK_STEP_EN 1
#define OS_TIME_DLY_HMSM_EN 1
#define OS_TIME_DLY_RESUME_EN 1
#define OS_TIME_GET_SET_EN 1
#define OS_TIME_TICK_HOOK_EN 1
#define OS_TMR_CFG_MAX 16
#define OS_TMR_CFG_NAME_SIZE 16
#define OS_TMR_CFG_TICKS_PER_SEC 10
#define OS_TMR_CFG_WHEEL_SIZE 2
#define OS_TMR_EN 1


/*
 * udp_stream_sata configuration
 *
 */

#define ALT_MODULE_CLASS_udp_stream_sata fabric_udp_stream
#define UDP_STREAM_SATA_BASE 0x21000180
#define UDP_STREAM_SATA_IRQ -1
#define UDP_STREAM_SATA_IRQ_INTERRUPT_CONTROLLER_ID -1
#define UDP_STREAM_SATA_NAME "/dev/udp_stream_sata"
#define UDP_STREAM_SATA_SPAN 64
#define UDP_STREAM_SATA_TYPE "fabric_udp_stream"


/*
 * udp_stream_sca_0 configuration
 *
 */

#define ALT_MODULE_CLASS_udp_stream_sca_0 fabric_udp_stream
#define UDP_STREAM_SCA_0_BASE 0x21000080
#define UDP_STREAM_SCA_0_IRQ -1
#define UDP_STREAM_SCA_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define UDP_STREAM_SCA_0_NAME "/dev/udp_stream_sca_0"
#define UDP_STREAM_SCA_0_SPAN 64
#define UDP_STREAM_SCA_0_TYPE "fabric_udp_stream"

#endif /* __SYSTEM_H_ */
