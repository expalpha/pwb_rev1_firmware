#include <alt_types.h>
#include <stdint.h>
#include <unistd.h>
#include <sca_protocol.h>
#include <sca_protocol_regs.h>
#include <io.h>

void SCA_Write(alt_u32 BASE_ADDR_CTRL, alt_u32 BASE_ADDR_STAT, uint8_t addr, uint8_t len,  uint64_t cmd) {
   uint32_t reg;
   
   do { usleep(1); } while(IORD(BASE_ADDR_STAT, SCA_PROTOCOL_REG_STAT) != 0);
   reg = (len << 8) | (addr & 0x7F);
   
   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_MSB, (cmd >> 32) & 0xFFFFFFFF);
   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_LSB,  cmd & 0xFFFFFFFF);
   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_CMD, reg);
}

uint64_t SCA_Read(alt_u32 BASE_ADDR_CTRL, alt_u32 BASE_ADDR_STAT, uint8_t addr, uint8_t len) {
   uint64_t val;
   uint32_t reg;
   
   // wait for SCA to be ready 
   do { usleep(1); } while(IORD(BASE_ADDR_STAT, SCA_PROTOCOL_REG_STAT) != 0);
   
   reg = (len << 8) | 0x80 | (addr & 0x7F);

   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_MSB, 0);
   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_LSB, 0);
   IOWR(BASE_ADDR_CTRL, SCA_PROTOCOL_REG_CMD, reg);
   do { usleep(1); } while(IORD(BASE_ADDR_STAT, SCA_PROTOCOL_REG_STAT) != 0);
   
   val = IORD(BASE_ADDR_STAT, SCA_PROTOCOL_REG_MSB);
   val = val << 32;
   val |= IORD(BASE_ADDR_STAT, SCA_PROTOCOL_REG_LSB);
   
   return val;
}

