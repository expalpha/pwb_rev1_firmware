#ifndef __GXB_MODULE_REGS_H__
#define __GXB_MODULE_REGS_H__

// Control Registers
#define GXB_MOD_REG_CTRL_RESET_COUNTERS			0
#define GXB_MOD_REG_CTRL_SERIAL_LPBK_EN			1
#define GXB_MOD_REG_CTRL_RX_INVERT  			2
#define GXB_MOD_REG_CTRL_LINK_CTRL  			3
#define GXB_MOD_NUM_CTRL_REGS 					4

// Status Registers
#define GXB_MOD_REG_STAT_PLL_LOCKED   			0
#define GXB_MOD_REG_STAT_IS_RX_LOCKED_TO_DATA  	1
#define GXB_MOD_REG_STAT_IS_RX_LOCKED_TO_REF   	2
#define GXB_MOD_REG_STAT_SIGNAL_DETECT      	3
#define GXB_MOD_REG_STAT_RX_ERR_DETECT     		4
#define GXB_MOD_REG_STAT_RX_ERR_DISPARITY     	5
#define GXB_MOD_REG_STAT_RX_PATTERN_DETECT      6
#define GXB_MOD_REG_STAT_RX_SYNC_STATUS   		7
#define GXB_MOD_REG_STAT_LINK_STATUS          	8
#define GXB_MOD_REG_STAT_CNT_PLL_LOCKED  		9
#define GXB_MOD_REG_STAT_CNT_IS_LOCKED_TO_DATA 	10
#define GXB_MOD_REG_STAT_CNT_IS_LOCKED_TO_REF 	11
#define GXB_MOD_REG_STAT_CNT_SIGNAL_DETECT 		12
#define GXB_MOD_REG_STAT_CNT_RX_ERR_DETECT  	13
#define GXB_MOD_REG_STAT_CNT_RX_ERR_DISPARITY  	14
#define GXB_MOD_REG_STAT_CNT_RX_SYNC_STATUS 	15
#define GXB_MOD_REG_STAT_CNT_LINK_STATUS     	16
#define GXB_MOD_REG_STAT_CNT_RX_PATTERN_DETECT  17
#define GXB_MOD_REG_STAT_CNT_BADK_RX            18
#define GXB_MOD_REG_STAT_CNT_STOP_RX            19
#define GXB_MOD_REG_STAT_CNT_PKTS_TX            20
#define GXB_MOD_REG_STAT_CNT_PKTS_RX            21
#define GXB_MOD_REG_STAT_CNT_OCTETS_TX          22
#define GXB_MOD_REG_STAT_CNT_OCTETS_RX          23
#define GXB_MOD_REG_STAT_CNT_TRIG_TX            24
#define GXB_MOD_REG_STAT_CNT_TRIG_RX            25
#define GXB_MOD_REG_STAT_CNT_OVFL_RX            26
#define GXB_MOD_REG_STAT_CNT_STOP_TX            27
#define GXB_MOD_REG_STAT_CNT_FLOW_TX            28

#define GXB_MOD_NUM_STAT_REGS       			29

#endif /* __GXB_MODULE_REGS_H__ */
