#ifndef SCA_PROTOCOL_REGS_H_
#define SCA_PROTOCOL_REGS_H_

// Control Registers
#define SCA_PROTOCOL_REG_CMD 			( 0 )
#define SCA_PROTOCOL_REG_MSB			( 1 )
#define SCA_PROTOCOL_REG_LSB			( 2 )
#define SCA_PROTOCOL_NUM_CTRL_REGS 						( 3 )

// Status Registers
#define SCA_PROTOCOL_REG_STAT						( 0 )
#define SCA_PROTOCOL_REG_MSB				( 1 )
#define SCA_PROTOCOL_REG_LSB					( 2 )
#define SCA_PROTOCOL_NUM_STAT_REGS			( 3 )

#endif /* SCA_PROTOCOL_REGS_H_ */
