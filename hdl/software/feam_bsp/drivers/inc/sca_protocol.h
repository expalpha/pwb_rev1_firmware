#ifndef SCA_PROTOCOL_H_
#define SCA_PROTOCOL_H_

#include <alt_types.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void SCA_Write(alt_u32 BASE_ADDR_CTRL, alt_u32 BASE_ADDR_STAT, uint8_t addr, uint8_t len, uint64_t cmd);
uint64_t SCA_Read(alt_u32 BASE_ADDR_CTRL, alt_u32 BASE_ADDR_STAT, uint8_t addr, uint8_t len);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SCA_PROTOCOL_H_ */
