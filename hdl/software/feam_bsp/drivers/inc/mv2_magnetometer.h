/*
 * Copyright (c) 2016, TRIUMF
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of TRIUMF.
 *
 * Created by: Bryerton Shaw
 * Created on: May 16, 2016
 */

#ifndef SPI_MV2_MAGNETOMETER_H
#define SPI_MV2_MAGNETOMETER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define SPI_MV2_MAGNETOMETER_INSTANCE(name, dev) extern int alt_no_storage
#define SPI_MV2_MAGNETOMETER_INIT(name, dev) while (0)

#define SPI_MV2_MAGNETOMETER_AXIS_X 0
#define SPI_MV2_MAGNETOMETER_AXIS_Y 1
#define SPI_MV2_MAGNETOMETER_AXIS_Z 2
#define SPI_MV2_MAGNETOMETER_AXIS_T 3	// Temperature 'Axis'

typedef struct {
	// Register 00
	uint8_t ma;		// Measurement Axis
	uint8_t re;		// Resolution
	uint8_t ra;		// Range
	uint8_t os;		// Output Selection
	// Register 01
	uint8_t lmr;	// Large Measurement Range
	uint8_t emr; 	// Extended Measurement Range
	uint8_t hc;		// High Clock
	uint8_t inv;	// Invert
	uint8_t lp;		// Low Power
	uint8_t po;		// Permanent MISO output
	uint8_t sp;		// Status Position
	// Register 02
	uint8_t tc;	// Temperature Compensation 
} mv2_settings;

typedef uint16_t (*mv2_spi_write)(uint8_t addr, uint8_t data);
typedef uint8_t (*mv2_spi_read)(uint8_t addr);

void MV2_ReadSettings(mv2_spi_read rd, mv2_settings* settings);
void MV2_WriteSettings(mv2_spi_write wr, mv2_settings* settings);

uint16_t MV2_ReadAxis(mv2_spi_write wr, uint8_t next_axis_to_read, mv2_settings* settings);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SPI_MV2_MAGNETOMETER_H */
