// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:38:59 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
HutLCh1/bdtBalF+YWaLOPJ0ABwSQD6Js7v0dr3DDeFyUBc15eEV4tUP9It8b7/R
mugG9H1TJLVfSoPbTi/7TjqURI6lv4REBmWXCzvpvFYnNqIlblZ0da04957pAJL+
OfwVPmRR8JOEbDJYk5dahnsOn3K3ugtDp9dMrqMKvyE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2176)
b6a5nImlz8+QaAVN7yOSug5f8W7h+Lj4f1LkGiLdwkL6yaNMTnE9krMbq0pNF/Tp
d2a9CNgdtoU9VM4LAk3P9hSBU1tmAKwTF00Ws5hNqKQs7B9yuw+9l4mViAxGIYDJ
endKwzbHSiR8Cj5I39vl+B+/gI2g5swXXUL/tG7zrkqbLN2dHq7u9JLWTx7xMsjG
3WoXOTM+NcseOlfuk6XRWxG/NYATbdfWTgyrjuXkpZaPxNvC47Z6x5xYHhOXiGsd
Pkh/FI1O0G5Qt1+epatrQQqWqTegEvo3BqfQZg2AZJY/OKCWCNjBdPPBM/8PwpWm
5toWUjTHYbB/LpQdpZetnmkMfUgF0igvo/cGm9m/bR6dRWlBkoPqdQLjmhBlC3+L
6O22P1LGCyB4UJ8d5iCXKfEq5/C5HS3WGAtE7H+LKxpQ6IefpGYpc73RAXEQ2+ic
1p8EDQx1miAzRiXWrxhh0e1mwjV93QvruyzAq59sQwZ2wJuLbV52edCMuoaPLVEE
pIcOuRewwrG3aFH6hktwotXv4UIFgOuan/QOd9U9FcPb8hIbb7DuIul75ykgase2
oFjQVkEJJORWV4O8WC50zdS7reVLFFxNa/AGhnq0Jn7x8q8eTlvaSGYo2PmKByqr
ee4nJsC8MC6zNcZW4B1lDBySIanHKFaL4RJygFG75H+28Fj76I4nKJlRas3PPipv
o6BOQrWFrDfKDjV7G0Q+0ePfVTdCn2WvQXU3FAeO1B9n09AhhxupsSCtC25MhLk2
upkY5VgFxtFCDZsZ6KHgYhNTPUBzxr4kneM5xTNhWwfWBxJD1QpEdhBwoLEA3vFA
M0qtJUgW62s5O7yaxrwhdbRPFDMjW5Fb9nFMIckAiwcSuoNjtPYFoH0V38l4pSvR
ZBWBFc9GD/iYItmYJErpD0oe05w/2Dj5qEGtJ81rS/ySGwQ/fjXiXDuiwT9j0JxZ
TEuWVKqXQYgZXndfIasGP1Ly/Qzs8r4eoHQiCihBA3wy8esfLJr8i95yyH1TOwH8
8vU2AjwLd/JBQManxxk4iWmrmhQxO6WLtmW353MUTfiDxxJarkZC8LGJhhlzcVMd
bKzFMPcC5tS1WcAlqQ4GHVTpU4AQN+UYuSgEch2yQt/iiEve0ajpvmviZZJ9bWqF
QLn86cAqfKbA5nfF32xdqpqQcPtvQlPUZ2n59k5fb/M19bIGQsLm9D0VhVgIsgWC
z6+dW5b9sYFvf4GEXI2mdFn93kA7WwkEke1NHRCxVIs/U1mBArPff5XtmcO/Yz9t
T5wBpZsHJm9oEPSrozLpdGIyxpFK5CroiGZj6Ti6IIVzaWFaSWBZM1tkhp2uDHaQ
WLpeClAT7z91kZvE0+7etLg38nY0l5A7eT/5IwqLtkJHKrAP1Sd71jzkdkLvKter
bTt8/keH3XiLm+YBaM5NTSr7IIUJ42XU1KTRtUn1Ay+Q/8qqVyS9YWDEJJcMQww2
O5eq9R9PvqOgL3wxqkxFMmvcRPg23C33AbRORNQL47iCdS7cgOWR1zuWsc09YG8y
fJd5U9DBqnsVld/1LJOGOzLSnGDCuPRyK7e+y4pcWHkmcPbUNUOG+Xr5uuxf7huW
aLSHJXr0hhR0rCd3pwx36Jwzc97gEba6u5Rdufd1tiP70LempiprN1J9nX61ScQ5
iyzO39mNXXsAcQaMXc3kJm0t5qz8gRtKFnWU/g+3PbMGVNaf8STsTjpZbNMO8Ogo
iZrFj73DTI8Uip2IUV8vG3Qi1EObg/d50pLb2Ge5a7mACzO+LxIdq7XXQsoUU6k7
vT5EyyHoWAmZ+yf5hVas3joHS3i5sc0vDjOgO+p226WRimLVhrKK0Z2Q1ONhcJ8I
k+eRK9WcO6P2Xy56xBOfV8lOzUnCtYDfBu3R6UemeaOhcOE2rR0X2VQawDimmra7
dAttPupQiGsqYrMIeXvv8npdRjyNaj6X7PAiLC5JaItaZNIfT6zCEv1P6/VDGpOd
q6NRWJI2ru7hb8ft8leKq4ywC1B7jWBnR1A6quSJBoEyZopy5n0Syi7YLUZHfe90
pAxz2dZiK3cSSEM7eJkQ4rbgRD2n47OIoH30X8n/j2Hw5VAzkCdAFmiy9n8uQKg+
ZQTsAo1R2izuFV3MtuC4dCaeQ8fh2LXcJoZ18ECwr8OWk8CicxgYJnjxJZoeTBm6
sW8Bji5Kj5dF3f8qBvsL7UP/dE2BvDqBeKdft2VvAfkAsHa9pdVuJBjjYGDLBwpA
Rdn40xxHLlmTIYM0tGvpRNo3aOEZGPxGOo0CoUSCq/vl6tV0VYTc8pXUWdvMa8b9
Um+q+Lt9Ajfi4w4odVnL3GtdH1rMtGsltwcD5S4TOnG/YvM54Hy/wJBnaWkfPYo2
X4+oCPFormOVv0/XicFETtCtxbZY+VXoiN+ez0tZIyffQwOva32HS+QALcRRzDQq
cc14JeYswcUF3vGLCjTu4Ak26RoGbVxlGvudfJvyNOILJHkYSOJrdCXqdaLHLy5M
DUYe1NYw5wRMRNFCkqSqfJGEM8O7C36Bk7XHXSuijuU0i8tKf9YLQgfDLZNo1UmX
wtpa5qHAIeOjevo8u+9/fKkihfGkJptBlJo+ege6UZZ/0axtai4uI2aoveV1nFyV
BD8+SVAScAxFvVxOUm1MDSr266OeIBC48uEmKSG9jitIEsmQWNJ9iYudoVDVGzuj
d6WlgI/qp5gAEdJaoUaHPFWMaCV6PzucO31KT6s7W+2ifRkyCRBmtc2nIbCq64o9
Z7oqtKWu5cUkxERpmIRnzQs4u3+buKjqwQnSCnOCxMqMOrcQDl33Hqn+n7ZuX6WL
pJRF/rlVRtrGcXO3gAcQI5TiD5EnxuzBtZVuA1BbALOdAgwr9/UX8o7W9yy5LOx7
WWLdQOsuciomxr2ZLLk7Jg==
`pragma protect end_protected
