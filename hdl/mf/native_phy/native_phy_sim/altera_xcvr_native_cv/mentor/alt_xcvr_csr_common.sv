// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:38:59 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
K7IuL5GIIRjgvbT9h8epLb6dbvHG3gxX5lS28bpyeYVUcNXpb6QU/w36QPf+eSvV
0/OE2ejftqFFNEd3SeRsA8NsAUIs/7+4OhrFeJ7JKLmMB5gKcI/jUyosFgY64/D1
VNldinX6miqgTeURFpZvcgwke5lclocZyGTvB6rCWsA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 13344)
kVjt/wUR3Sdj9YkKj1ctSp64XqHL7w34k9+HeynvfEadwxHVPNjsPCI7gf4b2Iim
08xwYy4B/1karwpmMhdeWiRe5LF2FMYUosLHg2C8kCg6Ex/5Iua/rXlJvRvppfu2
OWXTSQIdzPEYDj/WDuLwbSsdJeqfgi4BNXQcf2GclISW67WgAauMP5hDLpgmTFjG
nub07uZa/qWUjVPGHgPsUWXeOrcp8+u4j3M6Jv0WnSri3KWjHTRvDYoXH6LdlLgY
UuceQHLCrW56CxJDi8D92QfR+krmtDrWta3HBV2vKTj3WCH/cxxD2oSU8xY2Jjlt
D+YvNj3+xpH8q8GPS8c1cpEwdJIxB19xqe9EWQF7bO+/ZqxA3u7P9OnoTsMY9jZi
TBjiCdCoC8RO3qvsKgVmAuGfM0Ka1HmRWUPjS+knZ/rUlyENJY/q4bedFsDJDokj
zfPaDZetd4P6Ti7KMz8WMnOdyGmNCVUNmTxndAHz3xvPnWCANtMa/XNNmg/YSFyT
XgR7jyfIWC71u9yKPXHkUd4he/1U2cOuuCkrxOd68H5P+zenTar2I2S83d8tX44O
rZE/h6F/LtA63EFWBJCfcsmIVBHeewE4QULuQoJBg31vg2DZY7da8n3GpFvV4z0J
v4sNeQy7IBINu+/szUgqrDeDRxz4DkoRIh3X4HkM8qnblvoQlRwKxr3q1B9QY5eu
TyKnzHUpDDiGYkVYfe027e04JyJ4YfJErwUEylmR8NRm2/TFlcUzkb2yhwfHDxE+
iq7ZDa+ooU4ivCjmTHBDzdx7HA8ZGEj4M9UkTTGbxcIG2fzf6K0oNae0ZHqesxVN
6RvJ6iMz14imcO72BWjhL6U7UrLh3mHHT9oPP/Izm8BlRQFJLCGuwSaYtDDxgl1k
Upq6/GZ9OceEbzUcKy98EqLinLI3lpWj+X9fXweRfKEfEUNwcw6SkGGbZRboTDip
L6gMU5iADpt814pzjFvTtWrNAzPup9/x+aF4XpebP7Uia7zGHnVH81peb2VPR4z5
TUMZBloT/PIpR1isPotODQJIOJ3y/dBaxcFHt5x79YAWdZ1dwKBuD3ZSSpqfefEt
DtuBn8yLuE2SpBprM0LPkPdH/sqEqA2Yejmf3+nGBTPWXHj2ZUkhRHTQGFY4ZcnB
mkMF2M8sJbeHBLT3lCl7fmRelXJ9SkhIjYAgq/GP30D0Apivknhbj4R2cdTw0avI
CYQmTk9MvSM3zByB5dfRHxtZX/tjghRT4RgjEAyD14MCVRPQ8YUNs5cmBaS+T06H
MrYgK4W++xRf9wlCoqlftF5DioS6QgOddkpZMp+V+G36emMWTnDxXAqh/5BK9bxJ
uipLBvggy+PS915xaWPx4aI8E45Wxp3v9DoUMjAmNBjBklYOoMg0bjRkp0bLWRnu
sBZmXhkAoDJJU5hfL7pFWFEA1aZduvBIBSkeWxzQrG+jbnd6ksxcRZW79ehA3Aq4
LBvjwtZlrEJXEBX+aRSKpd8kH6qyl+jOn51/OVsxyb7iLQFv9gUwxbigd5ilkhLu
EALYO0s2YRxAY/XawKs2sYe4oy3MDrp7gDS9phSFhaNjXM2BMlrfuzg45BfWPOKM
bjWhXkY+tHesqE1gFmXLwrEN03fE7TMnj4JNJmAc8tyJKro3CSgSvAfIN4ZIzuy9
c0LKIvstHaCbU5luH4DPfCmPhpTLQLy7VfBVvTti3muVH9l1kM6ebYHoxm8MOXU4
BOSUq4rJ3UYmHppfb/YOQJDnlaUS+3U0ZAjL1VhRYa+gnSSS8+za9dvGIvcByLdE
EUx5rHBrj/j7tGkLa/1FgLmb1eB/c+gTY7uF84LLjNVnj7YPxNNUT0PL2SWEqXy8
cd8LzXtpS6fX1nGHtxVnI8WPsmz/VJZvuIAH0/wK7JiBIm9/p9grKEeVzXsq8ufK
0LQDAJt5XGBbxyN8CdteZrm9a0/vRxCAWr+cl2cn2W9zEUxDwFEtUrIw3KME6hii
SDtf8XTz7qX45vlYedxMjObzyrWZTRjorKJx1ynwUReTcc2z/MJ/pHNEFghAp7M9
MMgWXtrkn4wsCPk+q0OnjY4IFNhY5qCIIaHitxZxxuqhtLw5z6vI/sUqWYDbGRmN
/Nq2wdpKFAVAqE7BjhhUPW5TwziGi3gAHXTiU6TCorrkHjPjw+Y1dIOMgEBl1TS/
SedTMsFAdaLxlv3ae7iL2VNso9L0kztLw1KKRpyz5Ucew1KMOdSzZKfuD/sZ0CHk
fHJvA3iYj1/br8DgtzL0UMmK7sJRxj1Jbc/1CMTPd0KCfjkC7khZhatI6t9y0USK
3I7lbyvCKex83foGo9npoILvHwGFFzztDcFSapbGwklZ3uUOdVEW6/k3CPTFkxUd
88g/Uv1VlVZFie+qblmpCDQp0cp4au4WzqWGXo6CUP1vvnIFFUIUeZSQ8BCHplTi
c7YKK5Y5OQro4I7Ul36+F7ar0dguD7TZemMrQFAxEDoTjRF/r49tVtsmY78RyYrv
j8ecy51iD3C7uzNOarNDuMfZRUcgk0VoyKwM2xm65SQpU18oK2T7qF+FEUOGwyZ0
3L6J7xalIHZ8BOZFjoWk5u2GP6XeQqiyFTRSbyuf5CzssDlefEdU6CL+yhO+B8Ny
Nm4lIJeX3rLuFmCFUvEnudKcjnrUbNa0hIJ9KFI/o1Pyb7K4R8SCoiJRf+T2CdAk
XiE0Qn8/Br0wCwK3FRwgqdYL27/2cVr2y2/qkZnvpzzNO9vTZaqkHI81UA/akXPQ
0Chxr/OSNInziOchclR8I3r77Bn7pW7EXkOdE3KWlG7s27B1fLBhtMpvz43xxKlL
pz7/X737EKrq7FmVCdTw19XOjpufPS+VSscdabBBJnAPzfXWpIyQCg4QpfuMONvY
BBiI/3lbLqpxQRTav+cu5hNpA8hvfMOoipN+dI2BNhwXN0Y6VWcZdULg0RvJgxVp
igAtdRfsgPUUzhrUGl2Z0jvd6+XPVkDqaLWCj2z2gajGoLy8NUkyM2qsK+X5ekPH
52FFDooaiKkptzbJxF0YguNxMt1W2yixAWAlo7Ar3mmw6VJp8l1wlszQMBJEFGW4
CUU6H+RF2O8VDe4Z1uhSEHyWkOCjIEeFffNd7w/ItMO3OaaGTIqYJL1e+hU6PeRH
bo/+gxR+wKWojxf9oIzmwIJvmGHW9hndUJHw0VZ5N2r0g668l2Rgqvknu+u1hyG4
8q+zR9zO+lPLjumWlZGLiR7EVmzVVIK7d5MWhLOz2F7JyUVLReVxtsI53hA0SzgM
0uTb/BH9L1Hgjo2Ymgt3FLtyv0rlrUD3gWbllQxO3t0c4uWYhk3WAZd1gPMnfJKV
BQEXjchjhoVcOFMRGzhY40cHcvnvU6SCJsgaOqytdi7wxdX0iQu0m0fNkbvDQCEP
bC5ttn2vc3GFOy8sj/2z8kpIvnq/LpLJLUam6AhOB9J/V4baqwG1V6EIy/hLti0f
DWpKfWruq4dEDYjrzX3st5d2BvPTpIexQL+ayyhijxGMTz6ZiN1cbYsSrwK5SttG
FibEE2OxH7ES/L8zzwuHgJ3kgNgBfzJGTguk94uikrLxdKVqxwzV3wL6yobHPGvG
mFm6aJDBesgE9aNdwTsTmtCNw5Tc2Fg7fwguCEEa7ZtnYrgFYGN0BQd6QdfyM8i6
oaEuiF0tZI477uvi+hc9zq8+jBoK73TxqpnXQDPuH2YH7H72jgGhf9q0awFbAVgH
LiASGiI55Pd/9/gmUATIH5izwzb7uvXrg0QOvfhc2llZXjO1eDJC4QXJQ9Q3IEYR
NXVlBXiuyiiGSWltIVII5M1RyExz2ujV+jRz1rDCDM3pNa0g8NkjXXsO5uufMoF1
7NnHmHWFi5606Q2ITan9HWG82uwvGDeqlzsZLgb4ApvERxHHv9f/E+NLZQRNLvhW
g3HwPatnBhRhDrNacouljm73cMYUdSzABGEzVnjwV7iI6413EICV0QTphBWJddNR
rKOgHLt8Y+jqR1EcKL4ZkX5pAXIw7r/6x95+LaxoJXzjpaO8D8FmLheKT2jkpBWe
p5B1TqNxAj47v2Yowz/UtAAj6k0ElYHJ7Z7wzCjnkP2ApnpElDwysWTCk9/Ulc97
JxGigBoAgnKQ3SgJ9My+RkZ0Sc1Q5lOzTJfGmDftbUH2ePqLj9XRE5MzmYY5QGjR
rVQHYQlcrfZv0k9+Vi+4BnSVOJVszvFfMdpTaSu9ob0jJNJA0imoHSHgcn4v8Qi4
6rEYwovgXjgZu6IQjzl99Xgm3yB7reJcFNgr1jtuu443bjFK0JAQ38eBwjHSwb1z
5PTEfQHfFhL3qo5DF9+dwF/ASA6X1KYHY+rzK0gir9Gm4GnglHZFptcGpNhlGRva
VAR2L461bK2SA7Iq3wRurmUymTCWQcjgvO2RIKo0To8Q8u/ct8x2Zq39L6FaMhSZ
vJlHFyIJkhrSCxrmgwlK2vRoYcCm4P1eDGagDa2y5u4QK5QSGD1GMvLkX8c/znRe
Tyznne/g7Nj+r3Z9SZ//kH25hQXH+m46+viMhtQqVhqnCn0yM5tWKmrmJreYDKN3
b1U59hz7NaWI4BdLT4VX/OI4Z4rJkgnlgzVNSDz6guXpgyrLklJYyuS72QXt1ZB/
BbEOoG900N0UZwcJAVSm+m+RxCojrs4YM9PeAj3pyvmI4ffBGpsPvR5sA409Y4wL
LElFYo2tAoHwjs7Ajfcru8QfEnQ7rYF9clwqMipDPJGtg6ZUx+p2LvDAzgt5fVYi
NASPBNtiaOEBiCmI0+iTJ3v+jAzP/BD8xE9nmNeIyr7cLCz5K2UwgNPHLcstL/JK
+fSJC2AAXR2wT/UlttWj0JQKockOoUzjvtXio37/nS5W8d7c6u712vsX7cTMvUto
j5/FoelSJmBCDF6Hxel46jy1UcRCWELSL4Pe3EJaze+9UWXrbIZkYdLT5BBFBGI+
bH/SHs8iK1dMpCKXbQbVMGYa+Nk237hxC+3eLOwCH6olwgG29ewFjCa21bYTVh+d
1TLHZEjG9HnbAQcJMLLVQ+Bg8LBfYnYrJTuid73qbvNozsiweBmU1qxkI3vZlJ6t
mgVIr8dP+od9psGJtz2GPnwOJVXe6L8BIznga6OXNMdsLLCz6jwRXkJF32tkRVoe
Af+aempeVjsV6B+jgEcOXoHl17jwnmBB7h0XS1hoHuTiFOqOALb1EyAPAU+9WU5i
HsTUbF03gR+OICHE5nkZfWV03Y9ua3A6X/otzoG8DjkcDHeU9nik/Viiu2DFHb/N
gfSLC3h0YW7JcSECnZ4Ex/iv+T+yfEDokU32aiEJuMqHwkA9eNWEc6STiLYJGtfa
zaB2ErLOsOb61XBujg4dU6Q6tBZQrV/POK4nTLkZNZGEaDHhh+KYs5PP0XMgKbrB
NIGXV+WrKb6ta8TNkYRCj8wfxp9z8HIJ6/qOVbfJ8Zpn+pO7NP0MNN4Md2As3BGq
LUA8Pj8yQp1XVwfAbZDriCNnFigb2OWwer2+vuUOoaSAXSKVxFR+oBBN6iLeye0w
hUCiTw46Fay7F1nzeh6RuJLEi4MNcfWBIW1iqgwmaBXmSXgWoW+RbNMfuLW2QV3y
BlAY8RopAuQ7GoYA6T0edap704x2zf9iPePERQCWBuF5Uie5RsZ9WfxNUkYwr7q0
hwbMTiRMg3+NXjGo6hyjSYT4Zd5NaShPkqdVzL8RpovalhkQYswixkqzYyeVBasE
KpJHV8nlg5Rs2w/oEhz9WZ08GGZs/PaowCFdV8c+n3SkeRyPVpBuyLhj3E9meDIH
FfrB4As0DWGD5j7BMUpRfPzpJaryFJBhPR3mPnenDSLUBQVLhJ1qdaXF8pMezuGP
tu1Fodc9xwS4c7JfEfAYb2LamFMZTWm0mhAXqvOewJD5aYjski+8oPPeIc4qp6s8
C8DydjP6b9JZcskn9VXv4SWcNSSp0rao4vdkMcmYfkLDr6h5HopyEUWG7QNCHIx5
wyQuxipUqdp6QJiZXL8nqixUNasCoBcMOLIJ6XkyRmoBNh3fM58gvt1oJ2/XluOG
TylwNO5gNQTTFKC3+tc35EKwRGO0hEuuQ8jhe4jeQl/FuFBvFq0pCx/9jdXLi7iT
lOcml9wPjlMOvFfNetnUXIEdBhyF2zCcTW+B+E5lDYTW7Ho6ThQ1SvFHX0F9XwKb
6PqSTTrugGgBtou7jPSixfKyFJm7HzMX8tOevNUMWg8PoMbPdXRn7dINNlQWbYUC
p+Rv7M133z0W4nPq5prBEXj9BiFdmeVNdjqoqsn+m//99FMOLZ1cRPB3qe9ViNij
O1MdejJkoS1RNZeV4gPBf+nISFEIiih+r/i5VSZxIg0wqIcY3KD0JWulzPd+IVu5
lwD2y8EyB080rEYDpwoTZCzq3gzc8eirEzQMc3HsUSQhNrcRT9FqZm32jGXFn4tk
PVhUlDAlgdMkpRc8kDc+Y9FtRNsW2S2SfDLLQlB6NalusDIWO4qvHbUPLbuUBjQC
pCJRJn0FmrFRV6w27NZt3eFdNo8TyQPZHGYX/3Z5lx2aJApPWDNqwkkrRxUfsnk4
VWIlD/1a8cmM6vFlovZ1i8026chHMm3Ed2+fy6qoNctJfsYglQ0xlfa1SSjMhvCw
56p7OjNKGv4yWU+wB9X38rCmVIcLlpeO0U8WKQ+woHoy0LDGiYoAuGecIWRVxgAR
GR6zAH1/FelMMfAQPpai75xwHiomIkCs/wcWDHVurvoYx1SJdXKl0SR8+kdLeQSB
ovk7yw/tKXpKMd6+l6AH28+yex6UHmznBlatLRyXF9O6GHBV3hW6DhBlhH7rQlWZ
hCSbc8OwdpoKvrN778bHyp9ixVNjuWzKgl4gTPS414NtOxe5XjqFzqfeYVmZKt87
Qr+iEyCmpbd9QQcz/ML/eyDXvXAnG9U6AAeuwvlHCdg+SL/cz6Yyma69cQbfzZFs
D0l5ns0Gb3CgZqQfCoIlNevh1T37ZTwl5/qyL2hUfl6B/5aD7pyAxNTKMbxwGRJQ
7Q+7CR5f0Vc+YtWdNlkYOxKyKYpd3OUfOgkbaYJRg6fvVEBAcCFUn02W4r9THw95
V5xP/pGBM9ARiOn+7NMW+lQMPbkIJO7BbU9etaVsPK07upZp4AoFf3x4HAbJZf/1
EGHWDqMEBYYDQpwB5vlelWmoryHTObFSGvP+3Swgdb7ogyYbs+vWze2KvJqIgFFl
GRVI4MoYOW8bXhxS4FPO5WJeCbeeDtNs74Ty8IxzL1CVa07YjkzHsp1BMeO0Stwk
xU6P7r4IbnlbFP3kknD97orDPE+r1mpLtj5rMAhxElkZLIO9Ur9K8JWr76FxjBUR
nZUHdu8RDL+skqdMw5dghX4HOVz2TIKZI7+wZMFav/HkdzRvD9pltip+f2nQv/Fy
MzauK6MLfaTVwhYvD+y6TSY64d3AUQzxb403kBIs9iy5Li1SDH+xYHR7UYGHJ1nI
kcqRC3pxb8SlVfjYxTokWJF41ZqhoCZ09lMAYBggYI5pnLojBnO57vwhA7B25Tzr
f3wHTdh+9N0U0Pd7F9seVRLLilV6loFe1uuRmUcxGTrO/v2b/ilUMlAiq83+dk2m
hpBib9yN7n3E19NI1IODRohEFI5Z9PK+zAflGCcSQNZs0TjyB+N+IOeTOc3Ph4d6
Nl7BcQSVEpvQj0XlQypBL/DP3zYuTBD/xUHERfkcDU8NY2cHlbpcRyP+E//JponU
K4UmfLzDuGAmAkaSb3gUShPIyMXiHvrCLrcdpcOF7RVIzzRai+kzObo5ywIvweq0
QSmnPUYBwRmibHJICUV+DbIEOKCqJqnJFbJ7TVTeHq8CFiI7hroPCUd2YSSlP9dC
YG5fq80vAaI55c9j0VXWolB2T/HD4WVA/s5ClZQ91RT3DD8YeKeyj2WnZG81uPir
pBsrS3Erl02B2H3Tk8RZ6jUHmv4AcUjfpEMSw/p4zyZ0u0/GLEGvV7PkArEvMGEc
/4Zgzh2MkwtU4inooPymaOndfp7StCUAcIUQpqxLDFgvyyoM2ceRK86f2CBew7YE
Fo+9nHzBGxZLil+ed5Mi0o80SmkcnM8XJKYQUJxBlK4/CJ7Vgy/CYsjKTmexqyY1
8SY4g8AIWJEFvILrYxwFV09F8BI9zQXkalk1BkK4RAUeFelvg1jq5l1h8ca0BAHU
d/UccG/NAITKO5jI2yk9eTLWRfJqxbw5NNW07Glm0OsLXe5pnV/b3y5mCgqGQFKN
CLBNKbVhm8hCF89s1BhMsuepncD5M3VXsVFuecnDUPtOy9Z30LQbqjEOSeDWsFd9
Ywfy7K2dqIK/xA3CF0sq5fjXFs89okAYrv+HLrhs5rP3pZkfN/CxUys9bkAjWHDQ
qWoU/sFrFfrRoVW3NA7RYIcIUTGFp+Z55QVM1tjI4/t/UM59HxWnHuX2u37J+qMD
wkVe0YnCk6/KyNPz+j1iIyWdV+jRR5GpenX37bcAgl7/JQC56CkJ+Vk5EbOo8MfY
An/OnW4CsS70URpTl7KwtHXRaYigwE7UmAIBrEtkIKZK3U4aV1mJ9d9+aOG3vVUc
FqNj2AWRCu47F9i8w9A+VoR23wOfY1flPwrmutIJ8+tcs/+aoAshsHfwDAFwF9x9
QEWVlnFLjLC4BKC+Q8QetlZ9nMG5n0dRG6djrDs+WyUriTry0dZCxj7iykOyERJE
cXSICiZcjs/8sIrBwpa/ZLLDr6P4mpC7oZ/ip1UAt9Fzq97TP4nKR98lGduhrnVi
of629Lid5UmoKb/bulmFSgvNciGrC2aE1Fm7Oz+Fw65rtGg2UEk69mkCKrnzJOvR
WBvE3TMN6/3bfIdYlSQCqYdxr3kj7Ss0EGANCsrHO7gABu5gP9RIpNWgTHU6w+7B
VNn18M85D9tlFHrtKty8VuZHzK8hJhcMmrR/UbRrCMa8IEZv/M1GxroFnKD5qHeD
uOIlgIxbNzkTrn7wq9nraZsIxDs0O4jmGYK10Bzwmq7uPzdoH+NpRGlzUN2QbRj/
D8dgIf4mWObOKq1eT1WiFB5KxCpkTsGJ4n77otHTJpUFsZw3zKAp8z9BUwoymndO
YdBJlygM1MUbXeM4vDbwCQv+yfyBTDaB8p875Nin+V6EwxCYHSB5jhJaSFqM7nqy
yCRtcGAkbzASla+QJ5e3RmEMBHFlj51dNc+aGb/5zpPDAGDaL3hmdUqtDUoAnBne
EA/tW+Q0S9t9Xg6yAjrAibbdGXdBQwtT/xPT+30OAvoZaBGIm1ZPcqpQaDM3bkgi
xr6R/1hrzz9mq5NSqXLA8KebOPmPfTZyjCcKBxxE9ItzOQydLv/aLvg5BBZyRu0Q
x17pCOquh7YGq4nUUn30yKY2ZgrtnjpESFutyCz4ATabs5XPtnVhLD0ZGsM9ngEs
8eYZl8WdeXpKgx+D/WbZuqlL9wpWIyGktX5t2DmoToDy62PkBh04lxVBMxu5/cO9
vsqMLnMxxIgKnZ6tbwMfhp5ddoHAk05sKlIwPnLM3W+FNFBYgIGs+JEZZBNCSqxa
8zrHKP2Lgb1vypPoVOB8CaRDXaj86TrKP5LpsPysiuKcd5dRM5hmTWw67s/65xqN
St3wBa71DYNOBPFbm6XteJC1E5j28o7L3W62KaPbotbwT6Zp7ia1hdHYASPWTp19
mOGD+X1cw9kuzEgwW4Im2CGf8XPlyIAa8kTwKVarjHBU0jeK+sP+lmvFbUAaikM6
DbiFYMvBJvRk+WDmLDV7AuRcYA+xs/BlzPklXDubjOakAeIg0eyI5zxjl5p0Mm+o
cYjeAN3Kl/YNXCYTHNK88eVS2x4jpfRAhXZoy7MK8uymODEq6MSejiDHt1YtXw6Z
rKMqys+EyoCKkbxAj6Qhw/wb9g+BeRIis4V/3jsR2jOI8MH2JJcBJNF0W//ya9nS
tunIZFqFiNTSdiMbTKlgGbwqDZgGjP/JVb5v9SUcjUqCXztMX2c2yu3E8BI0TOe2
ptNSdLjYLooEZ5wkb0Q7Ekql7wUo2FpCn22qg5YB97gOPbXm/Q/IL8WNmltyECiS
FioTaZhfTSYtMeKcsiwYPArJhJwdzOb9Ci3FIWDlYn13Qyegg2fkiMaQK7PRJaPs
zyyfkYE6XK69wBTINbDq5RG2ZszR6YEPkxS+5s/2Yk4RbQfumvFnj/IjwBahiaG0
5el2D+P/dWax1BqmL8otmrKWqVEqQIh++r/OQRyjeJXTd6WyuW8sg9YdZv7cje3Y
8BjoSUL4697SWkAaPUTJuUbxaBTiOFF1MFAFBZ1+w753WzFrlen1blvGGxJIeZr5
HaTDS/18zEuW33DV5YOqfJAPUO07CPmXhiV0YMQtCAbe7PLeUlskWze8sOlXMPMk
xdr2x6W+dRSA8BfrwP+OXkjobBfB8eBH3kKSng0E+TFDTWzmr6feRbYsAcPUgb2U
rZvKXa178D35DaVcoBXj92lFyiuXkDHoCy4kD+IvuBLJzMCtEL/A/xWVDxw35y5E
PWUTwli9Bg7V3eshcobEUd6eswPMNA0mZ6sk0Uozo9RHqG6YHNuhTg+t4Nb9ZPeC
XWaZQUTSK2HucGzQbzNHbkOxrb30K9rzDulsPpjAis7eQAlQcM02K3UTPKpGGmLq
2QZwCqO8TjXURU3f6hhNf0Wqz3Xw6ab+1QoPJ33gxeAQUVvIS6YoxQ8gMfE2oGPp
U1kvEcsTl6kYco3Ftz5uVSf2sPsJm56EjoJMA3iFmjY7czCMRTExm/xq0NXm+B8g
BNeF4G7rrViqtpimUlcZXYreWnj2Ywsk8qj2hfryykggleQgNotvsFVkUvDIYltx
HdHYWH8dbxGwceTLWqWzbJYncH4UdNtF+c3IbHPWEsGe0jdq2eLeN/6YbLYZGWhQ
LotZqENRkV55rfBlIc1Hy7PTyLcgxUaFW1Z0xqxvtysH8WM+gJHJ4Of0HVe3WkR7
J0AVuJb/juLaIDTGs0JugiOvH0EekTFMmhOiXmBp4SzmwCCKbD/RNy+Pf+X6f5P5
3bQr55+U/zF8y9MnWGliEK0hRGSutt3P0kAy/rPTGbsjhOsljxFNNY9AAdTBLnSC
LDbiV0b+gL8qVLv9l1tFpAs3BsXgFixrj5pEf9DRlHKMcbtEYgHmPTRzVCRGc1w3
hQJK4xnX3S1kdSBO26tcs/rkXKvCNXTc2Heu7NK/QdwxyLltRWH8FcDeBMwrOgzL
lEEFfzP1Q/ki3oE8opfdDVjiUN2d9IW9MzYyYi0CaPdrq5pE2ShKVl/YJs2XhUoO
W/fgBRUox4PHXVPkqTp9SqfVdJQNXfz/kkOEDqwPVuvIFoJt4JP38rzUXa4dEvPi
POV/5+kYtnTXYT7Y1jDnKbjJUZ1BlTd4K803gaypU/DUq+tQOYGyE4yq2ai8lYT8
hL7G4RJ0d4P4ze8EkVcZuGUWRpsC2WTFzf8aGmouXGpYWauDExHQYWkCQ+UMCX4z
9TlEFphzJkwFrtXuL7QISqvdgC334f5Kjg4jKWz1UHqFQEAQZSmqcrcypInYcYAj
m5P15D2MfVaKaC22+whf0W9EAmQN3dRyryTQtc77mtgfakIg4CfhEL28bpgaPg+4
Y3whEuroAYG+rigV5P69kuDidC8bSB8U2a9UXpKpVEK5SBVu3cP1DDv8bqLtis0l
rVbWO3VF1W4GLQMJw/Io8I0a9zS0WVPq7TbIF+Db30FsCiBDlqczYRgCE4gQGLbe
oY4kwdM9PtgBlVAKkOvQaBak2fq9vJ/XEut6204ZWSQzNizVnXxSCYxhk8HRBvuz
nr0g9EpT7C06d3zfzztE2mM/y2X7ihjnFkPHLwJXXMCNOR1OVa0M/neUF+YDHIH3
WA7Y48lT3npqgoPnv5fUZMc0VFJ1dBFSlR3mgkscAnkwCE1nV9UX3ilzwJPL5+yG
IujC9TC3an1scJlPkNNGWZqPGUj9IlPigc8K7VExqHPwcNsBtl5ll7ew++v14Alg
GPd7qR9TM5TmiXG6y891ttxgvVa8JDjl47o/1ygaaiGzlFhstjQ4uIoG+QCJnUlm
nXMxjZvaz8s3TZ40Gwb5GU0jREBwwwxUwJz/D/stcBtoLJiF7PZ22WU1X5hqS6Lu
8XxsLGVAKburaJyivV/Hw20W7Kx9Tz2/JZeAsL+QOzRmnHeFk7dEr+t9KDaexDPs
LPcFKmwefV9flZZq55wHuj6ctRNSCIi6T/h/YIdeYLtvAgVtpkdGIuh2nnM6CAIq
bLAo6CwORyJXoL4refcR8DHCVHAQG+xxJadn9Py9fL3PFz4Lz3QCt9NZY1xJkm3j
0HQPehePaSbYiURkcPB8EdSKk5nXWBjh/Lm1LHABp1X0qU0GI9AUrTsuWoCIS/Ru
bgeawaqkvmMVejYRW4KdICcMrKqqERMsPlJyyVJsZEmWFHP1vTi52MGm1JlVPYdZ
MwPX5yoC2BjtmbxlOUMRRfij18v24rkaqbWs3Fihe59RG5asFTYmwa4j5GSNj5TZ
+0+lRT4fpq+/REUFlktBw6Ib76TxdwM+FbtpLwyh/L10hVqB+6/XP2jt29l2Atkq
NDD5/P3yN7eaL3x1HEZtz7NTofRKeYSP9ru2wQxAv+IaoLpX/UhmBJd/U/pr9emb
9LiAi7T9kT9f3RI1Zr1eVSBCGb11nfFqrQ9W+HPs22QQLjXF95zpX4b9rizROYA5
ADwy6GlwJusxqvYZphksCocCZNbK+VwolgzhKTO0ruas+fNZmtn0dBxwScWMjMCv
+vjLnFblL7y/OSAYwDaLvOWVVCx9pp/mRtNcmooAjlvdNW3QODpqB2NxpdqxccbX
0MwKYZqR++MrrLnJ2joMHNpdsmubH3/GKLUcS4ifOZWng3GtwLHHrdoYHCad8Mee
m4UBNWZtF8m6xKIbLw7354/5zRyDHeJAIMpN0YEzECxs/w+oNncNSjES4fJppQVT
xl0WMvvXOR2MCDp+t40HEQvPC4TAPOt0jbRY+O5J53cYdmGIQgT0U77TQpNDH9kl
93y2YjONn1WeVV4QCc2RwzmrAPr9GChp902/1Eq+zOHSir3vTh8DqmMwDyW43exC
rUFerZqpIttqM2pMzOO2LnJRaaf4SFJd3/piqjxjOjM+wFT5Q6WYknSXXXppPQY6
GQW25x4OUUBSOLt7TXWmoW7Wy3pXmwLcyqe0eTeJRX5kX2PPEItTm+uK+w3uJlkN
BIZtbh5OWnH735Lt4HgEjx70JmpywgDpFTXBBRi2JKiW96KPXFTqKtOi4JwSq9QS
IW5Cu5NmPWxwz2N6SieC/SfmPr9syjUp6I6Yn1NLQFo+RMD999s62e2m+HYJ838X
hznkeWt/44JbWpHS3Jqi1I1x2ijArp7URC3po1W6A61JbMZXNLSsw2+gPd/MNh8o
SDFBDwFgJwe1HbKIK0NEoK/Ug4r8SSpS/26za7zHoQNkPGoqhHrmvYbDQ3HsT3Q3
4dN84DCmtOmpTQIV0OI/AA4Zb0iFPkH7EMRCptvpHJ88SIamAihcUEM74OzrHPdy
lt2SoWtsw8paf3qtk08DGXIbeLbTYqGyG2JS+PXQEM725PfIhGM2um0BF/tn0NiJ
onTC0flLOm/80BnSfGMfzUxLQuwI7W+hdCDCEwhY7kP+dnpC0inEzpdUKKjsRlch
fFecTwFw8yy71/G/IoRm5kXXTgsByDSIa/Cecq8itZzF6BrDJpZyf/upgMjphG1u
bBBzg0MB1VKP2XEFig4/nxg9oqI8MBFD3cyzxwkaUFa2d12sTCsnrHJT23xNcxQ4
WAkUQYigYTVoNx/OnCvcqAtf6vwypX9ICpRpGkKVwZUJJCBsBjD/n97fkDKzg6hM
1nFwvOLvzeIvmhh/8GDSU6+8CG2DvXnr3fIcvryqksyOHRM8P/Br2su3LSYBejhs
eDJ4HUoz9KrRYy6dzgVFrcmkcFVjZsMZ5XB3LjHa+JXdVw2utFEw+bHWPDmbDsdb
xP3kan/IX+LPpfwdyNZXQOnxBCivAepQDo25nJcrqSqnmJsAM5mi0p5Z9tDF9SmF
WD4O6Zm0ZZux+A3jQwggmwifNnheUKo5H8pxT2YnPc6rx3yp7PsQs9XXQsK8ysTB
6wIseJq4mEcRclpLbVQ2jD6v4fDnsgaN7/eIJswE+vlVRc8T0tws+AzSiksyxgug
wR6mibZNESRYpqA7q+OWj1qCOd1ycKQfYsSKAUIl8wHWXcAIXip/ZSy8ds9KC+TJ
MA2JE4RcijcdS70oQ4cUbBxAkPDXOWxaw/TYSOdU5X77YqRtrz1xopF4aFiHMiU3
l3s3iOnxb6JZKw88SIQOKguK5XNI7x/qWUw7Imhy3v4QWNyVReOpNP/14CK58djN
y9GxBI501+zS8grJMzmHKCl2YQt2svfr5RbmP6YiIH5hz3xh1ZbRPWQ7ZbVVmUpp
LV1Fs5hWzrV/AP/zDS6iDlCY/Co43oF2IAMHhfWF2IPfgPH8IXudz8ABKAcrM6Uw
ut9qkbhYpWEDL6cVM2KDNTTJtpVgHAr0h/Hg1fZBrGAzCHVtUP1AKNuMKRZfQzLs
ByKPQu7N8fkVtdUHJSBc8oqIo1vaxkDqwSuz2ZWRTZQk2/tclhn/u0hPALv9oDCi
rTIw9PCPk9FSboiQis+APT6kx9mdQ7YDEwZGnY4QyCY6UqvzhpvrrzszAHYTMIFt
VpGHXhA8Y8VQRPEqmLNcW5sdpfFM0WRPzThdN+fpKiSk8LG2rRMotzhisKBm7A2i
0QtwF0q8OPJAbAYkotzhHpS84JF9daZ+KjcDfWdtzolelB1IzJDk3GU6spw5P6gY
xI0opGj+BQJQyqDypPEl1f6qFfgyAjyTw7Gbka+ZjJ1Dhc8iZvBRB2ZeyBt9oq3j
AWRNqdyGkMsg2tFAO6cLcvTqEwI6MGJT7vQoKr3c69Wj+zLTyYwPkLIbiqW8oHuJ
zov4FNd9sTo2tZt9dIQj0piPfPBvYC/8LFlBCYPLiKeZ62DCEAOGTJZ6VTJHoP2U
sKI+s6LuhQUAZ3HfO09QxE4mcGuu+I+rdmctsKmYW1stZkrki7nN7yXriVrAS/xY
olX9Q9s4mzzntbtKV8OH/MUTpu3Nz9HvNbnnpcvLiy8yR0Hm4mECK0vbHw7jLr04
9d+LqknfgxcO/KaoFlQptVGuMKoRvgL+wkhRf2iJ/EpGX6CBrevuWqLKhVDIIvpw
JXs2frHO9cwUCWlQSsJ+uBAKg44Br37svFweS2F1IJ922n0+f1Li6vvWS879/Uem
gT4ukslrM20LGFJGynlqNuC6ld/4HMYO+aG9wR+FfUas6GCHC8yo/Fy6WwAWIxsc
I0TE0v+SFh7lLuphHRjON5ilZe4BEi53nGU2KcMErD7LbUGk9GDbMTqw86sdOL3Z
3Pi59sqXa5AlauhbkkJADVS3T53WWQbz/ptX2otM9zfmgkHMjxkt1LVlDLwbCxLU
9b/pwhJjoz9+JvawaptmSeoq6mjNz8n8xBitLw9plevI7mXFVegcvqFrqyKWEyQW
HVDsyiUXAVn6VwsGQC0lCyTAG/jlrV43R6J4LCChLMNJnHNz9HOw/8wNNvqe8Qx2
i+c8n4ydnXnNnpxbEjP/AW8g/zKX6zavyaLDeTf6kms2CyZTnHVtS1p5Lt3ZmvB3
PDtvHrizm20kBAxNtV/s7jdwvfMeBFYChjSmpstGrsHMHUOZmDMkDwS1QA8agSAe
6ivTAitQFabC6UXIqbiRB2GSE6rwgQei40ssAWgwhBLa+HdB8t5/1GNvtGOfOplj
C8b8duIBiQTzitNoPoN0g6XKwfbwXzTOKhVlFkeVtO17F+iTMPsKvkmg5UpiaI1d
Uhwc4kVQ4i7KtBzt+qHquvFqwZvVIjsJyZWNILN05eLxZenO8n/cd4iUBUKqpBOo
BluVehw/WxSNsN1aRW6KBWglkpLJqv6uo3L8VtoKJzyyA4RP1tAcP2ORuLiWcRCo
cuYl/XZCm+eZGnyEgs2PKYYugB6tEN/M6mqiSLfDB46U9xa0IbTAYJGr26yCzVXD
updXL5O/Cg8wTh6xvUciXF22ipSBW8KuSmNW/KQ/nYmbgBlgAeXEup21P71J6CLO
DACTKWuhBV+552tVI6kT/vTt5woIvth/mr9c9flU+tW92wkQk2zhoCevQTShhWNb
UwLGs5s0d9WiI2nEmw1zPMs4E+5IhrHIvHMFjS4apf3bYJhJhjalxiYvGwKUrSs+
b7wOfIWV3wAYGxKbPUTv7Uo6QQUcF8DMpg0NaKmuBLZxNpNFEV8jJs1re0NFUYti
cg+t+05Kvg/xxTeiC2sgMMQJT9KnSLjjLAEmGVjj+qeZJB7bsfrO6kyNHS33nakM
diM7Pf5aYvQcv+Tko9vB1fpys1KbnHHhmjCZmDZotryU4i7eAaEGiS2606Z49Zwk
F0deyyOhJv2vcfc061eM8m8KciI3jkQGYUPo+c99JLIqzs7ppEOhi0TZsn3mi8I7
j7R5Tsosm8NZl++M9uCXnjSkfNWqyLK3fSd/VlkOVR5laH2KXkIHBRCT7EhaSWv3
0I0Nchxwgagk0oLxKX3uK6PaPQUBfXiiWTzu7tvfxnPdpBA7VyyQM1h6hBjWDg+x
md/veOkmNgNSwbqQZPzngl0T9HBMJcKLdJP44Aezeq4A68ANBNISjDOB3baaHl7B
uNZ12aWVNJYdpiJFbDgY8IJXjGFEgm2CPZBQw8baT2b5j1jFe5smv6UWhgsM/xQL
2CWu9hZMLdjXteQsUCZoID5o+f1lFlSmEWxZsxPWcSCnEhOjfbg8v9AILKb0Hx+7
LcZUDIUnbqytrqAc6QU9e1STyjyqWj82amXswcHvPNDt9Wc7jvi7Ag6se0xV1U+d
hREA2RG1uVsh2Z8AFwOI8m/ce9q/QZ5wFqPL47PZGSLE7uAoEJfErmXzNlUme4k5
l6fLXeyINiF0DNT8OzCr80gz7azQuLVMQLW8dbTMOFw+GbGq5GrPCEgxwXaxSVfM
0tyaICFgwGO0nSrqRxPX6l+cajyMN8D8a7Fog8cmFF8HwtYie4ZAE4Rp5IaUxMqJ
MOddxN1rqGnVIXgdZsZ0zDAGAo4u6OzMt7Sh+OsowDHe7qUhkoR/cJZvv3c9A7Rz
ricrDMCZkJXt8Uk8TlAXszSklNrxnsqIg8tgDfon0cbU9cvQEEzTlX2eVvYMCewh
UAbgeiaIyssNpxAHTgjm6PTGrlWk9pbfsdJxwHJAlcgggGYoWrZ0WyeUPA8XfUow
cm4OR0N5L1Mc3vm8926Gfkqtq0fwdOrBMH9mX9v3m7Vt8mbRZnxnLLR8PMyc+K9h
Wa6qCw+tnmXtOLCDDgiYypkFL0DLjaC/s0GgLZT3NCRrqiMn3QzUg8QZsroaupQT
iTzijALn4xp4BMQMUaWRaMPWcQKfXZkg9CTNEpFMq846vslbPTp54798LUN3c8sr
M9uzauMhotvCznfwE3olR0j5wiEL3R4IVrkEERIEQHFLqW5vBejbWEL0jD07eiWZ
0vtkTdKiEJOyOD6TixtPukE1rclukP9kwHyZO//6kEsBwzmLj9V/2BwPYbe41bQ2
pQCVs6xtn00tZYEt9xEyXE8HNl0H9GA81zGtAvXIe1qBGdVSDeNGhS88I7MSXGnb
Dcxj5uXdfXYMi3pFHH3QNO1XBmepi+1jb48NcOcUIeywx5z7Sfhq4/J9u4oWTFvr
WtwQBOMuW0Kj0rkhbZn3+mSS1umq8eCYgIOj9MtqPdwMalS/S6Cg5Fckjmm3iZo/
ANgpYWLvF9YLxr1MzAh85O4YOZ9Z83prrfGd5c8IcY4Yk1ySPjJXgC9nMJOltzbc
oOHO5QXIstwIHyPgpplkhkUX1G5cZaFyCVCVz4ZxgNMvMU4/66lhkoc8jz/Ty25x
`pragma protect end_protected
