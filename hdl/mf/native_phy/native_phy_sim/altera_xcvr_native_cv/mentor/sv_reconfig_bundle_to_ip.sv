// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:39:03 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
N41tMYMoGjZY4AWt3ZE6UlthspxJd/d5HTR+gHWr9EKsl9kFr98n3i/ObUg/fVYz
0TmsKTPZnBl395c6T2whHcbFRK2M0qV2r2KNrqSG1OX2OkXPwWckd0y2qWAR4PgZ
H+0oy6WUhgvADHdmKb+7pWBy3jdsCbihQB5YNMRp4zM=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2288)
WCNgGUV517b0aloiN9LiobN87RTk+6Y/yyycxdOizpGD4BDB0f7Z/Ncsp11W0AVO
H3kpbyBOo+9j7K6akvLVIVYaB0Zr67dc1ojc7Gc7USwL1OhF1cOP22K71svjjuRB
1ZU7QtZdy3YZwtvVH4Y1qmgv63Cv/YQkQZLFID725cWI9r4pVwq9Za2CqQnERikw
Xv/sHlpPX4k0GBy0UDfGH8kOWD7ULDd6QLYd/ZEACLxwG58WIZHYESqaMEDsgYoj
zZng1NkSKdmDs2MvgrkiLaxNNacCxI/aWBXR5k3nHf5M0KAiHM62iI513Kmz2fxT
ACSijgdnArOTYlkQwrj5o4WVEdcuXd30G7IZW0Y81dTOhjD+YUGjy10Gx4rIVhs4
lYSOWeOgJUJkOSQo0RADO8CoWqqMaRRyTke3KoztWD4ncJKGGTZfIE+qnFrQFK69
xz2N70+z3ZY/FA5tL0i9FdKFLr7jvuYISmPsDjtc0yekzXi5qa0Rexqj0mgW6/kY
JfNrDi8PzFi95aA6c0K2d0940AbVtFfqfCkgOAQnm0Kfs8iUUH8mWbuIopF+WIdV
JGszgXXsHseuLhRqoZLjNtkTnKlLu/DCCxw0xdYeYXyCrSScxMpoFsWgrshz3u0u
nz9do51FPlB+Qg126R1x4AR/xHcagFChPk0bFshyjXCNlNOPbqXD9eiJclLPM4d+
oloK9qCBJkQWygYyOAwUDMOa91kORqLzlklxcObjX78632L8/PYsxnUppiPy9/50
vpH238iZJNePV161Pa49lQJQ0+anrhjPNfWmVpinWdR1+mL0RR/CxrWKBAAcAdhe
WlFtM1+61IaLyiQ2gTyTUEuuQVcoQ8f4eU79sfBo6yJiHdylYTrgYEN7Sv6/qNLt
txGI/mRz94l7/oXesnAUheSZAem1tGFdVAW3+KMSivHU3UdopHMZ8O50tOqz9jnW
TkEX5IVHGVvsfcq4W0Rc+Qa7g7NmbmOh26iFuM0xCrwrK+4R7t0es5g85Qe3xmMQ
Fz/QmUdfNNgXW2ZX/so8vj4uXfFTEQCrIAV5ChX0k1lRohMVP951gjvDL3ab4OzC
i50LoH6T/bLOHWmD6VxfBNuLx8vgicLmf1vBfy+s+ErNk/pKCL1Anu+ylHAhPw3c
4mZmGzs2CHmhA6/Pad0QLFoULkYenXzu9LkbsDviTttYGx2rNMqx0CmP0I97j5BS
v9rUpupMwSVCKEVlLEat/v0xhBQZhkoCzxWDg4o1BRy0ld294v+uf7j7z1tKwMwo
Z47aFGVqGW0WlkzI5+comioJ3dFePNXqFLAoG1Ilvd2AWYXCHvBL12D0pbpa1e8X
YqwdasUijxF4wihu2bxT1POOd8IGVPdTV4oXtSQn8mRx4Wv3wD4sX54gtHAUOtEH
z52HC7iynsRmsjP+s6fQdO8VFwvGdLkZp/e/seg4DeHdHR0zELegnvgw+xYfvo/A
2oEzi2v7uP19bOl85QD6cMtv5Ju7xXAFzzqBSl9hqZDBRc0n6fkU/YqWoo0LvqRR
RTZqzpCSXoDKBRrNIwFNIKxy0mjytcGQzN+HNbNvjkGtLIsOvL2baCu6zfz8swLJ
tgd2WBLuoNmQqAq+S9FHcLEklBJ7IlsFLsgxPv55m2wgf5+cRQBf1A/qtFNCcT9z
K1mHKYfpOIsntU+KqICgcdqB8JP1JeY71j1qHJxJ7CgmLTxWubCKOTafgP32P+mb
ya5g5eDdEoqECUEDMWLW0OkKs6JLauvayXg4aVkEhzctOfY3enAheVYH71Tef+9X
l7m+qNdVHvTiRQdz6r1o8iYdD20Q2278k3e/c4FwjEXL8F0MGhP2Xd+kmtY9lIXp
hAUOAwrYKmfTuhda9v9fJ1pAYOomD/f4+k5gKFjOcVvgWvix+qAvM/YvWWlISw3s
OhSW0qJRpqRyN9UsirnbJ/33saJnJLS11OTPeHugjDo5frQejVu5e8Z8ZtERjhxs
nwdrellyJa8wyCf/isiiJ48HIdcVmEnRzkwtw0yEUQaleeCRJFYfgv3P3Y4HT6pw
n+ryGpXreLYPT188O0SvF9ROH+gCZqa75GWzyqLU4IxrPzp0sL4wp1E16ZtsHY0E
Cz7C43v5wH5Ti3suq24jqio3iuiC9AqfQexOwkbRZcX8tBXMTdry0cZqM6u8M+2l
bOSQC3BMeTaIhEcks3MjI6rKhS3Lu/gIHk7Xiifh3N0rVigYmuyGMLIJqFZRsdor
LUtZHfKWu2/Wjx6KRgOgWnU2/yBM/lkm4FgkEcarGQL3akvavTcSz677yQvN0qhD
PH/IbmvgqqkZtrDxVBHCws+bcgsC/Wl8fScpe5CfFlOk/63wB81sx4wrg/MFka6I
bchFxYjcFcK29QTLtjx1z+8YcBXc1RfDToqbOmOF9T9wVMH0huPG6vSgNY3p4r6L
BrRRfPqxbGQsCaiMtD2vD/bb/Cb3noBTpXxucfD53evfsFGFIuGYEQdYDtMlxfcE
aYBemKpDIcPCgFFMRfZ7jv+hdTci/ke25/3+72dApJyyIi/tZAmPlMrWmdLcyUb8
TLiUjTu8TAxmCBF0ZaLs1KordytqjdvG+bZ6TnEbdE54boq9fJecK8gL02keQi+R
rjmGidliZ9BAyHZSjMdJpml3xbjtas1u+zhyZsHDNjQAf57dgfPn5BEo81jCvHyb
aETZfQnkSCrrfkDZlee6lfm+tj9U5pEu/cpnTxRX7iglInptlUpRvDbgbG9Btlss
RkGt6d22tOftwLBXwMk6YqVOTDQxjX0xUXfYUFwXr+yPnR1vvRfzzB5nOp+M8cKz
FbMC1rKibN+oBvoUtgZ495Vt7P2kscMyeOuPgQCvwdag2Uo/j4c9qm34j0D31Hdc
hCOOe1Dg3zIdGbfvRHKB59+e3Uw8HUzqexhOJ9AmYiXjBdyGpBRZZu5fDtT5b1hH
UQbX5CHJH+UIK170W5dXnH6vw9xeDt79EVoasxdBUCK2OBQpKU46bWknzG9y6n6h
w1qp0s7uq4ZX49TSNxV9K8gVvKG7NY+nF3ec9oQVSpY=
`pragma protect end_protected
