// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:39:03 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Y0ilnyk3Yvj/004lH09jUCXFhLQEr4uv9eBDumzL3CVCMQxD+38+vqy2XR3O6qWs
R+ELnmAdUvEPOeNhyWO9qe/DrFr35sczoayFUrQI1a5/G4Y8VcHhSnCSCc54Tgvj
pYkgQtqwDVo7+971e+XMW7RU4fGYYsIJsI0934wSdZg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3040)
lSMAAsflZnBTp7bwU0THad/3mAifiX+iZ8Zg+IclRh8lsDxac8QVA3vn5NN8ByLM
KVBm8MNwqJ1nz1JgfQ74pnUkSYzzbeRpAg78Sj+IQT/1ch+t513Aufikxnmc+yPw
zuvM0VHUusGzfpeluO336IeXTa1GydvZpDUxij9GJsCPshoD9z5AUXQ5KUwzEuD9
MqYNhQZWh9SXNUBLQyHJRqd2YQDTBIVODDcKy4zmp/zXkfUS/lTQmPYRaYm7/jMZ
sExUAoWqv3cNXU1xEhmX9mKSVbvPZcNNBRorprbnVurOgzfn2KKcmF2bwROjswV2
TviOIxfJcevFWcrot5KbZA9GKtsyKK7fjdXGtJYKy1lKGVMG3pS+FR5biPX1ViYf
+B4QFeZBtg6v+bTVXb5Xz+t6Z8+J4LPyppIIuIe911tWYQ/K62y8HAf2hX8/suCl
vorQYlAH7RKSAk1bJ/HQ8hFzaaPyg9OoMbPBUBLv3ODi8lv0jsYKQ3EXm2crUpt4
65M/eQFUmasi60oP/bfjLcvCisjp9CA6VFAobccxI3THZfMB4tETy/+YQ7gzsFnb
exTBZuGYhMEMrxzd4uuPRsWZYYZSTiSbJRxiQbCB9XLyX2cJyUoJ/Ns2lIBQmuZS
+hcxJsql39KyZbiURGmzldJxD4dyj8r3cfrTqzxI1LOHOekyN8CI8sALhvFzORhK
XNRarndwFBz7cLCCAwFaK0EYhRDlbD+mp1fMIE91gnqWyiw7LzdrH5cOe3hATAnd
X8a4xk5vX3aqALm3+TwAJdzj96wpY7H6xcMhr+dD9MyAB6leFuJC/6haM58JnHZe
gTHxdCEMUV9JbzzambIJGql2vNOgY95WJ/gVFyvV6AFlcZxJrKwkil0UcpALBZ6C
cKbhPspuRC2h4GZikyqQQ6WOy4cmFIDLxg0iD61pXRJd/UPo89U+5ZBV8g/vSnR+
2GVaEfxNKCk2A6E0Gm0vZoIhEt/ZQLS4/kFo0p13yWl+2ubqec80wFZ77SGP2Y2s
tpKBL5IxuiOdKv577/bl9jMfrKTAO+V4A3Du6yy+uDVMkdaLbY8oUmS2od4TFqPP
HiuqgYrVOPVIEKOvS8mUBS2vulbbDZSCNQ2HkySEIdSsQh/q3CT+ATsOn8jjwcPS
Sbf1u5JfUqBAXUQtnbrkAfJknZOXMbUJrianV8XGasWnu5xw3/HiO/yNHXevyFKo
bSQY0YE1A1Mje53/THkp3Pg7QHv8Hz4mLNybT2CUSNKrixpcAc/dy4cezFf+Cejg
eyqR2YPCAF8HdwbWRj7mfg2ub7QwGoPk0iSB1GwgVY8AJKbiX4Z3hIAVLllxjRP4
tBKshAioPfoI5pBXNthQxBS9CVOjePYpjfF2OZ/QyB/69dzWvI8CJ7aV4cjmp9hV
9IEsI7BjeJEaEJQuJUkCxqV67k3acgjH998x4ou2UODAOLdqS1szZH9DpRAvLwtT
JVlQaT+Ugb0FiHaAiGNYR896WvFuKUZVUjIgb19lrWKFRxo/gje13sLNmxwOXoyJ
Y47xyAnOeO1pJcvtxhwrcOEMBtyb04qLSwJOR/lFg0bh7Y82h0pASVwP/clNhu+j
xzYCA4XcJyhzHVz1vjkaNoaTsScdpe3RCVEI5TRACOwvOG7dY+JbPuVtHwKux+HV
aycVESAdn1hxLFNbiSyQqtDzvhk18/XFzk4x2Yz+KKXiZgVaO4Rke/wdCZc2dcFr
iz+rYVdwPAnt+/EWextqZh223ASO8A8P/ZsCIz1g4O1JclKwYRpNUySe2c+6Dm6W
4zK7o9+QegZ8eGrzJFHp2oaYOCFKkegsYian+G1CGeFmJoXY+y6FgICYikUNkI05
x/KVIYEhHvdmk+Q1paf+k4XVtkYEb/wY+fUZJaGJyaYn5ngcYFzQbgkfxQ8VctUu
lnW2LBhqD9QWMynh2VYdj+eapMtIbHG8ExUb8K/+13PHe2uqt/aSGNcXcLu3nv4b
UmLb1BS336+V+4na2EtOqIiN55fuuu+yrNFO76sMWdpMwlwb7CPwJOadYH/uHL3R
ZnszHhN1agVwdQt3fMMA68kOT8invbMBDYDMG66wermw3zDxlyap9fl45cksEumn
GdezMYiZsjfJRnAh1H6L0CiBsrN/qP/U/21/o820ewBjuIbSvzSk0c5UTvpnZ+s6
byOJknhu2Cx/Uus0Dk+m3SLUbEKd1lPBIc7dj+3cMDb/Kw1D6UEt2yRLAUDzKg6F
TL2amus9zemis0x5KYusySRRwfIkt4qeputp9DMCaXdA2EKr5MceLVtXpZKe7s53
vyJ9we/ETIIgZSokk100aQ5AqDxInyIuCBtUHyCVapNuH1NKn+6Nm4T3DtV3AKuL
WHNzusodjW/vLYFfwx1fr6ykyI+iQ10zdXKxLBV9EgjQViz3XVB7/SmjkaR0G1nz
+JG7cz/RhSVdoaC4s8+PnGrujkvFL+KT0m0KIw+/J6EyaA5H2oHFjH3PZTuH4koj
oeM1WTnmNDkMhlRsytdybp77Jtw9o24Si/zWi9hNnoEhkWBq3nxHceOTIsPPQorp
hOdwTLuLg/Lx3npLx3FNJychF7fmbOc/agcKBoOu6AtIXfePkybK07oJ/Ys/FZus
wniGiliCX9E82zRCw1kxZLl/3x3/sl/+ZO7OZzVEZFjjZVw5BrJLD+EWmC5CIs2J
1xc4GWCIRYTq5Vh37F526BLyWxZxOk4U7c8kaak0zPm34RbvxYrhiqH9TLMR2Cua
mjh+aI+pRzKHPZJjrc0wbSiF5Feq94JcQ6qVIBxVxvj5B+h3GdAQkFaJ4EdvhEg+
C8HDmlTaKXRQzpO+VXok9VrsWbiMRxfc/QVIcZQ/fc0Jax8rkWpskh9K1naks7KQ
VAZARMjx51Mrpzb2wgVPvue1xjcm7KaGRIWtnqhHOhKUTTWsYVg9CR+tTl9JCeDV
B/xdlXhb/xDqhLrscYgRlpECDF7oaYzXqro24q7jwsOuw+731QFee8yDb3h8weI8
5q6ZIArcWmrX/qeKXEYhireyiarEwoECOyDlZMUNByngsrSF/Z9mwt0KRBDohc/z
Z/k4YHSatJYtjKt4gRhyaP7081pvR9itvokQ+oaqZ2ZcecH5KA413QCKEZZygW2R
23poHIFHZlpcGp7AoJAsOH6R6fsQhdWuCTnThgJpYxM5RfnQx/Qrw+VAdeKJ3GgE
8dk1r1eBhqmpdMaEAucWpCIa4+0MQqbXanmA4pF8gkz23SKlCRn4bm8K3ahPZRUE
sLKRd5JSzZ94tS2afCqbblazliBvTJOUfspdMvsySYuB5ml4g5Q3uW3mCX9HGMc+
jQZm0g2JcWB6ejiwtQ1HyQmm4UsHK6qNdxU4vHMd8+BK0jfmcjs/TTWOn9cllSnu
lY8kL1y1r7AZ7GDX+UGWZFLnO0jnRKsy55VjaokZVDEfNudQaBgU+KEyrHTfl7lm
m/tSzgXAptC4WVWEmflc6YKAn6dTovFa+IQpFJE1wrmXnPkR90fcd59ENQWJpfyz
5rdT53DzgUogNP4aME/6bNordQBKGwqKIoLzDHnx8j38GgT3yUeszc8f3kA+9Fuq
PpIdzA3cM9th5/dH7qnVDzEZa2qAt9LmaYCAW1x2s6XZDhw4skI445cKLpDJ6ePU
X/4SNQU95IWUvDS4g1BI2NhLP6Salx90sGYDq7DevPHTrCtYEoNv8v9/GuVnyMpL
RaGoaoK+h8eUgqg1ngw+qbxmjYhVj5qOs8H5W1nkl43mo2XNsfmcSmhcInO52I73
2dkgWTZFolEVtf70lTbKydoKfcnFajrhSvwYlXO7RoFqfGkOSuHJMxb2R8i9UGsc
q2WTjcsdWXzhGONhYIbJWkx/tx/ZDH1ijdI1jQo3p15hM200/P41Hk+rsdbNDh11
j5vm8eG7PhwM+i7USRDTAV/yfQYoGQhhr0ntPrEIeJMkfIDfWkrPJlDprG5GemLl
tkePgaOLaBNi4A53BQgWldSiMoVkXaPWTtJ6vamdIkKL5NNvGUPEwSxnkUdNurEE
oLyLS+k3wZEk/Ke58YDExA==
`pragma protect end_protected
