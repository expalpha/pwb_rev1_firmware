// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:39:00 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
L144SiF494g2myOPvTdXK/sjr6DiMTXYrFQxu6eb/aWVCGYYq+teQQyqaE+CGs32
wBz914+z34PZ+EU7zxSBuEjTNMVHpf1ZMvQHm5ov7ak9BQTiJ7UQ4STxoYjjGPPq
lWGwC+RLuxXrMCQwRI/1cEz5XSZKpf3izmn0BVRsXAQ=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1760)
McYpuB9oqyQSb7neeebL3RoNiEzUZkVlbDdZvLOWi9ozRRTbzH4fvLtlS1HeCm4f
/lfja7keoO9Qp1zYVyvkOQ639AbqE+d7ZI4MI98z22flBeyOjlSGv2Z5FzwYJkys
SJDwjD2nnMLDxMIJscFbncyZCANHZHpzqbUF4MybYwNs4Y6YzzbKmY886zEwK1Ut
hL2RLLP+PKR7kTnvMH1y4gzNIT2Ab3XEYN7493rDUAlArah7fMYGTSvDi/8t7jyX
gjXR1mU4KdO3hvGa/UkR979JGpybf7HLhHUEjyEtUIBixHbLx7fAtuqsl5tKZ42r
yWmKw4PCNmPPImc444zKKHfJ7B9zQdHSyI8YCFS5DkQ3XW8dJXBcrYtW7681n141
0loCYMJTCHFIXKeK5j6JFAX0VrTqPjk/AHc3jkmXMzNnbk7mHLp3la88goyIJUnf
UyXiO/eEEzA1NSoS9+V7Sdd/oLJEmFzKU/iHBXZGU5en3Jng2L3lQSaC6H4I3fqm
dodDmc7EUYOFQCICIH4/VCbeJIamrZ4bbf0VZHR5NAC5sM3JA3qlgvSHLkZp+QsB
QOSI77pe5RJUgyLPwWwb0Tr8k5jUw1xjonTnnN4RFOGHrBCrfruTJIs91JJJRdWo
SWfc+yrUlND3rfgcU5lc/ViXY8ykXKAweKMTqj28cH+AXe6LxGIXI/7ZF2hhkcvK
3RyLWB8fPGKqQ2UmwSJmdiroSh03F4pdZJuQH72/8BqWdym0Qz+XkPF3+G8F0pUz
Sjvj26P/5MZLfPkOEqC4pjOoVICn+dff74ww84mAcxfnUQKDNJgHFnL+sZpMxVxo
QvqQXgt2Qx01BbxcJYAFR1+5FD2PDa9/DMh9SQ7SVKAlFhYJF2I7LnWkTrduh9ln
dgpTRSyWh/FOssTlr2ylAGjamsMA4sTz5HhqltrUWKAOjmLpkx8eP0cirwL8XcLu
P/4ZzZTsbkoHh8DNhJ9MBE0XrhLwApj6+v+5Sm8QCAo6ym0ULYSJ9kgWMDcQJLPt
Q3ax2RZ5cjQxxJTiP6Y8ZZZDnI6+xqJnRcBiu6IEIJ37VSdoZNwPfs9xRC1QuDu5
0VlUou/5iQN5wrljJ+lTaan3qFFCpjyYEtE2PrWGmbrY/Q/4+AaN+ywM9lfiy21S
DENKVMi247WC5koyI71Rw8pQC4V9ELrE0QjUwqZnC0SF7w7y2t5DkDESHJwh7WJk
qULVSqkb5Zcx6bNb5/KtaYcVo6ESmJ2xa3E01TOZqaXTaTX4pnS3/mh+doFSZWqa
s1jUZBybCRAiJMjemHJV1qgQHsxAsZS8wPsaRzFzVQm3UP/o4p7Er+z4EeRI+vTs
3cs44FQaEJiq8SWH0tmQnhpdNefQpGCd0nPYQz8A6oSotEFuhKOXRd46n3azABXw
iFZDbOmZUqWTIO1pksAuyC9Klc8j9aR9IZssfAvp5qyX+94XRPh65zYvnJYd12+J
WPcw1oyebZ358YSXmVjza12XDaojsMBhp1kOjXjzmp68wAhWbjNQgCET8wCZwFyl
svvJ8/3XeU+cy+9U+nHILJSlnk2l54j+peTCdNPlxh6IOOGfmhO21A4uBLAvvHQm
80+t/dNK0XP8/dQnvNWh4ZbTblCie9pNhKcU8LMqDtxkmR3bke/uuo4H8fKGswoy
tLbYw+DstAfSGWSkFEvW02jdUAj9FVbH2M6qR2zvuL+M8QenVupG5FFbhTKjYYDo
/I3m/Sr+GDogocSyJMXot7edxK0+ctK6XXMJ5/3TXySWbgOZDdrbdSwTz/qqKWyu
cMKyFWdTNo47RcGGknVcEzSVAjl8W4CrefCADsHAynf8G4JyF+neMz/T2FrVZUzc
aHzsFRC9XBuw89oSDY1Y0cOVQZzy8OvB/if7sjXmG+9gf2e8FtxVEiMiwspVRId4
SACWW1fUFiEONxvNzHC8hWK44ZtIwIYbA5GFh3nnK+xAgY/90O5DB4qARxCNpc+O
cxgVSdZ/K4KKaQ0NDzevzWCyiW5a7Ao6EnL2YICGCNBbHXTqV+zFSq6sYP1LSYLo
/XUJ167JZUked+OVJIqNOS8bktPDu9UaboKZ8Cj9El7f46FHWsYlLd2zbj/J9QQB
5jzMMpF+dONt9e3hvB+SRrixeqdNfTnJCaK7p+3rAR8WyZvtTfjJ9ZGp3L7Kv+jY
Sa1qImesSJyhW0cqxcTXz9leFDv1NkPhlh/ECujS93H5aOZHd0NWd2wQCdYk2zv6
Kh8+SlQxKvCK1GERCkyPLc4FpIDJuIjUE8xXNaKfvmALn4PJjzr9Tfhdn6N7MS7k
ma+GNlqWooNT8XswwkLzEvByQ42eo88MlP5TRqlmC10=
`pragma protect end_protected
