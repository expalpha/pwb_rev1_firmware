// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std.2
// ALTERA_TIMESTAMP:Thu Jul 20 10:38:57 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Tb0ja+BeYebX2HFtcMaATMNmyUK6RZBp9WvzW1zolSARGvHc//zOzERTZzPs6la1
019lf6tW5GkCIg09tidemQ7GrdhkU6HJ31gML5B4/gof73i8L2aXU4vYfvdsjVTF
MR3QNVMgUSv+7tYsGIHyGYwwT1ghtjkdMYgRwVMIkBk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 12336)
JPHUy26343udp7iFYcpbb0r0Oc/h0yVYMr5xQtU5ZTUQ6ODYgydCijkuGu0ZpZrY
P5Y05Nn53C/hLfQ6YZosJ3RTIJTuSUhvw0Tkplk6GHWctMZLljtnMH9djVkrzccJ
RBEYBfrQ/SjE+CucENm1PRlHLdESb9htn8UVd3VwHb5xMdR9L5Io+sMLkUFimDLf
XNUx9WzlpU/gcTf9fgZPUJRG9kwj9/4P/Pfj80IESxfVFZzzYo0tq8tHRxazLMRM
kNn36SzfkedjoU0pc/IZSbXKetB/i2LQ7NKoA7QZ+57CCEjaOGAFOwXoe60/U2Ul
+ekBn4Xurkuq2jR4QbgH2L0KqUvCT/rhkAypsvNEOnAvYMuMH/IdJYlfegfPqPfU
0hAllh5KLctkwcKijMQ6ev0AYQ3CWgsvn6XrMPEwTiJFwFsmc9r+G4V9Fxxs0I1R
IDxDA5T6OIWNYH04lTyBTeonP2OCbJj+wewc2Mo7w248kfslp4G4+VITenWllh0Y
mAOWULVmMzm3nF5megOHlqtjLWt9x1NvuY8lecorJCL8wKw8mZXZOdhGSRpo2uNa
qL7QLRpoXewu4GuIZTWnwCl9u+gQNA+Cbt0qwF1y7f4SDZZG8pYoq8viRQj/zy0Z
dke5J3QdZJ5W0Y0p7L1JWXGBDtBncODnEvVr8OC+bdpfNHJrN0gNK9MPhWnFtyZf
GK0cw74h7UAlTc0bRhLqRapmlsCO6coHuF0ljHm/9wyIrK7mgvcnPoymZMI2AHyo
HeN/zd2iIhpLeg301EqPce68lOyYRIxs8ZjjKHZPdLy2YURHZUAxqFerM2ngCdYm
tVA+pBoGzB0SZJdvIomqpZxA6nAf1+DcOvBkWd7QnjKHEGW6We4eCEvvyHw4opAh
4RLUYQqfKk61NRicE8VsN7+MXfhJ72H+scL0JmfDPdrNMSirfCVRflZp35Q4eJaW
9taMqxHU84Xn9JMc7HAqV6vkdG6c68tn/vrwKERy9fnpnAmbYgfXWfcoVE90o7+d
CdhZkmZRh+WuybgdwGDdUPG02GxqkO21N3DvXApO/ZUthFlJ0pNnK9WBFIKiRAeq
AUzo7moCGh5Nh4Kq5Q1feYebAW92+TdsAhJuU6s0e+UaxRhWBVRHB6OGAQVskZvS
WnQY3K4FLTs4XALFfSK8ykhMMqyjRf9QSjNAQTjxfdw5RBkN1hqu0wxvbVabQrg1
pBBX9Qlil/s63g1ILYV/mvXthw1yagSD84GdbtCeDJDQwOq1XXwvM2sCyd5Vh1Q0
5zQXFVmz2rwnjv9A+1mYas13LGsWwa2LaAFgY+abCWIm4t11mXHxLvCIReBjLPjB
j6OQky8Frw/G6hXAbDGQEy1WzmFHHTiY3hOlsYmZQZ+gYq/BSGFl5zo7LijJZrst
juB4RGJikUvQwRE2DM7OIc4ZtG843wA4qPsPLG/cYVMO0u6CsgnkmwhFIiBkUJ+B
EDNbAx6atQKOToGWdf2ON880Qy/OU01lidt+XHuEVQTg0s8w2/P9RuzfAOq11bGm
VA+/3m2d8mGHVnxgh08g/J0Rw4LqK1nGVDhx8ebWv7bv5terEaiu21WBzpUnYhVz
nyik11ymnKQIrGZiwTz8+3a5t/p6hUJd4BJ0ta+BvmcDRxLfCjMJ3BYzFGsFSfRM
3/grcxjuk2vwdT1x5JYNLML+Y6SqNAaGcu8KmM8I5+tu8hI/3PXPlwCh271t557O
sH1UL0CMAE4Di4thNtDhsqRO/IZ+r6oPSn3FX4SwZeq0/PfkRj0/oIWivy4mxypX
U4XHk3bHqs4+8Jzp5jwT/7b6LASj8gKFFvgAVPYDvblezyBnivfB22TAjhdY81XL
d/zGyIYaaLyN/WLnHn8L6X3dLDN1etGtF1wLS+G/rbhax+XofpCklSnsNBLwIPbR
QS8lG3UCxLF5m7LpCE8clu/6pSwZwwjqvoFTr3g81yw4XtPYvLrAKNkfa297fjcS
o3IXXl4Rj83pRA4nZ/MdCYsP0parLFvF0i/Rd2bm51iDUo+2BSUhVZ4zFUac3pZ5
g+20S614xAcKa+FaU7JGCo0tHmt3xrG8F4bZJQ6cp6AF090N2B8TT8eRnbdL3hN3
CsGnh+Q5Q3wT6bGvcGsVH9fv/5KzTrI0q4dpBhQA7axt488sLvDOWKsIVMcKK1Xv
B0yxj3pAMfkxy4GA0zQWDnvNzVPmKgfoIKS2ll4+zTpbBXwia+ll6dDN0pQYkv/3
6T/n9IhELrlSgHH9lAZqF/f+D1gBuWycyCfQnN/HuyhRCa5K3wTxv+vBJByJsP82
2VE3nt1/gyxyYbWr4UVxfnYwjBQjtpU5L61w1+XmDpt3krKr2vbSOjNgsqYt/9eU
gzkXEAI5pZh719NK2wdCX4AJSUM/lSwad1ZT67IMZTPi+S9MrYZDjouQYqk96elk
Si4Twefb8tF1MxJjzp73VHWA3LmlDSY72wX8tYasWfwGG2Tx+QP6OVp6iJKC9JtX
YkxlWNjXZoFDc7RK2RqlDA7sRsk8+Er+EixymUrE9fVCQ8Tnl5SXiWvJFiCsbGDS
kKnXsdqsNK9dSeNBwyT+9u3j1Lhi/K2cqjZS2Cw0WPrqOCLjEaeIcVmEevtirHG4
8QLDG2VXz2J0ZO9tziCCBpEJD8f/6hC9c9dJT322my8Ejy29boLjXPyU0Ep1qz+V
tjIHKxaLsH1wOOrHWIm8OnhGusNqdOk1TSrb3/T0T+sVrZH1eITs9yfkdKzchI7Q
55A51fDMTL/UOYWSY3I6rglH7KLChzH3VAg75DDXdgqbf/KNmIji2P9gAfdffDFB
7W/shCSrUfRzzwNihy2iTWpeW65N+c2mPP5iFPgk/Pwdk7GymFQCfF8VRc2z31KN
5OOqOhpSBHZ2SgiFgi4fYJxiivKTsk4cSi5kjJM2dmTbEJ3vMgxZi7Wez0N7OKdF
ARCw3qVm4lfUS3yxeotbgmInBR5xdKOgFDvnzodFmX8JGY2LPYKTAkUne8imKUSN
vFD+CiVy9nmEL7XnC/s4ErXAJQOcfB/b7rAK1Rlil8kPSEvKfWwExBtG97zjEeFQ
k2bEeXT3BF95t6Oz1ctYXuS1llBWahULEfX7C4otDdhhmuYW2DqD8/+xMoTnw7ie
oQR01ZA9J8CRtlkA8tRvxaUIN6RdR7vaW/5o5ockN71ItFnv3K0ur7HFf3eFwWc9
UGBLSe7VPc9ImAjYzpDpSSbVmwodxZz8GOYrCS/yr/X5jjdfFnCgstWCuHRcPRnS
xzrIu1iw5gwHaJdGMvRw3XEananZt41q1/DGTHmLxytoDiKmUKZf1ZohcmnO26bt
5yW6X9zNZQqc8Cr9AksfyxPFxMAzYWf8V8cvoYXGCXky9jckS4J3MZwoSYahVyz+
bqZvPfbTFAwIZm8Ut0IVOXApVhrn2guC+QPdSOK9u7eOC0K51PvqNSfH1gPGm8CG
dP59bvKywLz3pRPIzCC58CwWtCUR6dqaSZD7w/IsQQkJJJBkwJn33RJhQg/DBT0R
Eu25HFX9TUjDRPhMEBFRMcRGfJKzcmXXV9rUCFib9cTwMn8rp0kH0TEQkbSOthsn
CghIkyfY3F2NLZdjZqeei942Lp2MGDFmT083gJj3DaeS8pER8CxiW/9ZrylTUTL9
5d+ua/PDOu+nTYVIhLFXK5a/lx9KbrODY6PKU0VjAiw4dWcpDMpfMwE3obXNcMqC
KYjrY5+HNJ4JmFc26DUpKKnKFgpNZhekB8t+FHdj+Z1iGu+ouPm3nXHzZZA5OF1f
wWhXXHFEr+hHOa1iDnvnWTZS/Ic0rMaIhXt6y69vW3htoyR8Nnh+9GLbR848P1DI
GewJsW97r58WQwg4yqw+RLATNjOGIJcz1BSFHPABcFfHFIH9XlrMesAxC+QR7yZ8
6vFc//l6BSYw9jhm93OUvnUxwczB9M7Q6tD1sCkzIj/19iJBNcYiCgpCSGuzaDqK
H2hJtodhmqP+7jaLr3iCTSSBoM1d/kcNmLDSgHL+HQN1m/wsfJYBMLHgZabxFlOZ
kqeIpfXxlEs69orB15G3kwqENEb64hkiF/noDaEFb7jFPX8L7ky805HeGY1VT2k0
3QCX5xQALCv9FApPR+32bCpcohxV7Vy1jiP++494eC6CLXSCyGwyoRCZHx/MEjih
wMOVgHKBV+0WVgzupwfoLTm0ylXoSA3USfEQd7Jq234xE6K3RULw2cz4AWRXFVos
mgnx0fYLpxcdFX87wqKnz+uNCiY+xtP6DNutTq6FL/jEkJk62Kx5c/nRaB2uMOCP
dufEwJJuFzP3fo3vSBjB7m8Rz8u+roLYhiYVWGjoDu0wWw0w+HoCu8XWUg7OPW7M
z3smee3/rFuPIDmvPmWOM6wxcmf2B+GaLrbFoB+PJIAHpbeMfcYcXpMRptfUsOg0
Huvv1WuilCRlBQKDh3xRyjmNbrhEwXrxL9ga+7BLqcVBYlqUuHtp/likeInHLgbz
g/t49CuSDS+tTo5v3FVIra+OLWu9i6XbN+JVBi5JGouFBxaHcBz5LX1xQRR/PMz2
2WdNsZ9qVBF/jTFJdJK4SRAqlX7/cS2vHePOIMlKPbrjJH6izW7HCSZJPgwYO+9g
nBth3PhH+7zwHMYpfZwj83EWh45lQ28fqMXPl94og+qloIZmq4Dj/kRvxP3BDxJO
IXjkvgpe8xccw3+175mpcYc50rn5z10QxlaGxxmyUsADqQ1iBSFjW3PwwcwPqX5Y
Ci86Tsltu+r70PhIQotCXvNEi9scmKOs2coGO6znCg8ADSDROv1ZHlKnBg/Ldf1r
7lMyzISF2Ha+2/rRl9NATvtL7nSZ6q3C960RvkPOWhAHCt2rH25538rX4xYsSbcH
yHSSi9q9SyzbAMEugF4fs1AgFKRQfnsrVl/YGspUMzuXtm8gmtcrkfPTsmt7CHT3
AhcV7cemhzbMwoTwwLRodkq/ymrTNH4j0a5Ra2NHJF1S74+xaMzoJIqchxIlMTHl
KD5FmlgzFddB8cDaYZ5cev8huIBIWMw5ow1ZFfsBFBI/wxeOvnmcgP+OMnl8Mg7s
tNpAuc+aLo+z4mNRbXDVnLqmGtcK5wx5z7ZkJa8JpBTTzuRqsyJnU/lLW8w+Zj2E
f8MhuJgrP0NcQiB3kPp3gre5BONDg2PHTd/kynejn/trT6n0QdaOnSjdOLv7Naxz
ktiAdZC1QZzXDSGGVacGhvfcE8Kx3bZsxxPWurj6V8r+kPKEAAGL8q+1AkVPeIqU
sjWCrDqpRHT9LYTqI7jdNAEUde7nJ3Bfu2+XONYqjbw1z+RjI8jViY6OAGkpO8zF
C9YjFI4E8PcoxFeckpj35jcABMH++OT4M4dxqGHiwvdFcqJRQ1GhugvuQWPM1fdP
2xtaLOno4nNcDXDINSWaU0kpdZwNYfcDXAbxZfwBLVJZ2qcj8anBylODCoEV92YI
BDU5fakN77YHLaC9Sbws2dDgnyzNc/857guJJoqFEbtEkLff9+D0r71Uv1eKDNHi
UiSy7+zWzKzeCUtPzFPAMJ/MQ4E2rVr5o/vXCOriN5rLCUM4n1/LrcNPWSRZGn/p
iZHzBdmgRLeZb/r18TiyI+apXtdDk+qP7gHnVOB+RjzlPh23bgkaGe/e2b+Yi/88
wZ7WoNuZCWS8iZ54/LNPC6Cj4Xbhm7tH3wr4/A9aeIh9oCHjd2UyteDOHwN6LVd5
zfPCSBCDdWyqOG7Ry5W/fBs4n00dMovZKoCarCHEXtjY27s8hciRnkdOj2xWRD5F
EXTqqNdFiUHd3RfvHPcLTpPE+nAWlE/vWNwjK4i6RbN9m4Hpgv4kZDZkoSAV+dzG
4prVsCbuiZaXWw5DXdmaviqdu2E94S8rHoC0nj899XZ/E6cXuiTa/lsd+LPAmjut
QDTcIFm6fxjQARlB7e8EHxWC3OGRCPf9GVcEZuT6fgq9CnKgsSqhMZ7aiTUXp9zY
M8LLRTkjrj9G1X9I/CnbOgcHMj/bexg+f0ErJRCoRde8hTmJsphxvASBzyM92RLd
pasq3W0e39jVhqfhnMhtVvRILW8XqMGmsDiANGx57MJh9tVJKLTtQQknlYVHBMOv
gw+ExG7JPYSKCw9rvWaJvQlvXMk/ltXuAlvmbYHNZ7ktaFeH3ia+W9sz5+SHG7/X
C5F5udhEo8SB5p1VZtHGv++GR91tJBQUXSUI7rz1BeBgIJBCcPOs9XP0/fFUFf2Z
dDEj0Jc+EWH8x1deUV+VcsUhUOVf3LCRCX0dLahy2pwOgn+3+sc/XhjpKu+AR8w6
JQZmx7GXldcN7JMcXi0o/yjVAigeDgKnzOavPlc9uQ8Cmo+o0ZhGhgx+JkpB2mUK
PcvPIKYD/SVkkVhBHEtvpkM9lGhxitQDFYe/DQTHbMCMTxj7shqViuyOMe6GrHhK
SPT9ro4Sw0N/942d6JwcWTJ3Kf4t/92gqx42EBWWqfS8J5cKbTUrKVlhh2WZcAwl
ailBTb85aSeTcRyn8P9m9TWa0Qu3bgk/HugWHKHnN7I3i9GWgskjDYz0weUiKhFM
LtS3CiBjH30VIzaJF1XOtOxRJpHa3+IcL4SO92XKzF1dRXLOWzm7pS8H3lMyjb8F
LwCbYNZgOoKKThEImlT1nCVaRCEziG4aJmPJDH84prmbhs3CltvTge7gZdiXoVsL
OXqm+iow0JZM+tBobYw3n4qz+UkHyCYksJML/YOhKRS0hIy0zsAniqz3feUk/42u
/bm+iDU5/V7KPZhORAJh2Wxkqzqwjq52iMKSnDdcG6QPelgrd+82Dpw2g+6VJ5Hi
KpS4+QQ6CeasfpSsB6ALbB1yDEiaplV484pGvvOiuw2NXHJdcXjFp5UWewT5syUO
mNd1vZ4hriXTpzJMAqmMHVjgVYuAhvmjWLHJYZb14B5uQFLjbYPGpdBoVypGNkhT
CBEOKyrX9fajmB2B3KmqfC8mkR9v7QL0DoKNstZfiPN86eHYdd/X0DJ5hGIkmpYL
WQ6zD8426gzTKTyS+AtoETz2fxvbVuDrTLA8FK+LK1YS+BEleqV1A+N5/biam3ah
qLPMqmT6iD4B4nWunX7IVUhYLb8wPOlz3Atpreg7msF6+MyGHzn7A06j8/V+rhBR
8Ap/j326V15GSFA1uWl0KAO+U8M+d32wwyAtjRHiaD8//TrhudmzxZzPUmml1TKC
F3prfo6r+6Y3ldZWCFicneMPYe4M+UZFJVDlu+RkX9OKXNJavEeCfI8vNyceyjwn
o2C8H0exZ/u+0uFETVzwdBZpmk9I+6QzSoAMiIF2xixYSbzx6jrWtkBOK/lJplNW
xb36v9MFQBkSFhNzP0avvW/lRtRK0JkVzUHC3mJKYYYa6aMufn0EfyS/AeAZF11T
cwynHF+3umez4GWYcvYIA9p911EfmHrTVQKWXnBsowlYW4JQ2Hkgl7fHcr5lTWdP
g8xAHfB6mDtSpjoMMMNmhgTBZ9VvGiRE+eOW0n5YjoEmIC1Y+NYeQ2lmS1psqFWl
G0eLxFNqLGM6XOJyN1ZBv5DWqjsRVVjwXvL8tKC44JuBz1hjpgOzavhJl57UQwuX
VX64/tqAvrMYX9BFMQUXZPOVwXihRCLp41OS6SouptUAV21ogUaS73UrOfPJMtXt
DKVvJKx/fBLzhcWNHCaFUp61w5UwTp+eR+LGos0iEJWAyKTv0eCBN5KyEvzsU9k7
hqURY/jZzhKPdck4Y1vj5173cr6s7MBSF3cUdkEOo1pVf+iNeYvTY+5adiS0zMtp
Zh/NIwBx/sHJqKZnb/iG2BhBYGUuUl7z/49gK3Hv3FLsmIVLtY1ViDBuInEqnEUl
LZAjLAmBSk0gDSjNFg/cv0HKmDIWMLf+5gcOumHEDUnvy52g6D/EvuSKH/UHApim
hrD7Yn1Mlm95pV2dlGJcZdFewMFYKiYvNJ8Kjq/KcGuelnVvP33/lkHtcRzE71Bx
9P7ynSBS11ziqq7AHmGXzLvvEKVv0dgVJJ4YIIdKA3YheJzDZdV4kq0NR1HfxeDu
7LhIXR9MdgfjRzVowk1/deW3rwHHoiZoAk8rFLbzmpEcWFjAygH2Ys6tlafHy6sf
gvbbTEfPahe4Ft2FjiQTV7AnL5J6dzT8WjUWeUnT0fb4VhGws/3JFFkDPSBqb45t
7qMhQQ2sg9cIw6wIbyysbGqmGt5N++9UiMuWilLk0uHn8Y7YMmueCrrAxKX5tZyd
frPb7vbUO2saduG4cZWiO41zssz71zj8J8QyXGBKMjdZsdNPc8ek2G0bsf105mGt
qFcpmy/AW/MtEu0ZaGC8Hwr0bW5gtirvmEENA4Ya9eJwLGVDkgIwD7d8VoHnZrlY
jA+f/RxawP6nwZaM4QjKcU8erXDtEnPfgNtl0EKXyIgJIpppDyV0I4Nbj/XXaiCj
7HGvBG61c9+d3YtLoHm1qITZt4xE9mG13yNK9+5ZRlllAnjnoGkhuMHScDElIs8A
axAM5yJE3wlq/39FW0JE8zZ5pxDBM03ARBcY7T0pfmWV2fCbnYsgUQktEKdzsqcv
pc0osPQk1+7Wen6tTBR0RiXZVGMNWwGW9RHJ5xdQzgaYck8jRlrfbMf7C/Uo8ysy
gonOYi9WImTmPY/1HMEnsK9AKu81jXNsvqHy+Vz7rLUQuv5w9zsxtXP9prKS8V6R
Jw51hTJD6fgo8AuqDMaGnL0O1G27uLOp3yO0ajC2hXWmn6+0kvYjQvZCSHGNoDH5
fRnQS1+QJoeJGfXonb78IcbsWdudUQUx5ylOBnqfHScwRzvrJJjJtb60bp4BDIk5
0g5Av3w0ZGd125JdFXlBIVzsAz8sydXZhNw8u4g8dqEgvixOoEA7Rdl0eManBaBH
kK9aVer2PYAGjDwvm7bNRaCo+P7HvVB97dmpdnlmU8/NV80J0yyFo0lq7H+djBG/
5empngLoOtPuX/vNQZHdiMTY7B9zYqmQFUecx3lDAPTncK4UR1GEV/b7bt+SUiDu
8wxwH6yc2o5ZMkGmaS8goS9nBCNOs/4pAWx30+La4UbKcM7Cn1tMKjnokPxUCNiG
10AizVk56OpSvEsr79Q7/3i5kiTSFq+r6IcXBtqRUqP0cULzjrtA1J6DJkhWGJ3A
ShEnHnAvBDz0+EolHg5Mt1gdpZfpdBSfQoeHPFNgR5EBpYZLw7rhuLNXcqKCaSYF
W2P6KLDBB9xJbLlbiRNnRbqmLMcp8CtUudwNZ5+HxRiF9bNfPen5K+udMoRQ9mJs
8tmZEJpORbvvKYr2P8CyaQKyW7qFH9K8CsBMGvAcOfiOSFcujWmakW6zI0BY023M
dqwtQkhW50pgPPBKqjHLY4AZvLYCMqFKurb+pVKfDzqakpIOl3M8rlKXcQe0wfah
ldMQfWkWgU57Fa3oNY1PnoNb/R+gLzov7bm2xdu1V2BB95ohoZmbAHUBE7ZBXbqm
1/OCPgDOfNZd9sib9dFFwTKbmqzDDCcMOdUNbnHIL49fl9bnfCVOAC8cRfbUa8uq
/5zrxYW30tlbVNCOpixJyhHnL1g7D213KK8h4TKs47pFLXzPUhOsjurH6okEQ6Na
iiXoGvB0BZjuxTIgzntHI5lI5re9COufSbxp9xOUwqq55ADUYJhlFRu0iiKZT309
mbCIhhXXWxNCvjjnrESRfU8pTFRjjzs03hG3nVF0Abq4YoHBI8Bpt1qOtsnpRRKX
R8pMQWnGI/91V64qq0UhE4T3vUS4h0WiBDIy6DdVgKHPyAZxkFd5nygtPtFmlhn6
S6i6BOqew1DTfmmpvKdflH7dbz3uA/aVQMJLgC6r3eJssofFo1DE12hmYxIgcQ8A
9ad2EidyndsYGaWHwp6TnGBLICMx6/aVKSNeT5mAlXw0PUt9/QzcuQerHcwB9PU6
IMN+GAbEB2K/To6ocgUq4ZQaib2437Hi2gsOY89hNDQb2sIsCA3hoiCFC+GLm+if
AijLy0SrnIfJklpFSIABVRaA6bj+NLo60EeNYDWlm1Cqie1bU8FI9z1kStXkaYqX
Hq5DTq6doEZf4frUVC/J6E/3s4NIS9QLnYDhmcb1IiJu5K5Z5uM3u1U/VF4dZHjV
bznn3Gos97sqvOJn7JflwoKk5wby0u9mGjG1hjk45SMphhwXkqt6oxQ6SFeuwDPy
Gn20/+Mk8V84g8iiX4VdQQa7KU30DZf2mOwK/+UCR4WqbFrNiZRi5flb0V1gELnx
JiLrJ15q5/MqgVRmVU5yvTCihjdcOfotDfFe6RGSvFKfJ/Nrmeu7uB/STnZQihHl
sRlfqF/KQKj8R6ONiSIigbrByBxHGdjSwleJcIE57TsuOyYJb5QBwhbGZ3gZNJHs
FbSTeDBZC+XkOVPCbkBK8/YcRQuPRHJvqJJ+i3nQOa9Nm2LdK8VFtgkm09EK6Nc0
ILrluNS+YrhqgMlhnkvVZ0vSGuCzQTDfGJwJcVYQi6JQ/yurKkgI7T8CZ/CYVjzx
GttQrjkbTkEla6zn4oroetSEtNLlixfZ+NdUKbZ158AIqMYdmhTaABEoJ1GSVgaX
EiixcfXRdSXfE9NLIYf1R3Q7P/XwvsO4TovPEk2/3kn2jK2nhNXDTcU4f8uE11pI
MiMa9SYpKG49BNIJFBuPdFp/UoOXnWLgGWRQiAF3ZlF7evJ6CbEpoWyXubbSTpdQ
zXtIjJKTRIdJkreNA7d141oZAR8qZXp9tMFN4iEwmp4oTJ09jcc1naC2NtnjixqU
4Kuby/wzEM2tDQ++7+PO+pxkVUkyC2ReRtvW9xkBy8Xs1hevpI+1zThm4j21gE0C
kucpYnO57mTjvHtGs6xWzWibdYJonRLxHNv5mtqqUQd4FlDC3JDI85m23tjIkfqs
qQwNieMDP/Zxph1cH5K+uzv7EV3Ma7yHw1MM7/bOKZIqeA7Qk1iHDJBwwizZ0lwZ
qoDZTR9s//IJ3f/c9pSGyQ/oyeYYXh7gybXHAnMSV+1tTKTgLhPfCW4FPb/T2OH2
EU6yheX+j85pe4Rmejah/z0RmlMkAozWX8bLXJAcCKJT8y/23TiqF5wzCI+FxUeu
EeXpLc+jstxsKSksUrOR1xPDWYdjuXAUHL32sP77p0TqylU0Q42XYOW3PKEIzu3p
lBpFwgWHgKoF99m9JMnRcPbgFarTGNttC9sD+FUukuxtrJny3IOqg/+cyZ4n0S1l
J+82u8h4T4Y2GR4XWdT3Q/dCEjCLGQGF/U6UuMoYUeIh9Z9J++7UyoOuT0TqG2cL
Nyk1pIhebzNN8PvvqrlraNLtc8dwxnSoJsNAnbZS+OPlbE43MN5FqrCXRIVUpTxh
lkr8wfNeCo4N1EnJrNHg6EKdTxPJ49l6h7IkW8j62+fvUc3WxW4qoBdtnPtHxALe
dRIdc2rsnV+DiD/Hv+EW2iqjOHuKRnMPSUP+7tS3svqI/lQDxoBQgZOfSUBfXaBi
7/mFDx79h273tIgs+eYwYKPYVHRvO24B/QFW537ed2hkekUXmdc3xCE10ELDaAC7
GNcQSNOLc7VEAe7C8Eh2L7oTFGhF1VU0InsXdgp4IGjyKgFoiKCuRtJnzRZBeA0Y
v9KteZNWOuI4UTmidbspBgFm95gE/4Rkel1t68bq1WAblCSNcQ5uf58bVI17m+5C
scmzdx2kbt0qXr8iNdYGcF87LAmeqQD2zuw+pLuzWodyLGduK4QaA1ZQ8OSldDEU
vs/Et6FhHwSc2QaeppYDvXjSM4tRH5PHsf/by9n8QUrjhD5rb9p1wKkH+LZn7vi0
BAIuFn/yEqwSLzzBv+axbtog1I8ccaCfRGTacMOU0VvHlmGv5Z+tg8tjNudCuVb4
AdLTh5GIE/e1W2tXcyQJLeB4k7WcfHendJ1roKR8BuXaVJ38sOuTAbksmrHJDuzc
7kenDKi65r5hzfjWRxrbc0e8xkPoxDDp3mHjYM4mkpkDtMs/5RGlys3B8ldeualm
pLI6/BlEHUV0HsODE8RBb7OnXa6Xwz5uMFXU1L3ip6dFX9/uBvb03wHu1TSwsN/H
KKPdDDhCmMZ7zNjFWQlNJaWLN3CBxJ1qpMgICeG5zqZsAcCmgN7hJeS8HnNCUaiI
qyY38dltlKmH51rip1jbDLsltu+HRxS70CBwSMgdI7/q72Poh2VP4rCGChzeNBMA
ZY0mym1XglurhT3kLsmj7Ua/MM6wpAYkGWlkP1Zeco/EYxO31XkRqzpVJD6ZoBwx
ri7D81jR1jZo/C/7+DoArKrPtkexniq+Hj2DdggB70U9qHSjaasj1WMJZ5PKuxY7
2DgI+Ts5GsvZPeedN/rLmYtccyu/lVOtxmlkredv5LRPekNQYOlPS18egYdfWl9U
HGCuYD6zDacodrZbq+BxkNUUI7s7u6gb0KyilC+exEVlnkcggLEaOE0pZ9QyGJmk
h0YRD9BecgWLBtdCwj8idA0CktrSVTcXgctARB4ouCiKe91esQWcGw9iPY3QD6Yp
reMsiSQi3f5F9UQXELw9Yhg6kLnw796qbTG/Fxw54SoDewBP6idiEuTgb7sJw72F
rOX0XwHsy7rxWulWJdZMswbO7jkgn8bSgJFoi62Pa6tnUd7XnvAzMHxj1nU36FZY
0O1viyc4By/bcnUrHcDpRuXz8LxCC2d+dWrDiKjg8AWdvnZjngqiTfC+7rb/279V
HKjUFuM7t0K24I53VSbmV/Ng1he/iTr/fqymiEJmCMzjJv9nj5fmroM6w24m3Vde
sXofg+BzvCjfZuVLC8e2YF0bfs2aS1UFNPIMldG2RtadiDk7AgRCUCvqbnHQOlt1
WRK1BYo8KOcHPZmqT5VLXyTBFlEsHsbf0xdNoBekMYhzRgsa9NVpXSc4ojLllD5M
FplTymUMJp4PIimd8bTUC6O4V0wdaa0AkwKQPb6Eu8ukzCbJjBrzZvpkD+7IAdFR
vy1FoLgj98g6UpFwW8wQbEquAPZWayE2eWoDHB8+Z3x32GpKfhOjSWlOqjOvdg8G
qlDfR9WLRoqSszNYbmXlw/ggaKNMJwfF4BX8twqCpSFkJ1CVk2wYzeu2ModLjEIF
aSZb8Yzsu5lyfdV7gVeyTrKA+yl7+ti9yLAOX7LQT+gjkqkKxHEER8embZ+tdzSw
zXASnrbamai9JtovgCUGKkfoxJ+4TR0e20Z8aVSPGGw+IIYjZ65g+E8gZ/u9f6QI
9pg65xsYkho315Eiu+RQVoXg75mGxoW457vhe3Njie4MhbDwxHD7mXieaIdKWjCy
QvAyJK6KEzqabJ+1R9BoH8NMVkmm/JGkx+8LB1W4j4B26laW/fgzcCUrICKhjiQb
4hFtqqS9H+sBnY0dusg/C4OyaxnaLAV954THNeVDiIUTfdsT07yhVBPtY+XO0KjD
1I2hrRB4E1LPT6Dx3+4Yo6nYhG3F/6s9FrCi3pbRnYLLJWnzkbw8N6mQ/bTkZV41
AEPWQR381RbUPWDLulaV3XRp7F3uL4HemdXdkm+Je0qCQQXtAwAv5q0IBLLNZut6
miO1v53GgUZqWES9ckHthEuZsI0VJawTwi+kG8eHQhnfIurShDf4NLVtEnZ1nC/c
UwWC45f5WBkKorqqgVell6jmxCDVrZfsfffdzND1+F+AN87zUpuThDoCX46FAikE
wi+6TMEJq53XRGWHKiv0zZMB0dwk4oEZ9BMjX0OCikOsc8RFzWNXYjVkpOur1R4m
YYjm91wi8rDwM8EVlgQrrDWBDQ2kvyAwNcpemkDwFyYjYCYXW8rXM9rRz1NbZ9Zx
2PVyqQJlHuutVkT0h8Cim/FyEGrXkNGj1RbFpxJXKsJd06zhUWopKUe4ANQUJgyM
wdBxi8aiIP3ke0gHTQq4G0yCGuG1QbxCbXicqrvd28fcswF0KsXJJq5e4c8zj3WK
EVK+OWTPlCPQePEEr/vNVr9Ml6a1BWm9Er/0vpowBzBMyQtgwwSxOItDUd64vtcc
G5VQ+vpCR9ABu8X/QJRGDQHSFOw4TJWjbYbR6Y99qZoSKU22Wfu3MiyRMj4cxgI9
K5/8rDhYDPkFugApyFdo8Dl9ZER3TcyxtPLuT0iLgBICeLNOZ8kclS2nVItR3CDg
QHqbwHZcjiyBlCM7Pw9mzt5AJ+2qwsZeq518OmRqup+l5ng6Iz9zDWuNRgIDi3C4
Tcwqi+I1Pq8DZaQ2jYQ9Ya3BvZaOnoAtRTtNYuUhpX/2hhA780KnIZBSIm+pmGV/
/Z0ncguQzr2qrq7g6tHU1HlAN4IEuLdXQZFeIQXyHgByI+MIGjGVxvQ90/YtH9QY
2iuKd2BdoFImIiVLjLulb9YdW1c4zdxM3ZH2d5EvRsgAumiwIwTJdQvK+nqfWDHO
kADKQuT1m2Q7o/pxp828IwcYLpbC7L8HxuuJtaBSPIeZui0y745dnEO1am6uAL4U
fW6zYdnqhS4M9q1D5JoKbZSztfpbuMZFdGpXh14RkTEdIMKmU1/g4NAO7/Mpbe7B
0v+85s9+XTFD+tCGYfdvAVENAj7ycJjYypkygfJowRhVozLCzeoRwQb4B8ehOvfT
eWB76EtC7ZTO8hLIFQ75Lvut4izcUwPAlliYu5JNDM+9JOxWBM/0cZ0sK9yaonY7
cg4Vmp05nABuRt9dy/8+kIP83tQdJhTjyGrPHvje2B9EfyCiJY7puAKDe/cf0N8i
z6fTdNnLSJzeG9t6/zHdhtlcCKxZvQOyGNAxyZaYR4sdqL/Dk2DcSBITpLbuAwk2
9O6GsCHpOsgySf37ge/7epTEcvhEi50wOZyvsVBHBd6iXd2c4gKpTUDzW819WSV+
2peX9LgRx1855DQ8KKui7hjCQt4Y/c8pmhDR0qC4E1NtSkieu0iGDBjzjaxftZVQ
vBehUPYG5VM3n40PC5SuoZZZAlGsBIKTHa6yWqUfjiKJQ5Kvj9v8NpnQWIc913gR
i8B98qva+GOW/9Dpoe+maawrgVA4WUlP9gZF0x1oDVNLheoQa+YEbFkuktoOQh8j
Ag7zE1TVIWeGO7h7nlsdVDbFzV4se6ni6G1OA841v04zQw1ZROTUqoznjsQXHUk6
54YxfjW9REC6StjTebX21om2ZB9qG6X8f4H0xrk9THojKIt2D/PnK3tbdv4W+XZH
dpAET2gkjYpyhYrsCOsdaQV436cuQcou+tMigrpBfpIOj9jznBfU6O2thwh4TffX
p/Q1dzMkM7RYA8YAcIpUKOZhHhO/Z3Z2iBg7ye1h+Idup/KDq0FchgnCFRNwq62H
WFSQZFizbtnlJX7PIQPHljA9QchWSfWudNeKsHHnHHtBL+WRswjgFbUaoMEudLRV
5pG+ernRQhDspqyPR74jan5iQ5arcieo7tUY72S4xgxI5vF88Nis2uqgtn57qI8O
jtvvNlMOMLYoqNi7dvwteCMIi2cwrf3I619gQ0QB8FQ/QedEXpmenuONSlgYtGdN
HECsldnPBDtyojTAjjErIjyjvIXUodikYxrkLjVEQNwdMR++uEVjDBfD/j/oCMc9
sIKHqT1JLUAzztfWU+1j6bwqncxFaU7G6iNDQ84LZvekk9BJeEXzvr/FdHV1C/t5
MPzb5YZEjx5u/M/t0MpekIyfcECsBGjk6coC3Cf0npohrYEACZc/eChi5f6BKKdb
ZkkHdXAlIwrtWFY/hT+efzW5pHziiWTl4fumo0x2h1hWs2uViIt96OjlZipS8BAm
UrFSneYqkj4hLiFDSSQC2TfyerL+K8YZTdAU5YRFQ5HcpN9rn6r+RcdJRgGFMqYq
kfqwWNvxAoar+d53aNITiN8aW7XmDuPZEmAuRNIR92N8LK0famyHi1Owniv238TX
KR9duL54LmxmHWTDBPjz0Saw3VftHRHtgReSraEg7/WPfFqpb4sKETJ8awGvM83h
fzPf35U1jR29JZj/T3JHg8OdFyan2F7jD7nm7cnuWmJbJlG0bVyfDUeKufb57FvK
MJ4q04xIHqINplBSIhzlPk3omz8TKJg1gy/eeOAt/EnfPzYFoCISEPBY5LnEz0AN
ukGk8INiY4N2UjN2dhqQ5eXnBHAxKMTUPakdmEdHnCVx9FFiNc8pl73KtuF0d7DN
7oN6he7745pojflj0QbSASAarr98qZaJpBXcolse1855jcsWIjKBycELOPJo/vvp
Zi022GdQWfuA1YrS7zNj/zbUMWJuXbHXDopLFy63TxzLQWfXPFjUPmryb0LjnS0M
vH/Ec3ii0vqZT2l1o1QvtvpxJpmvwaRSjl1IFTfUvS1A4iYKNsZaK7Ytyw95uj1y
FPSWlhfyrNosKD/u6+SHYeThtrDur/RKgknzxkzs+2MI/vZQM3lQ0O0czdmp+d4E
X3kSXOmGPslcpcy/YgAhK36iRPu2QD0Iq/kFXFt7I+0s0iYapCQo1H5IxEyp8+F0
v2RUmAk0PQZTAGW207Jf56JzJ9mZqIL8kMKY4VBFX6MkLZoCVJ0fs5yAcZFPZLwa
V4NIuUlj65Jrj9iX0ufGOy40nzPEa6i3ABFZKELzUD1yX3jBJEtjCeoaOqtcS16T
`pragma protect end_protected
