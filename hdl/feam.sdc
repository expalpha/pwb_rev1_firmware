#**************************************************************
# Create Clock
#**************************************************************
#create_clock -period 10.0 [get_ports OSC_100MHz]
create_clock -period 16.0 [get_ports CC_125MHz]
create_clock -period 16.0 [get_ports REFCLK]
# DCO period is 10 for 25 MHz ADC/SCA read (100MHz DDR SERDES) or 20 for 12.5 MHz ADC/SCA Read (50 MHz DDR SERDES)
create_clock -period 10.0 [get_ports ADC1_DCO]
create_clock -period 10.0 [get_ports ADC2_DCO]
# RD_CLK_MON is 40 for 25MHz or 80 for 12.5MHz
create_clock -period 40.0 [get_ports RD_CLK_MON]


#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks

#**************************************************************
# Set Clock Latency
#**************************************************************

create_clock -period 16 -waveform { 8.0 16.0 } -name clk_sca_write_virt
set_output_delay -max -8.0  -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Write[*]]
set_output_delay -min 8.0 -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Write[*]]

set_output_delay -max -8.0  -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Test[*]]
set_output_delay -min 8.0 -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Test[*]]
set_output_delay -max -8.0  -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Test[*](n)]
set_output_delay -min 8.0 -clock [get_clocks clk_sca_write_virt] [get_ports SCA_Test[*](n)]

set_output_delay -max -8.0  -clock [get_clocks clk_sca_write_virt] [get_ports WR_CLK_EN]
set_output_delay -min 8.0 -clock [get_clocks clk_sca_write_virt] [get_ports WR_CLK_EN]

# RD_CLK_MON is 40 for 25MHz or 80 for 12.5MHz
create_clock -period 40 -waveform { 20.0 40.0 }  -name clk_sca_read_virt
set_output_delay -max -20.0 -clock [get_clocks clk_sca_read_virt] [get_ports SCA_Read[*]]
set_output_delay -min 4.0 -clock [get_clocks clk_sca_read_virt] [get_ports SCA_Read[*]]
set_output_delay -max -20.0 -clock [get_clocks clk_sca_read_virt] [get_ports RD_CLK_EN]
set_output_delay -min 4.0 -clock [get_clocks clk_sca_read_virt] [get_ports RD_CLK_EN]

#set_clock_groups -exclusive -group [get_clocks {CC_125MHz clk_sca_write_virt}] -group [get_clocks {RD_CLK_MON clk_sca_read_virt}] -group [get_clocks {pll_main|main_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk pll_main|main_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk pll_main|main_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk pll_main|main_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] -group [get_clocks {altera_reserved_tck}]

#set_clock_groups -asynchronous -group {CC_125MHz} -group {RD_CLK_MON} -group {pll_main|main_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk} -group {pll_main|main_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk} -group {pll_main|main_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk} -group {pll_main|main_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk} -group {altera_reserved_tck}

proc puts_and_post_message { x } {
    # set module [lindex $quartus(args) 0]                                                                                                                
    # if [string match "quartus_map" $module] {                                                                                                           
    # } else {                                                                                                                                            
    if { ([info commands post_message] eq "post_message") || ([info procs post_message] eq "post_message") } {
        puts $x
        post_message $x
    }
    # }                                                                                                                                                   
}

proc print_collection { col } {
    if {[catch {
        foreach_in_collection c $col  {
            puts_and_post_message "   [get_clock_info -name $c]";
        }
    } ] } {
        if {[catch {
            puts_and_post_message "[query_collection $col -all -report_format]"
        } ] } {
            puts_and_post_message "   $col"
        }
    }
}

proc timid_asynchronous { clock_name clockB { max_delay  50.0 } { min_delay  -50 } } {
    set_max_delay -from  $clock_name -to $clockB $max_delay
    set_max_delay -from $clockB -to  $clock_name $max_delay
    set_min_delay -from  $clock_name -to $clockB $min_delay
    set_min_delay -from $clockB -to  $clock_name $min_delay
    #puts_and_post_message "=========================================================================="
    #puts_and_post_message "timid_asynchronous $clock_name from/to $clockB, max_delay = $max_delay min_delay = $min_delay which means:"
    #print_collection $clock_name
    #puts_and_post_message "from/to: "
    #print_collection $clockB
    #puts_and_post_message "=========================================================================="
}

proc timid_asynchronous_group { clock_list { max_delay  50.0 } { min_delay  -50 } } {
    puts [concat "timid_asynchronous_group: " [llength $clock_list] ":" [join $clock_list]]
    for {set i 0 } { $i < [llength $clock_list] } { incr i } {
        for {set j [expr $i + 1] } { $j < [llength $clock_list] } { incr j }  {
            puts [concat "timid_asynchronous clocks " [lindex $clock_list $i] " and " [lindex $clock_list $j]" ]
            timid_asynchronous [lindex $clock_list $i] [lindex $clock_list $j] $max_delay $min_delay;
        }
    }
}

# CC_125MHz
# RD_CLK_MON
# pll_main|main_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk
# pll_main|main_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk
# pll_main|main_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk
# pll_main|main_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk
# altera_reserved_tck

#feam_qsys|ddr|pll0|pll_avl_clk
#feam_qsys|ddr|pll0|pll_config_clk
#feam_qsys|ddr|pll0|pll_write_clk

timid_asynchronous_group {
    OSC_100MHz
    CC_125MHz
    RD_CLK_MON
    pll_main|main_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk
    pll_main|main_pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk
    pll_main|main_pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk
    pll_main|main_pll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk
    backup_link_phy|native_phy_inst|gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_tx_pcs|wys|txpmalocalclk
    backup_link_phy|native_phy_inst|gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|inst_av_pcs|ch[0].inst_av_pcs_ch|inst_av_hssi_8g_rx_pcs|wys|rcvdclkpma
}

# explicit synchronizers
set_false_path -from {delayer:debounce_ext_trig|q} -to {link_tx:link_tx|trig1}
set_false_path -from {link_rx:link_rx|link_rx_status_out} -to {link_tx:link_tx|link_rx_status1}
set_false_path -from {link_rx:link_rx|link_status_out} -to {feam:feam_qsys|gxb_module:gxb_module_link|avalon_stat_slave:avl_stat|r_status[*][*]}
set_false_path -from {link_rx:link_rx|stat_*_rx[*]} -to {feam:feam_qsys|gxb_module:gxb_module_link|avalon_stat_slave:avl_stat|r_status[*][*]}
set_false_path -from {link_tx:link_tx|stat_*_tx[*]} -to {feam:feam_qsys|gxb_module:gxb_module_link|avalon_stat_slave:avl_stat|r_status[*][*]}
set_false_path -from {link_rx:link_rx|link_flow_control_stop_remote_tx_out} -to {link_tx:link_tx|stop_remote_tx1}
set_false_path -from {link_rx:link_rx|link_flow_control_stop_our_tx_out} -to {link_tx:link_tx|stop_our_tx1}

# false-path the tangled DDR avalon-MM bus clocks
set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*} -to {sca_sigproc:sp|sca_event_control:flow_manager|*}
set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*} -to {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|*}
set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*} -to {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|memused[*]}
set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*} -to {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|data1[*]}
#set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF_62} -to {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|data1[29]}

set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|*} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}

# same entry 3 times, no kidding.
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|full1} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|full1} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|full1} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}

# same entry 3 times, no kidding
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|memused[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|memused[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_st_pipeline_stage:agent_pipeline_002|altera_avalon_st_pipeline_base:core|memused[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}

# same entry 3 times, no kidding
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_sc_fifo:ddr_avl_0_agent_rsp_fifo|mem_used[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_sc_fifo:ddr_avl_0_agent_rsp_fifo|mem_used[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}
set_false_path -from {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_sc_fifo:ddr_avl_0_agent_rsp_fifo|mem_used[48]} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}

set_false_path -from {sca_sigproc:sp|sca_event_control:flow_manager|sca_avl_write} -to {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*}

set_false_path -from {feam:feam_qsys|feam_ddr:ddr|altera_mem_if_hard_memory_controller_top_cyclonev:c0|hmc_inst~FF*} -to {feam:feam_qsys|feam_mm_interconnect_0:mm_interconnect_0|altera_avalon_sc_fifo:ddr_avl_0_agent_rsp_fifo|mem_used[*]}

# missing entries in the DDR SDC file per https://www.intel.com/content/www/us/en/programmable/support/support-resources/knowledge-base/solutions/fb64582.html
#set_false_path -from *|*c0|hmc_inst~FF_* -to *p0|*umemphy|*lfifo~LFIFO_IN_READ_EN_DFF
#set_false_path -from *|*p0|*umemphy|hphy_inst~FF_* -to *p0|*umemphy|*vfifo~INC_WR_PTR_DFF
#set_false_path -from *|*c0|hmc_inst~FF_* -to *p0|*umemphy|*vfifo~QVLD_IN_DFF
#set_false_path -from *|*p0|*umemphy|hphy_inst~FF_* -to *p0|*umemphy|*altdq_dqs2_inst|phase_align_os~DFF*

# sata_link_ctrl register is a constant

set_false_path -from {feam:feam_qsys|gxb_module:gxb_module_link|avalon_ctrl_slave:avl_ctrl|ctrl[3][*]}

# Make sure to run the SCA interface slow enough, or use proper timing constraints!
set_false_path -to   [get_ports {SCA_Sc_ck[*]}]
set_false_path -from [get_ports {SCA_Sc_dout[*]}]
set_false_path -to   [get_ports {SCA_Sc_din[*]}]
set_false_path -to   [get_ports {SCA_Sc_en[*]}]
set_false_path -from {sca_sigproc:sp|sca_event_control:flow_manager|sca_fifo_rst}

#set_false_path -from {sca_sigproc:sp|sca_event_control:flow_manager|event_not_ready}
set_max_delay -from {sca_sigproc:sp|sca_event_control:flow_manager|event_not_ready} 100
set_min_delay -from {sca_sigproc:sp|sca_event_control:flow_manager|event_not_ready} -100

#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

# We go directly out, avoid delay chains on this clock
set_false_path -to [get_ports {CLOCK_CDR_REF_OUT}]
set_false_path -to [get_ports {CLOCK_CDR_REF_OUT(n)}]

# Phase Domain must be used correctly, but allows a false path
set_false_path -from [get_registers {*|phase_domain_changer:*|ram*}] -to [get_registers {*|phase_domain_changer:*|q*}]

# Fixes DDR timing complaint, this is as good as we can get, the DDR reset is latched by an internal clock "feam_qsys|ddr|pll0|pll_write_clk"
set_false_path -from {delayer:rst_delay|q}

# Reset Synchronizers
set_false_path -from [get_registers {*|r_sync_rst}]

set_false_path -from [get_registers {*|altera_reset_synchronizer_int_chain_out}]
set_false_path -from {native_phy:backup_link_phy|altera_xcvr_native_av:native_phy_inst|av_xcvr_native:gen_native_inst.av_xcvr_native_insts[0].gen_bonded_group_native.av_xcvr_native_inst|av_pcs:inst_av_pcs|av_pcs_ch:ch[0].inst_av_pcs_ch|av_hssi_8g_rx_pcs_rbc:inst_av_hssi_8g_rx_pcs|wys~BURIED_SYNC_DATA*}
set_false_path -from {reset_synchronizer:sync_wr_reset|q} -to {sca_sigproc:sp|sca_event_control:flow_manager|event_descriptor_fifo:event_fifo|dcfifo:dcfifo_component|dcfifo_pvp1:auto_generated|dffpipe_3dc:rdaclr|dffe10a[0]}

# Synchronizers get a pass
#set_false_path -to [get_registers 	{*|synchronizer:*|register:data_sync[0].sync_reg|*}]
#set_false_path -to [get_registers	 {synchronizer:*|register:data_sync[0].sync_reg|*}]
#set_false_path -from [get_registers {*|synchronizer:*|register:data_sync[2].sync_reg|*}]
#set_false_path -from [get_registers {synchronizer:*|register:data_sync[2].sync_reg|*}]
#set_false_path -from {delayer:rst_delay|q} -to {synchronizer:sync_global_reset|register:data_sync[2].sync_reg|r_q[0]}
#set_false_path -from {delayer:rst_delay|q} -to {synchronizer:sync_global_reset|register:data_sync[1].sync_reg|r_q[0]}

# This *could* be moved to a virtual clock, but signal is immediately synchronized then debounced
#set_false_path -from	[get_ports {Ext_Trigger}]

#set_instance_assignment -name SYNCHRONIZER_IDENTIFICATION "FORCED IF ASYNCHRONOUS" -to [get_registers {*synchronizer:*|r_sync*}]
set_false_path -to [get_registers {*synchronizer:*|r_sync*}]
#set_false_path -from [get_registers {*synchronizer:*|r_sync*}]
set_false_path -from [get_registers {*synchronizer:*|r_sync*}] -to [get_registers {*synchronizer:*|r_sync*}]
set_false_path -from [get_registers {*synchronizer_counter:*|bin2gray:count_to_gray|r_gray*}] 

set_false_path -from [get_registers {*synchronizer_counter:sync_count|synchronizer:sync_gray|r_sync*}] -to [get_registers {*synchronizer_counter:sync_count|gray2bin:gray_to_count|r_bin*}]

set_false_path -from {feam:feam_qsys|sigproc:sigproc|avalon_ctrl_slave:avl_ctrl|ctrl[*][*]} -to {sca_sigproc:sp|sca_event_control:flow_manager|ch_forced[*][*]}
set_false_path -from {feam:feam_qsys|sigproc:sigproc|avalon_ctrl_slave:avl_ctrl|ctrl[*][*]} -to {sca_sigproc:sp|sca_event_control:flow_manager|ch_enabled[*][*]}

# These pins genuinely never matter, always false_path
set_false_path -from [get_ports {LMK_Stat_CLKin1}]
set_false_path -from [get_ports {LMK_SYNC_CLKin2}]
set_false_path -from [get_ports {LMK_StatLD}]
set_false_path -from [get_ports {LMK_Stat_Hold}]
set_false_path -to   [get_ports {ADC1_PWR_EN}]
set_false_path -to   [get_ports {ADC2_PWR_EN}]
set_false_path -from	[get_ports {SFP_ModDet}]
set_false_path -from	[get_ports {SFP_LOS}]
set_false_path -from	[get_ports {SFP_TxFault}]

# SPI false pathing
set_false_path -to   [get_ports {Ext_SPI1}]
set_false_path -to   [get_ports {Ext_SPI2}]
set_false_path -to   [get_ports {Ext_SPI3}]
set_false_path -to   [get_ports {Ext_SPI4}]

set_false_path -to   [get_ports {Temp_CLK}]
set_false_path -from [get_ports {Temp_SDO}]
set_false_path -to   [get_ports {Temp_SDI}]
set_false_path -to   [get_ports {Temp_CSn}]
set_false_path -to   [get_ports {Temp_RSTn}]

set_false_path -to   [get_ports {Temp_CLK}]
set_false_path -from [get_ports {Temp_SDO}]
set_false_path -to   [get_ports {Temp_SDI}]
set_false_path -to   [get_ports {Temp_CSn}]
set_false_path -to   [get_ports {Temp_RSTn}]

set_false_path  -to	[get_ports {LMK_CLK}]
set_false_path -from [get_ports {LMK_Stat_CLKin0}]
set_false_path  -to	[get_ports {LMK_Data}]
set_false_path  -to	[get_ports {LMK_LE}]

set_false_path -to   [get_ports {ADC1_SCK}]
set_false_path -from [get_ports {ADC1_SDO}]
set_false_path -to   [get_ports {ADC1_SDI}]
set_false_path -to   [get_ports {ADC1_CSn}]

set_false_path -to   [get_ports {ADC2_SCK}]
set_false_path -from [get_ports {ADC2_SDO}]
set_false_path -to   [get_ports {ADC2_SDI}]
set_false_path -to   [get_ports {ADC2_CSn}]

# I2C false pathing
set_false_path -to 	[get_ports {MAC_SCL}]
set_false_path -to 	[get_ports {MAC_SDA}]
set_false_path -from	[get_ports {MAC_SCL}]
set_false_path -from	[get_ports {MAC_SDA}]

set_false_path -to 	[get_ports {SFP_SCL}]
set_false_path -to 	[get_ports {SFP_SDA}]
set_false_path -from	[get_ports {SFP_SCL}]
set_false_path -from	[get_ports {SFP_SDA}]

# Altera JTAG, lets not worry about these either
set_false_path -from [get_ports {altera_reserved_tdi}]
set_false_path -from [get_ports {altera_reserved_tms}]
set_false_path -from	[get_ports {altera_reserved_tdo}]
set_false_path -from	[get_ports {altera_reserved_tck}]
set_false_path -to	[get_ports {altera_reserved_tdi}]
set_false_path -to 	[get_ports {altera_reserved_tms}]
set_false_path -to	[get_ports {altera_reserved_tdo}]
set_false_path -to	[get_ports {altera_reserved_tck}]

set_false_path -to [get_registers {sld_signaltap:*}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************



#**************************************************************
# Set Load
#**************************************************************

