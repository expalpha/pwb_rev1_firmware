module adc_data_aligner16 (	
	// ADC SERDES Output
	clk,
	rst,
	soft_rst,
	randomizer12,
	randomizer14,
	rx_clk,
	q,
		
	// Diagnostics
	locked,
	aligned,
	
	// ADC Input lines
	fco,  
	dco,  
	d
);

parameter PATTERN 		= 8'hF0;
parameter NUM_ADC 		= 2; // Number of ADCs
parameter NUM_CH	 		= 2; // Number of Channels per ADC
parameter SZ_DATA_IN 	= 2; // Number of bits per ADC channel pre-deserializer
parameter SZ_FCO_IN  	= 1; // Number of bits in FCO pre-deserializer
parameter SZ_DS_RATE	 	= 8; // Deserializer rate (in-to-out bits ratio)

localparam TOTAL_CH		= NUM_ADC*NUM_CH;
localparam SZ_FCO	 = (SZ_FCO_IN * SZ_DS_RATE);  // Outgoing FCO size
localparam SZ_DATA = (SZ_DATA_IN * SZ_DS_RATE); // Outgoing Data size
localparam NUM_CH_IN = (NUM_CH * SZ_DATA_IN) + (SZ_FCO_IN); // 8 data lines + FCO 

input wire  									  		clk;
input wire  					  				  		rst;
input wire												soft_rst;
input wire randomizer12;
input wire randomizer14;
input wire [NUM_ADC-1:0][SZ_FCO_IN-1:0]  		fco;
input wire [NUM_ADC-1:0] 					  		dco;
input wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA_IN-1:0] d;

output wire [NUM_ADC-1:0]  						locked;
output wire [NUM_ADC-1:0]  						aligned;
output wire [NUM_ADC-1:0]							rx_clk;
output reg [NUM_ADC-1:0][NUM_CH-1:0][11:0] 		q; // changed since the ADC is actually 12 bits here!

wire [NUM_ADC-1:0][NUM_CH-1:0][SZ_DATA-1:0] 	adc_data;
wire [NUM_ADC-1:0][SZ_FCO-1:0]    			  	adc_fco;	// only used internally to align
wire [NUM_ADC-1:0]									int_aligned;
wire [NUM_ADC-1:0]					 				bitslip;
wire [NUM_ADC-1:0]  									int_locked;

genvar n;
genvar i;

generate
for(n=0; n<NUM_ADC; n++) begin: gen_adc

	
	// Outgoing status bits
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_locked		( .clk	( clk ),	.rst	( rst ), .d ( int_locked[n] ),	.q ( locked[n] ));
	synchronizer #( .SZ_DATA( 1 ) ) sync_adc_aligned 	( .clk	( clk ),	.rst	( rst ), .d ( int_aligned[n] ),	.q ( aligned[n] ));	
	
	// Instantiate hard IP SERDES block 
	adc_serdes serdes_inst (
		.pll_areset 				( rst ),
		.rx_channel_data_align	( {NUM_CH_IN{bitslip[n]}} ),
		.rx_in 						( {d[n], fco[n]} ),
		.rx_inclock					( dco[n] ), 
		.rx_out 						( { adc_data[n], adc_fco[n] } ),
		.rx_outclock 				( rx_clk[n] ),
		.rx_locked 					( int_locked[n] )
	);

	// Trainer works in SERDES clock domain
	// Trains continously, and solely against FCO
	adc_trainer #(
		.NUM_CH(1),
		.SZ_CH(SZ_FCO)
	) train_inst (
		.clk			( rx_clk[n] ),	
		.training	( 1'b1 ),
		.aligned		( int_aligned[n] ),
		.bitslip		( bitslip[n] ),
		.pattern		( PATTERN ),
		.d_in			( adc_fco[n] )		
	);
	
	for(i=0; i<NUM_CH; i++) begin: gen_adc_ch
		integer k;
		reg [SZ_DATA-1:0] adc_data_unrandomized;
		wire [SZ_DATA-1:0] adc_data_out;
		
		// interleave data as according to ADC
		wire [SZ_DATA-1:0] adc_data_interleaved = {
			adc_data[n][i][7], 	adc_data[n][i][15],  adc_data[n][i][6], 	adc_data[n][i][14],
			adc_data[n][i][5],	adc_data[n][i][13],	adc_data[n][i][4],	adc_data[n][i][12],
			adc_data[n][i][3],	adc_data[n][i][11],	adc_data[n][i][2],	adc_data[n][i][10],
			adc_data[n][i][1],	adc_data[n][i][9],	adc_data[n][i][0],	adc_data[n][i][8] };
		
		always@(posedge rx_clk[n]) begin 	
			if(randomizer12) begin 
				for(k=5; k < SZ_DATA; k = k + 1) begin : gen_unrandomizer12_xor
					adc_data_unrandomized[k] <= adc_data_interleaved[k] ^ adc_data_interleaved[4];
				end
				adc_data_unrandomized[3:0] <= adc_data_interleaved[3:0];
			end else if(randomizer14) begin 
				for(k=5; k < SZ_DATA; k = k + 1) begin : gen_unrandomizer14_xor
					adc_data_unrandomized[k] <= adc_data_interleaved[k] ^ adc_data_interleaved[2];
				end
				adc_data_unrandomized[3:0] <= adc_data_interleaved[3:0];
			end else begin 
				adc_data_unrandomized <= adc_data_interleaved; 
			end	
		end 
		
		wire clk_rst_n;
		wire rx_clk_rst_n;

		wire clk_rst = ~clk_rst_n;
		wire rx_clk_rst = ~rx_clk_rst_n;
		
		wire rst_phasematcher_n = ~(soft_rst);

		reset_n_synchronizer phase_reset0 (
			.clk( clk ),
			.d( rst_phasematcher_n ),
			.q( clk_rst_n )
		);

		reset_n_synchronizer phase_reset1 (
			.clk( rx_clk[n] ),
			.d( rst_phasematcher_n ),
			.q( rx_clk_rst_n )
		);

		
		// Put the ADC data back into the RD_CLK_MON (SCA) clock domain
		wire [11:0] adc_data_sync;
		
		phase_domain_changer #(
			.SZ_DATA	( 12 ),
			.DEPTH 	(8)
		) phase_fifo (  
			.wr_clk	( rx_clk[n] ),
			.wr_rst	( rx_clk_rst ),
			.rd_clk	( clk ),
			.rd_rst	( clk_rst ) ,
			.d			( adc_data_unrandomized[15:4] ),
			.q			( adc_data_sync )
		);
		
		always@(posedge clk) begin
			q[n][i] <= adc_data_sync;
		end
				
	end 
end
endgenerate

endmodule
