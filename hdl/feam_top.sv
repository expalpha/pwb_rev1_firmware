// TRIUMF Electronics Group
// Acquisition module for AlphaG TPC detector

//
// Clock frequencies from Timequest:
//
// OSC_100MHz - 10 ns - 100 MHz   (main clock from clock cleaner OSC0)
// REFCLK     - 16 ns -  62.5 MHz (sata link clock from clock cleaner OUT2)
// CC_125MHz  - 16 ns -  62.5 MHz (sca write clock from clock cleaner OUT3)
// RD_CLK_MON - 40 ns -  25 MHz   (sca read clock  from clock cleaner OUT4)
// ADC1_DCO   - 10 ns - 100 MHz   (from external)
// ADC2_DCO   - 10 ns - 100 MHz   (from external)
// DDR3_CK    -  2.2  - 450 MHz   (from NIOS/Qsys)
// adc1_serdes        -  25 MHz
// adc2_serdes        -  25 MHz
// altera_reserved_tck - 30 MHz
// backup_link_phy    - 125 MHz
// clk_sca_read_virt  -  25 MHz
// clk_sca_write_virt -  62.5 MHz
// eth_tse     - 125 MHz
// pll_main[0] - 125 MHz (PLL from clk_osc_100)
// pll_main[1] - 100 MHz
// pll_main[2] -  62.5 MHz
// pll_main[3] -  12.5 MHz
// pll_ref     - 125 MHz (ethernet clock? PLL from clk_osc_100)
//
// clk_osc_100 - alias for OSC_100MHz
//
// DDR clocks:
//
// pll_afi_clk      -  2.222 ns - 450 MHz
// pll_avl_clk      - 15.555 ns - 64.29 MHz
// pll_avl_phy_clk  - same
// pll_config_clk   - 46.666 ns - 21.43 MHz
// pll_dq_write_clk - 450 MHz
// pll_write_clk    - 450 MHz
//
// Clock cleaner clocks:
//
// CLK_OSC0 - Bnk78_125MHz  - to FPGA Bank7A8A H16/H15 - pin OSC_100MHz
// CLK_OUT0 - ADC_CLK_SCA34 - 25 MHz   - to FrontEnd1
// CLK_OUT1 - ADC_CLK_SCA12 -          - to FrontEnd2
// CLK_OUT2 - XCVR_125MHz   - 62.5 MHz - to FPGA_XCVR REFCLK0L V4/U4 - pin REFCLK
// CLK_OUT3 - lvds_125Mhz   -          - to FPGA Bank3 N9/P9      - pin CC_125MHz
// CLK_OUT4 - RD_CLK_Mon    - 25 MHz   - to FPGA Bank4 U13/V13    - pin RD_CLK_MON
// CLK_OUT5 - RDCLK_SCA     -          - to SCA read clock only
// CLK_OUT6..9 - unconnected
// CLK_OUT10 - unconnected  - 62.5 MHz - unconnected
// CLK_OUT11 - WRCLK_SCA    -          - to SCA write clock only
//
// Clock cleaner power up state:
//
// Pins:
//
// Stat_CLKin0 "input with pull-down" (input clock pin select)
// Stat_CLKin1 "input with pull-down" (input clock pin select)
// SYNC_CLKin2 "input with pull-up"   (SYNC)
// StatLD      "output (push-pull) PLL1 & PLL2 LDD"
// Stat_Hold   "output (push-pull) MICROWIRE readback"
//
// PLLs:
//
// MODE 0 (dual PLL, internal VCO)
// CLKin0_PreR_DIV 0 (62.5 MHz)
// CLKin1_PreR_DIV 0 (from FPGA)
// CLKin2 PreR_DIV 0 (125 MHz)
// PLL1_R1 96 (0.651/1.302 MHz)
// PLL1_N 192 (125/250 MHz OSCin/VCXO frequency)
//

`default_nettype none

module feam_top
  (
   input   OSC_100MHz, // free running oscillator output from the clock cleaner (main clock. KO Aug 2020)
   input   CC_125MHz,  // Ethernet clock (same clock as to REFCLK) (Actually: SCA write clock, 62.5 MHz. KO Aug 2020)
   
   output  CLOCK_CDR_REF_OUT, // SATA link recovered clock divided by 2 output to clock cleaner, 62.5 MHz (KO Aug 2020)
   
   input   Ext_Trigger,     // external trigger on the clock line connector

   // external SPI connector
   output  Ext_SPI1,        // 2.5V SCLK
   output  Ext_SPI2,        // 2.5V MOSI
   input   Ext_SPI3,        // 2.5V MISO
   output  Ext_SPI4,        // 2.5V CS_N

   // temperature readout chip
   output  Temp_CLK,        // 2.5V
   input   Temp_SDO,        // output level from LTC2983 is 3.3V cmos
   output  Temp_SDI,        // 2.5V 
   output  Temp_CSn,        // 2.5V
   output  Temp_RSTn,       // 2.5V

   // clock cleaner LMK04816
   output  LMK_CLK,         // 2.5V
   output  LMK_Data,        // 2.5V, MOSI
   output  LMK_LE,          // 2.5V
   output  LMK_Stat_CLKin0, // 2.5V CLKin pin select (was MISO)
   output  LMK_Stat_CLKin1, // 2.5V CLKin pin select
   input   LMK_SYNC_CLKin2, // 2.5V MISO
   input   LMK_StatLD,      // 2.5V, lock detect PLL1 and PLL2 
   input   LMK_Stat_Hold,   // 2.5V, defaults to uWire readback on powerup
   
   input   REFCLK,          // 62.5 MHz sata link clock

   // ethernet [0] and sata link [1]
   input  [1:0] GXB_RX,
   output [1:0] GXB_TX,

   // ADC1
   output  ADC1_PWR_EN, // Enable power to SCA 3.3V regulator for SCA 1 & 2
   input   ADC1_Out1A,  // LVDS
   input   ADC1_Out1B,  // LVDS
   input   ADC1_Out2A,  // LVDS
   input   ADC1_Out2B,  // LVDS
   input   ADC1_FR,     // LVDS
   input   ADC1_DCO,  	// LVDS
   output  ADC1_SCK,    // 2.5V level out to 2.5V - 1.8V Translator
   input   ADC1_SDO,    // 1.8V level from ADC		
   output  ADC1_SDI,    // 2.5V level out to 2.5V - 1.8V Translator	
   output  ADC1_CSn,    // 2.5V level out to 2.5V - 1.8V Translator

   // ADC2
   output  ADC2_PWR_EN, // Enable power to SCA 3.3V regulator for SCA 3 & 4
   input   ADC2_Out1A,  // LVDS
   input   ADC2_Out1B,  // LVDS
   input   ADC2_Out2A,  // LVDS
   input   ADC2_Out2B,  // LVDS
   input   ADC2_FR,     // LVDS
   input   ADC2_DCO,    // LVDS CURRENTLY NOT IN USE
   output  ADC2_SCK,    // 2.5V level out to 2.5V - 1.8V Translator
   input   ADC2_SDO,    // 1.8V level from ADC		
   output  ADC2_SDI,    // 2.5V level out to 2.5V - 1.8V Translator	
   output  ADC2_CSn,    // 2.5V level out to 2.5V - 1.8V Translator

   // DDR3 DRAM
   output [13:0] DDR3_CA, // DDR3 ram uses 1.5V SSTL I/O standard
   output [2:0]  DDR3_BA,
   inout  [31:0] DDR3_DQ,
   inout  [3:0]  DDR3_DQS_p,
   inout  [3:0]  DDR3_DQS_n,
   output        DDR3_RAS_n,
   output        DDR3_CAS_n,
   output [3:0]  DDR3_DM,
   output        DDR3_WE_n,
   output        DDR3_CKE,		
   output        DDR3_CK_p,
   output        DDR3_CK_n,
   output [0:0]  DDR3_CS_n,
   output        DDR3_ODT,		
   input         DDR3_OCT_RZQ, // precision 240 ohm resistor to ground for OCT
   output        DDR3_RESET_n,
   output        DDR3_Vtt_EN,  // Enable power to Vtt 0.75V regulator

   // ethernet MAC chip
   inout    MAC_SCL,     // 2.5V
   inout    MAC_SDA,     // 2.5V

   // SFP module
   input    SFP_ModDet,  // 3.3V
   input    SFP_LOS,     // 3.3V	
   input    SFP_TxFault, // 3.3V
   inout    SFP_SDA,     // 2.5V
   inout    SFP_SCL,     // 2.5V

   // SCA control
   output [3:0]     SCA_Sc_ck,
   output [3:0]     SCA_Sc_din,
   input  [3:0]     SCA_Sc_dout,
   output [3:0]     SCA_Sc_en,
   output reg [3:0] SCA_Test,
   output [3:0]     SCA_Read,
   output [3:0]     SCA_Write,
   
   input    RD_CLK_MON,  // SCA read clock
   
   output   RD_CLK_EN,
   output   WR_CLK_EN
);

localparam NUM_TRIGGER_SOURCES = 6;
localparam NUM_SCA = 4;
localparam SZ_TIMESTAMP = 48;
localparam MAX_SCA_BINS = 511;
localparam SZ_ADC_DATA = 12;

localparam NUM_SCA_CH = 79;

localparam SZ_TRIG_SRC = (NUM_TRIGGER_SOURCES > 1) ? $clog2(NUM_TRIGGER_SOURCES) : 1;
localparam SZ_TRIG_DLY = 16;

localparam NUM_MEM_SLOTS = 32;
localparam SZ_MEM_ADDR  = 29;

assign DDR3_Vtt_EN = 1'b1;

assign LMK_Stat_CLKin0 = 1'b1; // see clock cleaner description of Active Clock Input - Pin Select Mode - here we try to select the internal oscillator CLKin2 clock by driving LMK_Stat_CLKin0 to 0 and LMK_Stat_CLKin1 to 1.
assign LMK_Stat_CLKin1 = 1'b1; // see clock cleaner description of Active Clock Input - Pin Select Mode - here we try to select the internal oscillator CLKin2 clock by driving LMK_Stat_CLKin0 to 0 and LMK_Stat_CLKin1 to 1.

wire Ext_SPI_MOSI;
wire Ext_SPI_MISO;
wire Ext_SPI_CSn;
wire Ext_SPI_CLK;

wire [1:0][139:0] reconfig_to_gxb;
wire [1:0][91:0] reconfig_from_gxb;
wire [1:0] adc_locked;
wire [1:0] adc_aligned;
wire [NUM_SCA-1:0][SZ_ADC_DATA-1:0] adc_data;

wire [NUM_SCA-1:0] sca_enable;
wire [8:0] sca_samples_to_read;


wire serdes_rst;
wire [1:0] adc_randomizer_en;
wire use_sata_gxb;

wire clk_rcvd_sfp;
wire clk_rcvd_link;

wire ddr_done;
wire ddr_cal_fail;
wire ddr_cal_success;
wire run_status;

wire clk_sca_rd = RD_CLK_MON;
wire [1:0] clk_serdes;
wire clk_sca_wr;

wire [4:0] sca_delay_amount;
wire [3:0] sca_read_request;

// send out a pulse every 256 clocks, guaranteed to be somewhere in a collected sample
reg [8:0] cnt;
reg [3:0] sca_test_regs;

wire sp_run;
wire sp_rst;

wire sp_manual_trig;
wire [NUM_SCA-1:0] sp_test_pulse_ena;
wire [NUM_SCA-1:0][15:0] sp_test_pulse_width;
wire [SZ_TIMESTAMP-1:0] sp_ts_run;
wire [NUM_SCA-1:0][7:0] sp_start_delay;

wire triggered;
assign run_status = sp_run;

wire clk_osc_100 = OSC_100MHz;

wire			sca_avl_beginbursttransfer;
wire 			sca_avl_waitrequest_n;
wire [7:0] 	sca_avl_burstcount;
wire [SZ_MEM_ADDR-1:0] sca_avl_address;
wire [63:0] sca_avl_writedata;
wire [7:0] 	sca_avl_byteenable;
wire 			sca_avl_write;

   wire                 eth_avl_readdatavalid;
   wire [7:0]           eth_avl_burstcount;
   wire 		eth_avl_waitrequest_n;
   wire 		eth_avl_beginbursttransfer;
   wire [SZ_MEM_ADDR-1:0] eth_avl_address;
   wire [127:0]           eth_avl_readdata;
   wire [15:0]            eth_avl_byteenable;
   wire                   eth_avl_read;

wire busy;

wire [NUM_SCA-1:0] 			event_val;
wire [NUM_SCA-1:0] 			event_rdy;
wire [NUM_SCA-1:0][31:0]  	event_dat;
wire [NUM_SCA-1:0][1:0] 	event_epy;
wire [NUM_SCA-1:0] 			event_sop;
wire [NUM_SCA-1:0] 			event_eop;

   wire event_not_ready;

wire  		udp_event_val_out;
wire  		udp_event_rdy_eth;
wire  		udp_event_rdy_sata_link;
wire [31:0] udp_event_dat;
wire [1:0] 	udp_event_epy;
wire  		udp_event_sop;
wire  		udp_event_eop;

wire  		event_segment_val;
wire  		event_segment_rdy;
wire [31:0] event_segment_dat;
wire  		event_segment_sop;
wire  		event_segment_eop;

wire trigger; 										// Trigger Signal generated by Trigger Decision block (located in QSYS)
wire [SZ_TRIG_DLY-1:0] trigger_delayed; 	// Amount trigger signal was delayed from initial request
wire [SZ_TRIG_SRC-1:0] trigger_source; 	// Source of trigger request

wire ext_trigger_src;			// External Trigger Pin (use to be 'sata' trigger, now a pin on a header)
wire man_trigger_src;			// Generated by NIOS/ESPER
wire periodic_trigger_src;		// TBI
wire int_pulse_trigger_src; 	// Generated by internal test pulse code
wire ext_pulse_trigger_src;	// Generated by external test pulse code

wire ext_trigger_src_sync;

wire invert_ext_trigger; // invert external signal Ext_Trigger before it drives any logic

// Disable trigger sources we haven't finished implementing yet
assign periodic_trigger_src = 1'b0;
assign man_trigger_src = sp_manual_trig;
assign ext_pulse_trigger_src = 1'b0;

/*
assign Ext_SPI1 = ((Ext_SPI_CSn == 0) && Ext_SPI_CLK) ? 1'bZ : 1'b0;
assign Ext_SPI2 = ((Ext_SPI_CSn == 0) && Ext_SPI_MOSI) ? 1'bZ : 1'b0;
assign Ext_SPI_MISO = Ext_SPI3;
assign Ext_SPI4 = (Ext_SPI_CSn == 0) ? 1'b0 : 1'bZ;
*/

assign Ext_SPI1 = Ext_SPI_CLK;
assign Ext_SPI2 = Ext_SPI_MOSI;
assign Ext_SPI_MISO = Ext_SPI3;
assign Ext_SPI4 = Ext_SPI_CSn;

// test Ext_SPI output drivers
//assign Ext_SPI1 = clk_12_5;
//assign Ext_SPI2 = clk_12_5;
//assign Ext_SPI_MISO = Ext_SPI3;
//assign Ext_SPI4 = clk_12_5;

wire xTemp_CLK;
wire xTemp_SDI;
wire xTemp_CSn;
wire xTemp_RSTn;

// test Temp_SPI output drivers
//assign Temp_CLK  = clk_12_5;
//assign Temp_SDI  = clk_12_5;
//assign Temp_CSn  = clk_12_5;
//assign Temp_RSTn = clk_12_5;

assign Temp_CLK  = xTemp_CLK;
assign Temp_SDI  = xTemp_SDI;
assign Temp_CSn  = xTemp_CSn;
assign Temp_RSTn = xTemp_RSTn;

// Synchronize external trigger to internal trigger clock
synchronizer ext_trigger_sync (
	.clk( clk_sca_wr ),
	.rst( clk_sca_wr_rst ),
	.d( Ext_Trigger ^ invert_ext_trigger ),
	.q( ext_trigger_src_sync )
);

// De-bounce synced external trigger signal
delayer #(
	.DELAY_CNT( 4 )
) debounce_ext_trig (
	.clk	( clk_sca_wr ),
	.rst	( clk_sca_wr_rst ),
	.d		( ext_trigger_src_sync ),
	.q		( ext_trigger_src )
);

////////////////////////////////////////
// Startup Reset Sequencing and main PLL
////////////////////////////////////////

wire xpoweron_rst_n;

// 500ms delay
delayer #(
	.DELAY_CNT( 50000000 )
) initial_rst_delay (
	.clk	( clk_osc_100 ),
	.rst	( 1'b0 ),
	.d		( 1'b1 ),
	.q		( xpoweron_rst_n )
);

reg reset_from_nios = 0;
reg reset_from_nios1 = 0;
reg reset_from_nios2 = 0;
reg reset_from_nios3 = 0;
reg reset_from_nios4 = 0;
reg xuse_sata_gxb = 0;
   
always_ff @(posedge clk_osc_100) begin
   if (use_sata_gxb & (!xuse_sata_gxb)) begin
      reset_from_nios <= 1;
   end else begin
      reset_from_nios <= 0;
   end
   xuse_sata_gxb <= use_sata_gxb;
   reset_from_nios1 <= reset_from_nios;
   reset_from_nios2 <= reset_from_nios1;
   reset_from_nios3 <= reset_from_nios2;
   reset_from_nios4 <= reset_from_nios3;
end // always_ff @ (posedge clk_osc_100)

wire reset_from_niosx = reset_from_nios | reset_from_nios1 | reset_from_nios2 | reset_from_nios3 | reset_from_nios4;

wire sata_link_reboot_pulse3;  // reboot command received from sata link
   
wire poweron_rst = (~xpoweron_rst_n) | reset_from_niosx | sata_link_reboot_pulse3;
//wire poweron_rst = (~xpoweron_rst_n);

wire pll_main_locked;
wire clk_125;
wire clk_100;
wire clk_62_5;
wire clk_12_5;

main_pll pll_main (
	.rst		( poweron_rst ),
	.locked 	( pll_main_locked ),
	.refclk		( clk_osc_100 ),
	.outclk_0 	( clk_125 ),
	.outclk_1	( clk_100 ),
	.outclk_2	( clk_62_5 ),
	.outclk_3	( clk_12_5 )
);

wire pll_ref_locked;
wire clk_ref_125;

ref_pll pll_ref (
	.rst		( poweron_rst ),
	.locked 	( pll_ref_locked ),
	.refclk		( clk_osc_100 ),
	.outclk_0 	( clk_ref_125 )
);

wire pll_rst_n;

// 250ms delay
delayer #(
	.DELAY_CNT( 25000000 )
) rst_delay (
	.clk	( clk_osc_100 ),
	.rst	( poweron_rst ),
	.d		( pll_main_locked & pll_ref_locked ),
	.q		( pll_rst_n )
);

// Keep track of the PLL reset count to see if we are losing the PLL intermittently after power-up
wire [31:0] pll_rst_count;
pattern_counter #(
	.SZ_PATTERN( 2 )
) pll_rst_counter (
	.clk		( clk_osc_100 ),
	.clk_cap	( clk_osc_100 ),
	.rst 		( 1'b0 ),
	.pattern 	( 2'b10 ),	// Pattern to match against
	.d			( pll_rst_n ),			// Input signal to pattern match 
	.q			( pll_rst_count )// Output Count
);

assign clk_sca_wr = CC_125MHz; 
wire clk_sca_wr_rst;
wire clk_sca_rd_rst;

wire clk_sca_wr_rst_sync;
reset_synchronizer sync_wr_reset		( .clk	( clk_sca_wr ),	.d ( serdes_rst ), 		.q ( clk_sca_wr_rst_sync ));
oneshot wr_rst_os ( 
	.clk	( clk_sca_wr ),
	.rst	( 1'b0 ),
	.d		( clk_sca_wr_rst_sync ),
	.q		( clk_sca_wr_rst )
);
//wire clk_sca_wr_rst_n = ~clk_sca_wr_rst;


wire clk_sca_rd_rst_sync;
reset_synchronizer  sync_rd_reset		( .clk	( clk_sca_rd ),	.d ( serdes_rst ), 		.q ( clk_sca_rd_rst_sync ));
oneshot rd_rst_os ( 
	.clk	( clk_sca_rd ),
	.rst	( 1'b0 ),
	.d		( clk_sca_rd_rst_sync ),
	.q		( clk_sca_rd_rst )
);


wire clk_100_rst_n;
wire clk_125_rst_n;
wire clk_62_5_rst_n;
wire clk_12_5_rst_n;

wire clk_100_rst = ~clk_100_rst_n;
wire clk_125_rst = ~clk_125_rst_n;
//wire clk_62_5_rst = ~clk_62_5_rst_n;
//wire clk_12_5_rst = ~clk_12_5_rst_n;

reset_n_synchronizer sync_rst_125 ( .clk( clk_125 ), .d( pll_rst_n ), .q( clk_125_rst_n ));
reset_n_synchronizer sync_rst_100 ( .clk( clk_100 ), .d( pll_rst_n ), .q( clk_100_rst_n ));
reset_n_synchronizer sync_rst_62_5 ( .clk( clk_62_5 ), .d( pll_rst_n ), .q( clk_62_5_rst_n ));
reset_n_synchronizer sync_rst_6_25 ( .clk( clk_12_5 ), .d( pll_rst_n ), .q( clk_12_5_rst_n ));

///////////////////////////////
// ADC SERDES AND ALIGNER BLOCK
///////////////////////////////

/*
The ADC data arrives as two sets of 8 bits per channel, with two channels per ADC
The ADC has been swapped out from 14 to 12 bits, so although it is represented as 16 bits here
it is trimmed down to 12-bits prior to leaving the 'data aligner' block so as to save resources
the data is sign-extended when written to the DDR, and not before.

Simplistic view of the SERDES data format:
FCO 11110000
A1	 DDDDDDDD
B1	 DDDDDDDD

FCO 11110000
A2	 DDDDDDDD
B2	 DDDDDDDD

*/

// Put the SERDES in reset if either SERDES is not locked but IS powered
wire serdes_soft_rst = ( ~adc_locked[0] & ADC1_PWR_EN) | ( ~adc_locked[1] & ADC2_PWR_EN);

adc_data_aligner16 #(
	.PATTERN		( 8'hF0 ),
	.SZ_FCO_IN	( 1 ), // Number of bits in FCO pre-deserializer
	.NUM_ADC		( 1 ), // Number of ADCs
	.NUM_CH		( 2 ), // Number of Channels per ADC
	.SZ_DATA_IN	( 2 ), // Number of bits per ADC channel pre-deserializer
	.SZ_DS_RATE	( 8 )	 // Deserializer rate (in-to-out bits ratio)
) adc1_serdes ( 
	.clk			( clk_sca_rd ),						// Internal Data clock domain
	.rst			( clk_sca_rd_rst ),		// Reset	
	.soft_rst	( serdes_soft_rst ),
	.randomizer12 ( adc_randomizer_en[0] ),				// Must match the ADC randomizer setting set via SPI
	.randomizer14 ( adc_randomizer_en[1] ),				// Must match the ADC randomizer setting set via SPI
	.fco			( ADC1_FR ),  							// Frame Clock, align the incoming adc data to this
	.dco			( ADC1_DCO ),  						// Data Clock sent by ADC	
	.d				( {
						// note: ADC channel swap between 1 and 2 is done to match schematics, SCA0 = ADC1_Out2, SCA1 = ADC1_Out1, SCA2 = ADC2_Out2, SCA3 = ADC2_Out1
						{ADC1_Out1B, ADC1_Out1A}, 
						{ADC1_Out2B, ADC1_Out2A}
					}), 
	.rx_clk		( clk_serdes[0] ),
	.locked		( adc_locked[0] ),					// ADC SERDES PLL Locked?
	.aligned		( adc_aligned[0] ),					// ADC SERDES DATA is Aligned? (FCO = 0xF0) 
	.q				({ adc_data[1], adc_data[0] })	// ADC SERDES DATA clocked out using RD_CLK_MON 
);

adc_data_aligner16 #(
	.PATTERN		( 8'hF0 ),
	.SZ_FCO_IN	( 1 ), // Number of bits in FCO pre-deserializer
	.NUM_ADC		( 1 ), // Number of ADCs
	.NUM_CH		( 2 ), // Number of Channels per ADC
	.SZ_DATA_IN	( 2 ), // Number of bits per ADC channel pre-deserializer
	.SZ_DS_RATE	( 8 )	 // Deserializer rate (in-to-out bits ratio)
) adc2_serdes ( 
	.clk			( clk_sca_rd ),						// Internal Data clock domain
	.rst			( clk_sca_rd_rst ),		// Reset	
	.soft_rst	( serdes_soft_rst ),
	.randomizer12 ( adc_randomizer_en[0] ),				// Must match the ADC randomizer setting set via SPI
	.randomizer14 ( adc_randomizer_en[1] ),				// Must match the ADC randomizer setting set via SPI
	.fco			( ADC2_FR ),  							// Frame Clock, align the incoming adc data to this
	.dco			( ADC2_DCO ),  						// Data Clock sent by ADC	
	.d				({
						// note: ADC channel swap between 1 and 2 is done to match schematics, SCA0 = ADC1_Out2, SCA1 = ADC1_Out1, SCA2 = ADC2_Out2, SCA3 = ADC2_Out1
						{ADC2_Out1B, ADC2_Out1A},
						{ADC2_Out2B, ADC2_Out2A} 
					}),
	.rx_clk		( clk_serdes[1] ),
	.locked		( adc_locked[1] ),					// ADC SERDES PLL Locked?
	.aligned		( adc_aligned[1] ),					// ADC SERDES DATA is Aligned? (FCO = 0xF0) 
	.q				({ adc_data[3], adc_data[2] })	// ADC SERDES DATA Clocked on RD_CLK_MON 
);


wire [NUM_MEM_SLOTS-1:0][SZ_MEM_ADDR-1:0] sp_mem_slot;
wire sp_mem_ready;
wire [47:0] mac_addr;
wire test_mode;
wire auto_mode;

wire [NUM_SCA-1:0][NUM_SCA_CH-1:0] sca_ch_enable;
wire [NUM_SCA-1:0][NUM_SCA_CH-1:0] sca_ch_force;

// We don't want an SCA enabled if it's ADC is powered down, or if it's SERDES is not locked, or if it's not selected to be enabled
wire [NUM_SCA-1:0] scas_master_enable = { sca_enable[3], sca_enable[2], sca_enable[1], sca_enable[0] };
//wire [NUM_SCA-1:0][NUM_SCA_CH-1:0][SZ_ADC_DATA-1:0] sca_ch_threshold;
wire [NUM_SCA-1:0][31:0] sca_ch_ctrl;

wire [NUM_SCA-1:0] sca_int_pulser;

// Toggle sca test pin on negative edge
always@(negedge clk_sca_wr) begin
	SCA_Test <= sca_int_pulser;
end

wire sp_test_pulse_interval_ena;
wire [31:0] sp_test_pulse_interval;

sca_test_pulser test_pulser (
	.clk				( clk_sca_wr ),
	.rst				( clk_sca_wr_rst ),
	.trigger_in		( sp_manual_trig ),
	.trigger_out	( int_pulse_trigger_src ),	
	.ena_interval	( sp_test_pulse_interval_ena ),
	.interval		( sp_test_pulse_interval ),
	.ena_sca			( sp_test_pulse_ena ),
	.pulse_width	( sp_test_pulse_width ),	
	.q					( sca_int_pulser )
);


wire [3:0] busy_sources;

   wire    clk_event = clk_100;
   wire    rst_event = clk_100_rst;

   wire [47:0] mac_addr_clk_event;

   synchronizer #(.SZ_DATA(48)) sync_mac_addr
     (
      .clk(clk_event),
      .rst(rst_event),
      .d(mac_addr),
      .q(mac_addr_clk_event)
      );

////////////////////
// Signal Processing
////////////////////
sca_sigproc #(
	.NUM_TRIGGER_SOURCES( 6 ),
	.MAX_BINS		( MAX_SCA_BINS ),
	.SZ_TIME			( SZ_TIMESTAMP ),
	.SZ_ADC_DATA	( SZ_ADC_DATA ),
	.NUM_MEM_SLOTS	( NUM_MEM_SLOTS ), // power of 2 amount
	.SZ_MEM_ADDR	( SZ_MEM_ADDR )
) sp (
	.clk_sca_wr			( clk_sca_wr ),
	.clk_sca_rd			( { clk_sca_rd, clk_sca_rd, clk_sca_rd, clk_sca_rd } ),
	.clk_event				( clk_event ),	
	.rst					( clk_sca_wr_rst ),
	.rst_event				( rst_event ),
	.rst_sca_rd			( { clk_sca_rd_rst, clk_sca_rd_rst, clk_sca_rd_rst, clk_sca_rd_rst } ),
	.run					( sp_run ),
	.run_timestamp		( sp_ts_run ),
	
	.hw_id				(mac_addr_clk_event),
	.sca_start_delay	( sp_start_delay ),
	.busy_sources		( busy_sources ),
	
	.mem_ready			( sp_mem_ready ),
	.mem_slots			( sp_mem_slot ),
	
	.auto_mode			( auto_mode ),
	.test_mode			( test_mode ),
	
	.trigger				( trigger ), // Used to trigger 
	.trigger_delayed	( trigger_delayed ),
	.trigger_source	( trigger_source ),
	
	.sca_enable			( scas_master_enable ),			// SCA Enable for transmission
	.sca_compression	( 4'h0 ),	// SCA Compression Type to use on channel data
	
	.sca_samples_to_read	( sca_samples_to_read ),
	.adc_data			( adc_data ),
	
	.busy					( busy ),
	
	.sca_avl_beginbursttransfer	( sca_avl_beginbursttransfer ),			
	.sca_avl_waitrequest				( ~sca_avl_waitrequest_n ),
	.sca_avl_burstcount				( sca_avl_burstcount ),		
	.sca_avl_address					( sca_avl_address ),
	.sca_avl_writedata				( sca_avl_writedata ),
	.sca_avl_byteenable				( sca_avl_byteenable ),
	.sca_avl_write						( sca_avl_write ) ,	
	
	.eth_avl_readdatavalid			( eth_avl_readdatavalid ),			
	.eth_avl_burstcount				( eth_avl_burstcount ),
	.eth_avl_waitrequest				( ~eth_avl_waitrequest_n ),
	.eth_avl_beginbursttransfer	( eth_avl_beginbursttransfer ),			
	.eth_avl_address					( eth_avl_address ),
	.eth_avl_readdata					( eth_avl_readdata ),
	.eth_avl_byteenable				( eth_avl_byteenable ),
	.eth_avl_read						( eth_avl_read ),

	.event_val			( event_val ),
	.event_rdy			( event_rdy ),
	.event_dat			( event_dat ),
	.event_epy			( event_epy ),
	.event_sop			( event_sop ),
	.event_eop			( event_eop ),

        .event_not_ready ( event_not_ready ),
		
	.ch_ena				( sca_ch_enable ),
	.ch_forced			( sca_ch_force ),
        //.ch_threshold           ( sca_ch_threshold ),
        .ch_ctrl                ( sca_ch_ctrl ),
	
	.sca_write_ena		( SCA_Write ),
	.sca_write_clk		( WR_CLK_EN ),
	.sca_read_ena		( SCA_Read ),
	.sca_read_clk		( RD_CLK_EN )
);

   wire        kludge_counter = event_not_ready; // count stalls of ddr reader state machine by flow control

   //
   // SATA Link
   //

   wire [31:0] link_ctrl_const; // constant control signals on the avalon bus clock. before use, sync to the correct clock
   wire        sata_link_udp_stream_in_enable = link_ctrl_const[0];
   wire        udp_stream_out_enable = !link_ctrl_const[1];
   wire        sata_to_eth_enable = link_ctrl_const[2];
   wire        eth_to_sata_enable = link_ctrl_const[3];

   wire        enable_stop_our_tx    = link_ctrl_const[4];
   wire        stop_our_tx           = 0; // link_ctrl_const[5];
   wire        enable_stop_remote_tx = link_ctrl_const[6];
   wire        stop_remote_tx        = 0; // link_ctrl_const[7];

   wire        tx_test_pattern_udp   = link_ctrl_const[8];
   wire        tx_test_pattern_eth   = link_ctrl_const[9];
   wire        udp_delay_enable      = link_ctrl_const[10]; // enable udp packet delay
   wire        reboot_tx             = link_ctrl_const[11]; // send a reboot command

   wire        sata_to_nios_disable  = link_ctrl_const[12];
   wire        nios_to_sata_disable  = link_ctrl_const[13];

   wire [19:0] udp_delay_value;
   assign udp_delay_value[11:0]       = 12'h00;
   assign udp_delay_value[19:12]      = link_ctrl_const[31:24]; // udp packet delay

   // NativePhy signals

   wire link_pll_locked;
   wire link_rx_cal_busy;
   wire link_tx_cal_busy;
   wire link_tx_core_clk;
   wire link_rx_core_clk;
   wire link_signal_detect;
   wire link_is_locked_to_data;
   wire link_is_locked_to_ref;
   wire link_rx_inv;
   wire link_rx_signal_detect;
   wire link_rx_err_detect;
   wire link_rx_disp_err;
   wire link_rx_sync_status;
   wire link_rx_pattern_det;
   wire link_serial_loopback_en;
   wire [7:0] link_tx_d;
   wire [7:0] link_rx_d;
   wire       link_tx_k;
   wire       link_rx_k;
   
   reg  link_reset;
   reg  link_reset_done;
   reg [7:0] link_counter;

   always_ff @(posedge REFCLK) begin
      if (link_reset_done) begin
         ; // nothing to do
      end else if (link_counter[7]) begin
         link_reset <= 0;
         link_reset_done <= 1;
      end else begin
         link_counter <= link_counter + 8'b1;
         if (link_counter[6]) begin
            link_reset <= 1;
         end
      end
   end

   native_phy backup_link_phy
     (
      .pll_locked               ( link_pll_locked ),
      .pll_powerdown            ( 1'b0 ),
      
      .tx_analogreset           ( link_reset ), // Active high, edge sensitive, asynchronous reset signal
      .tx_digitalreset          ( link_reset ),
      .rx_analogreset           ( link_reset ),
      .rx_digitalreset	        ( link_reset ),
      
      .tx_pll_refclk		( REFCLK ), // 62.5 MHz
      .rx_cdr_refclk		( REFCLK ), // 62.5 MHz
      
      .tx_serial_data		( GXB_TX[1] ),
      .rx_serial_data		( GXB_RX[1] ),
      .rx_pma_clkout		( clk_rcvd_link ), // recovered receive data clock
      
      .tx_std_clkout		( link_tx_core_clk ),
      .tx_std_coreclkin         ( link_tx_core_clk ),
      .rx_std_clkout		( link_rx_core_clk ),
      .rx_std_coreclkin         ( link_rx_core_clk ),
      
      .tx_cal_busy		( link_tx_cal_busy ),
      .rx_cal_busy		( link_rx_cal_busy ),
      .reconfig_to_xcvr         ( reconfig_to_gxb[1] ),
      .reconfig_from_xcvr	( reconfig_from_gxb[1] ),
      
      .tx_parallel_data         ( link_tx_d ),
      .tx_datak                 ( link_tx_k),
      
      .rx_parallel_data         ( link_rx_d ),
      .rx_datak                 ( link_rx_k ),
      
      .rx_std_polinv		( link_rx_inv ), // invert RX polarity
      .rx_seriallpbken          ( link_serial_loopback_en ), // loopback mode
      
      .rx_is_lockedtoref	( link_is_locked_to_ref ),
      .rx_is_lockedtodata	( link_is_locked_to_data ),
      .rx_errdetect		( link_rx_err_detect ), // 8b10b invalid 10-bit code or disparity error
      .rx_disperr		( link_rx_disp_err ), // 8b10b disparity error
      .rx_runningdisp		( ),
      .rx_patterndetect         ( link_rx_pattern_det ), // programmed word alignment pattern has been detected in the current word boundary
      .rx_syncstatus		( link_rx_sync_status ), // word aligner identifies the word alignment pattern ...
      .rx_std_signaldetect	( link_rx_signal_detect ), // This signal is required for the PCI Express protocol. If enabled, the signal threshold detection circuitry senses whether the signal level present at the RX input buffer is above the signal detect threshold voltage that you specified.
      
      .unused_tx_parallel_data( ),
      .unused_rx_parallel_data( )
      );

   // drive the 125MHz/62.5MHz recovered rx clock to the clock cleaner

   wire clock_realign_request = 0; // NB: used to be fed from link_sar where it is not connected to anything

   reg clk_rcvd_link_div2;
   always@(posedge clock_realign_request, posedge clk_rcvd_link) begin
      if(clock_realign_request) begin
	 clk_rcvd_link_div2 <= 1'b0;
      end else begin
	 clk_rcvd_link_div2 <= clk_rcvd_link_div2 ^ 1'b1;
      end
   end

   assign CLOCK_CDR_REF_OUT = clk_rcvd_link_div2;

   //////////////////////////////////
   // SATA link receiver and demux //
   //////////////////////////////////

   wire sata_link_rx_clk = link_rx_core_clk;

   wire sata_link_err_rx_clk;     // sata link error (any error)

   wire link_status;
   wire link_rx_status;
   wire link_flow_control_stop_our_tx;
   wire link_flow_control_stop_remote_tx;

   wire sata_link_source8_ready;
   wire sata_link_source8_valid;
   wire [7:0] sata_link_source8_data;

   wire sata_link_trigger_pulse3; // trigger received from sata link

   wire [31:0] link_stat_trig_tx;
   wire [31:0] link_stat_stop_tx;
   wire [31:0] link_stat_pkts_tx;
   wire [31:0] link_stat_octets_tx;
   wire [31:0] link_stat_flow_tx;

   wire [31:0] link_stat_badk_rx;
   wire [31:0] link_stat_trig_rx;
   wire [31:0] link_stat_stop_rx;
   wire [31:0] link_stat_ovfl_rx;
   wire [31:0] link_stat_octets_rx;
   wire [31:0] link_stat_pkts_rx;
   
   link_rx link_rx
     (
      .rx_clk   ( link_rx_core_clk ),
      .rx_data  ( link_rx_d ),
      .rx_k     ( link_rx_k ),

      .pll_locked         ( link_pll_locked ),
      .rx_is_lockedtodata ( link_is_locked_to_data ),
      .rx_signal_detect   ( link_rx_signal_detect ),
      .rx_errdetect       ( link_rx_err_detect ),
      .rx_disperr         ( link_rx_disp_err ),
      .rx_patterndetect   ( link_rx_pattern_det ),
      .rx_syncstatus      ( link_rx_sync_status ),
      
      .source8_ready ( sata_link_source8_ready),
      .source8_valid ( sata_link_source8_valid),
      .source8_data  ( sata_link_source8_data),

      .link_rx_status_out    (link_rx_status),
      .link_status_out       (link_status),
      .link_flow_control_stop_our_tx_out     ( link_flow_control_stop_our_tx ),
      .link_flow_control_stop_remote_tx_out  ( link_flow_control_stop_remote_tx ),
   
      .trig_pulse3_out       ( sata_link_trigger_pulse3 ), 
      .reboot_pulse3_out     ( sata_link_reboot_pulse3  ),
      .err_out               ( sata_link_err_rx_clk),
      
      .stat_badk_rx		( link_stat_badk_rx ),
      .stat_trig_rx		( link_stat_trig_rx ),
      .stat_stop_rx		( link_stat_stop_rx ),
      .stat_ovfl_rx		( link_stat_ovfl_rx ),
      .stat_octets_rx	        ( link_stat_octets_rx ),
      .stat_pkts_rx		( link_stat_pkts_rx )
      );
   
   wire       link_trigger_src;
   
   synchronizer link_trigger_sync
     (
      .clk  ( clk_sca_wr ),
      .rst  ( clk_sca_wr_rst ),
      .d    ( sata_link_trigger_pulse3 ),
      .q    ( link_trigger_src )
      );
   
   wire       sata_rx_clk = clk_100; // sata receiver slow clock of our choice. fast clock is fixed at 125 MHz
   wire       sata_rx_rst_n = clk_100_rst_n;
   
   wire [31:0] sata_to_nios_data; // sata_rx_clk clock
   wire        sata_to_nios_ready;
   wire        sata_to_nios_valid;
   wire        sata_to_nios_sop;
   wire        sata_to_nios_eop;
   wire [1:0]  sata_to_nios_empty;
   
   wire [31:0] sata_to_eth_data; // sata_rx_clk clock
   wire        sata_to_eth_ready;
   wire        sata_to_eth_valid;
   wire        sata_to_eth_sop;
   wire        sata_to_eth_eop;
   wire [1:0]  sata_to_eth_empty;
   
   wire [31:0] sata_link_rx_out2_data; // sata_rx_clk clock
   wire        sata_link_rx_out2_valid;
   wire        sata_link_rx_out2_ready;
   wire        sata_link_rx_out2_startofpacket;
   wire        sata_link_rx_out2_endofpacket;
   wire [1:0]  sata_link_rx_out2_empty;

   sata_link_rx sata_link_qsys_rx
     (
      // fast clock signals
      
      .clk_125_clk                                 (sata_link_rx_clk), // SATA receive fast clock (fixed 125 MHz)
      .reset_125_reset_n                           (1'b1),

      .st_bytes_to_packets_0_in_bytes_stream_ready (sata_link_source8_ready), // out
      .st_bytes_to_packets_0_in_bytes_stream_valid (sata_link_source8_valid), // in
      .st_bytes_to_packets_0_in_bytes_stream_data  (sata_link_source8_data),  // in

      // slow clock signals

      .clk_clk                                     (sata_rx_clk), // SATA receive slow clock (our choice)
      .reset_reset_n                               (sata_rx_rst_n),

      .demultiplexer_rx_out0_data                  (sata_to_eth_data), // sata_rx_clk clock
      .demultiplexer_rx_out0_valid                 (sata_to_eth_valid),
      .demultiplexer_rx_out0_ready                 (sata_to_eth_ready | (!sata_to_eth_enable)),
      .demultiplexer_rx_out0_startofpacket         (sata_to_eth_sop),
      .demultiplexer_rx_out0_endofpacket           (sata_to_eth_eop),
      .demultiplexer_rx_out0_empty                 (sata_to_eth_empty),

      .demultiplexer_rx_out1_data                  (sata_to_nios_data), // sata_rx_clk clock
      .demultiplexer_rx_out1_valid                 (sata_to_nios_valid),
      .demultiplexer_rx_out1_ready                 (sata_to_nios_ready),
      .demultiplexer_rx_out1_startofpacket         (sata_to_nios_sop),
      .demultiplexer_rx_out1_endofpacket           (sata_to_nios_eop),
      .demultiplexer_rx_out1_empty                 (sata_to_nios_empty),

      .demultiplexer_rx_out2_data                  (sata_link_rx_out2_data), // sata_rx_clk clock
      .demultiplexer_rx_out2_valid                 (sata_link_rx_out2_valid),
      .demultiplexer_rx_out2_ready                 (sata_link_rx_out2_ready | (!sata_link_udp_stream_in_enable)),
      .demultiplexer_rx_out2_startofpacket         (sata_link_rx_out2_startofpacket),
      .demultiplexer_rx_out2_endofpacket           (sata_link_rx_out2_endofpacket),
      .demultiplexer_rx_out2_empty                 (sata_link_rx_out2_empty)
      );

   ///////////////////////////////////
   // SATA link transmitter and mux //
   ///////////////////////////////////

   wire sata_link_tx_clk = link_tx_core_clk;
   wire sata_link_tx_rst = 1'b0;
   wire sata_link_tx_rst_n = ~sata_link_tx_rst;

   wire        sata_link_tx_ready;
   wire        sata_link_tx_valid;
   wire [7:0]  sata_link_tx_data;

   link_tx link_tx
     (
      .tx_clk		( link_tx_core_clk ),
      .tx_data		( link_tx_d ),
      .tx_k		( link_tx_k ),	

      .link_rx_status_async_in    ( link_rx_status ),
      .link_status_async_in       ( link_status ),
      .link_flow_control_stop_our_tx_async_in    ( (link_flow_control_stop_our_tx & enable_stop_our_tx) | stop_our_tx),
      .link_flow_control_stop_remote_tx_async_in ( (link_flow_control_stop_remote_tx & enable_stop_remote_tx) | stop_remote_tx),

      .trig_async_in	( ext_trigger_src    ),
      .reboot_async_in	( reboot_tx ),

      .sink8_valid_in   ( sata_link_tx_valid ),
      .sink8_data_in    ( sata_link_tx_data  ),
      .sink8_ready_out  ( sata_link_tx_ready ),
      
      .stat_trig_tx	( link_stat_trig_tx ),
      .stat_stop_tx	( link_stat_stop_tx ),
      .stat_pkts_tx	( link_stat_pkts_tx ),
      .stat_octets_tx	( link_stat_octets_tx ),
      .stat_flow_tx	( link_stat_flow_tx )
      );
   
   wire [31:0] nios_to_sata_data; // clk_event (sata transmit) clock
   wire        nios_to_sata_ready;
   wire        nios_to_sata_valid;
   wire        nios_to_sata_sop;
   wire        nios_to_sata_eop;
   wire [1:0]  nios_to_sata_empty;

   wire [31:0] nios_to_sata_pattern;
   wire [7:0]  nios_to_sata_counter;

   always_ff @(posedge clk_event) begin
      if (tx_test_pattern_eth & nios_to_sata_valid & nios_to_sata_ready) begin
         nios_to_sata_counter <= nios_to_sata_counter + 8'h01;
         nios_to_sata_pattern <= { nios_to_sata_counter, nios_to_sata_counter, nios_to_sata_counter, nios_to_sata_counter };
      end
   end

   wire [31:0] eth_to_sata_data; // clk_event (sata transmit) clock
   wire        eth_to_sata_ready;
   wire        eth_to_sata_valid;
   wire        eth_to_sata_sop;
   wire        eth_to_sata_eop;
   wire [1:0]  eth_to_sata_empty;

   wire [31:0] udp_event_pattern;
   wire [7:0]  udp_event_counter;

   always_ff @(posedge clk_event) begin
      if (tx_test_pattern_udp & udp_event_val & udp_event_rdy_sata_link) begin
         udp_event_counter <= udp_event_counter + 8'b1;
         udp_event_pattern <= { udp_event_counter, udp_event_counter, udp_event_counter, udp_event_counter };
      end
   end


   sata_link_tx sata_link_qsys_tx
     (
      // fast clock signals
      
      .clk_125_clk                                   (sata_link_tx_clk), // transmit fast clock 125 MHz, link_tx_core_clk
      .reset_125_reset_n                             (sata_link_tx_rst_n),

      .st_packets_to_bytes_tx_out_bytes_stream_ready (sata_link_tx_ready),
      .st_packets_to_bytes_tx_out_bytes_stream_valid (sata_link_tx_valid),
      .st_packets_to_bytes_tx_out_bytes_stream_data  (sata_link_tx_data),

      // slow clock signals

      .clk_clk                                       (clk_event), // transmit slow clock (our choice)
      .reset_reset_n                                 (~rst_event),

      .multiplexer_tx_in0_data                       (tx_test_pattern_eth ? nios_to_sata_pattern : nios_to_sata_data),
      .multiplexer_tx_in0_valid                      (nios_to_sata_valid & (!nios_to_sata_disable)),
      .multiplexer_tx_in0_ready                      (nios_to_sata_ready),
      .multiplexer_tx_in0_startofpacket              (nios_to_sata_sop),
      .multiplexer_tx_in0_endofpacket                (nios_to_sata_eop),
      .multiplexer_tx_in0_empty                      (nios_to_sata_empty),
      .multiplexer_tx_in0_channel                    (),

      .multiplexer_tx_in1_data                       (eth_to_sata_data),
      .multiplexer_tx_in1_valid                      (eth_to_sata_valid & eth_to_sata_enable),
      .multiplexer_tx_in1_ready                      (eth_to_sata_ready),
      .multiplexer_tx_in1_startofpacket              (eth_to_sata_sop),
      .multiplexer_tx_in1_endofpacket                (eth_to_sata_eop),
      .multiplexer_tx_in1_empty                      (eth_to_sata_empty),
      .multiplexer_tx_in1_channel                    (),

      .multiplexer_tx_in2_data                       (tx_test_pattern_udp ? udp_event_pattern : udp_event_dat),
      .multiplexer_tx_in2_valid                      (udp_event_val),
      .multiplexer_tx_in2_ready                      (udp_event_rdy_sata_link), // out
      .multiplexer_tx_in2_startofpacket              (udp_event_sop),
      .multiplexer_tx_in2_endofpacket                (udp_event_eop),
      .multiplexer_tx_in2_empty                      (udp_event_epy),
      .multiplexer_tx_in2_channel                    ()
      );

//////////////////////////
// Event Data Transmission
//////////////////////////

// Take packets and chop them into 1472 byte sized packets
// for consumption by the Altera UDP offload engine
packet_chunker #(
	.NUM_STREAMS 				( NUM_SCA ),
	.MAX_CHUNK_LEN_IN_WORDS	( 362 ), // leave space for 6 words of extra info around payload (5 header, 1 footer)
	.NUM_CHUNKS					( 3 )
) udp_packet_chunker (										
	.clk			( clk_event ),
	.rst			( rst_event ),	
	.dev_id		( mac_addr_clk_event[31:0]),
	.in_sop		( event_sop ),
	.in_eop		( event_eop ),
	.in_dat		( event_dat ),
	.in_val		( event_val ),
	.in_empty	( event_epy ),
	.in_rdy		( event_rdy ),
	.out_sop		( event_segment_sop ),
	.out_eop		( event_segment_eop ),
	.out_val		( event_segment_val ),
	.out_dat		( event_segment_dat ),
	.out_rdy		( event_segment_rdy )
);

// Take the packets and add their length to the start of the packet
// for the ALtera UDP Offload engine to work properly
packet_length_prepender #(
	.PKT_DEPTH( 368 ), // 368 words * 4 bytes per word = 1472 bytes, defaults to correct size for 1500 MTU, 28 bytes for IP+UDP header
	.NUM_PACKETS( 3 )	
) udp_len_prepender (
	.clk			( clk_event ),
	.rst			( rst_event ),	
	.in_sop		( event_segment_sop ),
	.in_eop		( event_segment_eop ),
	.in_dat		( event_segment_dat ),
	.in_val		( event_segment_val ),
	.in_empty	( 2'b00 ), // due to how the packet chunker works, it does only produces 32-bit aligned packets
	.in_rdy		( event_segment_rdy ),
	.out_sop	( udp_event_sop ),
	.out_eop	( udp_event_eop ),
	.out_val	( udp_event_val_out ),
	.out_dat	( udp_event_dat ),
	.out_empty	( udp_event_epy ),
	.out_rdy	( udp_event_rdy )
);

   reg udp_delay;
   reg [19:0] udp_delay_counter;

   always_ff @(posedge clk_event) begin
      if (udp_delay) begin
         if (udp_delay_counter == udp_delay_value) begin
            udp_delay <= 0;
            udp_delay_counter <= 0;
         end else begin
            udp_delay <= 1;
            udp_delay_counter <= udp_delay_counter + 20'b1;
         end
      end else if (udp_event_eop) begin
         udp_delay <= 1;
         udp_delay_counter <= 0;
      end else begin
         udp_delay <= 0;
         udp_delay_counter <= 0;
      end
   end

   wire udp_event_rdy = (udp_event_rdy_eth | (!udp_stream_out_enable)) & udp_event_rdy_sata_link & (!(udp_delay & udp_delay_enable));
   wire udp_event_val = udp_event_val_out & udp_event_rdy;

// Wire all the busy signals together
wire master_busy = (busy);

   reg [31:0] fpga_timestamp;
   build_timestamp build_ts ( .data_out( fpga_timestamp ) );

   ///////
   // QSYS 
   ///////
   feam feam_qsys
     (
      .clk_125_clk_clk       ( clk_125        ),
      .clk_125_rst_reset_n   ( clk_125_rst_n  ),
      .clk_100_clk_clk       ( clk_100        ),
      .clk_100_rst_reset_n   ( clk_100_rst_n  ),
      .clk_62_5_clk_clk      ( clk_62_5       ),
      .clk_62_5_rst_reset_n  ( clk_62_5_rst_n ),
      .clk_12_5_clk_clk      ( clk_12_5       ),
      .clk_12_5_rst_reset_n  ( clk_12_5_rst_n ),
   
      .ddr_pll_ref_clk125_clk    ( clk_osc_100 ),
      .ddr_global_reset_reset_n  ( pll_rst_n   ), // DDR controller global_reset signal
	
	.spi_adc1_MISO                                     ( ADC1_SDO ),                                      //                        spi_adc1.MISO
	.spi_adc1_MOSI                                     ( ADC1_SDI ),                                      //                                .MOSI
	.spi_adc1_SCLK                                     ( ADC1_SCK ),                                      //                                .SCLK
	.spi_adc1_SS_n                                     ( ADC1_CSn ),                                      //                                .SS_n
	.spi_adc2_MISO                                     ( ADC2_SDO ),                                      //                        spi_adc2.MISO
	.spi_adc2_MOSI                                     ( ADC2_SDI ),                                      //                                .MOSI
	.spi_adc2_SCLK                                     ( ADC2_SCK ),                                      //                                .SCLK
	.spi_adc2_SS_n                                     ( ADC2_CSn ),                                      //                                .SS_n
	
	.spi_external_MISO                                 ( Ext_SPI_MISO ),                                  //                    spi_external.MISO
	.spi_external_MOSI                                 ( Ext_SPI_MOSI ),                                  //                                .MOSI
	.spi_external_SCLK                                 ( Ext_SPI_CLK ),                                  //                                .SCLK
	.spi_external_SS_n                                 ( Ext_SPI_CSn ),                                  //                                .SS_n
	
	.spi_temp_MISO          ( Temp_SDO ),        // MISO
	.spi_temp_MOSI          ( xTemp_SDI ),        // MOSI
	.spi_temp_SCLK          ( xTemp_CLK ),        // SCLK
	.spi_temp_SS_n          ( xTemp_CSn ),        // SS_n

	.spi_clockcleaner_MISO  ( LMK_SYNC_CLKin2 ), // MISO
	.spi_clockcleaner_MOSI  ( LMK_Data ),        // MOSI
	.spi_clockcleaner_SCLK  ( LMK_CLK ),         // SCLK
	.spi_clockcleaner_SS_n  ( LMK_LE ),          // SS_n
     
	.i2c_sfp_scl_pad_io                                ( SFP_SCL ),                                 //                         i2c_sfp.scl_pad_io
	.i2c_sfp_sda_pad_io                                ( SFP_SDA ),                                 //                                .sda_pad_io
	.i2c_mac_scl_pad_io                                ( MAC_SCL ),                                 //                         i2c_mac.scl_pad_io
	.i2c_mac_sda_pad_io                                ( MAC_SDA ),                                 //                                .sda_pad_io

        // DDR_avl1 port for writing to memory

        .clk_ddr_avl1_clk_in_clk                         (clk_sca_wr),
        .clk_ddr_avl1_clk_in_reset_reset_n               (~clk_sca_wr_rst),
        .ddr_avl_1_waitrequest_n                         (sca_avl_waitrequest_n),
        .ddr_avl_1_beginbursttransfer                    (1'b0),
        .ddr_avl_1_address                               (sca_avl_address[28:3]), // 26 bits, 64 bit wide bus
        //.ddr_avl_1_readdatavalid                         (not used),
        //.ddr_avl_1_readdata                              (not used),
        .ddr_avl_1_writedata                             (sca_avl_writedata),
        .ddr_avl_1_byteenable                            (sca_avl_byteenable),
        .ddr_avl_1_read                                  (1'b0),
        .ddr_avl_1_write                                 (sca_avl_write),
        .ddr_avl_1_burstcount                            (sca_avl_burstcount[2:0]), // 3 bits

        // DDR_avl2 port for reading from memory
                
        .clk_ddr_avl2_clk_in_clk                         (clk_event),
        .clk_ddr_avl2_clk_in_reset_reset_n               (~rst_event),
        .ddr_avl_2_waitrequest_n                         (eth_avl_waitrequest_n),
        .ddr_avl_2_beginbursttransfer                    (1'b0),
        .ddr_avl_2_address                               (eth_avl_address[28:4]), // 25 bits, 128 bit wide bus
        .ddr_avl_2_readdatavalid                         (eth_avl_readdatavalid),
        .ddr_avl_2_readdata                              (eth_avl_readdata),
        .ddr_avl_2_writedata                             (128'b0),
        .ddr_avl_2_byteenable                            (eth_avl_byteenable),
        .ddr_avl_2_read                                  (eth_avl_read),
        .ddr_avl_2_write                                 (1'b0),
        .ddr_avl_2_burstcount                            (eth_avl_burstcount[2:0]), // 3 bits

        // DDR memory pins
                
	.ddr_mem_mem_a                                     ( DDR3_CA ),                    //  memory_0.mem_a
	.ddr_mem_mem_ba                                    ( DDR3_BA ),                    // .mem_ba
	.ddr_mem_mem_ck                                    ( DDR3_CK_p ),                  // .mem_ck
	.ddr_mem_mem_ck_n                                  ( DDR3_CK_n ),                  // .mem_ck_n
	.ddr_mem_mem_cke                                   ( DDR3_CKE ),                   // .mem_cke
	.ddr_mem_mem_cs_n                                  ( DDR3_CS_n ),                  // .mem_cs_n
	.ddr_mem_mem_dm                                    ( DDR3_DM ),                    // .mem_dm
	.ddr_mem_mem_ras_n                                 ( DDR3_RAS_n ),                 // .mem_ras_n
	.ddr_mem_mem_cas_n                                 ( DDR3_CAS_n ),                 // .mem_cas_n
	.ddr_mem_mem_we_n                                  ( DDR3_WE_n ),                  // .mem_we_n
	.ddr_mem_mem_reset_n                               ( DDR3_RESET_n ),               // .mem_reset_n
	.ddr_mem_mem_dq                                    ( DDR3_DQ  ),                   // .mem_dq
	.ddr_mem_mem_dqs                                   ( DDR3_DQS_p ),                 // .mem_dqs
	.ddr_mem_mem_dqs_n                                 ( DDR3_DQS_n ),                 // .mem_dqs_n
	.ddr_mem_mem_odt                                   ( DDR3_ODT ),                   // .mem_odt
	.oct_0_rzqin                                       ( DDR3_OCT_RZQ ),               //  oct_0.rzqin
	
	.ddr_status_local_init_done          					(  ),   // mem_if_ddr3_emif_0_status.local_init_done
	.ddr_status_local_cal_success        					(  ), // .local_cal_success
	.ddr_status_local_cal_fail           					(  ),    // .local_cal_fail
	 
        .eth_tse_status_led_crs                            (),                             //              eth_tse_status_led.crs
	.eth_tse_status_led_link                           (),                            //                                .link
	.eth_tse_status_led_panel_link                     (),                      //                                .panel_link
	.eth_tse_status_led_col                            (),                             //                                .col
	.eth_tse_status_led_an                             (),                              //                                .an
	.eth_tse_status_led_char_err                       (),                        //                                .char_err
	.eth_tse_status_led_disp_err                       (),                        //                                .disp_err
	
	.gxb_reconfig_ch0_1_to_xcvr_reconfig_to_xcvr     	( reconfig_to_gxb[0]   ),
        .gxb_reconfig_ch0_1_from_xcvr_reconfig_from_xcvr 	( reconfig_from_gxb[0] ),
        .gxb_reconfig_ch2_3_to_xcvr_reconfig_to_xcvr     	( reconfig_to_gxb[1]   ),
        .gxb_reconfig_ch2_3_from_xcvr_reconfig_from_xcvr 	( reconfig_from_gxb[1] ),
	.gxb_reconfig_reconfig_busy_reconfig_busy        	( ),
	.gxb_reconfig_rx_cal_busy_tx_cal_busy            	( link_tx_cal_busy ),
	.gxb_reconfig_tx_cal_busy_tx_cal_busy           	( link_rx_cal_busy ),
	
   .eth_tse_pcs_ref_clk_clock_clk                     ( clk_ref_125 ),                      //       eth_tse_pcs_ref_clk_clock.clk
   .eth_tse_serdes_control_rx_recovclkout             ( clk_rcvd_sfp ),              //          eth_tse_serdes_control.rx_recovclkout
   .eth_tse_serdes_control_reconfig_togxb             ( reconfig_to_gxb[0]),              //                                .reconfig_togxb
   .eth_tse_serdes_control_reconfig_fromgxb           ( reconfig_from_gxb[0] ),            //                                .reconfig_fromgxb
   .eth_tse_serial_rxp                                ( GXB_RX[0] ),                                 //                  eth_tse_serial.rxp
   .eth_tse_serial_txp                                ( GXB_TX[0] ),                                 //                                .txp    	

	.sca_0_sc_din                                      ( SCA_Sc_din[0] ),                                       //                           sca_0.sc_din
	.sca_0_sc_ck                                       ( SCA_Sc_ck[0] ),                                        //                                .sc_ck
	.sca_0_sc_en                                       ( SCA_Sc_en[0] ),                                        //                                .sc_en
	.sca_0_sc_dout                                     ( SCA_Sc_dout[0] ),                                      //                                .sc_dout
	
	.sca_1_sc_din                                      ( SCA_Sc_din[1] ),                                       //                           sca_1.sc_din
	.sca_1_sc_ck                                       ( SCA_Sc_ck[1] ),                                        //                                .sc_ck
	.sca_1_sc_en                                       ( SCA_Sc_en[1]  ),                                        //                                .sc_en
	.sca_1_sc_dout                                     ( SCA_Sc_dout[1] ),                                      //                                .sc_dout
	
	.sca_2_sc_din                                      ( SCA_Sc_din[2] ),                                       //                           sca_2.sc_din
	.sca_2_sc_ck                                       ( SCA_Sc_ck[2] ),                                        //                                .sc_ck
	.sca_2_sc_en                                       ( SCA_Sc_en[2] ),                                        //                                .sc_en
	.sca_2_sc_dout                                     ( SCA_Sc_dout[2] ),                                      //                                .sc_dout
	
	.sca_3_sc_din                                      ( SCA_Sc_din[3] ),                                       //                           sca_3.sc_din
	.sca_3_sc_ck                                       ( SCA_Sc_ck[3] ),                                        //                                .sc_ck
	.sca_3_sc_en                                       ( SCA_Sc_en[3] ),                                        //                                .sc_en
	.sca_3_sc_dout                                     ( SCA_Sc_dout[3] ),                                      //                                .sc_dout

        .udp_stream_sca_0_clk_clk                        ( clk_event ),
        .udp_stream_sca_0_reset_reset_n                  ( ~rst_event ),

        .udp_stream_sca_0_dc_fifo_in_data                ( udp_event_dat ),
        .udp_stream_sca_0_dc_fifo_in_valid               ( udp_event_val & udp_stream_out_enable),
        .udp_stream_sca_0_dc_fifo_in_ready               ( udp_event_rdy_eth ),
        .udp_stream_sca_0_dc_fifo_in_startofpacket       ( udp_event_sop ),
        .udp_stream_sca_0_dc_fifo_in_endofpacket         ( udp_event_eop ),
        .udp_stream_sca_0_dc_fifo_in_empty               ( udp_event_epy ),

        .udp_stream_sata_clk_clk                         (sata_rx_clk),
        .udp_stream_sata_reset_reset_n                   (sata_rx_rst_n),

        .udp_stream_sata_dc_fifo_in_data                 (sata_link_rx_out2_data),
        .udp_stream_sata_dc_fifo_in_valid                (sata_link_rx_out2_valid & sata_link_udp_stream_in_enable),
        .udp_stream_sata_dc_fifo_in_ready                (sata_link_rx_out2_ready),
        .udp_stream_sata_dc_fifo_in_startofpacket        (sata_link_rx_out2_startofpacket),
        .udp_stream_sata_dc_fifo_in_endofpacket          (sata_link_rx_out2_endofpacket),
        .udp_stream_sata_dc_fifo_in_empty                (sata_link_rx_out2_empty),
                
        .nios_to_sata_clk_clk                            (clk_event),
        .nios_to_sata_clk_reset_reset_n                  (~rst_event),
        .nios_to_sata_dc_fifo_tx_out_data                (nios_to_sata_data),
        .nios_to_sata_dc_fifo_tx_out_valid               (nios_to_sata_valid),
        .nios_to_sata_dc_fifo_tx_out_ready               (nios_to_sata_ready),
        .nios_to_sata_dc_fifo_tx_out_startofpacket       (nios_to_sata_sop),
        .nios_to_sata_dc_fifo_tx_out_endofpacket         (nios_to_sata_eop),
        .nios_to_sata_dc_fifo_tx_out_empty               (nios_to_sata_empty),

        .eth_to_sata_clk_clk                             (clk_event),
        .eth_to_sata_clk_reset_reset_n                   (~rst_event),
        .eth_to_sata_dc_fifo_rx_out_data                 (eth_to_sata_data),
        .eth_to_sata_dc_fifo_rx_out_valid                (eth_to_sata_valid),
        .eth_to_sata_dc_fifo_rx_out_ready                (eth_to_sata_ready | (!eth_to_sata_enable)),
        .eth_to_sata_dc_fifo_rx_out_startofpacket        (eth_to_sata_sop),
        .eth_to_sata_dc_fifo_rx_out_endofpacket          (eth_to_sata_eop),
        .eth_to_sata_dc_fifo_rx_out_empty                (eth_to_sata_empty),
                
        .sata_to_eth_clk_clk                             (sata_rx_clk),
        .sata_to_eth_clk_reset_reset_n                   (sata_rx_rst_n),
        .sata_to_eth_dc_fifo_tx_in_data                  (sata_to_eth_data),
        .sata_to_eth_dc_fifo_tx_in_valid                 (sata_to_eth_valid & sata_to_eth_enable),
        .sata_to_eth_dc_fifo_tx_in_ready                 (sata_to_eth_ready),
        .sata_to_eth_dc_fifo_tx_in_startofpacket         (sata_to_eth_sop),
        .sata_to_eth_dc_fifo_tx_in_endofpacket           (sata_to_eth_eop),
        .sata_to_eth_dc_fifo_tx_in_empty                 (sata_to_eth_empty),

        .sata_to_nios_clk_clk                            (sata_rx_clk),
        .sata_to_nios_clk_resett_reset_n                 (sata_rx_rst_n),
        .sata_to_nios_dc_fifo_rx_in_data                 (sata_to_nios_data),
        .sata_to_nios_dc_fifo_rx_in_valid                (sata_to_nios_valid & (!sata_to_nios_disable)),
        .sata_to_nios_dc_fifo_rx_in_ready                (sata_to_nios_ready),
        .sata_to_nios_dc_fifo_rx_in_startofpacket        (sata_to_nios_sop),
        .sata_to_nios_dc_fifo_rx_in_endofpacket          (sata_to_nios_eop),
        .sata_to_nios_dc_fifo_rx_in_empty                (sata_to_nios_empty),
                
  	.sigproc_clk_sp                                		( clk_sca_wr ),
	.sigproc_rst_sp                                		( clk_sca_wr_rst ),
        .sigproc_run_status                                ( run_status ),
        .sigproc_ts_run                                    ( sp_ts_run ),
        .sigproc_force_run                                 ( sp_run ),
	.sigproc_force_rst                                 ( sp_rst ),
        .sigproc_trigger_out                               ( sp_manual_trig ),
        .sigproc_start_delay                               ( sp_start_delay ),
	.sigproc_test_pulse_interval_ena		   ( sp_test_pulse_interval_ena ),
	.sigproc_test_pulse_interval		           ( sp_test_pulse_interval ),
        .sigproc_test_pulse_ena                            ( sp_test_pulse_ena ),
        .sigproc_test_pulse_width                          ( sp_test_pulse_width ),
	.sigproc_mem_ready				   ( sp_mem_ready ),
	.sigproc_mem_slot				   ( sp_mem_slot ),
	.sigproc_test_mode			           ( test_mode ),	
	.sigproc_auto_mode				   ( auto_mode ),	
	.sigproc_event_fifo_full			   ( 32'h0 ),
	.sigproc_event_fifo_err				   ( 32'h0 ),
	.sigproc_event_fifo_busy		           ( 32'h0 ),
	.sigproc_sca_enable				   ( sca_enable ),
	.sigproc_sca_samples_to_read			   ( sca_samples_to_read ),
	.sigproc_sca_ch_enable				   ( sca_ch_enable ),
	.sigproc_sca_ch_force				   ( sca_ch_force ),
	//.sigproc_sca_ch_threshold		           ( sca_ch_threshold ),
	.sigproc_sca_ch_ctrl		                   ( sca_ch_ctrl ),
	
	.board_export_adc_pwr_en                           ( { ADC2_PWR_EN, ADC1_PWR_EN } ),
	.board_export_temp_rstn				   ( xTemp_RSTn ),
        .board_export_clk_serdes                           ( { clk_serdes[1], clk_serdes[0] } ),
        .board_export_clk_io                               ( clk_12_5 ),
        .board_export_clk_sca_wr                           ( clk_sca_wr ),
        .board_export_clk_sca_rd                           ( clk_sca_rd ),
        .board_export_clk_rcvd_eth                         ( clk_rcvd_sfp ),
        .board_export_clk_rcvd_xcvr                        ( clk_rcvd_link ),
	.board_export_serdes_rst			( serdes_rst ),
	.board_export_adc_randomizer_en			( adc_randomizer_en ),
	.board_export_ext_test_pulse			( 1'b0 ),
	.board_export_adc_aligned		   ( adc_aligned ),
	.board_export_adc_locked		( adc_locked ),
	.board_export_lmk_sync_clkin2		( LMK_SYNC_CLKin2 ),
	.board_export_lmk_stat_clkin1		( LMK_Stat_CLKin1 ),
	.board_export_lmk_stat_hold		( LMK_Stat_Hold ),
	.board_export_lmk_stat_ld		( LMK_StatLD ),
	.board_export_sfp_moddet		( SFP_ModDet ),
	.board_export_sfp_los			( SFP_LOS ),
	.board_export_sfp_txfault		( SFP_TxFault ),
	.board_export_use_sata_gxb		( use_sata_gxb ),
	.board_export_trigger			( trigger ),
	.board_export_ext_gate			( /*Ext_Gate*/ ), // no external gate pin assigned yet for rev1	
	.board_export_mac_addr			( mac_addr ),
	.board_export_fpga_timestamp		( fpga_timestamp ),
	.board_export_invert_ext_trig		( invert_ext_trigger ),
	
	.gxb_module_link_tx_core_clk				( link_tx_core_clk ),
	.gxb_module_link_rx_core_clk				( link_rx_core_clk ),
	.gxb_module_link_pll_locked                      	( link_pll_locked ),
	.gxb_module_link_is_locked_to_ref                	( link_is_locked_to_ref ),
	.gxb_module_link_is_locked_to_data               	( link_is_locked_to_data ),
	.gxb_module_link_signal_detect                   	( link_rx_signal_detect ), // steal this for link status bits
	.gxb_module_link_rx_err_detect                   	( link_rx_err_detect ),
	.gxb_module_link_rx_disp_err                     	( link_rx_disp_err ),
	.gxb_module_link_rx_sync_status                  	( link_rx_sync_status ),
	.gxb_module_link_rx_pattern_det				( link_rx_pattern_det ),
	.gxb_module_link_rx_invert				( link_rx_inv ),
        .gxb_module_link_link_ctrl                              ( link_ctrl_const ),
	.gxb_module_link_link_status                     	( link_status ),

        .gxb_module_link_stat_trig_tx           ( link_stat_trig_tx ),
        .gxb_module_link_stat_stop_tx           ( link_stat_stop_tx ),
        .gxb_module_link_stat_pkts_tx           ( link_stat_pkts_tx ),
        .gxb_module_link_stat_octets_tx         ( link_stat_octets_tx ),
        .gxb_module_link_stat_flow_tx           ( link_stat_flow_tx ),

        .gxb_module_link_stat_badk_rx		( link_stat_badk_rx ),
        .gxb_module_link_stat_trig_rx		( link_stat_trig_rx ),
        .gxb_module_link_stat_stop_rx		( link_stat_stop_rx ),
        .gxb_module_link_stat_ovfl_rx		( link_stat_ovfl_rx ),
        .gxb_module_link_stat_octets_rx	        ( link_stat_octets_rx ),
        .gxb_module_link_stat_pkts_rx		( link_stat_pkts_rx ),
	
	.gxb_module_link_serial_lpbk_en                  	( link_serial_loopback_en ),
  	
	.trigger_control_clk_trig			( clk_sca_wr ),
	.trigger_control_rst_trig			( clk_sca_wr_rst ),
	.trigger_control_busy                           ( master_busy ),
	.trigger_control_busy_source			( busy_sources ),
	.trigger_control_ena                            ( 1'b1 ),
	.trigger_control_trigger_request                
                ({
                  link_trigger_src,       // Generated by UDP broadcast packet on port (TBI)
                  ext_pulse_trigger_src,  // Generated by external test pulse code
		  int_pulse_trigger_src,  // Generated by internal test pulse code
		  periodic_trigger_src,   // Covered by int_pulse_trigger_src?
		  man_trigger_src,        // Generated by NIOS/ESPER
		  ext_trigger_src         // External Trigger Pin (use to be 'sata' trigger, now a pin on a header)
		  }),
	.trigger_control_trigger                    ( trigger ),
	.trigger_control_trigger_delayed            ( trigger_delayed ),
	.trigger_control_trigger_source             ( trigger_source )
);


endmodule
