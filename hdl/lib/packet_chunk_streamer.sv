/*
	Creates WORDS 1-2 of Header (packet_chunk.sv adds words 0 and 3)
	Creates payload and CRC32-C of payload data
*/
`default_nettype none
module packet_chunk_streamer (
	clk,
	rst,
	
	in_sop,
	in_eop,
	in_dat,
	in_val,
	in_empty,
	in_rdy,

	out_sop,
	out_eop,
	out_val,
	out_dat,
	out_rdy
);

parameter STREAM_ID = 0;

// Maximum allowed value is 65532 / 4 = 16383 (note it is NOT 16384 
// setting MAX_CHUNK_LEN_IN_WORDS to GREATER than 16383 will result in CHUNK_LEN overflow
parameter MAX_CHUNK_LEN_IN_WORDS = 368; // (1472 / 4) = 368. Maximum payload size for IP MTU 1500

// Number of chunks to be able to buffer
parameter NUM_CHUNKS = 3;

localparam HEADER_LEN  = 3'd2; // header length in words
localparam EXTRA_WORDS = HEADER_LEN; // 2 for header

// Limit max chunk length so the packet chunker can't get put in an impossible state
localparam ACTUAL_MAX_CHUNK_LEN_IN_WORDS = (MAX_CHUNK_LEN_IN_WORDS > 16383) ? 16383 : MAX_CHUNK_LEN_IN_WORDS;

localparam MAX_CHUNK_LEN_IN_BYTES = ACTUAL_MAX_CHUNK_LEN_IN_WORDS * 4;

// Convert DEPTH to power of 2 value for RAM, s
localparam DEPTH 		= 2**$clog2((ACTUAL_MAX_CHUNK_LEN_IN_WORDS + EXTRA_WORDS) * NUM_CHUNKS); // 4 for header, 1 for CRC32
localparam SZ_DATA 		= 32;
localparam SZ_ST_WIDTH 	= SZ_DATA + 2; // DATA + SOP,EOP
localparam SZ_DEPTH 	= $clog2(DEPTH);
localparam SZ_CNT 		= $clog2(DEPTH+1);

localparam CRC32C_INIT 	= 32'hffffffff;
  
enum int unsigned { ST_IDLE = 0, ST_CONTINUE = 2, ST_DATA = 4, ST_H1 = 8, ST_H2 = 16 } in_state;

input wire clk;
input wire rst;

input wire in_sop;
input wire in_eop;
input wire [SZ_DATA-1:0] in_dat;
input wire in_val;
input wire [1:0] in_empty;
output reg in_rdy;

output reg out_val;
output reg out_sop;
output reg out_eop;
output reg [SZ_DATA-1:0] out_dat;
input wire out_rdy;

wire in_captured;
wire out_captured;
wire [SZ_DATA-1:0] in_data_masked;
wire [2:0] in_count;
wire [SZ_ST_WIDTH-1:0] rd_dat;
wire space_available;
wire [7:0] flags;

// output registers
reg [SZ_DEPTH-1:0] rd_pos;
reg [SZ_CNT-1:0] out_pkts;

// input registers
reg [SZ_ST_WIDTH-1:0] wr_dat;
reg wr_ena;
reg [SZ_DEPTH-1:0] wr_pos;
reg [SZ_DEPTH-1:0] wr_start;
reg [SZ_DEPTH-1:0] wr_end;

reg [15:0] chunk_id;
reg [15:0] chunk_len;
reg [15:0] MSG_ID;
reg [SZ_CNT-1:0] in_pkts;

// inferred RAM block
reg [SZ_ST_WIDTH-1:0] r_data [DEPTH-1:0];
reg final_chunk;

// Check for if we are out of packets to send
reg empty;

assign in_captured 		= in_val & in_rdy;
assign out_captured 		= out_val & out_rdy;
assign in_count 			= 3'd4 - in_empty;
assign space_available 	= (in_pkts - out_pkts) < NUM_CHUNKS;
assign flags				= { 7'h0, final_chunk };

// mask off input data based on in_empty, to enforce zero padding
assign in_data_masked = 
	(in_empty == 2'b00 ) ? in_dat[31:0] : 
	(in_empty == 2'b01 ) ? {in_dat[31:8],   8'h0} : 
	(in_empty == 2'b10 ) ? {in_dat[31:16], 16'h0} : {in_dat[31:24], 24'h0};

// inferred ram block with pass through logic to match read-during-write behaviour
assign rd_dat = ((rd_pos == wr_pos) && wr_ena) ? wr_dat : r_data[rd_pos];
  
always@(posedge clk) begin 
	if(wr_ena) 
		r_data[wr_pos] <= wr_dat;	
end
  
// read side logic
always@(posedge rst, posedge clk) begin
	if(rst) begin		
		rd_pos 	<= {SZ_DEPTH{1'b0}};
		out_pkts <= {SZ_CNT{1'b0}};
		out_val 	<= 1'b0;
		out_sop 	<= 1'b0;
		out_eop 	<= 1'b0;
		out_dat 	<= 32'h0;
		empty		<= 1'b1;	
	end else begin
		empty <= ((in_pkts - out_pkts) == 0) ? 1'b1 : 1'b0;
		if(out_captured) begin 							
			// Delay one clock cycle on end of packet so we can use a registered 'empty' signal for timing
			if(empty || out_eop) begin
				out_pkts <= out_pkts;
				rd_pos 	<= rd_pos;
				out_val 	<= 1'b0;
				out_sop 	<= 1'b0;
				out_eop 	<= 1'b0;
				out_dat 	<= 32'h0;
			end else begin
				out_pkts <= (rd_dat[32] == 1'b1) ? out_pkts + 1'b1 : out_pkts;
				out_val 	<= 1'b1;
				rd_pos 	<= rd_pos + 1'b1;
				out_sop 	<= rd_dat[33];
				out_eop 	<= rd_dat[32];
				out_dat 	<= rd_dat[SZ_DATA-1:0];
			end			
		end else begin
			if(!out_val) begin
				if(empty) begin
					out_pkts <= out_pkts;    
					rd_pos 	<= rd_pos;
					out_val 	<= 1'b0;
					out_sop 	<= 1'b0;
					out_eop 	<= 1'b0;
					out_dat 	<= 32'h0;
				end else begin            
					out_pkts	<= (rd_dat[32] == 1'b1) ? out_pkts + 1'b1 : out_pkts;
					rd_pos 	<= rd_pos + 1'b1;
					out_val 	<= 1'b1;
					out_sop 	<= rd_dat[33];
					out_eop 	<= rd_dat[32];
					out_dat 	<= rd_dat[SZ_DATA-1:0];			  
				end 
			end else begin
				out_pkts <= out_pkts;              
				rd_pos 	<= rd_pos;
				out_val <= out_val;
				out_sop <= out_sop;
				out_eop <= out_eop;
				out_dat <= out_dat;
			end
		end		
	end
end
													 
always@(posedge rst, posedge clk) begin 
	if(rst) begin
		in_state 	<= ST_IDLE;		
		in_rdy 		<= 1'b0;
		in_pkts 		<= 0;
		wr_ena 		<= 1'b0;
		wr_pos 		<= 0;
		wr_start 	<= 0;
		wr_end		<= 0;
		MSG_ID 		<= 16'h0;
		chunk_len 	<= 16'h0;
		chunk_id 	<= 16'h0;
		final_chunk <= 1'b0;
	end else begin
		case(in_state) 
		ST_IDLE: begin		
			in_pkts		<= in_pkts;
			chunk_id		<= 16'h0;
			chunk_len	<= in_count;
			wr_end		<= wr_end;
			wr_start 	<= wr_start;
			wr_pos 		<= wr_start + HEADER_LEN; // skip ahead and start writing to the data section
			wr_dat 		<= {1'b0, in_eop, in_data_masked};						
			MSG_ID		<= MSG_ID;			
			final_chunk	<= (in_eop) ? 1'b1 : 1'b0;						
			// Did we capture data?
			if(in_sop && in_captured) begin				
				in_state 	<= (in_eop) ? ST_H1 : ST_DATA; // if only getting one word, write the data and start on the header
				in_rdy   	<= (in_eop) ? 1'b0 : 1'b1;				
				wr_ena 		<= 1'b1;
			end else begin
				in_state 	<= ST_IDLE;
				in_rdy   	<= space_available;				
				wr_ena 		<= 1'b0;				
			end			
		end
		
		ST_CONTINUE: begin
			in_pkts 		<= in_pkts;
			wr_start 	<= wr_start;
			wr_end		<= wr_end;
			wr_pos 		<= wr_start + HEADER_LEN; // skip ahead and start writing to the data section
			wr_dat 		<= {1'b0, in_eop, in_data_masked};				
			MSG_ID		<= MSG_ID;			
			chunk_len	<= in_count;							
			final_chunk	<= (in_eop) ? 1'b1 : 1'b0;
			if(in_captured) begin	
				// received a new packet instead of ending this one properly, increment the message id and move on!
				in_rdy   	<= (in_eop) ? 1'b0 : 1'b1;
				in_state 	<= (in_eop) ? ST_H1 : ST_DATA; // if only getting one word, write the data and start on the header							
				wr_ena 		<= 1'b1;
				chunk_id    <= (in_sop) ? 16'h0 : chunk_id;
			end else begin
				in_rdy   	<= space_available;
				in_state 	<= ST_CONTINUE;				
				wr_ena 		<= 1'b0;
				chunk_id 	<= chunk_id;
			end
				
		end

		ST_DATA: begin	
			MSG_ID		<= MSG_ID;
			wr_end		<= wr_end;
			in_pkts 		<= in_pkts;
			final_chunk	<= (in_eop) ? 1'b1 : 1'b0;
			MSG_ID	<= MSG_ID;			
			wr_start <= wr_start;			
			// error condition, let's just ignore the old message and restart the whole thing
			// todo: capture the first word here and store it in temp registers, 
			// send out an error frame, then load this word into it's 'proper' place and continue on
			if(in_captured) begin				
				wr_ena 		<= 1'b1;					
				if(in_sop) begin				
					in_state 	<= (in_eop) ? ST_H1 : ST_DATA; // if only getting one word, write the data and start on the header
					in_rdy   	<= (in_eop) ? 1'b0 : 1'b1;									
					chunk_id 	<= 16'h0;
					chunk_len	<= in_count;	
					wr_pos 		<= wr_start + HEADER_LEN;	
					wr_dat		<= {1'b0, in_eop, in_data_masked};
				end else if(in_eop) begin
					in_state 	<= ST_H1;
					in_rdy 		<= 1'b0;
					chunk_id 	<= chunk_id;
					chunk_len	<= chunk_len + in_count;
					wr_pos 		<= wr_pos + 1'b1; 				
             	wr_dat 		<= {1'b0, in_eop, in_data_masked};
				// have we reached our segment length max, or are we within 4 bytes of it
				end else if((chunk_len + 3'd4) == MAX_CHUNK_LEN_IN_BYTES) begin 				
					// no more chunks allowed, bail out
					if(chunk_id == 16'hffff) begin
						in_state <= ST_IDLE; // back to waiting for SOP
						in_rdy 	<= space_available;					
						wr_pos 	<= wr_start;
                  wr_dat 	<= {1'b0, 1'b1, in_data_masked};
					// send out this chunk
					end else begin
						in_state <= ST_H1;
						in_rdy 	<= 1'b0;					
						wr_pos 	<= wr_pos + 1'b1;					
						wr_dat 	<= {1'b0, 1'b1, in_data_masked};
					end
					chunk_id 	<= chunk_id;
					chunk_len	<= chunk_len + in_count;										
				// just normal data, keep adding it into the message/chunk
				end else begin								
					in_state <= ST_DATA;
					in_rdy 	<= 1'b1;					
					wr_pos 	<= wr_pos + 1'b1;
					wr_dat 	<= {1'b0, 1'b0, in_data_masked};
					chunk_id 	<= chunk_id;
					chunk_len	<= chunk_len + in_count;
				end
			// no data available for capture, wait
			end else begin
				in_pkts 		<= in_pkts;
				in_state 	<= ST_DATA;
				in_rdy 		<= 1'b1;				
				wr_pos 		<= wr_pos;
				wr_ena 		<= 1'b0;
				wr_dat		<= 32'h0;
				chunk_len	<= chunk_len;
				chunk_id		<= chunk_id;	
			end			
		end
			
		ST_H1: begin
			final_chunk <= final_chunk;
			in_pkts 		<= in_pkts; // do this here so space_available is correct when sampled in H2
			in_state 	<= ST_H2;
			in_rdy 		<= 1'b0;
			wr_start 	<= wr_start;
			wr_end		<= wr_pos;
			wr_pos 		<= wr_start;
			wr_ena 		<= 1'b1;
			wr_dat 		<= {2'b10, MSG_ID[7:0], MSG_ID[15:8], STREAM_ID[7:0], flags };
			MSG_ID		<= MSG_ID;
			chunk_id		<= chunk_id;
			chunk_len	<= chunk_len;
		end
		
		ST_H2: begin
			final_chunk <= final_chunk;
			in_pkts 		<= in_pkts + 1'b1;
			in_state 	<= (final_chunk) ? ST_IDLE : ST_CONTINUE; // go back to waiting for the rest of the packet
			in_rdy 		<= 1'b0;
			wr_start 	<= wr_end + 1'b1;
			wr_end		<= wr_end;
			wr_pos 		<= wr_pos + 1'b1;
			wr_ena 		<= 1'b1;			
			wr_dat 		<= {2'b00, chunk_id[7:0], chunk_id[15:8], chunk_len[7:0], chunk_len[15:8]};
			MSG_ID		<= MSG_ID + 1'b1;
			chunk_id		<= chunk_id + 1'b1;
			chunk_len	<= chunk_len;
		end		
		endcase	
	end
end
  
endmodule
