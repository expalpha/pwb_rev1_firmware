/*
	Purpose
	-------
	Breaks up Avalon-ST packets into smaller chunks (packets) that can be re-assembled into the original packet
	
	Features
	--------
	- Small 16 byte header
	- Standard CRC32-C used for both header and payload check
	- Streaming, whole packet does not have to be received before chunks can be sent out
	- 32-bit aligned data structure
	- 4 cycle delay between 
	
	Format
	------
	WORD 1: seq_id (16) | STREAM_ID (8) | FLAGS (8)
	WORD 2: CHUNK_ID (16) | CHUNK_LEN (16)			
	WORD 3: PREV_PAYLOAD_CRC32C (32)
	WORD 4: HEADER_CRC32C (32)
	WORD 5 to N: PAYLOAD_DATA (32)
	...
	WORD N+1: PAYLOAD_CRC32C (32)

	Field Details
	-------------
	- seq_id: Packet ID, incremented on every packet created
	- STREAM_ID: User-defined means of uniquely identifying a packet segment module
	- CHUNK_ID: Chunk ID, reset on start of new message, incremented per chunk packet sent
	- CHUNK_LEN: Length of Chunk PAYLOAD in bytes
	- PREV_PAYLOAD_CRC32C: Last chunks payload crc, set to 0xFFFFFFFF for first chunk sent
	- HEADER_CRC32C: CRC32 Castagnoli (CRC32C), of the first three words
	- PAYLOAD_DATA: N bytes of data, if N is not a multiple of 4, bytes containing all zeroes (8'h0) will be padded to make it so. The padding is included in the CRC
	- PAYLOAD_CRC32C: CRC32 Castagnoli (CRC32C), of the PAYLOAD_DATA (including padding)
	
	Implementation Details
	----------------------
	- The STREAM_ID field is user defined, and is helpful in differentiating multiple 'streams' of packets from the same address (IP?)
	- The CHUNK_LEN field hold 16 bits, since this is large enough for the value 65535, but we are padding, **the maximum usable chunk size is 65532**
	- The CRC-32C used is initialized to 0xFFFFFFFF by default, and acts like a little-endian, reversed crc with reflected input and outputs (matches standard CRC-32C)
*/
`default_nettype none
module packet_chunker (
	clk,
	rst,
	
	dev_id,
	
	in_sop,
	in_eop,
	in_dat,
	in_val,
	in_empty,
	in_rdy,

	out_sop,
	out_eop,
	out_val,
	out_dat,
	out_rdy
);

parameter NUM_STREAMS = 16;

// Maximum allowed value is 65532 / 4 = 16383 (note it is NOT 16384 
// setting MAX_CHUNK_LEN_IN_WORDS to GREATER than 16383 will result in CHUNK_LEN overflow
parameter MAX_CHUNK_LEN_IN_WORDS = 368; // (1472 / 4) = 368. Maximum payload size for IP MTU 1500

// Number of chunks to be able to buffer
parameter NUM_CHUNKS = 2;

localparam SZ_DATA 		= 32;

input wire clk;
input wire rst;

input wire [31:0] dev_id;

input wire [NUM_STREAMS-1:0] in_sop;
input wire [NUM_STREAMS-1:0] in_eop;
input wire [NUM_STREAMS-1:0] [SZ_DATA-1:0] in_dat;
input wire [NUM_STREAMS-1:0] in_val;
input wire [NUM_STREAMS-1:0] [1:0] in_empty;
output reg [NUM_STREAMS-1:0] in_rdy;

output wire out_val;
output wire out_sop;
output wire out_eop;
output wire [SZ_DATA-1:0] out_dat;
input wire out_rdy;

wire [NUM_STREAMS-1:0] mux_in_sop;
wire [NUM_STREAMS-1:0] mux_in_eop;
wire [NUM_STREAMS-1:0] mux_in_val;
wire [NUM_STREAMS-1:0] [SZ_DATA-1:0] mux_in_dat;
wire [NUM_STREAMS-1:0] mux_in_rdy;

wire mux_out_sop;
wire mux_out_eop;
wire mux_out_val;
wire [SZ_DATA-1:0] mux_out_dat;
wire  mux_out_rdy;

genvar n;
generate
for(n=0; n<NUM_STREAMS; n+=1) begin : gen_stream

    packet_chunk_streamer #( 
        .STREAM_ID  ( n ),
        .MAX_CHUNK_LEN_IN_WORDS  ( MAX_CHUNK_LEN_IN_WORDS ),
        .NUM_CHUNKS ( NUM_CHUNKS )
    ) stream (
	    .clk        ( clk ),
	    .rst        ( rst ),
	    .in_sop     ( in_sop[n] ),
	    .in_eop     ( in_eop[n] ),
	    .in_dat     ( in_dat[n] ),
	    .in_val     ( in_val[n] ),
	    .in_empty   ( in_empty[n] ),
	    .in_rdy     ( in_rdy[n] ),
	    .out_sop    ( mux_in_sop[n] ),
	    .out_eop    ( mux_in_eop[n] ),
	    .out_val    ( mux_in_val[n] ),
	    .out_dat    ( mux_in_dat[n] ),
	    .out_rdy    ( mux_in_rdy[n] )
    );

end

if(NUM_STREAMS > 1) begin
	// MUX Streams into one
	packet_rr_mux #(
		 .NUM_STREAMS ( NUM_STREAMS ),
		 .SZ_DATA		( SZ_DATA )
	) rr_mux (
		.clk        	( clk ),
		.rst        	( rst ),
		.in_sop     	( mux_in_sop ),
		.in_eop     	( mux_in_eop ),
		.in_dat     	( mux_in_dat ),
		.in_val     	( mux_in_val ),
		.in_empty		( {NUM_STREAMS{2'b00}} ),
		.in_rdy     	( mux_in_rdy ),
		.out_sop    	( mux_out_sop ),
		.out_eop    	( mux_out_eop ),
		.out_val    	( mux_out_val ),
		.out_dat    	( mux_out_dat ),
		.out_empty		( ), // unused by chunker (always 32bit data due to padding)
		.out_rdy    	( mux_out_rdy )
	);

end else begin
	assign mux_out_dat = mux_in_dat[0];
	assign mux_out_val = mux_in_val[0];	
	assign mux_out_eop = mux_in_eop[0];	
	assign mux_out_sop = mux_in_sop[0];	
	assign mux_in_rdy[0] = mux_out_rdy;
end

endgenerate


// Package up mux packet with extra header and payload crc
packet_chunk #(
	.NUM_STREAMS ( NUM_STREAMS )
) chunk (
	.clk		( clk ),
	.rst		( rst ),	
	.dev_id	( dev_id ),
	.in_sop	( mux_out_sop ),
	.in_eop	( mux_out_eop ),
	.in_dat	( mux_out_dat ),
	.in_val	( mux_out_val ),
	.in_rdy	( mux_out_rdy ),
	.out_sop	( out_sop ),
	.out_eop	( out_eop ),
	.out_val	( out_val ),
	.out_dat	( out_dat ),
	.out_rdy	( out_rdy )
);

endmodule
