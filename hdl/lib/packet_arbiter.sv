module packet_rr_arbiter (
	clk,
	rst,
	ena,
	request,
	grant_binary,
	grant_onehot,
	grant_valid
);

parameter NUM_STREAMS = 128;

localparam SZ_STREAMS = $clog2(NUM_STREAMS) < 1 ? 1 : $clog2(NUM_STREAMS);

input wire clk;
input wire rst;

input wire ena;
input wire [NUM_STREAMS-1:0] request;

output wire [SZ_STREAMS-1:0] grant_binary;
output wire [NUM_STREAMS-1:0] grant_onehot;
output wire grant_valid;

wire [NUM_STREAMS-1:0] unmasked_request;
wire [NUM_STREAMS-1:0] unmasked_onehot;
wire [NUM_STREAMS-1:0] unmasked_thermo;

wire [NUM_STREAMS-1:0] masked_request;
wire [NUM_STREAMS-1:0] masked_onehot;
wire [NUM_STREAMS-1:0] masked_thermo;

wire [NUM_STREAMS-1:0] granted_onehot;
wire [NUM_STREAMS-1:0] granted_thermo;
wire [SZ_STREAMS-1:0] granted_binary;
wire granted_valid;

// mask logic
reg [NUM_STREAMS-1:0] next_mask;

assign granted_thermo = (masked_thermo[NUM_STREAMS-1] == 1'b0) ? unmasked_thermo : masked_thermo;
assign granted_onehot = (masked_thermo[NUM_STREAMS-1] == 1'b0) ? unmasked_onehot : masked_onehot;
assign masked_request = unmasked_request & next_mask;

assign grant_onehot = granted_onehot;
assign grant_binary = granted_binary;
assign grant_valid  = granted_valid;

genvar n;
generate
	for(n=0; n<NUM_STREAMS; n+=1) begin: gen_mux
		assign unmasked_request[n] = request[n];
	end
endgenerate

// generate PPC
ppc_or #(
	.NUM_CH( NUM_STREAMS )
) unmasked_arbiter (
	.d			( unmasked_request ),
	.q_onehot	( unmasked_onehot ),
	.q_thermo	( unmasked_thermo )
);

// generate PPC
ppc_or #(
	.NUM_CH( NUM_STREAMS )
) masked_arbiter (
	.d			( masked_request ),
	.q_onehot	( masked_onehot ),
	.q_thermo 	( masked_thermo )
);

// Continously convert granted onehot to binary
thermo2binary #(
	.NUM_CH( NUM_STREAMS )
) bin_enc (
  	.d		( ~granted_thermo ), // the thermometer calculated is inverted to the normal type of thermometer
	.q		( granted_binary ),
	.val	( granted_valid )
);

// Tricky bit, pipeline output and only change on end-of-packet
// or we aren't granted, and a grant appears
always@(posedge rst, posedge clk) begin
	if(rst) begin
		next_mask <= {NUM_STREAMS{1'b1}};
	end else begin    
		if(ena) begin
			next_mask <= {granted_thermo[NUM_STREAMS-2:0],1'b0};
		end else begin
			next_mask <= next_mask;  
		end
	end
end
   
endmodule
