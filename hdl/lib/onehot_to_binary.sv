// Only use if input is guaranteed one hot! Not a priority encoder
// val goes high if any input is high
module onehot_to_binary (
  d, q, val
);
  
parameter NUM_BITS = 4;
localparam SZ_BITS = $clog2(NUM_BITS);

input    wire  [NUM_BITS-1:0] d;
output   reg  [SZ_BITS:0] q;
output   reg val;
  
always_comb begin
	q = {(SZ_BITS+1){1'b0}};
	val = 1'b0;
	for(int i = 0; i < NUM_BITS; i++) begin
		if(d == (1'b1 << i)) begin
			q = i[SZ_BITS:0];
			val = 1'b1;
		end
	end
end
    
endmodule
