`default_nettype none
//
module skid_buffer
  (
   input wire clk,

   // sink side
   input wire       sink_valid,
   input wire [7:0] sink_data,
   output wire      sink_ready,

   // source side
   output wire       source_valid,
   output wire [7:0] source_data,
   input wire        source_ready
   );
   
   reg [7:0]        skid_data = 0;
   reg              skid_valid = 0;
   
   always_ff @(posedge clk) begin
      if ((sink_valid && sink_ready) && (source_valid && !source_ready)) begin
	 skid_valid <= 1;
      end else if (source_ready) begin
	 skid_valid <= 0;
      end

      if (sink_ready) begin
	 skid_data <= sink_data;
      end
   end
   
   always_comb begin
      sink_ready = !skid_valid;

      // sink has data or skid has data
      source_valid = (sink_valid || skid_valid);
      
      if (skid_valid) begin
        source_data = skid_data;
      end else begin
        source_data = sink_data;
      end
   end
   
endmodule
