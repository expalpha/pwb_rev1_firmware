//
// PWB SATA link transmitter
//
`default_nettype none
module link_tx
  (
   input  wire tx_clk,
   output reg [7:0] tx_data,
   output reg       tx_k,
   
   input  wire link_rx_status_async_in,
   input  wire link_status_async_in,
   input  wire link_flow_control_stop_our_tx_async_in,
   input  wire link_flow_control_stop_remote_tx_async_in,
   
   input  wire trig_async_in,
   input  wire reboot_async_in,

   input  wire       sink8_valid_in,
   input  wire [7:0] sink8_data_in,
   output wire       sink8_ready_out,

   output reg [31:0] stat_trig_tx,
   output reg [31:0] stat_stop_tx,
   output reg [31:0] stat_pkts_tx,
   output reg [31:0] stat_octets_tx,
   output reg [31:0] stat_flow_tx
   );

   wire [7:0]        sink8_data;
   wire              sink8_valid;
   reg               sink8_ready;
   
   assign sink8_valid     = sink8_valid_in;
   assign sink8_data      = sink8_data_in;
   assign sink8_ready_out = sink8_ready;

   reg               skid_valid;
   reg [7:0]         skid_data;
   
   // synchronize async inputs

   reg               link_rx_status1, link_rx_status2, link_rx_status;
   reg               link_status1, link_status2, link_status;
   reg               trig1, trig2, trig_new, trig_prev, send_trig;
   reg               trig_sent;
   reg               reboot1, reboot2, reboot_new, reboot_prev, send_reboot;
   reg               reboot_sent;

   always_ff @(posedge tx_clk) begin
      link_rx_status1 <= link_rx_status_async_in;
      link_rx_status2 <= link_rx_status1;
      link_rx_status  <= link_rx_status2;
      
      link_status1 <= link_status_async_in;
      link_status2 <= link_status1;
      link_status  <= link_status2;

      trig1 <= trig_async_in;
      trig2 <= trig1;
      trig_new <= trig2;
      trig_prev <= trig_new;
      if (trig_sent) begin
         send_trig <= 0;
      end else if (trig_new & !trig_prev) begin
         send_trig <= 1;
      end

      reboot1 <= reboot_async_in;
      reboot2 <= reboot1;
      reboot_new <= reboot2;
      reboot_prev <= reboot_new;
      if (reboot_sent) begin
         send_reboot <= 0;
      end else if (reboot_new & !reboot_prev) begin
         send_reboot <= 1;
      end
   end

   // stop_our_tx is a pulse sent from link_rx
   
   reg               stop_our_tx1, stop_our_tx2, stop_our_tx3, stop_our_tx;
   reg               stop_tx;
   reg [7:0]         stop_tx_counter;

   always_ff @(posedge tx_clk) begin
      stop_our_tx1 <= link_flow_control_stop_our_tx_async_in;
      stop_our_tx2 <= stop_our_tx1;
      stop_our_tx3 <= stop_our_tx2;
      if ((stop_our_tx2 == 1) && (stop_our_tx3 == 0)) begin
         stop_our_tx <= 1;
      end else begin
         stop_our_tx <= 0;
      end

      if (stop_our_tx) begin
         stop_tx <= 1;
         stop_tx_counter <= 0;
      end else if (stop_tx) begin
         if (stop_tx_counter > 8'h24) begin
            stop_tx <= 0;
            stat_flow_tx <= stat_flow_tx + 1;
         end else begin
            stop_tx_counter <= stop_tx_counter + 8'b1;
         end
      end
   end // always_ff @ (posedge tx_clk)

   // stop_remote_tx is a level but we only send the stop chars periodically

   reg               stop_remote_tx1, stop_remote_tx2, stop_remote_tx;
   reg               send_stop_remote_tx;
   reg [7:0]         send_stop_remote_tx_counter;
   reg               stop_remote_tx_sent;
   
   always_ff @(posedge tx_clk) begin
      stop_remote_tx1 <= link_flow_control_stop_remote_tx_async_in;
      stop_remote_tx2 <= stop_remote_tx1;
      stop_remote_tx  <= stop_remote_tx2;

      if (stop_remote_tx_sent) begin
         send_stop_remote_tx <= 0;
      end

      if (stop_remote_tx) begin
         if (send_stop_remote_tx_counter == 0) begin
            send_stop_remote_tx <= 1;
            send_stop_remote_tx_counter <= send_stop_remote_tx_counter + 8'b1;
         end else if (send_stop_remote_tx_counter > 8'h10) begin
            send_stop_remote_tx <= 1;
            send_stop_remote_tx_counter <= 0;
         end else begin
            send_stop_remote_tx_counter <= send_stop_remote_tx_counter + 8'b1;
         end
      end else begin
         send_stop_remote_tx <= 0;
         send_stop_remote_tx_counter <= 0;
      end
   end

   localparam K_CTRL1 	= 8'h1C; // K.28.0 /R/ // idle when link_rx_status is good
   localparam K_CTRL2 	= 8'h3C; // K.28.1     // stop tx
   localparam K_CTRL3 	= 8'h5C; // K.28.2     // tx stopped
   localparam K_CTRL4 	= 8'h7C; // K.28.3 /A/
   localparam K_CTRL5 	= 8'h9C; // K.28.4 /Q/
   localparam K_CTRL 	= 8'hBC; // K.28.5 /K/ // sync
   localparam K_INVALID	= 8'hF7; // K.23.7
   localparam K_TRIG	= 8'hFB; // K.27.7 /S/ // trigger
   localparam K_REBOOT  = 8'hFD; // K.29.7 /T/ // reboot
   
   reg               alternate;

   always_ff @(posedge tx_clk) begin

      if (trig_sent & !send_trig) begin
         trig_sent <= 0;
      end

      if (reboot_sent & !send_reboot) begin
         reboot_sent <= 0;
      end

      if (stop_remote_tx_sent & !send_stop_remote_tx) begin
         stop_remote_tx_sent <= 0;
      end
      
      if (send_reboot & !reboot_sent) begin
         sink8_ready <= 0;
         if (sink8_valid & sink8_ready) begin
            skid_data <= sink8_data;
            skid_valid <= 1;
         end
         tx_data <= K_REBOOT;
         tx_k <= 1'b1;
         //stat_reboot_tx <= stat_reboot_tx + 1;
         reboot_sent <= 1;
      end else if (send_trig & !trig_sent) begin
         sink8_ready <= 0;
         if (sink8_valid & sink8_ready) begin
            skid_data <= sink8_data;
            skid_valid <= 1;
         end
         tx_data <= K_TRIG;
         tx_k <= 1'b1;
         stat_trig_tx <= stat_trig_tx + 1;
         trig_sent <= 1;
      end else if (send_stop_remote_tx & !stop_remote_tx_sent) begin
         sink8_ready <= 0;
         if (sink8_valid & sink8_ready) begin
            skid_data <= sink8_data;
            skid_valid <= 1;
         end
         tx_data <= K_CTRL2;
         tx_k <= 1'b1;
         stat_stop_tx <= stat_stop_tx + 1;
         stop_remote_tx_sent <= 1;
      end else if (stop_tx) begin
         sink8_ready <= 0;
         if (sink8_valid & sink8_ready) begin
            skid_data <= sink8_data;
            skid_valid <= 1;
         end
         if (alternate) begin
            tx_data <= K_CTRL3;
         end else begin
            tx_data <= K_CTRL;
         end
         tx_k <= 1'b1;
      end else begin // if (stop_tx)
         if (skid_valid) begin
            // if data in skid buffer, transmit it
            skid_valid <= 0;
            sink8_ready <= 1;
            tx_data <= skid_data;
            tx_k    <= 1'b0;
            stat_octets_tx <= stat_octets_tx + 1;
            if (skid_data == 8'h7C) begin
               stat_pkts_tx <= stat_pkts_tx + 1;
            end
         end else if (sink8_valid & sink8_ready) begin
            // if data in input stream, transmit it
            sink8_ready <= 1;
            tx_data <= sink8_data;
            tx_k    <= 1'b0;
            stat_octets_tx <= stat_octets_tx + 1;
            if (sink8_data == 8'h7C) begin
               stat_pkts_tx <= stat_pkts_tx + 1;
            end
         end else begin
            // nothing to transmit, just assert our ready signal
            sink8_ready <= 1;
            if (link_rx_status && alternate) begin
               tx_data <= K_CTRL1;
            end else begin
               tx_data <= K_CTRL;
            end
            tx_k <= 1'b1;
         end
      end
      alternate <= ~alternate;
   end // always_ff @ (posedge clk)
endmodule
