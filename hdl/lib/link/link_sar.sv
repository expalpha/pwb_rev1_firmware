module link_sar (
	clk,
	rst,
	clock_align_in,
	clock_align_out,
	link_status,
	trig_in,
	trig_out,
	
	tx_clk,
	tx_data,
	tx_k,
	
	rx_clk,
	rx_data,
	rx_k,
	rx_err,
	
	//sink_error,
	//sink_valid,
	//sink_data,
	//sink_sop,
	//sink_eop,
	//sink_ready,
	//sink_empty,
	
	sink8_valid,
	sink8_data,
	sink8_ready,
	
	source_valid,
	source_data,
	source_sop,
	source_eop,
	source_ready,
	source_empty,
	
	stat_trig_tx,
	stat_trig_rx,
	
	stat_err_crc,
	stat_err_8b10,
	stat_pkts_tx,
	stat_pkts_rx,	
	stat_octets_tx,
	stat_octets_rx
);

parameter RX_FIFO_DEPTH = 16384;

input wire 				clk;
input wire				rst;

input wire 				clock_align_in;
output wire				clock_align_out;

output wire				link_status;

input  wire				tx_clk;
output wire	[7:0]	tx_data;
output wire [0:0]		tx_k;

input  wire trig_in;
output wire trig_out;

input  wire        	rx_clk;
input  wire [7:0]  	rx_data;
input  wire [0:0]		rx_k;
input  wire  			rx_err;

//input  wire 		sink_error;
//input  wire 		sink_valid;
//input  wire [31:0] 	sink_data;
//input  wire 		sink_sop;
//input  wire 		sink_eop;
//output wire 		sink_ready;
//input wire [1:0]   	sink_empty;

input  wire 		sink8_valid;
input  wire [7:0] 	sink8_data;
output wire 		sink8_ready;

output wire 			source_valid;
output wire [31:0] 	source_data;
output wire 			source_sop;
output wire 			source_eop;
input  wire 			source_ready;
output wire [1:0] 	source_empty;

output wire [31:0] 	stat_trig_tx;
output wire [31:0] 	stat_trig_rx;

output wire [31:0] 	stat_err_crc;
output wire [31:0] 	stat_err_8b10;
output wire [31:0] 	stat_pkts_tx;
output wire [31:0] 	stat_pkts_rx;	
output wire [31:0] 	stat_octets_tx;
output wire [31:0] 	stat_octets_rx;

wire [31:0] rx_src_dat;
wire rx_src_val;
wire rx_src_sop;
wire rx_src_eop;
wire rx_src_rdy;
wire rx_src_err;
wire [1:0] rx_src_epy;
wire rx_link_status;
wire remote_link_status;

assign link_status = rx_link_status & remote_link_status;


link_tx tx ( 
	.clk				( clk ),
	.rst				( rst ),
	.clock_align	( clock_align_in ),
	.link_status	( rx_link_status ),
	.remote_link_status ( remote_link_status ),
	.tx_clk			( tx_clk ),
	.tx_data			( tx_data ),
	.tx_k				( tx_k ),
	.trig_in			( trig_in ),
	//.sink_error		( sink_error ),
	//.sink_valid		( sink_valid ),
	//.sink_data		( sink_data ),
	//.sink_empty 	( sink_empty ),
	//.sink_sop		( sink_sop ),
	//.sink_eop		( sink_eop ),
	//.sink_ready		( sink_ready ),
	.sink8_valid		( sink8_valid ),
	.sink8_data		( sink8_data ),
	.sink8_ready		( sink8_ready ),
	.stat_trig_tx	( stat_trig_tx ),
	.stat_octets_tx( stat_octets_tx ),
	.stat_pkts_tx	( stat_pkts_tx )
);

endmodule
