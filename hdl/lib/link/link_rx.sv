//
// SATA Link receiver
//

`default_nettype none
module link_rx
  (
   // Cyclone5 NativePhy serial data
   
   input  wire 	     rx_clk,
   input  wire [7:0] rx_data,
   input  wire       rx_k,

   // Cyclone5 NativePhy error outputs:
   
   input  wire       pll_locked,
   input  wire       rx_is_lockedtodata, // rx_clk is locked to received data
   input  wire       rx_signal_detect, // rx signal voltage above threshold
   input  wire       rx_errdetect, // 8b10b invalid 10-bit code or disparity error
   input  wire       rx_disperr, // 8b10b disparity error
   input  wire       rx_patterndetect, // programmed word alignment pattern has been detected in the current word boundary
   input  wire       rx_syncstatus, // word aligner identifies the word alignment pattern ...

   // Avalon-ST output
   
   input  wire       source8_ready,
   output wire 	     source8_valid,
   output wire [7:0] source8_data,

   // Flow control
   
   output reg        link_flow_control_stop_our_tx_out,
   output reg        link_flow_control_stop_remote_tx_out,

   // Trigger output
   
   output reg        trig_pulse3_out,

   // Reboot output
   
   output reg        reboot_pulse3_out,

   // Link status

   output reg        link_rx_status_out, // receiver is happy with incoming data
   output reg        link_status_out,    // link is ready to use
   output reg        err_out,            // unhappy with anything

   // Statistics into ESPER

   output reg [31:0] stat_badk_rx,
   output reg [31:0] stat_trig_rx,
   output reg [31:0] stat_stop_rx,
   output reg [31:0] stat_ovfl_rx,
   output reg [31:0] stat_octets_rx,
   output reg [31:0] stat_pkts_rx
   );

   // pll lock monitor

   reg  have_rx_is_lockedtodata;
   reg  prev_rx_is_lockedtodata;
   reg [31:0] rx_is_lockedtodata_counter;
   
   always_ff @(posedge rx_clk) begin
      if (prev_rx_is_lockedtodata == rx_is_lockedtodata) begin
         if (rx_is_lockedtodata_counter == 32'hFFFFFFFF) begin
            // counter overflow
         end else begin
            rx_is_lockedtodata_counter <= rx_is_lockedtodata_counter + 1;
         end
      end else begin
         rx_is_lockedtodata_counter <= 0;
      end

      prev_rx_is_lockedtodata <= rx_is_lockedtodata;

      if (!rx_is_lockedtodata) begin
         have_rx_is_lockedtodata <= 0;
      end else if (rx_is_lockedtodata_counter > 300000000) begin
         // wait for 1000 ms = 125000000*8ns
         have_rx_is_lockedtodata <= 1;
      end
   end

   // link status monitor

   wire rx_ok = 1'b1
        & pll_locked
        & rx_is_lockedtodata
        & have_rx_is_lockedtodata
        & rx_signal_detect
        & !rx_errdetect
        & !rx_disperr
        & rx_syncstatus
        & 1'b1;

   reg  have_rx_patterndetect;
   reg  rx_status;
   reg  drop_rx_status;
   reg  rx_status_counting;
   reg [31:0] rx_status_counter;
   
   always_ff @(posedge rx_clk) begin

      if (!rx_ok) begin
         have_rx_patterndetect <= 0;
      end else begin
         if (rx_patterndetect) begin
            have_rx_patterndetect <= 1;
         end
      end

      // computed:
      // rx_ok - nativephy is happy
      // have_rx_patterndetect - have seen at least one rx_patterndetect

      // now compute rx_status

      if (drop_rx_status) begin
         rx_status <= 0;
         rx_status_counting <= 0;
      end else if (rx_ok) begin
         if (rx_status) begin
            // all good
         end else begin
            // rx_ok good, rx_status bad, change it
            if (have_rx_patterndetect) begin
               if (rx_status_counting) begin
                  // already counting
                  if (rx_status_counter > 300000000) begin
                     // see rx_ok for 1000 ms = 125000000 * 8ns
                     rx_status <= 1;
                     rx_status_counting <= 0;
                  end else begin
                     rx_status_counter <= rx_status_counter + 1;
                  end
               end else begin
                  // start the counter
                  rx_status_counter <= 0;
                  rx_status_counting <= 1;
               end
            end else begin
               // no pattern detect
               rx_status <= 0;
               rx_status_counting <= 0;
            end
         end
      end else begin
         // lost rx_ok: drop rx_status, stop the counter
         rx_status <= 0;
         rx_status_counting <= 0;
      end
   end
   
   localparam K_CTRL1 	= 8'h1C; // K.28.0 /R/  // idle when link_rx_status is good
   localparam K_CTRL2 	= 8'h3C; // K.28.1      // stop tx
   localparam K_CTRL3 	= 8'h5C; // K.28.2      // tx stopped
   localparam K_CTRL4 	= 8'h7C; // K.28.3 /A/
   localparam K_CTRL5 	= 8'h9C; // K.28.4 /Q/
   localparam K_CTRL 	= 8'hBC; // K.28.5 /K/  // sync
   localparam K_INVALID	= 8'hF7; // K.23.7
   localparam K_TRIG	= 8'hFB; // K.27.7 /S/  // trigger
   localparam K_REBOOT  = 8'hFD; // K.29.7 /T/  // reboot

   reg stop_tx;

   reg trig_out;
   reg reboot_out;

   reg [7:0] fifo_data;
   reg       fifo_write;

   always_ff @(posedge rx_clk) begin
      if (rx_status & rx_ok) begin
         if (rx_k) begin
	    case (rx_data) 
	      K_CTRL: begin // sync
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 0;
                 err_out <= 0;
                 link_rx_status_out <= 1;
                 //link_status_out <= 1;
                 stop_tx <= 0;
	      end
              
	      K_CTRL1: begin // remote tx_status is good
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 0;
                 err_out <= 0;
                 link_rx_status_out <= 1;
                 link_status_out <= 1;
                 stop_tx <= 0;
	      end
              
	      K_CTRL2: begin // stop our tx
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 0;
                 err_out <= 0;
                 link_rx_status_out <= 1;
                 link_status_out <= 1;
                 stop_tx <= 1;
                 stat_stop_rx <= stat_stop_rx + 1;
	      end
              
	      K_CTRL3: begin // remote tx is stopped
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 0;
                 err_out <= 0;
                 link_rx_status_out <= 1;
                 link_status_out <= 1;
                 stop_tx <= 0;
	      end
              
	      K_TRIG: begin
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 1;
	         reboot_out <= 0;
                 err_out <= 0;
                 stat_trig_rx <= stat_trig_rx + 1;
                 stop_tx <= 0;
	      end
	      
	      K_REBOOT: begin
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 1;
                 err_out <= 0;
                 //stat_reboot_rx <= stat_reboot_rx + 1;
                 stop_tx <= 0;
	      end
	      
	      default: begin
                 fifo_write <= 0;
                 fifo_data <= 8'hFF;
	         trig_out <= 0;
	         reboot_out <= 0;
                 err_out <= 1;
                 //drop_rx_status <= 1;
                 stat_badk_rx <= stat_badk_rx + 1;
                 stop_tx <= 0;
	      end
	    endcase
         end else begin // if (rx_k)
            stop_tx <= 0;
            if (!fifo_full) begin
               fifo_write <= 1;
               fifo_data <= rx_data;
	       trig_out <= 0;
	       reboot_out <= 0;
               err_out <= 0;
               stat_octets_rx <= stat_octets_rx + 1;
               if (rx_data == 8'h7A) begin
                  stat_pkts_rx <= stat_pkts_rx + 1;
               end
            end else begin
               fifo_write <= 0;
               fifo_data <= rx_data;
	       trig_out <= 0;
	       reboot_out <= 0;
               err_out <= 1;
               //drop_rx_status <= 1;
               stat_ovfl_rx <= stat_ovfl_rx + 1;
            end
         end // else: !if(rx_k)
      end else begin
         link_rx_status_out <= 0;
         link_status_out <= 0;
         drop_rx_status <= 0;
         fifo_write <= 0;
         fifo_data <= 8'hFF;
	 trig_out <= 0;
	 reboot_out <= 0;
         err_out  <= 0;
         stop_tx  <= 0;
      end
   end // always_ff @ (posedge rx_clk)

   //
   // https://electronics.stackexchange.com/questions/375215/how-does-one-read-a-fifo-outside-qsys-system-using-nios-ii
   // Valid = ~empty
   // Ready = rdreq
   // Data = q
   //
   // https://www.edaboard.com/showthread.php?339973-Altera-Avalon-ST-packet-transfer
   //
   // https://www.edaboard.com/showthread.php?339973-Altera-Avalon-ST-packet-transfer/page2
   //
   
   always_ff @(posedge rx_clk) begin
   end

   wire fifo_full, fifo_empty;

   link_rx_fifo fifo
     (
      .clock ( rx_clk ),
      .sclr  ( 1'b0 ),
      .wrreq ( fifo_write ),
      .data  ( fifo_data ),
      .full  ( fifo_full ),
      .rdreq( !fifo_empty & source8_ready ), // ACK
      .q ( source8_data ),
      .empty( fifo_empty )
      );

   assign source8_valid = !fifo_empty;

   //
   // scfifo_component.add_ram_output_register = "ON",
   // scfifo_component.intended_device_family = "Cyclone V",
   // scfifo_component.lpm_numwords = 32,
   // scfifo_component.lpm_showahead = "ON",
   // scfifo_component.lpm_type = "scfifo",
   // scfifo_component.lpm_width = 36,
   // scfifo_component.lpm_widthu = 5,
   // scfifo_component.overflow_checking = "ON",
   // scfifo_component.underflow_checking = "ON",
   // scfifo_component.use_eab = "ON";
   //

   reg ready1, ready2, ready3;

   always_ff @(posedge rx_clk) begin
      ready1 <= source8_ready;
      ready2 <= ready1;
      ready3 <= ready2;
      link_flow_control_stop_remote_tx_out <= !(ready1 | ready2 | ready3);
   end
      
   reg stop1, stop2, stop3;

   always_ff @(posedge rx_clk) begin
      stop1 <= stop_tx;
      stop2 <= stop1;
      stop3 <= stop2;
      link_flow_control_stop_our_tx_out <= (stop1 | stop2 | stop3);
   end
      
   reg trig1, trig2, trig3;

   always_ff @(posedge rx_clk) begin
      trig1 <= trig_out;
      trig2 <= trig1;
      trig3 <= trig2;
      trig_pulse3_out <= (trig1 | trig2 | trig3);
   end
      
   reg reboot1, reboot2, reboot3;

   always_ff @(posedge rx_clk) begin
      reboot1 <= reboot_out;
      reboot2 <= reboot1;
      reboot3 <= reboot2;
      reboot_pulse3_out <= (reboot1 | reboot2 | reboot3);
   end
      
endmodule
