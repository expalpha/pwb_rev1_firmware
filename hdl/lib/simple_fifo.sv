module simple_fifo (
	clk,
	rst,
	
	d,	
	val,
	
	ack,
	q,
	
	full,
	empty
);

input wire clk;
input wire rst;
	
input wire d;
input wire val;
	
input wire ack;
input wire q;
	
input wire full;
input wire empty;


endmodule
