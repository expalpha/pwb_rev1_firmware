`default_nettype none
module packet_chunk (
	clk,
	rst,
	
	dev_id,
	
	in_sop,
	in_eop,
	in_dat,
	in_val,
	in_rdy,

	out_sop,
	out_eop,
	out_val,
	out_dat,
	out_rdy
);

parameter NUM_STREAMS = 1;

localparam SZ_DATA = 32;
localparam CRC32C_INIT 	= 32'hffffffff;

input wire clk;
input wire rst;

input wire [31:0] dev_id;

input wire in_sop;
input wire in_eop;
input wire [SZ_DATA-1:0] in_dat;
input wire in_val;
output reg in_rdy;

output reg out_val;
output reg out_sop;
output reg out_eop;
output reg [SZ_DATA-1:0] out_dat;
input wire out_rdy;

reg [31:0] pkt_seq;
reg cap_val;
reg [SZ_DATA-1:0] cap_dat;
reg cap_eop;

enum int unsigned { 
	ST_IDLE 	= 0, 
	ST_H1 	= 1, 
	ST_H2 	= 2, 
	ST_H3 	= 4, 
	ST_H4		= 8,
	ST_H5		= 16,
  	ST_DATA 	= 32, 
  	ST_CRC	= 64,
  	ST_END 	= 128
} out_state;

reg [SZ_DATA-1:0] crc_c;
reg [SZ_DATA-1:0] crc_in;
wire [SZ_DATA-1:0] crc_dat;
					 
CRC32C_AVALON_ST crc_calc (
	.d		( crc_in ), 
	.c		( crc_c ), 
	.q		( crc_dat )
);

wire in_captured = in_rdy & in_val;
wire out_captured = out_rdy & out_val;
    
// Add extra information to mux'd packet stream (packet_num, header_crc, and payload_crc)
always@(posedge rst, posedge clk) begin 
	if(rst) begin
		out_state 	<= ST_IDLE;		
		out_val		<= 1'b0;
		out_sop		<= 1'b0;
		out_eop		<= 1'b0;
		out_dat		<= 32'h0;
      cap_dat		<= {SZ_DATA{1'b0}};
		cap_val 		<= 1'b0;
		crc_c			<= {SZ_DATA{1'b0}};
		crc_in		<= {SZ_DATA{1'b0}};
		pkt_seq 		<= 32'h0;
	end else begin
		case(out_state) 
		ST_IDLE: begin		          
			cap_dat <= in_dat;
        	cap_val <= in_captured;
			cap_eop <= 1'b0; // packet_chunk_streamer will always give a minimum of 3 word 'mini' chunk          
			crc_c		<= CRC32C_INIT;
			crc_in	<= {dev_id[7:0], dev_id[15:8], dev_id[23:16], dev_id[31:24]};		
			pkt_seq	<= pkt_seq;
			// Did we capture data?
			if(in_sop && in_captured) begin
				in_rdy   		<= 1'b0;
				out_state 		<= ST_H1;
				out_val			<= 1'b1;
				out_sop			<= 1'b1;
				out_eop			<= 1'b0;
				out_dat			<= {dev_id[7:0], dev_id[15:8], dev_id[23:16], dev_id[31:24]};		
			end else begin
				in_rdy   		<= 1'b1;
				out_state 		<= ST_IDLE;
				out_val			<= 1'b0;
				out_sop			<= 1'b0;
				out_eop			<= 1'b0;
				out_dat			<= {SZ_DATA{1'b0}};
			end
		end

		
		// Device ID has been sent out, send out packet sequence
		ST_H1: begin							
			pkt_seq	<= pkt_seq;		
			// Did we capture data?
			if(out_captured) begin
				in_rdy   	<= 1'b0;
				out_state 	<= ST_H2;
				out_val		<= 1'b1;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= {pkt_seq[7:0], pkt_seq[15:8], pkt_seq[23:16], pkt_seq[31:24]};		
				crc_c			<= crc_dat;
				crc_in		<= {pkt_seq[7:0], pkt_seq[15:8], pkt_seq[23:16], pkt_seq[31:24]};		
				cap_dat 		<= cap_dat;
				cap_val 		<= cap_val;
				cap_eop 		<= 1'b0;          
			end else begin
				in_rdy		<= 1'b0;
				out_state	<= ST_H1;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;
				cap_dat 		<= cap_dat;
				cap_val 		<= cap_val;
            cap_eop 		<= 1'b0;
			end
		end
		
		// Packet counter has been sent, send out first header
		ST_H2: begin			
			// Did we capture data?
			if(out_captured) begin
				pkt_seq		<= pkt_seq + 1'b1;
				in_rdy		<= 1'b1;
				out_state 	<= ST_H3;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_val		<= cap_val; // guaranteed to be true due to in_sop & in_capture in IDLE statement
				out_dat		<= cap_dat; // H2 (streamer H1)   
				crc_c			<= crc_dat;
				crc_in		<= cap_dat; // H2
				cap_dat 		<= 32'h0; 
        		cap_val 		<= 1'b0;
				cap_eop		<= 1'b0;
			end else begin
				pkt_seq	<= pkt_seq;
            in_rdy		<= 1'b0;
				out_state 	<= ST_H2;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;
				cap_dat 		<= cap_dat;
				cap_val 		<= cap_val;
            cap_eop 		<= cap_eop;
			end
		end

		// First header word from streamer sent out
		// Send out second header word from streamer
		ST_H3: begin
			pkt_seq	<= pkt_seq;
			if(out_captured) begin				
				in_rdy		<= 1'b0;
            out_state 	<= ST_H4;
				out_val		<= cap_val ? cap_val : in_captured;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= cap_val ? cap_dat : in_dat;              
				crc_c			<= crc_dat;
				crc_in		<= cap_val ? cap_dat : in_dat;
				cap_dat		<= 32'h0;
				cap_val		<= 1'b0;
				cap_eop 		<= cap_val ? cap_eop : in_eop;
			end else begin								
				out_state 	<= ST_H3;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;			
				if(!cap_val) begin
					in_rdy  <= ~in_captured;
					cap_dat <= in_dat;
					cap_val <= in_captured;
					cap_eop <= in_eop;
				end else begin
					in_rdy  <= 1'b0;
					cap_dat <= cap_dat;
					cap_val <= cap_val;
					cap_eop <= cap_eop;
          	end
			end
		end		
		
		// Second header sent, send out CRC
		ST_H4: begin
			pkt_seq	<= pkt_seq;
			if(out_captured) begin				
				in_rdy   	<= 1'b1;
				out_state 	<= ST_H5;
				out_val		<= 1'b1;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= {crc_dat[7:0], crc_dat[15:8], crc_dat[23:16], crc_dat[31:24]};
				crc_c			<= 32'h0;
				crc_in		<= 32'h0;
				cap_dat		<= 32'h0;
				cap_val		<= 1'b0;
			end else begin
				out_state 	<= ST_H4;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;					
				if(!cap_val) begin
					in_rdy  <= ~in_captured;
					cap_dat <= in_dat;
					cap_val <= in_captured;
					cap_eop <= in_eop;
				end else begin
					in_rdy  <= 1'b0;
					cap_dat <= cap_dat;
					cap_val <= cap_val;
					cap_eop <= cap_eop;
          	end
			end
		end	
		
		ST_H5: begin
			pkt_seq	<= pkt_seq;
			// Did we capture data?
			if(out_captured) begin
				in_rdy   	<= (cap_val) ? ~cap_eop : ~(in_eop & in_captured);
				out_state 	<= (cap_val & cap_eop) || (!cap_val & in_eop & in_captured) ? ST_CRC : ST_DATA;
				out_val		<= (cap_val) ? cap_val : in_captured;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= cap_val ? cap_dat : in_dat;		
				crc_c			<= CRC32C_INIT;
				crc_in		<= cap_val ? cap_dat : in_dat;											
				cap_dat		<= 32'h0;
				cap_val		<= 1'b0;				
			end else begin
				out_state 	<= ST_DATA;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;					
				if(!cap_val) begin
					in_rdy  <= ~in_captured;
					cap_dat <= in_dat;
					cap_val <= in_captured;
					cap_eop <= in_eop;
				end else begin
					in_rdy  <= 1'b0;
					cap_dat <= cap_dat;
					cap_val <= cap_val;
					cap_eop <= cap_eop;
          	end	
			end
		end			
		
		ST_DATA: begin
			pkt_seq	<= pkt_seq;
			// Did we capture data?
			if(out_captured) begin
				in_rdy   	<= (cap_val) ? ~cap_eop : ~(in_eop & in_captured);
				out_state 	<= (cap_val & cap_eop) || (!cap_val & in_eop & in_captured) ? ST_CRC : ST_DATA;
				out_val		<= (cap_val) ? cap_val : in_captured;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= cap_val ? cap_dat : in_dat;		
				crc_c			<= crc_dat;
				crc_in		<= cap_val ? cap_dat : in_dat;											
				cap_dat		<= 32'h0;
				cap_val		<= 1'b0;				
			end else begin
				out_state 	<= ST_DATA;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;					
				if(!cap_val) begin
					in_rdy  <= ~in_captured;
					cap_dat <= in_dat;
					cap_val <= in_captured;
					cap_eop <= in_eop;
				end else begin
					in_rdy  <= 1'b0;
					cap_dat <= cap_dat;
					cap_val <= cap_val;
					cap_eop <= cap_eop;
          	end		
			end
		end	

		ST_CRC: begin
			pkt_seq	<= pkt_seq;
			// Did we capture data?
			if(out_captured) begin
				in_rdy   	<= 1'b0;
				out_state 	<= ST_END;
				out_val		<= 1'b1;
				out_sop		<= 1'b0;
				out_eop		<= 1'b1;
				out_dat		<= {crc_dat[7:0], crc_dat[15:8], crc_dat[23:16], crc_dat[31:24]};
				crc_c			<= crc_c;
				crc_in		<= crc_in;					
				cap_dat		<= cap_dat;
				cap_eop		<= cap_eop;				
			end else begin
				in_rdy   	<= 1'b0;
				out_state 	<= ST_CRC;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
				crc_c			<= crc_c;
				crc_in		<= crc_in;					
				cap_dat		<= cap_dat;
				cap_eop		<= cap_eop;				
			end
		end	
		
		ST_END: begin
			pkt_seq	<= pkt_seq;
			crc_c		<= crc_c;
			crc_in	<= crc_in;					
			cap_dat	<= cap_dat;
			cap_eop	<= cap_eop;				
			if(out_captured) begin
				in_rdy   	<= 1'b0;
				out_state 	<= ST_IDLE;
				out_val		<= 1'b0;
				out_sop		<= 1'b0;
				out_eop		<= 1'b0;
				out_dat		<= 32'h0;
			end else begin
				in_rdy   	<= 1'b0;
				out_state 	<= ST_END;
				out_val		<= out_val;
				out_sop		<= out_sop;
				out_eop		<= out_eop;
				out_dat		<= out_dat;
			end
		end
		
		endcase	
	end
end

endmodule
