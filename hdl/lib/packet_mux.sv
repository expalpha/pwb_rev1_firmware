module packet_rr_mux (
	clk,
	rst,
	
	in_sop,
	in_eop,
	in_dat,
	in_val,
	in_empty,
	in_rdy,

	out_sop,
	out_eop,
	out_val,
	out_dat,
	out_empty,
	out_rdy
);

parameter NUM_STREAMS 	= 2;
parameter SZ_DATA 		= 32;
parameter SZ_SYMBOL 		= 8;

localparam SZ_STREAMS = $clog2(NUM_STREAMS) < 1 ? 1 : $clog2(NUM_STREAMS);
localparam SZ_EMPTY = $clog2(SZ_DATA/SZ_SYMBOL) < 1 ? 1 : $clog2(SZ_DATA/SZ_SYMBOL);

input wire clk;
input wire rst;

input wire [NUM_STREAMS-1:0] in_sop;
input wire [NUM_STREAMS-1:0] in_eop;
input wire [NUM_STREAMS-1:0] [SZ_DATA-1:0] in_dat;
input wire [NUM_STREAMS-1:0] in_val;
input wire [NUM_STREAMS-1:0] [SZ_EMPTY-1:0] in_empty;
output wire [NUM_STREAMS-1:0] in_rdy;

output wire out_val;
output wire out_sop;
output wire out_eop;
output wire [SZ_DATA-1:0] out_dat;
output wire [SZ_EMPTY-1:0] out_empty;
input wire out_rdy;
  
reg [NUM_STREAMS-1:0] ch_oh;
reg [SZ_STREAMS-1:0] ch_sel;

reg ena;
wire grant_valid;
wire [NUM_STREAMS-1:0] req;
wire [NUM_STREAMS-1:0] grant_onehot;
wire [SZ_STREAMS-1:0] grant_binary;

wire fifo_sop;
wire fifo_eop;
wire [SZ_EMPTY-1:0] fifo_epy;
wire [SZ_DATA-1:0] fifo_dat;
  
enum int unsigned { ST_IDLE = 0, ST_PACKET = 2 } mux_state;
 
// Round Robin Arbiter for MUX
packet_rr_arbiter #(
	.NUM_STREAMS( NUM_STREAMS )
) arbiter (
	.clk				( clk ),
	.rst				( rst ),
	.request			( req ),		
	.ena				( ena ), 
	.grant_binary	( grant_binary ),
	.grant_onehot	( grant_onehot ),
	.grant_valid	( grant_valid  )
);

wire fifo_full;
wire fifo_empty;
wire in_captured = in_val[ch_sel] & in_rdy[ch_sel];
  
// Set in_rdy to match channel selected and out_rdy
genvar n;
generate
	for(n=0; n<NUM_STREAMS; n+=1) begin : gen_channels   
		assign in_rdy[n] 	= (!fifo_full) & ch_oh[n];    
		assign req[n] 		= in_val[n] & in_sop[n];
	end
endgenerate
  
// Incoming Mux+FIFO state machine
always@(posedge rst, posedge clk) begin 
	if(rst) begin
		mux_state 	<= ST_IDLE;		
		ena			<= 1'b1; 
      ch_sel		<= {SZ_STREAMS{1'b0}};
      ch_oh			<= {NUM_STREAMS{1'b0}};
	end else begin
		case(mux_state) 
		// Not in a packet
		ST_IDLE: begin		          
			// Can always be selected, meaningless if still in IDLE
			// capture selected channel data and enter packet states
			// since selected channel isn't yet 'ready' we can't take its data yet
			if(grant_valid) begin			            	
				mux_state 	<= ST_PACKET;				
				ch_sel		<= grant_binary;
				ch_oh			<= grant_onehot;
				ena			<= 1'b0;
			end else begin
				mux_state 	<= ST_IDLE;
				ch_sel		<= {SZ_STREAMS{1'b0}};
				ch_oh			<= {NUM_STREAMS{1'b0}};          	
				ena			<= 1'b1;
			end			
		end
		
		// In a packet
		ST_PACKET: begin
			// Have we reached the end of the current packet?
			if(in_captured & in_eop[ch_sel]) begin
				// If there is another packet available in one of the mux inputs, go there, otherwise go idle
				mux_state	<= (grant_valid) ? ST_PACKET : ST_IDLE;
				ch_sel 		<= (grant_valid) ? grant_binary : {SZ_STREAMS{1'b0}};
				ch_oh			<= (grant_valid) ? grant_onehot : {NUM_STREAMS{1'b0}};  
				ena			<= (grant_valid) ? 1'b0 : 1'b1;			
			// Just keep going
			end else begin
				mux_state	<= ST_PACKET;
				ch_sel 		<= ch_sel;
				ch_oh			<= ch_oh;		
				ena			<= 1'b0; // wait until we've finished with this packet before updating the search mask
			end
		end

		endcase	
	end
end  

/*
reg fifo_ack;

// Outgoing Mux+FIFO state machine
always@(posedge rst, posedge clk) begin 
	if(rst) begin
		out_val		<= 1'b0;
		out_sop		<= 1'b0;
		out_eop		<= 1'b0;
		out_dat		<= {SZ_DATA{1'b0}};
		out_empty	<= {SZ_EMPTY{1'b0}};	
		fifo_ack		<= 1'b0;
	end else begin	
		if(!out_val) begin	
			out_val 		<= ~fifo_empty & fifo_ack;
			out_dat 		<= fifo_dat;
			out_sop 		<= fifo_sop;
			out_eop 		<= fifo_eop;
			out_empty 	<= fifo_epy;
			fifo_ack		<= ~fifo_empty;
		end else begin
			// Captured data
			if(out_rdy) begin
				out_val 		<= ~fifo_empty & fifo_ack; //  can we pull more from the FIFO immediately?
				out_dat 		<= fifo_dat;
				out_sop 		<= fifo_sop;
				out_eop 		<= fifo_eop;
				out_empty 	<= fifo_epy;
				fifo_ack		<= ~fifo_empty;
			// Hold steady, upstream sink didn't capture data yet
			end else begin
				out_val 		<= out_val;
				out_dat 		<= out_dat;
				out_sop 		<= out_sop;
				out_eop 		<= out_eop;
				out_empty 	<= out_empty;
				fifo_ack		<= 1'b0;
			end
		end
	end
end  
*/

wire out_captured = !fifo_empty & out_rdy;

assign out_val = !fifo_empty;

mux_fifo internal_fifo (
	.clock( clk ),
	.sclr ( rst ),

	.wrreq( in_captured ), // in_rdy relies on fifo_full so this is enough
	.data ( {in_sop[ch_sel], in_eop[ch_sel], in_empty[ch_sel], in_dat[ch_sel] } ),
	.full	( fifo_full ),
	
	.rdreq( out_captured ), // ACK
	.q		( { out_sop, out_eop, out_empty, out_dat} ),
	.empty( fifo_empty )
);

endmodule
