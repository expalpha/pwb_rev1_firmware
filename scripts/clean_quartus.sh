#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$SCRIPTDIR/../hdl
PROJECT_NAME="feam"
PROJECT_REV="rev1"

# go to project dir
cd $PROJECT_PATH
quartus_sh --clean $PROJECT_NAME
