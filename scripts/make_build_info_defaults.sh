#/bin/sh

if [ ! -d build ]; then 
    mkdir build
fi 

if [ ! -f src/build_number.h ]; then 
    buildnum=0
    minor_version="0"
    major_version="0"
    version="$major_version.$minor_version"
    timestamp=$(date +%s)
    timestamp_fmt=$(date -d@$timestamp)

    echo "#ifndef BUILD_NUMBER_STR" > src/build_number.h
    echo "#define BUILD_NUMBER_STR \"$buildnum\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h

    echo "#ifndef BUILD_NUMBER" >> src/build_number.h
    echo "#define BUILD_NUMBER $buildnum" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h

    echo "#ifndef BUILD_TIMESTAMP" >> src/build_number.h
    echo "#define BUILD_TIMESTAMP $timestamp" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
   
    echo "#ifndef VERSION_MAJOR" >> src/build_number.h
    echo "#define VERSION_MAJOR $major_version" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h

    echo "#ifndef VERSION_MINOR" >> src/build_number.h
    echo "#define VERSION_MINOR $minor_version" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
 
    echo "#ifndef VERSION_STR" >> src/build_number.h
    echo "#define VERSION_STR \"Ver $version  Build $buildnum - $timestamp_fmt\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h

    echo "#ifndef VERSION_STR_SHORT" >> src/build_number.h
    echo "#define VERSION_STR_SHORT \"$version.$buildnum\"" >> src/build_number.h
    echo "#endif" >> src/build_number.h
    echo >> src/build_number.h
    
   git_hash="$(git rev-parse --verify HEAD)"
   retval=$?
   if [ $retval -eq 0 ]; then 
       echo "#ifndef GIT_HASH_STR" >> src/build_number.h
       echo "#define GIT_HASH_STR \"${git_hash}\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   else 
       echo "#ifndef GIT_HASH_STR" >> src/build_number.h
       echo "#define GIT_HASH_STR \"Unknown\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   fi

   git_branch="$(git rev-parse --abbrev-ref HEAD)"
   retval=$?
   if [ $retval -eq 0 ]; then 
       echo "#ifndef GIT_BRANCH_STR" >> src/build_number.h
       echo "#define GIT_BRANCH_STR \"${git_branch}\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   else 
       echo "#ifndef GIT_BRANCH_STR" >> src/build_number.h
       echo "#define GIT_BRANCH_STR \"Unknown\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   fi

   git_tag="$(git tag --points-at ${git_hash})"
   retval=$?
   if [ $retval -eq 0 ]; then 
       echo "#ifndef GIT_TAG_STR" >> src/build_number.h
       echo "#define GIT_TAG_STR \"${git_tag}\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   else 
       echo "#ifndef GIT_TAG_STR" >> src/build_number.h
       echo "#define GIT_TAG_STR \"\"" >> src/build_number.h
       echo "#endif" >> src/build_number.h
       echo >> src/build_number.h
   fi

    user_name=`git config user.name`
    retval=$?
    if [ $retval -eq 0 ]; then 
        echo "#ifndef BUILT_BY_USER_STR" >> src/build_number.h
        echo "#define BUILT_BY_USER_STR \"$user_name\"" >> src/build_number.h
        echo "#endif" >> src/build_number.h
        echo >> src/build_number.h
    else 
        echo "#ifndef BUILT_BY_USER_STR" >> src/build_number.h
        echo "#define BUILT_BY_USER_STR \"First Last\"" >> src/build_number.h
        echo "#endif" >> src/build_number.h
        echo >> src/build_number.h
    fi

    user_email=`git config user.email`
    retval=$?
    if [ $retval -eq 0 ]; then 
        echo "#ifndef BUILT_BY_EMAIL_STR" >> src/build_number.h
        echo "#define BUILT_BY_EMAIL_STR \"$user_email\"" >> src/build_number.h
        echo "#endif" >> src/build_number.h
        echo >> src/build_number.h
    else 
        echo "#ifndef BUILT_BY_EMAIL_STR" >> src/build_number.h
        echo "#define BUILT_BY_EMAIL_STR \"noreply@example.com\"" >> src/build_number.h
        echo "#endif" >> src/build_number.h
        echo >> src/build_number.h
    fi
fi