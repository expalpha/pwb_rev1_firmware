#!/bin/sh

echo This script will update the ELF file inside the SOF, RPD and JIC files
echo
echo This script will *NOT* update the bootloader. Use update_bootloader.sh instead!

cd hdl
quartus_asm --read_settings_files=off --write_settings_files=off feam -c rev1
quartus_cpf -c ../bin/feam_rev1.cof

echo Done
