#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$SCRIPTDIR/../hdl
PROJECT_NAME="feam"
PROJECT_REV="rev1"

# go to project dir
cd $PROJECT_PATH
quartus_sh --flow compile $PROJECT_NAME -c $PROJECT_REV
grep -i critical ../bin/rev1.sta.rpt

#end
