#!/bin/sh

echo This script will update the bootloader and the ELF file inside the SOF, RPD and JIC files

cd hdl
quartus_cdb --read_settings_files=off --write_settings_files=off --update_mif feam -c rev1
quartus_asm --read_settings_files=off --write_settings_files=off feam -c rev1
quartus_cpf -c ../bin/feam_rev1.cof

echo Done
