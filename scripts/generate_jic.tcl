set module [lindex $quartus(args) 0]
set project [lindex $quartus(args) 1] 
set revision [lindex $quartus(args) 2] 

if [string match "quartus_asm" $module] {
	# Include commands here that are run after the assember
	post_message "Generating files from $project\_$revision.cof"
 
	# If the command can't be run, return an error.
	set status [catch {exec quartus_cpf -c ../bin/$project\_$revision.cof} output option]
	if {$status == 0} {
		post_message "Successfully ran $project\_$revision.cof"
	} else {		
		set err_info [lassign [dict get $option -errorcode] err_type]
		switch -exact -- $err_type {
			default {
				post_message -type error "Error occurred running $project\_$revision.cof\nError: $output"	
			}
		}
	}
}