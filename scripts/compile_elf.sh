#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$SCRIPTDIR/../hdl
NIOS_PATH=$PROJECT_PATH"/software"
NIOS_PROJECT="feam"
NIOS_PROJECT_BSP=$NIOS_PROJECT"_bsp"
BOOTLOADER_PROJECT="feam_bootloader"
BOOTLOADER_PROJECT_BSP=$BOOTLOADER_PROJECT"_bsp"

## Might as well recreate the bootloader if necessary
#echo "Generating FEAM Bootloader BSP"
#nios2-bsp-generate-files --silent --settings=$NIOS_PATH/$BOOTLOADER_PROJECT_BSP/settings.bsp --bsp-dir=$NIOS_PATH/$BOOTLOADER_PROJECT_BSP
echo "Making FEAM Bootloader Project"
make --quiet -C $NIOS_PATH/$BOOTLOADER_PROJECT all mem_init_generate 

## Then compile the main project
#echo "Generating FEAM BSP"
#nios2-bsp-generate-files --silent --settings=$NIOS_PATH/$NIOS_PROJECT_BSP/settings.bsp --bsp-dir=$NIOS_PATH/$NIOS_PROJECT_BSP
echo "Making FEAM Project"
make --quiet -C $NIOS_PATH/$NIOS_PROJECT all 
echo
echo To update the sof, pof, jic and rpd files, run: ./scripts/update_elf.sh
echo
echo If the bootloader has changed, run: ./scripts/update_bootloader.sh
echo
