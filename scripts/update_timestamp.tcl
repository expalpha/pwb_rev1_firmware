# Creates a constant in a verilog file with the specified hex value
proc generate_timestamp {hex_val str_val} {
    if { [catch {
        set fh [open "../hdl/build_timestamp.v" w ]
        puts $fh "module build_timestamp (data_out);"
        puts $fh "	output wire \[31:0\] data_out = 32'h${hex_val}; // ${str_val}"
        puts $fh "endmodule"
        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

foreach { flow project revision } $quartus(args) { break }

# Grab clock seconds
set timestamp [clock seconds]

# Format the time into a nice datetime string
set ts_in_str [clock format ${timestamp} -format "%Y-%m-%d %H:%M:%S"] 

# Format the time into a hex string
set ts_in_hex [format %x ${timestamp}]

if { [catch { generate_timestamp $ts_in_hex $ts_in_str } res] } {
    post_message -type critical_warning "Couldn't generate build_timestamp.v. $res"
} else {
    post_message "Updated build_timestamp.v to ${ts_in_str}"
}
