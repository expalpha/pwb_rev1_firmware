#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
PROJECT_PATH=$SCRIPTDIR/../hdl
QSYS_NAME="feam"

#qsys-generate  $PROJECT_PATH/$QSYS_NAME.qsys --synthesis=VERILOG --output-directory= $PROJECT_PATH/$QSYS_NAME/synthesis --family="Cyclone V" --part=5CGXFC7C7F23C8

ip-generate $PROJECT_PATH/$QSYS_NAME.qsys \
--remove-qsys-generate-warning \
--file-set=QUARTUS_SYNTH \
--output-dir=$PROJECT_PATH/$QSYS_NAME/synthesis \
--project-directory=$PROJECT_PATH \
--report-file=sopcinfo:$PROJECT_PATH/$QSYS_NAME.sopcinfo \
--report-file=regmap:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.regmap \
--report-file=qip:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.qip \
--report-file=debuginfo:$PROJECT_PATH/$QSYS_NAME/synthesis/$QSYS_NAME.debuginfo
