#!/bin/sh

SCRIPTDIR=$(dirname "$(readlink -f "$0")")
sh $SCRIPTDIR/compile_qsys.sh
sh $SCRIPTDIR/compile_nios.sh
sh $SCRIPTDIR/clean_quartus.sh
sh $SCRIPTDIR/compile_quartus.sh

#end

